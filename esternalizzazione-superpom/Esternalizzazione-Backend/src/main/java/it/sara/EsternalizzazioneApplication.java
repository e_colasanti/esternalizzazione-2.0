package it.sara;

import javax.servlet.MultipartConfigElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import it.sara.esternalizzazione.business.GestioneRemind;
import it.sara.esternalizzazione.db.dao.EsternalizzazioneDao;


@SpringBootApplication
@ComponentScan("it.sara")
@EnableScheduling
public class EsternalizzazioneApplication  extends SpringBootServletInitializer{
	
	
	@Autowired
	static EsternalizzazioneDao esternalizzazioneDao;
		

	public static void main(String[] args) {
		SpringApplication.run(EsternalizzazioneApplication.class, args);
		
		GestioneRemind gestioneRemind=new GestioneRemind();
		gestioneRemind.scheduledTask();	
	
//		List<Esternalizzazione> listaEsternalizzazioneRCNull= esternalizzazioneDao.findByRCNull();
//		
//		List<Esternalizzazione> listaEsternalizzazioneRischiosita=esternalizzazioneDao.findByTipoEstAndRischiosita();
//		
//		for (Esternalizzazione esternalizzazione : listaEsternalizzazioneRischiosita) {
//			
//			System.out.println("ID: esternalizzazione: "+esternalizzazione.getIdEsternalizzazione()+" Responsabile: "+esternalizzazione.getResponsabileSingoloControllo());
//			
//		}
	
	
	
	
	}
		

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
				
        return application.sources(EsternalizzazioneApplication.class);
    }
	
	
	
	@Bean
	  public MultipartConfigElement multipartConfigElement() {
	      org.springframework.boot.web.servlet.MultipartConfigFactory factory = new org.springframework.boot.web.servlet.MultipartConfigFactory();
	      factory.setMaxFileSize("100Mb");
	      return factory.createMultipartConfig();
	  }
	

}
