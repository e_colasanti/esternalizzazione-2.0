package it.sara.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

import it.sara.esternalizzazione.business.GestioneUtente;

@EnableAutoConfiguration
@EnableGlobalMethodSecurity(securedEnabled = true)
@Configuration
@Profile("webSeal")
public class SecurityWebseal  extends WebSecurityConfigurerAdapter{

	@Value("${managerDn}")
	String managerDn; 
	
	@Value("${managerPassword}")
	String managerPassword;
	
	@Value("${urlLdap}")
	String urlLdap;
	
	@Value("${groupBase}")
	String groupBase;
	
	@Value("${userBase}")
	String userBase;
	
	@Value("${groupFilter}")
	String groupFilter;
	
	@Value("${userFilter}")
	String userFilter;
	
	@Autowired
	GestioneUtente gestioneUtente;

	private Logger logger = LoggerFactory.getLogger(SecurityWebseal.class);

    @Override
    public void configure(WebSecurity security){
        security.ignoring().antMatchers("/css/**","/js/**","/webjars/**","/xsl/**","/dist/**","/vendor/**");
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	//logger.debug("Gruppo ispettore: " + gestioneUtente.gruppoIspettore);
    	logger.debug("Gruppo responsabile: " + gestioneUtente.gruppoResponsabile);
    	//logger.debug("Gruppo commerciale: " + gestioneUtente.gruppoCommerciale);
    	//logger.debug("Gruppo approvatore: " + gestioneUtente.gruppoApprovatore);
    	http
            .authorizeRequests()
                .antMatchers("/html/**").permitAll()
                .antMatchers("/","/index.html","/pippo.html","/rest/**").hasAnyRole(/*gestioneUtente.gruppoDirettoreGenerale, gestioneUtente.contryManager, gestioneUtente.rvz, gestioneUtente.rva, gestioneUtente.gruppoApprovatore,gestioneUtente.gruppoCommerciale,gestioneUtente.gruppoIspettore,*/gestioneUtente.gruppoResponsabile)
                .anyRequest().authenticated()
                .and()
            .formLogin()
            	.loginProcessingUrl("/login")
                .loginPage("/login")
                .successHandler(createSimpleUrlAuthenticationSuccessHandler())
                .permitAll()
                .and()
            .logout()
                .permitAll()
                .and()
            .csrf().csrfTokenRepository(csrfTokenRepository()).and()
            .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
            //.addFilterAt(preAuthenticatedWebSealProcessingFilter(), RequestHeaderAuthenticationFilter.class)
            .addFilterBefore(preAuthenticatedWebSealProcessingFilter(), UsernamePasswordAuthenticationFilter.class)
            ;
    }

    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.ldapAuthentication().contextSource()
//        .managerDn(managerDn)
//        .managerPassword(managerPassword)
//        .url(urlLdap)
//        .and()
//        .groupSearchBase(groupBase)
//        .userSearchBase(userBase)
//        .groupSearchFilter(groupFilter)
//        .userSearchFilter(userFilter)
//        ;
        
        auth.authenticationProvider(this.authenticationProvider());
    }

    private CsrfTokenRepository csrfTokenRepository() {
    	HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
    	repository.setHeaderName("X-XSRF-TOKEN");
    	return repository;
    } 

    private SimpleUrlAuthenticationSuccessHandler createSimpleUrlAuthenticationSuccessHandler() {
    	SimpleUrlAuthenticationSuccessHandler risultato = new SimpleUrlAuthenticationSuccessHandler();
    	risultato.setAlwaysUseDefaultTargetUrl(true);
    	risultato.setDefaultTargetUrl("/");
    	return risultato;
    }
    
    @Bean
    public PreAuthenticatedWebSealProcessingFilter preAuthenticatedWebSealProcessingFilter(){
    	PreAuthenticatedWebSealProcessingFilter webSealFilter = new PreAuthenticatedWebSealProcessingFilter();
    	try {
			webSealFilter.setAuthenticationManager(this.authenticationManager());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	//webSealFilter.setAuthenticationManager(this.authenticationManager());
    	return  webSealFilter;
    }
    
    
    public AuthenticationProvider authenticationProvider(){
    	AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> uds = new PreAuthenticatedWebSealGrantedAuthoritiesUserDetailsService();
    	AuthenticationProvider ap = new PreAuthenticatedWebSealAuthenticationProvider();
    	
    	((PreAuthenticatedWebSealAuthenticationProvider)ap).setPreAuthenticatedUserDetailsService(uds);
    	
    	return ap;
//    	List<AuthenticationProvider> l = new LinkedList<AuthenticationProvider>();
//    	l.add(ap);
//    	//AuthenticationProvider uap = new UsernaPreAuthenticatedWebSealAuthenticationProvider();
//    	return new ProviderManager(l);
    }
}
