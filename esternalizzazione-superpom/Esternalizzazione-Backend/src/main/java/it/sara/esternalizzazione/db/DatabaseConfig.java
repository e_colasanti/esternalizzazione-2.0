package it.sara.esternalizzazione.db;

import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import oracle.jdbc.pool.OracleDataSource;

@Configuration
@EnableJpaRepositories
@EnableTransactionManagement
public class DatabaseConfig {

	@Value("${databaseDriver}")
	String databaseDriver;
	
	@Value("${databaseUrl}")
	String databaseUrl;
	
	@Value("${databaseUser}")
	String databaseUser;
	
	@Value("${databasePassword}")
	String databasePassword;
	
	@Bean
	public DataSource dataSource() throws NamingException, SQLException {
		  	
//		Context ctx = new InitialContext();
//		DataSource ds = (DataSource) ctx.lookup ("jdbc/oracleDS");
//		if(ds!=null)
//			System.out.println("######################## DATA SOURCE: "+ds+" ########################" );
		
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName(databaseDriver);
		ds.setUrl(databaseUrl);
		ds.setUsername(databaseUser);
		ds.setPassword(databasePassword);
		
		
	
		
		return ds;
			  
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean  entityManagerFactory() {
	
	    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
	    //vendorAdapter.setGenerateDdl(true);
	    vendorAdapter.setDatabasePlatform("org.hibernate.dialect.Oracle10gDialect");
	    
	    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
	    factory.setJpaVendorAdapter(vendorAdapter);
	    factory.setPackagesToScan("it.sara.esternalizzazione.db");
	    try {
			factory.setDataSource(dataSource());
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    factory.afterPropertiesSet();
	    
	    return factory;
	}

	@Bean
	public PlatformTransactionManager transactionManager() {

	    JpaTransactionManager txManager = new JpaTransactionManager();
	    txManager.setEntityManagerFactory(entityManagerFactory().getObject());
	    return txManager;
	}
	
}
