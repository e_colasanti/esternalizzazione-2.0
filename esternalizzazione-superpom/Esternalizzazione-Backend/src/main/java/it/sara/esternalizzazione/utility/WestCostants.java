/**
 * 
 */
package it.sara.esternalizzazione.utility;

/**
 * @author f.vescovi
 *
 */
public class WestCostants {


	public static String ENVIRONMENT_SVIL="SVIL"; 
	public static String ENVIRONMENT_COLL="COLL"; 
	public static String ENVIRONMENT_PROD="PROD"; 


	/**
	 * Costanti Tabella tipo esternalizzazione	
	 */
	public final static String FUNZIONEFONDAMENTALE="Funzione Fondamentale"; 
	public final static String FUNZIONEATTIVITAESSENZIALEIMPORTANTE="Funzione o Attivita Essenziale ed Importante"; 
	public final static String ALTREATTIVITA="Altre Attivita"; 
	public final static String RICHIESTOSUPPORTOLEGALE="Richiesto Supporto Dir. Legale"; 


	/**
	 * Costanti Tabella Tipo Fase 
	 */

	public final static String FASEINSERIMENTO="Fase Inserimento";

	public final static String FASEVALUTAZIONE="Fase Valutazione";

	public final static String FASEPREDISPOSIZIONE="Fase Predisposizione del contratto";

	public final static String FASEVALUTAZIONESERVIZIOOUTSOURCING="Fase di valutazione del servizio di outsourcing";

	public final static String FASESCADENZASERVIZIOOUT="Fase di Scadenza del servizio di outsourcing ";



	/**
	 * Costanti Tabella Tipo Utente
	 */
	public final static String RE="Responsabile Esternalizzazione (qualsiasi direzione)";

	public final static String RC="Responsabile del Controllo (qualsiasi dipendente)";

	public final static String UO="Utente Operativo (qualsiasi dipendente)";

	public final static String FL="Funzione Legale";

	public final static String FP="Funzione Personale";

	public final static String BC="Business Continuity";

	public final static String FA="Funzione Acquisti";

	public final static String CA="Compliance";

	public final static String AU="Audit";

	public final static String RM="Risk Management";


	/**
	 * Costanti Tipo Task
	 */

	public final static String MG="MAIL GENERICA";

	public final static String CO="COMUNICAZIONE OBBLIGATORIA";

	public final static String AR="ATTESA RISPOSTA";

	public final static String MFA="MAIL FUNZIONE ACQUISTI";

	public final static String LND="LETTERA NOMINA DIPENDENTE"; 

	public final static String INCIVASS="INSERIMENTO NUMEROCOMUNICAZIONE IVASS";

	public final static String RAA= "RICHIESTA AUTORIZZAZIONE RESPONSABILE";

	public final static String IFA= "INSERIMENTO FORNITORE IN ANAGRAFICA";

	public final static String RRC="RICHIESTA INSERIMENTO RESPONSABILE CONTROLLO";

	public final static String RVBC="RICHIESTA VALUTAZIONE RISCHI BC"; 

	public final static String RVSLA="RICHIESTA VERIFICA SLA";
	
	public final static String RRAC="RICHIESTA RESPONSABILE AZIONE CORRETTIVA";
	
	public final static String RFAVC="RICHIESTA FUNZIONE ACQUISTI VARIAZIONE CONTRATTUALE"; 
	
	public final static String RFADCC="RICHIESTA FUNZIONE ACQUISTI DISDETTA CESSAZIONE CONTRATTUALE"; 
	
	public final static String RFLCIVASS="RICHIESTA FUNZIONE LEGALE COMUNICAZIONE IVASS"; 
	



	/**
	 * Costanti Tipo Ambito Allegato
	 */

	public final static String TIPO_AMBITO_ALLEGATO_TASK="ALLEGATO TASK";

	public final static String TIPO_AMBITO_ALLEGATO_PIANO_REINTERNALIZZAZIONE="ALLEGATO PIANO REINTERNALIZZAZIONE";

	public final static String TIPO_AMBITO_ALLEGATO_SLA="ALLEGATO SLA";

	public final static String TIPO_AMBITO_ALLEGATO_CONTROLLO="ALLEGATO CONTROLLO";

	public final static String TIPO_AMBITO_ALLEGATO_LETTERA_NOMINA_DIPENDENTE="ALLEGATO LETTERA NOMINA DIPENDENTE";

	public final static String TIPO_AMBITO_ALLEGATO_CONTRATTO="ALLEGATO CONTRATTO";

	public final static String TIPO_AMBITO_ALLEGATO_OGGETTO_ATTIVITA="ALLEGATO OGGETTO ATTIVITA";
	
	public final static String TIPO_AMBITO_ALLEGATO_MOTIVAZIONE="ALLEGATO MOTIVAZIONE ESTERNALIZZAZIONE";
	


	/**
	 * Costanti Tipo Campo Attivazione Esternalizzazione 
	 */

	public static final String NOME_FORNITORE="NOME FORNITORE";

	public static final String RISCHI_BC="RISCHI BC";

	public static final String SPAZIO_ECONOMICO_EUROPEO="SPAZIO ECONOMICO EUROPEO";

	public static final String RESPONSABILE_CONTROLLO_ESTERNALIZZAZIONE="RESPONSABILE CONTROLLO ESTERNALIZZAZIONE";

	public static final String INSERIMENTO_FUNZIONE_ACQUISTI="INSERIMENTO CAMPI FASE PREDISPOSIZIONE FUNZIONE ACQUISTI ";

	public static final String INSERIMENTO_FUNZIONE_LEGALE="INSERIMENTO CAMPI FASE PREDISPOSIZIONE FUNZIONE LEGALE ";
	
	public static final String INSERIMENTO_AZIONE_CORRETTIVA="INSERIMENTO AZIONE CORRETTIVA ";
	



	/**
	 * Costanti Tipo Campo Predisposizione Esternalizzazione 
	 */

	public static final String COMUNICAZIONE_IVASS ="COMUNICAZIONE IVASS";

	public static final String VERIFICA_SLA ="VERIFICA SLA";


	/**
	 * Costanti Stato Task
	 */
	public static final String TASK_ASSEGNATO ="A";

	public static final String TASK_COMPLETATO ="C";

	public static final String TASK_VALIDATO ="V";


	/**
	 * Costanti File Controller
	 */

	public static final String ALLEGATO_ESTERNALIZZAZIONE="ESTERNALIZZAZIONE";

	public static final String ALLEGATO_TASK="TASK";



	/**
	 * Costanti generiche
	 */
	public static final String OK="S";
	public static final String KO="N";

	public static final String day="86400";

	public static final String week="604800";

	public static final String twoWeek="1209600";

	public static final String month="2592000";//31->2.678.400 //30->2.592.000 //28->2.419.200 
	
	public static final String year="31556926";




	private WestCostants() {}

}
