package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class AllegatoWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5033647566635946536L;
	private MultipartFile file;
	private String pathFile;
	private String nomeFileOrigine;
	private Integer idAllegato;
	private Long dimensione;
	private String contentType;
	private byte[] contenuto;
	private String ambito;
	private String contesto;
	
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public String getPathFile() {
		return pathFile;
	}
	public void setPathFile(String pathFile) {
		this.pathFile = pathFile;
	}
	public String getNomeFileOrigine() {
		return nomeFileOrigine;
	}
	public void setNomeFileOrigine(String nomeFileOrigine) {
		this.nomeFileOrigine = nomeFileOrigine;
	}
	public Integer getIdAllegato() {
		return idAllegato;
	}
	public void setIdAllegato(Integer idAllegato) {
		this.idAllegato = idAllegato;
	}
	public Long getDimensione() {
		return dimensione;
	}
	public void setDimensione(Long dimensione) {
		this.dimensione = dimensione;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public byte[] getContenuto() {
		return contenuto;
	}
	public void setContenuto(byte[] contenuto) {
		this.contenuto = contenuto;
	}
		
	/**
	 * @return the ambito
	 */
	public String getAmbito() {
		return ambito;
	}
	/**
	 * @param ambito the ambito to set
	 */
	public void setAmbito(String ambito) {
		this.ambito = ambito;
	}
	/**
	 * @return the contesto
	 */
	public String getContesto() {
		return contesto;
	}
	/**
	 * @param contesto the contesto to set
	 */
	public void setContesto(String contesto) {
		this.contesto = contesto;
	}
	@Override
	public String toString() {
		return "AllegatoWeb [pathFile=" + pathFile + ", nomeFileOrigine=" + nomeFileOrigine + ", idAllegato="
				+ idAllegato + ", dimensione=" + dimensione + ", contentType=" + contentType + "]";
	}

	
}
