package it.sara.esternalizzazione.business;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.bean.TipoEsternalizzazione;
import it.sara.esternalizzazione.db.bean.TipoTask;
import it.sara.esternalizzazione.db.bean.TipoUtente;
import it.sara.esternalizzazione.db.dao.EsternalizzazioneDao;
import it.sara.esternalizzazione.db.dao.TaskDao;
import it.sara.esternalizzazione.db.dao.TipoTaskDao;
import it.sara.esternalizzazione.db.dao.TipoUtenteDao;
import it.sara.esternalizzazione.utility.Utility;
import it.sara.esternalizzazione.web.bean.EsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.PaginaEsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.PaginaTaskWeb;
import it.sara.esternalizzazione.web.bean.TaskWeb;
import it.sara.esternalizzazione.web.bean.UtenteWeb;

@Component("GestioneTask")
public class GestioneTask {


	private Logger logger = LoggerFactory.getLogger(GestioneTask.class);


	@Autowired
	EsternalizzazioneDao esternalizzazioneDao;

	@Autowired
	TaskDao taskDao;


	@Autowired
	TipoTaskDao tipoTaskDao;
	
	@Autowired
	TipoUtenteDao tipoUtenteDao;


	@Autowired
	GestioneWSDL gestione;


	@Autowired
	GestioneMail gestioneMail;


	@Autowired
	GestioneTipoTask gestioneTipoTask;


	public PaginaTaskWeb getListaAttivita(UtenteWeb utente, List<Integer> stati, int numeroPagina,int righePagina) {

		Sort lSort = new Sort(new Sort.Order(Sort.Direction.DESC, "dataCreazione"));
		Pageable p = new PageRequest(numeroPagina, righePagina, lSort);
		Page<Task> l =null;
		List<TaskWeb> lista = new LinkedList<TaskWeb>();
		boolean isResponsabile=false;
		String codiceDirezione=null;
		
		if(   utente.getResponsabile()!= null && !("").equals(utente.getResponsabile())	)
			isResponsabile=true;
	
		codiceDirezione=utente.getZzenteacq();
		
		TipoUtente tipoUtente= tipoUtenteDao.findByCodDirezione(codiceDirezione);
		
		l = taskDao.findAll(p);
	
		if (l==null)return null;


		for (Task task : l) {		

            if( task.getTipoUtente().getCodiceDirezione()!=null) 
            {
            	if(task.getTipoUtente().getCodiceDirezione().equals(codiceDirezione) || task.getUtenteResponsabile().equals(utente.getUsername()))
			        lista.add(new TaskWeb(task));
            }
            else
            {
            	if(task.getUtenteResponsabile().equals(utente.getUsername()))
			        lista.add(new TaskWeb(task));            	
            }
            
		}
		PaginaTaskWeb risultato = new PaginaTaskWeb();
		risultato.setListaAttivita(lista);
		risultato.setPaginaCorrente(l.getNumber());
		risultato.setRecordPerPagina(l.getNumberOfElements());
		risultato.setTotalePagine(l.getTotalPages());
		risultato.setTotaleRecord(l.getTotalElements());
		return risultato;
	}


	@SuppressWarnings("unused")
	private List<TaskWeb> caricaListaTaskWeb(List<Integer> stati, String query, Integer pageNumber, Integer righePerPagina)throws UnsupportedEncodingException {
		Sort lSort = new Sort(new Sort.Order(Sort.Direction.DESC, "dataCreazione"));
		Pageable p = new PageRequest(pageNumber, righePerPagina, lSort);

		String queryDecoded = URLDecoder.decode(query, "UTF-8");

		Page<Task> listaTask = taskDao.findByStatoInLike(stati,"%"+queryDecoded.toLowerCase()+"%",p);
		if (listaTask==null)return null;

		List<TaskWeb> lista = new LinkedList<TaskWeb>();
		for(Task task:listaTask) {
			lista.add(new TaskWeb(task));			
		}
		return lista;
	}


	/**
	 * Ritorna il Task per mostrarne i contenuti
	 * @param idTask
	 * @return
	 */
	public TaskWeb  getTaskById(Integer idTask){

		Task task=taskDao.findByIdTask(idTask);

		TaskWeb taskWeb=new TaskWeb(task);

		return taskWeb;

	}


	/**
	 * Ritorna la lista dei task associati all'id esternalizzazione 
	 * @param idEsternalizzazione
	 * @return
	 */	
	public List<Task> getListaTaskByEstId(Integer idEsternalizzazione){
		List<Task> listaTask=new ArrayList<>();

		List<Task> list=taskDao.findAll();
		for (Task task : list) {
			if(task.getEsternalizzazione().getIdEsternalizzazione()==idEsternalizzazione)
				listaTask.add(task);
		}
		return listaTask;

	}


	/**
	 * Ritorna la lista dei task associati all'id esternalizzazione 
	 * @param idEsternalizzazione
	 * @return
	 */	
	@SuppressWarnings("unlikely-arg-type")
	public List<Task> getListaTaskByIdAndStatusAndOwner(String idEsternalizzazione,String status,String owner){
		List<Task> listaTask=new ArrayList<>();

		List<Task> list=taskDao.findAll();
		for (Task task : list) {
			if(idEsternalizzazione.equals(task.getEsternalizzazione().getIdEsternalizzazione()+"") &&
					status.equals(task.getStato()) && 
					owner.equals(task.getUtenteOwner())
					)
				listaTask.add(task);
		}
		return listaTask;

	}


	@Transactional(rollbackFor=Exception.class)
	public Task generaTask(Esternalizzazione esternalizzazione,String fase,String responsabile,String delegato){

		Task newTask = null;
		try {

			TipoTask tipoTask =gestioneTipoTask.getTipoTaskByIdTipoEsternalizzazione(esternalizzazione.getTipoEsternalizzazione().getIdTipoEsternalizzazione());		

			Task task = new Task(); 
			task.setDataCreazione(new Date());       
			task.setDataFine(new Date());
			task.setProgLavTask(new Random().nextInt());
			task.setStato("A");
			task.setTipoTask(tipoTask);
			task.setUtenteOwner(responsabile);
			task.setUtenteResponsabile(responsabile);
			task.setUtenteDelegato(delegato);
			task.setEsternalizzazione(esternalizzazione);

			newTask =this.taskDao.save(task);


			if(checkMail(tipoTask))
				task.setDescrizione("Email inviata alla: "+tipoTask.getTipoTask());


		}catch (Exception e) {
			logger.error("Errore durante il salvataggio del Task");
			e.printStackTrace();
		}

		return newTask;

	}



	public boolean checkMail(TipoTask tipoTask) {

		return true;
	}


	public PaginaTaskWeb cercaTask(List<Integer> stati, String query, Integer pageNumber, Integer righePerPagina) {

		try {	
			List<TaskWeb> lista =  caricaListaTaskWeb(stati,query,  pageNumber,  righePerPagina);

			PaginaTaskWeb risultato = new PaginaTaskWeb();
			risultato.setListaAttivita(lista);
			risultato.setPaginaCorrente(1);
			risultato.setRecordPerPagina(10);
			risultato.setTotalePagine(1);
			risultato.setTotaleRecord(new Long(lista.size()));
			return risultato;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}



	@Transactional(rollbackFor=Exception.class)
	public void salvaTask(TaskWeb taskWeb){

		try {
			if(taskWeb!= null)
			{
				Task task=taskDao.findByIdTask(Integer.valueOf(taskWeb.getIdTask()));
				if(task!=null) {
					task.setStato(taskWeb.getStato());
					if(("C").equals(taskWeb.getStato()))
						task.setDataFine(new Date());
				}
				taskDao.save(task);            	  
			}

		}catch (Exception e) {
			logger.error("Errore durante il salvataggio dell'esternalizzazione");
			e.printStackTrace();
		}
	}




}
