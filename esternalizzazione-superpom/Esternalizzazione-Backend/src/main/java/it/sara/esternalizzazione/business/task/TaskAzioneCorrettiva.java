/**
 * 
 */
package it.sara.esternalizzazione.business.task;

import java.util.Date;
import java.util.Random;

import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.bean.TipoTask;
import it.sara.esternalizzazione.db.bean.TipoUtente;
import it.sara.esternalizzazione.utility.WestCostants;

/**
 * @author f.vescovi
 *
 */
@Component("TaskAzioneCorrettiva")
public class TaskAzioneCorrettiva extends AbstractTaskFactory {
	
	
	public TaskAzioneCorrettiva() {}

	/* (non-Javadoc)
	 * @see it.sara.esternalizzazione.business.task.AbstractTaskFactory#createTask(it.sara.esternalizzazione.db.bean.Esternalizzazione)
	 */
	@Override
	public Task createTask(Esternalizzazione esternalizzazione) {
			
		Task task=new Task();	 
		task.setEsternalizzazione(esternalizzazione);
		TipoTask  tipoTask=gestioneTipoTask.getTipoTaskByTipoTask(WestCostants.RRAC);
		task.setTipoTask(tipoTask);	
		task.setTipoCampoAttivatoEst(WestCostants.INSERIMENTO_AZIONE_CORRETTIVA);
		task.setProgLavTask(new Random().nextInt());
		task.setDataCreazione(new Date());       
		task.setDataFine(null);
		task.setStato("A");
		TipoUtente tipoUtente=gestioneTipoUtente.getByDescTipoUtente(WestCostants.RE);
		task.setTipoUtente(tipoUtente);											
		task.setUtenteOwner(esternalizzazione.getUtenteModificaEsternalizzazione());
		task.setUtenteResponsabile(esternalizzazione.getCompilataDa());
		task.setUtenteDelegato(esternalizzazione.getResponsabileSingoloControllo());
		task.setFaseEsternalizzazione(WestCostants.FASEPREDISPOSIZIONE);
		task.setCodiceDirezione(tipoUtente.getCodiceDirezione());						
		taskDao.save(task);
		gestioneMail.inviaMail(task);	
		return task;
	}

	/* (non-Javadoc)
	 * @see it.sara.esternalizzazione.business.task.AbstractTaskFactory#createTask(it.sara.esternalizzazione.db.bean.Esternalizzazione, java.lang.String)
	 */
	@Override
	public Task createTask(Esternalizzazione esternalizzazione, String descTask,String descUtente) {
		Task task=new Task();	 
		task.setEsternalizzazione(esternalizzazione);
		TipoTask  tipoTask=gestioneTipoTask.getTipoTaskByTipoTask(descTask);
		task.setTipoTask(tipoTask);	
		task.setTipoCampoAttivatoEst(WestCostants.INSERIMENTO_AZIONE_CORRETTIVA);
		task.setProgLavTask(new Random().nextInt());
		task.setDataCreazione(new Date());       
		task.setDataFine(null);
		task.setStato("A");
		TipoUtente tipoUtente=gestioneTipoUtente.getByDescTipoUtente(descUtente);
		task.setTipoUtente(tipoUtente);											
		task.setUtenteOwner(esternalizzazione.getUtenteModificaEsternalizzazione());
		task.setUtenteResponsabile(esternalizzazione.getCompilataDa());
		task.setUtenteDelegato(esternalizzazione.getResponsabileSingoloControllo());
		task.setFaseEsternalizzazione(WestCostants.FASEPREDISPOSIZIONE);
		task.setCodiceDirezione(tipoUtente.getCodiceDirezione());						
		taskDao.save(task);
		gestioneMail.inviaMail(task);
		return task;
	}
	
	
	
	
	

}
