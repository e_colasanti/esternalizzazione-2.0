package it.sara.esternalizzazione.db.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="WEST_FASE_ESTERNALIZZAZIONE")
public class FaseEsternalizzazione implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -3191949587928959886L;


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GENERA_ID")
	@SequenceGenerator(name="SEQ_GENERA_ID", sequenceName="SEQ_GENERA_ID", allocationSize=1)
	@Column(name="ID_FASE_ESTERNALIZZAZIONE")
	Integer idFaseEsternalizzazione;


	@Column(name="DATA_INSERIMENTO")
	Date dataInserimento;


	@Column(name="DATA_AGGIORNAMENTO")
	Date dataAggiornamento;


	@Column(name="UTENTE_INSERIMENTO")
	String utenteInserimento;


	@Column(name="UTENTE_AGGIORNAMENTO")
	String utenteAggiornamento;

	@Column(name="DATA_INIZIO_VALIDITA")
	Date dataInizioValidita;

	@Column(name="DATA_FINE_VALIDITA")
	Date dataFineValidita;

	@ManyToOne(fetch=FetchType.LAZY)	
	@JoinColumn(name="ID_TASK")
	Task task;
	

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ESTERNALIZZAZIONE")
	Esternalizzazione esternalizzazione;


	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_FASE")
	TipoFase tipoFase;

	@Column(name="DATA_ASSEGNAZIONE")
	Date dataAssegnazione;


	@Column(name="UTENTE_RESPONSABILE")
	String utenteResponsabile;


	@Column(name="UTENTE_DELEGATO")
	String utenteDelegato;


	@Column(name="STATO")
	String stato;


	@Column(name="RISPOSTA_LEGALE")
	String rispostaLegale;


//	@Column(name="ID_TASK")	
//    private Set<Task> listaTask;	
//
//	/**
//	 * @return the listaTask
//	 */
//	@OneToMany(fetch=FetchType.EAGER,targetEntity=Task.class,mappedBy="fase")
//	public Set<Task> getListaTask() {
//		return listaTask;
//	}
//
//
//
//	/**
//	 * @param listaTask the listaTask to set
//	 */
//	public void setListaTask(Set<Task> listaTask) {
//		this.listaTask = listaTask;
//	}



	/**
	 * @return the idFaseEsternalizzazione
	 */
	public Integer getIdFaseEsternalizzazione() {
		return idFaseEsternalizzazione;
	}



	/**
	 * @param idFaseEsternalizzazione the idFaseEsternalizzazione to set
	 */
	public void setIdFaseEsternalizzazione(Integer idFaseEsternalizzazione) {
		this.idFaseEsternalizzazione = idFaseEsternalizzazione;
	}









	/**
	 * @return the dataInserimento
	 */
	public Date getDataInserimento() {
		return dataInserimento;
	}


	/**
	 * @param dataInserimento the dataInserimento to set
	 */
	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}
	/**
	 * @return the dataAggiornamento
	 */
	public Date getDataAggiornamento() {
		return dataAggiornamento;
	}
	/**
	 * @param dataAggiornamento the dataAggiornamento to set
	 */
	public void setDataAggiornamento(Date dataAggiornamento) {
		this.dataAggiornamento = dataAggiornamento;
	}
	/**
	 * @return the utenteInserimento
	 */
	public String getUtenteInserimento() {
		return utenteInserimento;
	}
	/**
	 * @param utenteInserimento the utenteInserimento to set
	 */
	public void setUtenteInserimento(String utenteInserimento) {
		this.utenteInserimento = utenteInserimento;
	}
	/**
	 * @return the utenteAggiornamento
	 */
	public String getUtenteAggiornamento() {
		return utenteAggiornamento;
	}
	/**
	 * @param utenteAggiornamento the utenteAggiornamento to set
	 */
	public void setUtenteAggiornamento(String utenteAggiornamento) {
		this.utenteAggiornamento = utenteAggiornamento;
	}
	/**
	 * @return the dataInizioValidita
	 */
	public Date getDataInizioValidita() {
		return dataInizioValidita;
	}
	/**
	 * @param dataInizioValidita the dataInizioValidita to set
	 */
	public void setDataInizioValidita(Date dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}
	/**
	 * @return the dataFineValidita
	 */
	public Date getDataFineValidita() {
		return dataFineValidita;
	}
	/**
	 * @param dataFineValidita the dataFineValidita to set
	 */
	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}
	/**
	 * @return the task
	 */
	public Task getTask() {
		return task;
	}

	/**
	 * @param task the Task to set
	 */
	public void setTask(Task task) {
		this.task = task;
	}
	/**
	 * @return the esternalizzazione
	 */
	public Esternalizzazione getEsternalizzazione() {
		return esternalizzazione;
	}


	/**
	 * @param esternalizzazione the esternalizzazione to set
	 */
	public void setEsternalizzazione(Esternalizzazione esternalizzazione) {
		this.esternalizzazione = esternalizzazione;
	}









	/**
	 * @return the tipoFase
	 */
	public TipoFase getTipoFase() {
		return tipoFase;
	}









	/**
	 * @param tipoFase the tipoFase to set
	 */
	public void setTipoFase(TipoFase tipoFase) {
		this.tipoFase = tipoFase;
	}









	/**
	 * @return the dataAssegnazione
	 */
	public Date getDataAssegnazione() {
		return dataAssegnazione;
	}









	/**
	 * @param dataAssegnazione the dataAssegnazione to set
	 */
	public void setDataAssegnazione(Date dataAssegnazione) {
		this.dataAssegnazione = dataAssegnazione;
	}









	/**
	 * @return the utenteResponsabile
	 */
	public String getUtenteResponsabile() {
		return utenteResponsabile;
	}









	/**
	 * @param utenteResponsabile the utenteResponsabile to set
	 */
	public void setUtenteResponsabile(String utenteResponsabile) {
		this.utenteResponsabile = utenteResponsabile;
	}









	/**
	 * @return the utenteDelegato
	 */
	public String getUtenteDelegato() {
		return utenteDelegato;
	}









	/**
	 * @param utenteDelegato the utenteDelegato to set
	 */
	public void setUtenteDelegato(String utenteDelegato) {
		this.utenteDelegato = utenteDelegato;
	}









	/**
	 * @return the stato
	 */
	public String getStato() {
		return stato;
	}









	/**
	 * @param stato the stato to set
	 */
	public void setStato(String stato) {
		this.stato = stato;
	}









	/**
	 * @return the rispostaLegale
	 */
	public String getRispostaLegale() {
		return rispostaLegale;
	}









	/**
	 * @param rispostaLegale the rispostaLegale to set
	 */
	public void setRispostaLegale(String rispostaLegale) {
		this.rispostaLegale = rispostaLegale;
	}









	@Override
	public String toString() {
		return "Stato [idFaseEsternalizzazione=" + idFaseEsternalizzazione + ", dataInserimento="
				+ dataInserimento + ", dataAggiornamento=" + dataAggiornamento + ", utenteInserimento="
				+ utenteInserimento + ", utenteAggiornamento=" + utenteAggiornamento + ", dataInizioValidita="
				+ dataInizioValidita + ", dataFineValidita=" + dataFineValidita + "]";
	}
}
