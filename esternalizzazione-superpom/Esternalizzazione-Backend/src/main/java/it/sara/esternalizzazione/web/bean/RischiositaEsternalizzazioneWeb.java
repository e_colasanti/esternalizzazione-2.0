package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import it.sara.esternalizzazione.db.bean.RischiositaEsternalizzazione;


public class RischiositaEsternalizzazioneWeb extends TipoListaWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8640946392873875795L;
	
	public RischiositaEsternalizzazioneWeb(RischiositaEsternalizzazione rischiositaEsternalizzazione) {
		this.setCodice(rischiositaEsternalizzazione.getIdRischiositaEsternalizzazione()+"");
		this.setDescrizione(rischiositaEsternalizzazione.getDescrizioneRischiositaEsternalizzazione());
	}
}
