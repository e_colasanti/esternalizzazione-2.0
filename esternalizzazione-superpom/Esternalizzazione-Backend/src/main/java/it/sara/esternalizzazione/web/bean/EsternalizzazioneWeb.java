package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import it.sara.esternalizzazione.db.bean.Controllo;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.bean.TipoEsternalizzazione;
import it.sara.esternalizzazione.utility.Utility;


public class EsternalizzazioneWeb implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 812001261480819257L;
	
	/*fase inserimento*/
	private String idEsternalizzazione;
	private String dataCreazioneEsternalizzazione;
	private String compilataDa;	
	private String codiceDirezione;
	private String titoloEsternalizzazione;
	private String tipoEsternalizzazione;
	private String descrizioneTipoEsternalizzazione;
	private String dataModificaTipologiaEsternalizzazione;
	private String utenteModificaEsternalizzazione;
	private String motivazioneEsternalizzazione;
	private String oggettoAttivita;
	/*fase valutazione*/	
	private String approvazioneCDA;	
	private String beneficioConseguibile;
	private String descrizioneBeneficioConseguibile;
	private String dataProbabileInizioEsternalizzazione;
	private String nomeFornitore;
	private String spazioEconomicoEuropeo;
	private String affidabilitaCompetenzaFornitore;
	private String rischiositaEsternalizzazione;
	private String descrizioneRischiositaEsternalizzazione;
	private String valutazioneRischioEsternalizzazione;
	private String verificaConflittiInteressi;
	private String descrizioneVerificaConflittiInteressi;
	private String rischiBC;
	private String descrizioneRischiBC;
	/*fase di predisposizione*/
	private String valutazioneRischiBC;
	private String pianoReinternalizzazione;
	private String descrizionePianoReinternalizzazione;
	private String rispettoRemunerazione;
	private String responsabileControlloEsternalizzazione;
	private String registrazioneRCEsternalizzazione;
	private String utenteOperativoEsternalizzazione;
	private String registrazioneUOEsternalizzazione;
	private String dataInizioEfficaciaContratto;
	private String numeroComunicazioneIVASS;
	private String numeroRDA;
	private String numeroODA;
	private String numeroProtocolloContrattuale;
	private String periodoPreavvisoContrattuale;
	private String tipologiaRinnovo;
	private String slaContrattualmenteDefiniti;
	private String descrizioneControllo;
	private String tipoControllo;
	private String descrizioneTipoControllo;
	private String dataInizioControllo;
	private String dataFineControllo;
	private String periodicita;
	private String descrizionePeriodicita;
	private String vincoloPagamentoFattura;
	private String responsabileSingoloControllo;
	private String esitoControllo;
	private String descrEsitoControllo;
	private String descrizioneEsitoControllo;
	private String azioneCorrettiva;
	private String descrizioneAzioneCorrettiva;
	private String stato;
	private String notaAzioneCorrettiva;
	//campi aggiunti per assegnazione utente
	private String idUO;
	private String idRC;
	private List<UtenteWeb> listaConoRE;
	private UtenteWeb utenteWs;
	private UtenteWeb responsabileWs;
	private List<Task> listaTask;
	
	private List<ControlloWeb> listaControlli;
	
	
	
	public EsternalizzazioneWeb(){
		super();
	}

	public EsternalizzazioneWeb(Esternalizzazione esternalizzazione){
		this.idEsternalizzazione= String.valueOf(esternalizzazione.getIdEsternalizzazione());
		this.stato=esternalizzazione.getStato()+"";
		this.compilataDa= esternalizzazione.getCompilataDa();
		this.codiceDirezione= esternalizzazione.getCodiceDirezione();
		this.titoloEsternalizzazione = esternalizzazione.getTitoloEsternalizzazione();
		this.tipoEsternalizzazione = esternalizzazione.getTipoEsternalizzazione() != null ? ""+esternalizzazione.getTipoEsternalizzazione().getIdTipoEsternalizzazione():null;
		this.descrizioneTipoEsternalizzazione = esternalizzazione.getTipoEsternalizzazione() != null ?esternalizzazione.getTipoEsternalizzazione().getDescrizioneTipoEsternalizzazione(): null;
		this.utenteModificaEsternalizzazione = esternalizzazione.getUtenteModificaEsternalizzazione();
		this.motivazioneEsternalizzazione = esternalizzazione.getMotivazioneEsternalizzazione();
		this.beneficioConseguibile = esternalizzazione.getBeneficioConseguibile() != null ? ""+ esternalizzazione.getBeneficioConseguibile().getIdBeneficioConseguibile():null;
		this.descrizioneBeneficioConseguibile = esternalizzazione.getBeneficioConseguibile() != null ? esternalizzazione.getBeneficioConseguibile().getDescrizioneBeneficioConseguibile():null;
		this.nomeFornitore = esternalizzazione.getNomeFornitore();
		this.spazioEconomicoEuropeo = esternalizzazione.getSpazioEconomicoEuropeo();
		this.affidabilitaCompetenzaFornitore = esternalizzazione.getAffidabilitaCompetenzaFornitore() ;
		this.rischiositaEsternalizzazione  = esternalizzazione.getRischiositaEsternalizzazione()!= null ? ""+ esternalizzazione.getRischiositaEsternalizzazione().getIdRischiositaEsternalizzazione():null;
		this.descrizioneRischiositaEsternalizzazione = esternalizzazione.getRischiositaEsternalizzazione()!= null ?esternalizzazione.getRischiositaEsternalizzazione().getDescrizioneRischiositaEsternalizzazione():null;
		this.valutazioneRischioEsternalizzazione = esternalizzazione.getValutazioneRischioEsternalizzazione();
		this.verificaConflittiInteressi  = esternalizzazione.getVerificaConflittiInteressi()!= null ? ""+ esternalizzazione.getVerificaConflittiInteressi().getIdVerificaConflittiInteressi():null;
		this.descrizioneVerificaConflittiInteressi = esternalizzazione.getVerificaConflittiInteressi()!= null ?esternalizzazione.getVerificaConflittiInteressi().getDescrizioneVerificaConflittiInteressi():null;
		this.rischiBC = esternalizzazione.getRischiBC()!= null ? ""+ esternalizzazione.getRischiBC().getIdRischiBC():null;
		this.descrizioneRischiBC = esternalizzazione.getRischiBC()!= null ? ""+ esternalizzazione.getRischiBC().getDescrizioneRischiBC():null;
		//formattazione date
		
		//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if (esternalizzazione.getDataCreazioneEsternalizzazione()!=null)
			this.dataCreazioneEsternalizzazione= Utility.getStringFromDate(esternalizzazione.getDataCreazioneEsternalizzazione());
		if (esternalizzazione.getApprovazioneCDA()!=null)
			this.approvazioneCDA= Utility.getStringFromDate(esternalizzazione.getApprovazioneCDA());//sdf.format(esternalizzazione.getApprovazioneCDA());
		if (esternalizzazione.getDataModificaTipologiaEsternalizzazione()!=null)
			this.dataModificaTipologiaEsternalizzazione=Utility.getStringFromDate(esternalizzazione.getDataModificaTipologiaEsternalizzazione());//sdf.format(esternalizzazione.getDataModificaTipologiaEsternalizzazione());
		if (esternalizzazione.getDataProbabileInizioEsternalizzazione()!=null)
			this.dataProbabileInizioEsternalizzazione=Utility.getStringFromDate(esternalizzazione.getDataProbabileInizioEsternalizzazione());//sdf.format(esternalizzazione.getDataProbabileInizioEsternalizzazione());
		if (esternalizzazione.getDataInizioEfficaciaContratto()!=null)
			this.dataInizioEfficaciaContratto=Utility.getStringFromDate(esternalizzazione.getDataInizioEfficaciaContratto());//sdf.format(esternalizzazione.getDataInizioEfficaciaContratto());				
		if (esternalizzazione.getDataInizioControllo()!=null)
			this.dataInizioControllo=Utility.getStringFromDate(esternalizzazione.getDataInizioControllo()); //sdf.format(esternalizzazione.getDataInizioControllo());
		if (esternalizzazione.getDataFineControllo()!=null)
			this.dataFineControllo=Utility.getStringFromDate(esternalizzazione.getDataFineControllo()); //sdf.format(esternalizzazione.getDataFineControllo());
		
		this.valutazioneRischiBC = esternalizzazione.getValutazioneRischiBC();
		this.pianoReinternalizzazione = esternalizzazione.getPianoReinternalizzazione() != null ? ""+ esternalizzazione.getPianoReinternalizzazione().getIdPianoReinternalizzazione():null;
		this.descrizionePianoReinternalizzazione = esternalizzazione.getPianoReinternalizzazione() != null ? esternalizzazione.getPianoReinternalizzazione().getDescrizionePianoReinternalizzazione():null;
		this.rispettoRemunerazione = esternalizzazione.getRispettoRemunerazione();
		this.responsabileControlloEsternalizzazione =esternalizzazione.getResponsabileControlloEsternalizzazione();
		this.registrazioneRCEsternalizzazione = null;//esternalizzazione.getRCEsternalizzazione() != null ? esternalizzazione.getRCEsternalizzazione().getRegistrazione():null;
		this.utenteOperativoEsternalizzazione =null;// esternalizzazione.getUOEsternalizzazione()!= null ? ""+ esternalizzazione.getUOEsternalizzazione().getIdControlloOperativo():null;
		this.registrazioneUOEsternalizzazione =null;//esternalizzazione.getUOEsternalizzazione()!= null ? ""+ esternalizzazione.getUOEsternalizzazione().getRegistrazione():null;
		this.numeroComunicazioneIVASS = esternalizzazione.getNumeroComunicazioneIVASS();
		this.numeroRDA = esternalizzazione.getNumeroRDA();
		this.numeroODA = esternalizzazione.getNumeroODA();
		this.numeroProtocolloContrattuale  = esternalizzazione.getNumeroProtocolloContrattuale();
		this.periodoPreavvisoContrattuale = esternalizzazione.getPeriodoPreavvisoContrattuale();
		this.tipologiaRinnovo = esternalizzazione.getTipologiaRinnovo();
		this.slaContrattualmenteDefiniti = esternalizzazione.getSlaContrattualmenteDefiniti();
		this.descrizioneControllo= esternalizzazione.getDescrizioneControllo();
		this.tipoControllo = esternalizzazione.getTipoControllo()!= null ? ""+ esternalizzazione.getTipoControllo().getIdTipoControllo():null;
		this.descrizioneTipoControllo = esternalizzazione.getTipoControllo()!= null ? esternalizzazione.getTipoControllo().getDescrizioneTipoControllo():null;
		this.periodicita = esternalizzazione.getPeriodicita()!= null ? ""+ esternalizzazione.getPeriodicita().getIdPeriodicita():null;
		this.descrizionePeriodicita = esternalizzazione.getPeriodicita()!= null ? esternalizzazione.getEsitoControllo().getDescrizioneEsitoControllo():null;
		this.vincoloPagamentoFattura= esternalizzazione.getVincoloPagamentoFattura();
		this.responsabileSingoloControllo = esternalizzazione.getResponsabileSingoloControllo();
		this.esitoControllo = esternalizzazione.getEsitoControllo()!= null ? ""+ esternalizzazione.getEsitoControllo().getIdEsitoControllo():null;
		this.descrizioneEsitoControllo = esternalizzazione.getEsitoControllo()!= null ? esternalizzazione.getEsitoControllo().getDescrizioneEsitoControllo():null;
		this.azioneCorrettiva = esternalizzazione.getAzioneCorrettiva()!= null ? ""+esternalizzazione.getAzioneCorrettiva().getIdAzioneCorrettiva():"";
		this.descrizioneAzioneCorrettiva = esternalizzazione.getAzioneCorrettiva()!=null ? esternalizzazione.getAzioneCorrettiva().getDescrizioneAzioneCorrettiva():null;
		this.notaAzioneCorrettiva = esternalizzazione.getNotaAzioneCorrettiva();
		this.idRC=esternalizzazione.getIdRC();
		this.oggettoAttivita=esternalizzazione.getOggettoAttivita();		
		this.listaControlli=transformerToControlloWeb(esternalizzazione.getControlli()==null?new ArrayList<Controllo>():esternalizzazione.getControlli());
	}

	
	private List<ControlloWeb> transformerToControlloWeb(List<Controllo> listControlli) {
		
		List<ControlloWeb> newControlloWebList=new ArrayList<ControlloWeb>();
		
		for (Controllo controllo : listControlli) {
			
		ControlloWeb controlloWeb=new ControlloWeb(""+controllo.getIdControllo(), ""+controllo.getTipoControllo().getIdTipoControllo(), controllo.getDescrizioneControllo(),
					""+Utility.getStringFromDate(controllo.getDataInizioControllo()), ""+Utility.getStringFromDate(controllo.getDataFineControllo()), ""+controllo.getPeriodicita().getIdPeriodicita(), controllo.getVincoloPagamentoFattura(),
					controllo.getResponsabileSingoloControllo(),""+controllo.getEsitoControllo().getIdEsitoControllo(), controllo.getDescrizioneEsitoControllo(),
					""+controllo.getAzioneCorrettiva().getIdAzioneCorrettiva(), controllo.getNotaAzioneCorrettiva(), ""+controllo.getEsternalizzazione().getIdEsternalizzazione());
			
		newControlloWebList.add(controlloWeb); 	
		}
		
		return newControlloWebList; 
		
	}
	
	
	
	public void toEsternalizzazioneDb(Esternalizzazione esternalizzazione,TipoEsternalizzazione TipoEsternalizzazione,String fase) {
		
		if((fase!= null && fase.equals("nuova")) ) {
			
			esternalizzazione.setCompilataDa(this.getCompilataDa());
			esternalizzazione.setCodiceDirezione(this.getCodiceDirezione());
			esternalizzazione.setTitoloEsternalizzazione(this.getTitoloEsternalizzazione());
			esternalizzazione.setTipoEsternalizzazione(TipoEsternalizzazione);
			esternalizzazione.setDataModificaTipologiaEsternalizzazione(new Date());			
			esternalizzazione.setUtenteModificaEsternalizzazione(this.getUtenteModificaEsternalizzazione());
			esternalizzazione.setMotivazioneEsternalizzazione(this.getMotivazioneEsternalizzazione());
			esternalizzazione.setDataCreazioneEsternalizzazione(new Date());
			esternalizzazione.setIdRC(this.getIdRC());
			esternalizzazione.setOggettoAttivita(this.getOggettoAttivita());
			
		}
		
		if((fase!= null && fase.equals("valutazione")) ) {
			
			if(this.getApprovazioneCDA()!= null && !this.getApprovazioneCDA().equals(""))
				esternalizzazione.setApprovazioneCDA(Utility.getDateFromString(this.getApprovazioneCDA()));
			
			if(this.getDataProbabileInizioEsternalizzazione()!= null && !this.getDataProbabileInizioEsternalizzazione().equals(""))
				esternalizzazione.setDataProbabileInizioEsternalizzazione(Utility.getDateFromString(this.getDataProbabileInizioEsternalizzazione()));
			
			esternalizzazione.setNomeFornitore(this.nomeFornitore);
			esternalizzazione.setSpazioEconomicoEuropeo(this.spazioEconomicoEuropeo);
			esternalizzazione.setAffidabilitaCompetenzaFornitore(this.affidabilitaCompetenzaFornitore) ;			
			esternalizzazione.setValutazioneRischioEsternalizzazione(this.valutazioneRischioEsternalizzazione);
			esternalizzazione.setSlaContrattualmenteDefiniti(this.slaContrattualmenteDefiniti);
			esternalizzazione.setStato(new Integer(this.stato));
			esternalizzazione.setResponsabileSingoloControllo(this.responsabileSingoloControllo);
		    esternalizzazione.setRispettoRemunerazione(this.rispettoRemunerazione);	
			esternalizzazione.setIdRC(this.getIdRC());
			esternalizzazione.setResponsabileControlloEsternalizzazione(this.getResponsabileControlloEsternalizzazione());
		   
		}
		
		if((fase!= null && fase.equals("predisposizione")) ) {
			
			esternalizzazione.setStato(new Integer(this.stato));
			
			if(this.getDataInizioEfficaciaContratto()!= null && !this.getDataInizioEfficaciaContratto().equals(""))
				esternalizzazione.setDataInizioEfficaciaContratto(Utility.getDateFromString(this.getDataInizioEfficaciaContratto()));		
			
			
			if(this.getDataInizioControllo()!= null && !this.getDataInizioControllo().equals(""))
				esternalizzazione.setDataInizioControllo(Utility.getDateFromString(this.getDataInizioControllo()));
			
			if(this.getDataFineControllo()!= null && !this.getDataFineControllo().equals(""))
				esternalizzazione.setDataFineControllo(Utility.getDateFromString(this.getDataFineControllo()));
			
			esternalizzazione.setValutazioneRischiBC(this.valutazioneRischiBC);	  
			//esternalizzazione.setRCEsternalizzazione(this.RCEsternalizzazione);		
			esternalizzazione.setNumeroComunicazioneIVASS(this.numeroComunicazioneIVASS);
			esternalizzazione.setNumeroRDA(this.numeroRDA);
			esternalizzazione.setNumeroODA(this.numeroODA);
			esternalizzazione.setNumeroProtocolloContrattuale(this.numeroProtocolloContrattuale);
			esternalizzazione.setPeriodoPreavvisoContrattuale(this.periodoPreavvisoContrattuale);
			esternalizzazione.setTipologiaRinnovo(this.tipologiaRinnovo);
			esternalizzazione.setVincoloPagamentoFattura(this.vincoloPagamentoFattura);
			esternalizzazione.setDescrizioneEsitoControllo(this.descrizioneEsitoControllo);		
			esternalizzazione.setNotaAzioneCorrettiva(this.notaAzioneCorrettiva);
			esternalizzazione.setDescrizioneControllo(this.descrizioneControllo);
			esternalizzazione.setIdRC(this.getIdRC());
			esternalizzazione.setResponsabileControlloEsternalizzazione(this.getResponsabileControlloEsternalizzazione());
			esternalizzazione.setResponsabileSingoloControllo(this.responsabileSingoloControllo);

		}
		
	}

	
	
	
	/**
	 * @return the oggettoAttivita
	 */
	public String getOggettoAttivita() {
		return oggettoAttivita;
	}

	/**
	 * @param oggettoAttivita the oggettoAttivita to set
	 */
	public void setOggettoAttivita(String oggettoAttivita) {
		this.oggettoAttivita = oggettoAttivita;
	}

	public String getIdEsternalizzazione() {
		return idEsternalizzazione;
	}

	public void setIdEsternalizzazione(String idEsternalizzazione) {
		this.idEsternalizzazione = idEsternalizzazione;
	}

	public String getDataCreazioneEsternalizzazione() {
		return dataCreazioneEsternalizzazione;
	}

	public void setDataCreazioneEsternalizzazione(String dataCreazioneEsternalizzazione) {
		this.dataCreazioneEsternalizzazione = dataCreazioneEsternalizzazione;
	}

	public String getCompilataDa() {
		return compilataDa;
	}

	public void setCompilataDa(String compilataDa) {
		this.compilataDa = compilataDa;
	}

	public String getCodiceDirezione() {
		return codiceDirezione;
	}

	public void setCodiceDirezione(String codiceDirezione) {
		this.codiceDirezione = codiceDirezione;
	}

	public String getTitoloEsternalizzazione() {
		return titoloEsternalizzazione;
	}

	public void setTitoloEsternalizzazione(String titoloEsternalizzazione) {
		this.titoloEsternalizzazione = titoloEsternalizzazione;
	}

	public String getTipoEsternalizzazione() {
		return tipoEsternalizzazione;
	}

	public void setTipoEsternalizzazione(String tipoEsternalizzazione) {
		this.tipoEsternalizzazione = tipoEsternalizzazione;
	}

	public String getDescrizioneTipoEsternalizzazione() {
		return descrizioneTipoEsternalizzazione;
	}

	public void setDescrizioneTipoEsternalizzazione(String descrizioneTipoEsternalizzazione) {
		this.descrizioneTipoEsternalizzazione = descrizioneTipoEsternalizzazione;
	}

	public String getDataModificaTipologiaEsternalizzazione() {
		return dataModificaTipologiaEsternalizzazione;
	}

	public void setDataModificaTipologiaEsternalizzazione(String dataModificaTipologiaEsternalizzazione) {
		this.dataModificaTipologiaEsternalizzazione = dataModificaTipologiaEsternalizzazione;
	}

	public String getUtenteModificaEsternalizzazione() {
		return utenteModificaEsternalizzazione;
	}

	public void setUtenteModificaEsternalizzazione(String utenteModificaEsternalizzazione) {
		this.utenteModificaEsternalizzazione = utenteModificaEsternalizzazione;
	}

	public String getMotivazioneEsternalizzazione() {
		return motivazioneEsternalizzazione;
	}

	public void setMotivazioneEsternalizzazione(String motivazioneEsternalizzazione) {
		this.motivazioneEsternalizzazione = motivazioneEsternalizzazione;
	}

	public String getApprovazioneCDA() {
		return approvazioneCDA;
	}

	public void setApprovazioneCDA(String approvazioneCDA) {
		this.approvazioneCDA = approvazioneCDA;
	}

	public String getBeneficioConseguibile() {
		return beneficioConseguibile;
	}

	public void setBeneficioConseguibile(String beneficioConseguibile) {
		this.beneficioConseguibile = beneficioConseguibile;
	}

	public String getDescrizioneBeneficioConseguibile() {
		return descrizioneBeneficioConseguibile;
	}

	public void setDescrizioneBeneficioConseguibile(String descrizioneBeneficioConseguibile) {
		this.descrizioneBeneficioConseguibile = descrizioneBeneficioConseguibile;
	}

	public String getDataProbabileInizioEsternalizzazione() {
		return dataProbabileInizioEsternalizzazione;
	}

	public void setDataProbabileInizioEsternalizzazione(String dataProbabileInizioEsternalizzazione) {
		this.dataProbabileInizioEsternalizzazione = dataProbabileInizioEsternalizzazione;
	}

	public String getNomeFornitore() {
		return nomeFornitore;
	}

	public void setNomeFornitore(String nomeFornitore) {
		this.nomeFornitore = nomeFornitore;
	}

	public String getSpazioEconomicoEuropeo() {
		return spazioEconomicoEuropeo;
	}

	public void setSpazioEconomicoEuropeo(String spazioEconomicoEuropeo) {
		this.spazioEconomicoEuropeo = spazioEconomicoEuropeo;
	}

	public String getAffidabilitaCompetenzaFornitore() {
		return affidabilitaCompetenzaFornitore;
	}

	public void setAffidabilitaCompetenzaFornitore(String affidabilitaCompetenzaFornitore) {
		this.affidabilitaCompetenzaFornitore = affidabilitaCompetenzaFornitore;
	}

	public String getRischiositaEsternalizzazione() {
		return rischiositaEsternalizzazione;
	}

	public void setRischiositaEsternalizzazione(String rischiositaEsternalizzazione) {
		this.rischiositaEsternalizzazione = rischiositaEsternalizzazione;
	}

	public String getDescrizioneRischiositaEsternalizzazione() {
		return descrizioneRischiositaEsternalizzazione;
	}

	public void setDescrizioneRischiositaEsternalizzazione(String descrizioneRischiositaEsternalizzazione) {
		this.descrizioneRischiositaEsternalizzazione = descrizioneRischiositaEsternalizzazione;
	}

	public String getValutazioneRischioEsternalizzazione() {
		return valutazioneRischioEsternalizzazione;
	}

	public void setValutazioneRischioEsternalizzazione(String valutazioneRischioEsternalizzazione) {
		this.valutazioneRischioEsternalizzazione = valutazioneRischioEsternalizzazione;
	}

	public String getVerificaConflittiInteressi() {
		return verificaConflittiInteressi;
	}

	public void setVerificaConflittiInteressi(String verificaConflittiInteressi) {
		this.verificaConflittiInteressi = verificaConflittiInteressi;
	}

	public String getDescrizioneVerificaConflittiInteressi() {
		return descrizioneVerificaConflittiInteressi;
	}

	public void setDescrizioneVerificaConflittiInteressi(String descrizioneVerificaConflittiInteressi) {
		this.descrizioneVerificaConflittiInteressi = descrizioneVerificaConflittiInteressi;
	}

	public String getRischiBC() {
		return rischiBC;
	}

	public void setRischiBC(String rischiBC) {
		this.rischiBC = rischiBC;
	}

	public String getDescrizioneRischiBC() {
		return descrizioneRischiBC;
	}

	public void setDescrizioneRischiBC(String descrizioneRischiBC) {
		this.descrizioneRischiBC = descrizioneRischiBC;
	}

	public String getValutazioneRischiBC() {
		return valutazioneRischiBC;
	}

	public void setValutazioneRischiBC(String valutazioneRischiBC) {
		this.valutazioneRischiBC = valutazioneRischiBC;
	}

	public String getPianoReinternalizzazione() {
		return pianoReinternalizzazione;
	}

	public void setPianoReinternalizzazione(String pianoReinternalizzazione) {
		this.pianoReinternalizzazione = pianoReinternalizzazione;
	}

	public String getDescrizionePianoReinternalizzazione() {
		return descrizionePianoReinternalizzazione;
	}

	public void setDescrizionePianoReinternalizzazione(String descrizionePianoReinternalizzazione) {
		this.descrizionePianoReinternalizzazione = descrizionePianoReinternalizzazione;
	}

	public String getRispettoRemunerazione() {
		return rispettoRemunerazione;
	}

	public void setRispettoRemunerazione(String rispettoRemunerazione) {
		this.rispettoRemunerazione = rispettoRemunerazione;
	}



	public String getResponsabileControlloEsternalizzazione() {
		return responsabileControlloEsternalizzazione;
	}

	public void setResponsabileControlloEsternalizzazione(String responsabileControlloEsternalizzazione) {
		this.responsabileControlloEsternalizzazione = responsabileControlloEsternalizzazione;
	}

	public String getRegistrazioneRCEsternalizzazione() {
		return registrazioneRCEsternalizzazione;
	}

	public void setRegistrazioneRCEsternalizzazione(String registrazioneRCEsternalizzazione) {
		this.registrazioneRCEsternalizzazione = registrazioneRCEsternalizzazione;
	}

	public String getUtenteOperativoEsternalizzazione() {
		return utenteOperativoEsternalizzazione;
	}

	public void setUtenteOperativoEsternalizzazione(String utenteOperativoEsternalizzazione) {
		this.utenteOperativoEsternalizzazione = utenteOperativoEsternalizzazione;
	}

	public String getRegistrazioneUOEsternalizzazione() {
		return registrazioneUOEsternalizzazione;
	}

	public void setRegistrazioneUOEsternalizzazione(String registrazioneUOEsternalizzazione) {
		this.registrazioneUOEsternalizzazione = registrazioneUOEsternalizzazione;
	}
	
	public String getDataInizioEfficaciaContratto() {
		return dataInizioEfficaciaContratto;
	}

	public void setDataInizioEfficaciaContratto(String dataInizioEfficaciaContratto) {
		this.dataInizioEfficaciaContratto = dataInizioEfficaciaContratto;
	}

	public String getNumeroComunicazioneIVASS() {
		return numeroComunicazioneIVASS;
	}

	public void setNumeroComunicazioneIVASS(String numeroComunicazioneIVASS) {
		this.numeroComunicazioneIVASS = numeroComunicazioneIVASS;
	}

	public String getNumeroRDA() {
		return numeroRDA;
	}

	public void setNumeroRDA(String numeroRDA) {
		this.numeroRDA = numeroRDA;
	}

	public String getNumeroODA() {
		return numeroODA;
	}

	public void setNumeroODA(String numeroODA) {
		this.numeroODA = numeroODA;
	}

	public String getNumeroProtocolloContrattuale() {
		return numeroProtocolloContrattuale;
	}

	public void setNumeroProtocolloContrattuale(String numeroProtocolloContrattuale) {
		this.numeroProtocolloContrattuale = numeroProtocolloContrattuale;
	}

	public String getPeriodoPreavvisoContrattuale() {
		return periodoPreavvisoContrattuale;
	}

	public void setPeriodoPreavvisoContrattuale(String periodoPreavvisoContrattuale) {
		this.periodoPreavvisoContrattuale = periodoPreavvisoContrattuale;
	}

	public String getTipologiaRinnovo() {
		return tipologiaRinnovo;
	}

	public void setTipologiaRinnovo(String tipologiaRinnovo) {
		this.tipologiaRinnovo = tipologiaRinnovo;
	}


	/**
	 * @return the slaContrattualmenteDefiniti
	 */
	public String getSlaContrattualmenteDefiniti() {
		return slaContrattualmenteDefiniti;
	}

	/**
	 * @param slaContrattualmenteDefiniti the slaContrattualmenteDefiniti to set
	 */
	public void setSlaContrattualmenteDefiniti(String slaContrattualmenteDefiniti) {
		this.slaContrattualmenteDefiniti = slaContrattualmenteDefiniti;
	}

	public String getDescrizioneControllo() {
		return descrizioneControllo;
	}

	public void setDescrizioneControllo(String descrizioneControllo) {
		this.descrizioneControllo = descrizioneControllo;
	}

	public String getTipoControllo() {
		return tipoControllo;
	}

	public void setTipoControllo(String tipoControllo) {
		this.tipoControllo = tipoControllo;
	}

	public String getDescrizioneTipoControllo() {
		return descrizioneTipoControllo;
	}

	public void setDescrizioneTipoControllo(String descrizioneTipoControllo) {
		this.descrizioneTipoControllo = descrizioneTipoControllo;
	}
	
	public String getDataInizioControllo() {
		return dataInizioControllo;
	}

	public void setDataInizioControllo(String dataInizioControllo) {
		this.dataInizioControllo = dataInizioControllo;
	}

	public String getDataFineControllo() {
		return dataFineControllo;
	}	

	public void setDataFineControllo(String dataFineControllo) {
		this.dataFineControllo = dataFineControllo;
	}

	public String getPeriodicita() {
		return periodicita;
	}

	public void setPeriodicita(String periodicita) {
		this.periodicita = periodicita;
	}

	public String getDescrizionePeriodicita() {
		return descrizionePeriodicita;
	}

	public void setDescrizionePeriodicita(String descrizionePeriodicita) {
		this.descrizionePeriodicita = descrizionePeriodicita;
	}

	public String getVincoloPagamentoFattura() {
		return vincoloPagamentoFattura;
	}

	public void setVincoloPagamentoFattura(String vincoloPagamentoFattura) {
		this.vincoloPagamentoFattura = vincoloPagamentoFattura;
	}

	public String getResponsabileSingoloControllo() {
		return responsabileSingoloControllo;
	}

	public void setResponsabileSingoloControllo(String responsabileSingoloControllo) {
		this.responsabileSingoloControllo = responsabileSingoloControllo;
	}

	public String getEsitoControllo() {
		return esitoControllo;
	}

	public void setEsitoControllo(String esitoControllo) {
		this.esitoControllo = esitoControllo;
	}

	public String getDescrEsitoControllo() {
		return descrEsitoControllo;
	}

	public void setDescrEsitoControllo(String descrEsitoControllo) {
		this.descrEsitoControllo = descrEsitoControllo;
	}

	public String getDescrizioneEsitoControllo() {
		return descrizioneEsitoControllo;
	}

	public void setDescrizioneEsitoControllo(String descrizioneEsitoControllo) {
		this.descrizioneEsitoControllo = descrizioneEsitoControllo;
	}

	public String getAzioneCorrettiva() {
		return azioneCorrettiva;
	}

	public void setAzioneCorrettiva(String azioneCorrettiva) {
		this.azioneCorrettiva = azioneCorrettiva;
	}

	public String getDescrizioneAzioneCorrettiva() {
		return descrizioneAzioneCorrettiva;
	}

	public void setDescrizioneAzioneCorrettiva(String descrizioneAzioneCorrettiva) {
		this.descrizioneAzioneCorrettiva = descrizioneAzioneCorrettiva;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getNotaAzioneCorrettiva() {
		return notaAzioneCorrettiva;
	}

	public void setNotaAzioneCorrettiva(String notaAzioneCorrettiva) {
		this.notaAzioneCorrettiva = notaAzioneCorrettiva;
	}

	public List<UtenteWeb> getListaConoRE() {
		return listaConoRE;
	}

	public void setListaConoRE(List<UtenteWeb> listaConoRE) {
		this.listaConoRE = listaConoRE;
	}

	public UtenteWeb getUtenteWs() {
		return utenteWs;
	}

	public void setUtenteWs(UtenteWeb utenteWs) {
		this.utenteWs = utenteWs;
	}	

	public UtenteWeb getResponsabileWs() {
		return responsabileWs;
	}

	public void setResponsabileWs(UtenteWeb responsabileWs) {
		this.responsabileWs = responsabileWs;
	}

	public String getIdRC() {
		return idRC;
	}

	public void setIdRC(String idRC) {
		this.idRC = idRC;
	}

	public String getIdUO() {
		return idUO;
	}

	public void setIdUO(String idUO) {
		this.idUO = idUO;
	}
	
	/**
	 * @return the listaTask
	 */
	public List<Task> getListaTask() {
		return listaTask;
	}

	/**
	 * @param listaTask the listaTask to set
	 */
	public void setListaTask(List<Task> listaTask) {
		this.listaTask = listaTask;
	}
	

	/**
	 * @return the listaControlli
	 */
	public List<ControlloWeb> getListaControlli() {
		return listaControlli;
	}

	/**
	 * @param listaControlli the listaControlli to set
	 */
	public void setListaControlli(List<ControlloWeb> listaControlli) {
		this.listaControlli = listaControlli;
	}

	@Override
	public String toString() {
		return "EsternalizzazioneWeb [idEsternalizzazione=" + idEsternalizzazione + ", dataCreazioneEsternalizzazione="
				+ dataCreazioneEsternalizzazione + ", compilataDa=" + compilataDa + ", codiceUtente=" + codiceDirezione
				+ ", titoloEsternalizzazione=" + titoloEsternalizzazione + ", tipoEsternalizzazione="
				+ tipoEsternalizzazione + ", descrizioneTipoEsternalizzazione=" + descrizioneTipoEsternalizzazione
				+ ", dataModificaTipologiaEsternalizzazione=" + dataModificaTipologiaEsternalizzazione
				+ ", utenteModificaEsternalizzazione=" + utenteModificaEsternalizzazione
				+ ", motivazioneEsternalizzazione=" + motivazioneEsternalizzazione + ", approvazioneCDA="
				+ approvazioneCDA + ", beneficioConseguibile=" + beneficioConseguibile
				+ ", descrizioneBeneficioConseguibile=" + descrizioneBeneficioConseguibile
				+ ", dataProbabileInizioEsternalizzazione=" + dataProbabileInizioEsternalizzazione + ", nomeFornitore="
				+ nomeFornitore + ", spazioEconomicoEuropeo=" + spazioEconomicoEuropeo
				+ ", affidabilitaCompetenzaFornitore=" + affidabilitaCompetenzaFornitore
				+ ", rischiositaEsternalizzazione=" + rischiositaEsternalizzazione
				+ ", descrizioneRischiositaEsternalizzazione=" + descrizioneRischiositaEsternalizzazione
				+ ", valutazioneRischioEsternalizzazione=" + valutazioneRischioEsternalizzazione
				+ ", verificaConflittiInteressi=" + verificaConflittiInteressi
				+ ", descrizioneVerificaConflittiInteressi=" + descrizioneVerificaConflittiInteressi + ", rischiBC="
				+ rischiBC + ", descrizioneRischiBC=" + descrizioneRischiBC + ", valutazioneRischiBC="
				+ valutazioneRischiBC + ", pianoReinternalizzazione=" + pianoReinternalizzazione
				+ ", descrizionePianoReinternalizzazione=" + descrizionePianoReinternalizzazione
				+ ", rispettoRemunerazione=" + rispettoRemunerazione + ", responsabileControlloEsternalizzazione=" + responsabileControlloEsternalizzazione
				+ ", registrazioneRCEsternalizzazione=" + registrazioneRCEsternalizzazione + ", UOEsternalizzazione="
				+ utenteOperativoEsternalizzazione + ", registrazioneUOEsternalizzazione=" + registrazioneUOEsternalizzazione
				+ ", dataInizioEfficaciaContratto=" + dataInizioEfficaciaContratto + ", numeroComunicazioneIVASS="
				+ numeroComunicazioneIVASS + ", numeroRDA=" + numeroRDA + ", numeroODA=" + numeroODA
				+ ", numeroProtocolloContrattuale=" + numeroProtocolloContrattuale + ", periodoPreavvisoContrattuale="
				+ periodoPreavvisoContrattuale + ", tipologiaRinnovo=" + tipologiaRinnovo
				+ ", SLAContrattualmenteDefiniti=" + slaContrattualmenteDefiniti + ", descrizioneControllo="
				+ descrizioneControllo + ", tipoControllo=" + tipoControllo + ", descrizioneTipoControllo="
				+ descrizioneTipoControllo + ", dataInizioControllo=" + dataInizioControllo + ", dataFineControllo="
				+ dataFineControllo + ", periodicita=" + periodicita + ", descrizionePeriodicita="
				+ descrizionePeriodicita + ", vincoloPagamentoFattura=" + vincoloPagamentoFattura
				+ ", responsabileSingoloControllo=" + responsabileSingoloControllo + ", esitoControllo="
				+ esitoControllo + ", descrEsitoControllo=" + descrEsitoControllo + ", descrizioneEsitoControllo="
				+ descrizioneEsitoControllo + ", azioneCorrettiva=" + azioneCorrettiva
				+ ", descrizioneAzioneCorrettiva=" + descrizioneAzioneCorrettiva + ", stato=" + stato
				+ ", notaAzioneCorrettiva=" + notaAzioneCorrettiva + "]";
	}
}
