package it.sara.esternalizzazione.web.bean;

import org.springframework.web.multipart.MultipartFile;

public class WebFile {

	private MultipartFile file;
	private String idOggetto;
	private String descrizioneOggetto;
	private String ambito;
	
	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getIdOggetto() {
		return idOggetto;
	}

	public void setIdOggetto(String idOggetto) {
		this.idOggetto = idOggetto;
	}

	public String getDescrizioneOggetto() {
		return descrizioneOggetto;
	}

	public void setDescrizioneOggetto(String descrizioneOggetto) {
		this.descrizioneOggetto = descrizioneOggetto;
	}

	public String getAmbito() {
		return ambito;
	}

    public void setAmbito(String ambito) {
		this.ambito = ambito;
	}

	
	
}
