package it.sara.esternalizzazione.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.web.bean.UtenteWeb;
import it.sara.esternalizzazione.ws.ClientWS;
import it.sara.esternalizzazione.ws.bean.TableOfZwestDatifornit;
import it.sara.esternalizzazione.ws.bean.TableOfZwsConoDirezione;
import it.sara.esternalizzazione.ws.bean.ZwestDatifornit;
import it.sara.esternalizzazione.ws.bean.ZwsConoDirezione;
import it.sara.esternalizzazione.ws.bean.ZwsWestConoResponsabileResponse;
import it.sara.esternalizzazione.ws.bean.ZwsWestDatiFornitoreResponse;
import it.sara.esternalizzazione.ws.bean.ZwsWestUtenteResponse;

@Component
public class GestioneWSDL implements IGestioneWSDL {
	@Autowired
	ClientWS clientWS;
	
	@Override
	public ZwsWestUtenteResponse loadUtente(String userid) {
		
		return clientWS.loadUtente(userid);
	}

	@Override
	public ZwsWestDatiFornitoreResponse loadDatiFornitori(String ragioneSociale1) {

		return clientWS.loadDatiFornitori(ragioneSociale1);
	}

	@Override
	public ZwsWestConoResponsabileResponse loadDatiConoRE(String userid, String zzenteacq) {

		return clientWS.loadDatiConoRE(userid, zzenteacq);
	}
	
	@Override
	public  List<ZwestDatifornit> getListaFornitori(String ragioneSociale1) {
		ZwsWestDatiFornitoreResponse responseWSDatiFornitore = new ZwsWestDatiFornitoreResponse();		
		responseWSDatiFornitore = loadDatiFornitori(ragioneSociale1);
		TableOfZwestDatifornit tableZwestDatifornit = responseWSDatiFornitore.getTbWest();
		if(responseWSDatiFornitore.getTbWest() == null) {
			return null;
		}	
		return tableZwestDatifornit.getItem();
	}
	
	@Override	
	public List<UtenteWeb> getListaConoRE(UtenteWeb utente) {

		ZwsWestConoResponsabileResponse responseWSConoResponsabile = new ZwsWestConoResponsabileResponse();		
		responseWSConoResponsabile = loadDatiConoRE(utente.getUsername(), utente.getZzenteacq());

		if(responseWSConoResponsabile == null) {
			return null;
		}

		TableOfZwsConoDirezione tableConoDirezione = responseWSConoResponsabile.getTbCono();		

		List<UtenteWeb> utentiConoRE = new ArrayList<>();
		List<ZwsConoDirezione> listaConoDir= tableConoDirezione.getItem();
		for(ZwsConoDirezione zwsConoDirezione:listaConoDir) {
			UtenteWeb utenteCono = new UtenteWeb();
			utenteCono.setNome(zwsConoDirezione.getNome());
			utenteCono.setCognome(zwsConoDirezione.getCognome());
			if(zwsConoDirezione.getUsrid().contains("@"))
			{
				utenteCono.setUsername(zwsConoDirezione.getUsrid().substring(0, zwsConoDirezione.getUsrid().lastIndexOf('@')));
			}
			else
				utenteCono.setUsername(zwsConoDirezione.getUsrid());
			
			utentiConoRE.add(utenteCono);
		}	

		return utentiConoRE;
	}
	
	
	
}
