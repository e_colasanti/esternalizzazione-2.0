/**
 * 
 */
package it.sara.esternalizzazione.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.sara.esternalizzazione.business.GestioneEsternalizzazione;
import it.sara.esternalizzazione.business.GestioneTipoUtente;
import it.sara.esternalizzazione.db.dao.EsternalizzazioneDao;
import it.sara.esternalizzazione.web.bean.PaginaEsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.TipoUtenteWeb;

/**
 * @author f.vescovi
 *
 */
@RestController
@RequestMapping("/rest/estrazioni")
@CrossOrigin(origins="*")
public class EstrazioniController extends BaseController {
	
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(EstrazioniController.class);
	
	@Autowired
	GestioneTipoUtente gestioneTipoUtente;
	
	@Autowired
	EsternalizzazioneDao esternalizzazioneDao;	
	
	@Autowired
	GestioneEsternalizzazione gestioneEsternalizzazione;	
	
	
	@RequestMapping(value={"/lista", "/lista/{stato}", "/lista/{stato}/{pageNumber}", "/lista/{stato}/{pageNumber}/{righePerPagina}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public PaginaEsternalizzazioneWeb listaEstrazioni(@PathVariable(value="stato", required=false) Integer stato, 
			@PathVariable(value="pageNumber", required=false) Integer pageNumber,
			@PathVariable(value="righePerPagina", required=false) Integer righePerPagina,
			HttpServletResponse response,
			HttpServletRequest request){
		
		logger.debug(request.getProtocol()+request.getServerName()+request.getLocalPort()+request.getContextPath());
		int numeroPagina = ((pageNumber==null)?0:pageNumber);
		int righePagina = ((righePerPagina==null)?5:righePerPagina);
		
		List<Integer> elencoStati = cercaStati(stato, response);
		String username = getUtente().getUsername();
		
		//return gestioneRemind.getListaRemind(getUtente(),elencoStati,numeroPagina,righePagina);
	    return null;
		
		
	}
	
	
	
	private List<Integer> cercaStati(Integer stato, HttpServletResponse response) {
		Collection<? extends GrantedAuthority> ga= SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		List<Integer> elencoStati = gestioneUtente.elencoStati(ga);
		if (stato != null && stato != -1) {
			boolean statoTrovato = false;
			for (Integer s:elencoStati)if(stato.equals(s))statoTrovato=true;
			if (!statoTrovato) {
				try {
					response.sendError(403,"Utente non autorizzato");
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}else {
				//se lo stato è diverso da null il filtro è lo stato
				elencoStati = new LinkedList<Integer>();
				elencoStati.add(stato);
			}
		}
		return elencoStati;
	}
	
	@RequestMapping(value={"/cerca/{pageNumber}/{righePerPagina}","/cerca/{stato}/{pageNumber}/{righePerPagina}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public PaginaEsternalizzazioneWeb cercaEstrazioni(@PathVariable(value="stato", required=false) Integer stato, 
											@PathVariable(value="pageNumber", required=false) Integer pageNumber, 
											@PathVariable(value="righePerPagina", required=false) Integer righePerPagina, 
								            @RequestParam(value="responsabileControlloEsternalizzazione", required=false) String responsabileControlloEsternalizzazione,
								            @RequestParam(value="responsabileSingoloControllo", required=false) String responsabileSingoloControllo,
								            @RequestParam(value="codiceDirezione", required=false) String codiceDirezione,								            
											HttpServletResponse response){
		int numeroPagina = ((pageNumber==null)?0:pageNumber);
		int righePagina = ((righePerPagina==null)?5:righePerPagina);
		List<Integer> elencoStati = cercaStati(stato, response);
		String query=null;
		return gestioneEsternalizzazione.cercaEstrazioni(elencoStati,responsabileControlloEsternalizzazione,responsabileSingoloControllo,codiceDirezione,((pageNumber==null)?0:pageNumber),((righePerPagina==null)?5:righePerPagina));
	}
	
	
	
	@RequestMapping(value={"/listaTipiUtente"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<TipoUtenteWeb> getTipiUtente(){
		logger.debug("get Tipi Utente...");
		return gestioneTipoUtente.getAll();
	}
	
	

}
