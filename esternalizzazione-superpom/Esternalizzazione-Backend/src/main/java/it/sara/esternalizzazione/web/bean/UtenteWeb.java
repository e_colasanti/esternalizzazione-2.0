package it.sara.esternalizzazione.web.bean;

import java.util.List;

public class UtenteWeb implements Cloneable{

	private String username;
	private List<String> ruoli;	
	private String nome;
	private String cognome;
	private String descrLivello;
	private String esito;
	private String livello;
	private String responsabile;
	private String sesso;
	private String zzenteacq;
	private String text;
    private String userWest;
    private String codiceDirezione;
    private String sessionID;
    private String lastAccessedTime;
    private String cookie;
    
    
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public List<String> getRuoli() {
		return ruoli;
	}
	public void setRuoli(List<String> ruoli) {
		this.ruoli = ruoli;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getDescrLivello() {
		return descrLivello;
	}
	public void setDescrLivello(String descrLivello) {
		this.descrLivello = descrLivello;
	}
	public String getEsito() {
		return esito;
	}
	public void setEsito(String esito) {
		this.esito = esito;
	}
	public String getLivello() {
		return livello;
	}
	public void setLivello(String livello) {
		this.livello = livello;
	}
	public String getResponsabile() {
		return responsabile;
	}
	public void setResponsabile(String responsabile) {
		this.responsabile = responsabile;
	}
	public String getSesso() {
		return sesso;
	}
	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
	public String getZzenteacq() {
		return zzenteacq;
	}
	public void setZzenteacq(String zzenteacq) {
		this.zzenteacq = zzenteacq;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getUserWest() {
		return userWest;
	}
	public void setUserWest(String userWest) {
		this.userWest = userWest;
	}
	/**
	 * @return the codiceDirezione
	 */
	public String getCodiceDirezione() {
		return codiceDirezione;
	}
	/**
	 * @param codiceDirezione the codiceDirezione to set
	 */
	public void setCodiceDirezione(String codiceDirezione) {
		this.codiceDirezione = codiceDirezione;
	}
	
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	
	/**
	 * @return the lastAccessedTime
	 */
	public String getLastAccessedTime() {
		return lastAccessedTime;
	}
	/**
	 * @param lastAccessedTime the lastAccessedTime to set
	 */
	public void setLastAccessedTime(String lastAccessedTime) {
		this.lastAccessedTime = lastAccessedTime;
	}
	/**
	 * @return the cookie
	 */
	public String getCookie() {
		return cookie;
	}
	/**
	 * @param cookie the cookie to set
	 */
	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
	@Override
	public Object clone() throws CloneNotSupportedException {

		return super.clone();
	}
	
	
	
	
}
