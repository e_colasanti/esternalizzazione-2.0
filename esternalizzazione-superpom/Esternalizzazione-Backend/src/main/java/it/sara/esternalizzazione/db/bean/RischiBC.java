package it.sara.esternalizzazione.db.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="WEST_RISCHI_BC")
public class RischiBC implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -68074125759236178L;
	
	@Id
	@Column(name="ID_RISCHI_BC")
	Integer idRischiBC;
	
	@Column(name="DESCRIZIONE_RISCHI_BC")
	String descrizioneRischiBC;

	
	@Column(name="DATA_INSERIMENTO")
	Date dataInserimento;

	
	@Column(name="DATA_AGGIORNAMENTO")
	String dataAggiornamento;

	
	@Column(name="UTENTE_INSERIMENTO")
	String utenteInserimento;

	
	@Column(name="UTENTE_AGGIORNAMENTO")
	String utenteAggiornamento;


	@Column(name="DATA_INIZIO_VALIDITA")
	Date dataInizioValidita;


	@Column(name="DATA_FINE_VALIDITA")
	Date dataFineValidita;
	
	
	public Integer getIdRischiBC() {
		return idRischiBC;
	}


	public void setIdRischiBC(Integer idRischiBC) {
		this.idRischiBC = idRischiBC;
	}


	public String getDescrizioneRischiBC() {
		return descrizioneRischiBC;
	}


	public void setDescrizioneRischiBC(String descrizioneRischiBC) {
		this.descrizioneRischiBC = descrizioneRischiBC;
	}


	public Date getDataInserimento() {
		return dataInserimento;
	}


	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}


	public String getDataAggiornamento() {
		return dataAggiornamento;
	}


	public void setDataAggiornamento(String dataAggiornamento) {
		this.dataAggiornamento = dataAggiornamento;
	}


	public String getUtenteInserimento() {
		return utenteInserimento;
	}


	public void setUtenteInserimento(String utenteInserimento) {
		this.utenteInserimento = utenteInserimento;
	}


	public String getUtenteAggiornamento() {
		return utenteAggiornamento;
	}


	public void setUtenteAggiornamento(String utenteAggiornamento) {
		this.utenteAggiornamento = utenteAggiornamento;
	}


	public Date getDataInizioValidita() {
		return dataInizioValidita;
	}


	public void setDataInizioValidita(Date dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}


	public Date getDataFineValidita() {
		return dataFineValidita;
	}


	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}


	@Override
	public String toString() {
		return "RischiBC [idRischiBC=" + idRischiBC + ", descrizioneRischiBC=" + descrizioneRischiBC
				+ ", dataInserimento=" + dataInserimento + ", dataAggiornamento=" + dataAggiornamento
				+ ", utenteInserimento=" + utenteInserimento + ", utenteAggiornamento=" + utenteAggiornamento
				+ ", dataInizioValidita=" + dataInizioValidita + ", dataFineValidita=" + dataFineValidita + "]";
	}
}
