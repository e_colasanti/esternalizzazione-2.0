/**
 * 
 */
package it.sara.esternalizzazione.business;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.business.thread.ScheduledTaskThread;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Remind;
import it.sara.esternalizzazione.db.dao.EsternalizzazioneDao;
import it.sara.esternalizzazione.db.dao.RemindDao;
import it.sara.esternalizzazione.db.dao.TipoUtenteDao;
import it.sara.esternalizzazione.web.bean.PaginaRemindWeb;
import it.sara.esternalizzazione.web.bean.RemindWeb;
import it.sara.esternalizzazione.web.bean.UtenteWeb;

/**
 * @author f.vescovi
 *
 */
@Component
public class GestioneRemind {


	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(GestioneRemind.class);


	@Autowired
	EsternalizzazioneDao esternalizzazioneDao;
	
	@Autowired
	RemindDao remindDao;
	
	
	@Autowired
	TipoUtenteDao tipoUtenteDao;



	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");



	public void reportCurrentTime() {
		logger.info("The time is now {}", dateFormat.format(new Date()));		
	}


	@Scheduled(fixedRate = 640000)	
	public void scheduledTask() {

		logger.info("GestioneRemind.scheduledTask()");

		@SuppressWarnings("unused")
		ScheduledTaskThread scheduledTaskThread=null;
		
		if(esternalizzazioneDao!=null) {

			List<Esternalizzazione> listaEsternalizzazioneRCNull= esternalizzazioneDao.findByRCNull();
			
			scheduledTaskThread=new ScheduledTaskThread(listaEsternalizzazioneRCNull, remindDao, tipoUtenteDao,"REMIND Responsabile non valorizzato"); 
			scheduledTaskThread.start();

			List<Esternalizzazione> listaEsternalizzazioneRischiosita=esternalizzazioneDao.findByTipoEstAndRischiosita();

			scheduledTaskThread=new ScheduledTaskThread(listaEsternalizzazioneRischiosita, remindDao, tipoUtenteDao,"REMIND Rischiosità Esternalizzazione non valorizzato"); 
			scheduledTaskThread.start();
			
			List<Esternalizzazione> listaEsternalizzazionePianoReint=esternalizzazioneDao.findByPianoReinternalizzazioneNull();

			scheduledTaskThread=new ScheduledTaskThread(listaEsternalizzazionePianoReint, remindDao, tipoUtenteDao,"REMIND Piano di Reinternalizzazione non valorizzato "); 
			scheduledTaskThread.start();
			
			
			List<Esternalizzazione> listaEsternalizzazionePeriodicita=esternalizzazioneDao.findByPeriodicita();

			scheduledTaskThread=new ScheduledTaskThread(listaEsternalizzazionePeriodicita, remindDao, tipoUtenteDao,"REMIND Periodicita"); 
			scheduledTaskThread.start();
			
			

		}

	}
	
	
	public PaginaRemindWeb getListaRemind(UtenteWeb utente, List<Integer> stati, int numeroPagina,int righePagina) {

		Sort lSort = new Sort(new Sort.Order(Sort.Direction.DESC, "dataInizio"));
		Pageable p = new PageRequest(numeroPagina, righePagina, lSort);
		Page<Remind> l =null;
		if(utente.getResponsabile()!= null) {
			List<String> codici =new ArrayList<>();
			codici.add(utente.getZzenteacq());			
			l = remindDao.findAllByTipoUtente(new Integer(1), p);
		}
		if (l==null)return null;

		List<RemindWeb> lista = new LinkedList<RemindWeb>();
		for (Remind remind : l) {		
			lista.add(new RemindWeb(remind));
		}
		PaginaRemindWeb risultato = new PaginaRemindWeb();
		risultato.setListaRemind(lista);
		risultato.setPaginaCorrente(l.getNumber());
		risultato.setRecordPerPagina(l.getNumberOfElements());
		risultato.setTotalePagine(l.getTotalPages());
		risultato.setTotaleRecord(l.getTotalElements());
		return risultato;
	}
	
	
	
	public PaginaRemindWeb cercaRemind(String query, Integer pageNumber, Integer righePerPagina) {

		try {	
			List<RemindWeb> lista =  caricaListaRemindWeb(query,  pageNumber,  righePerPagina);

			PaginaRemindWeb risultato = new PaginaRemindWeb();
			risultato.setListaRemind(lista);
			risultato.setPaginaCorrente(1);
			risultato.setRecordPerPagina(10);
			risultato.setTotalePagine(1);
			risultato.setTotaleRecord(new Long(lista.size()));
			return risultato;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
	
	
	@SuppressWarnings("unused")
	private List<RemindWeb> caricaListaRemindWeb(String query, Integer pageNumber, Integer righePerPagina)throws UnsupportedEncodingException {
		Sort lSort = new Sort(new Sort.Order(Sort.Direction.DESC, "dataInizio"));
		Pageable p = new PageRequest(pageNumber, righePerPagina, lSort);

		String queryDecoded = URLDecoder.decode(query, "UTF-8");

		Page<Remind> listaRemind = remindDao.findByTipoRemindInLike("%"+queryDecoded.toLowerCase()+"%",p);
		if (listaRemind==null)return null;

		List<RemindWeb> lista = new LinkedList<RemindWeb>();
		for(Remind remind:listaRemind) {
			lista.add(new RemindWeb(remind));			
		}
		return lista;
	}
	
	
	/**
	 * Ritorna il Remind per mostrarne i contenuti
	 * @param idTask
	 * @return
	 */
	public RemindWeb getRemindById(Integer idRemind){

		Remind remind=remindDao.findOne(idRemind);

		RemindWeb remindWeb=new RemindWeb(remind);

		return remindWeb;

	}
	

}
