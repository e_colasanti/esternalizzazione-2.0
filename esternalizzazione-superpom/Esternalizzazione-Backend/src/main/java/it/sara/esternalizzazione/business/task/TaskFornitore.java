/**
 * 
 */
package it.sara.esternalizzazione.business.task;

import java.util.Date;
import java.util.Random;

import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.bean.TipoTask;
import it.sara.esternalizzazione.db.bean.TipoUtente;
import it.sara.esternalizzazione.utility.WestCostants;

/**
 * @author f.vescovi
 *
 */
@Component("TaskFornitore")
public class TaskFornitore extends AbstractTaskFactory{

	
	public TaskFornitore() {}

	/* (non-Javadoc)
	 * @see it.sara.esternalizzazione.business.task.AbstractTaskFactory#createTask(it.sara.esternalizzazione.db.bean.Esternalizzazione)
	 */
	@Override
	public Task createTask(Esternalizzazione esternalizzazione) {
		// TODO Auto-generated method stub

		Task task=new Task();
		task.setEsternalizzazione(esternalizzazione);
		TipoTask  tipoTask=gestioneTipoTask.getTipoTaskByTipoTask(WestCostants.IFA);
		task.setTipoTask(tipoTask);	
		task.setTipoCampoAttivatoEst(WestCostants.NOME_FORNITORE);
		task.setProgLavTask(new Random().nextInt());
		task.setDataCreazione(new Date());       
		task.setDataFine(null);
		task.setStato("A");
		TipoUtente tipoUtente=gestioneTipoUtente.getByDescTipoUtente(WestCostants.FA);
		task.setTipoUtente(tipoUtente);						
		task.setUtenteOwner(esternalizzazione.getUtenteModificaEsternalizzazione());
		task.setUtenteResponsabile(esternalizzazione.getCompilataDa());
		task.setUtenteDelegato(esternalizzazione.getResponsabileSingoloControllo());
		task.setFaseEsternalizzazione(WestCostants.FASEVALUTAZIONE);
		task.setCodiceDirezione(tipoUtente.getCodiceDirezione());						
		taskDao.save(task);
		gestioneMail.inviaMail(task);		

		return task;
	}






}
