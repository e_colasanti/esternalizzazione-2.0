package it.sara.esternalizzazione.business;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.FaseEsternalizzazione;
import it.sara.esternalizzazione.db.bean.TipoFase;
import it.sara.esternalizzazione.db.dao.FaseEsternalizzazioneDao;

@Component("GestioneFaseEsternalizzazione")
public class GestioneFaseEsternalizzazione {
	
	
	private Logger logger = LoggerFactory.getLogger(GestioneFaseEsternalizzazione.class);

	
	@Autowired
	FaseEsternalizzazioneDao faseEsternalizzazioneDao;
	
		
	/**
	 * Ritorna la lista delle Fasi delle Esternalizzazioni dato l'id esternalizzazione 
	 * @param idEsternalizzazione
	 * @return
	 */
	public List<FaseEsternalizzazione> getListaFaseEsternalizzazioneByEstId(Integer idEsternalizzazione){
		List<FaseEsternalizzazione> listaTask=new ArrayList<>();
		
		List<FaseEsternalizzazione> list=faseEsternalizzazioneDao.findAll();
		for (FaseEsternalizzazione faseEsternalizzazione : list) {
			if(faseEsternalizzazione.getEsternalizzazione().getIdEsternalizzazione()==idEsternalizzazione)
				listaTask.add(faseEsternalizzazione);
		}
		return listaTask;
		
	}
	
	
	
	@Transactional(rollbackFor=Exception.class)
	public FaseEsternalizzazione salvaFaseEsternalizzazione(Esternalizzazione esternalizzazione,TipoFase tipoFase){
		
		FaseEsternalizzazione newFaseEsternalizzazione=null;
		
		try {
			
			FaseEsternalizzazione faseEsternalizzazione=new FaseEsternalizzazione();
			faseEsternalizzazione.setTipoFase(tipoFase);
			faseEsternalizzazione.setEsternalizzazione(esternalizzazione);
			
			newFaseEsternalizzazione=this.faseEsternalizzazioneDao.save(faseEsternalizzazione);
			
		}catch(Exception ex) {			
			logger.error("Errore durante il salvataggio della FaseEsternalizzazione");
			ex.printStackTrace();			
		}
		
		return newFaseEsternalizzazione;
		
		
	}

	
	

}
