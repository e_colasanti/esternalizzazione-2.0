package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.EsitoControllo;

public interface EsitoControlloDao extends CrudRepository<EsitoControllo,Integer> {
	
	public EsitoControllo findByIdEsitoControllo(Integer idEsitoControllo);
	public List<EsitoControllo> findAll();
}
