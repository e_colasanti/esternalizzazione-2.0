//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.11 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.08.08 alle 02:27:44 PM CEST 
//


package it.sara.esternalizzazione.ws.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TbCono" type="{urn:sap-com:document:sap:soap:functions:mc-style}TableOfZwsConoDirezione" minOccurs="0"/&gt;
 *         &lt;element name="Usrid" type="{urn:sap-com:document:sap:rfc:functions}char30"/&gt;
 *         &lt;element name="Zzenteacq" type="{urn:sap-com:document:sap:rfc:functions}char3"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tbCono",
    "usrid",
    "zzenteacq"
})
@XmlRootElement(name = "ZwsWestConoResponsabile")
public class ZwsWestConoResponsabile {

    @XmlElement(name = "TbCono")
    protected TableOfZwsConoDirezione tbCono;
    @XmlElement(name = "Usrid", required = true)
    protected String usrid;
    @XmlElement(name = "Zzenteacq", required = true)
    protected String zzenteacq;

    /**
     * Recupera il valore della proprietà tbCono.
     * 
     * @return
     *     possible object is
     *     {@link TableOfZwsConoDirezione }
     *     
     */
    public TableOfZwsConoDirezione getTbCono() {
        return tbCono;
    }

    /**
     * Imposta il valore della proprietà tbCono.
     * 
     * @param value
     *     allowed object is
     *     {@link TableOfZwsConoDirezione }
     *     
     */
    public void setTbCono(TableOfZwsConoDirezione value) {
        this.tbCono = value;
    }

    /**
     * Recupera il valore della proprietà usrid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsrid() {
        return usrid;
    }

    /**
     * Imposta il valore della proprietà usrid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsrid(String value) {
        this.usrid = value;
    }

    /**
     * Recupera il valore della proprietà zzenteacq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZzenteacq() {
        return zzenteacq;
    }

    /**
     * Imposta il valore della proprietà zzenteacq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZzenteacq(String value) {
        this.zzenteacq = value;
    }

}
