/**
 * 
 */
package it.sara.esternalizzazione.workflow;

import java.util.Map;

/**
 * @author f.vescovi
 *
 */
public interface Workflow {
	
	/**

	* Method for processing workflow.

	*

	* @param parameters

	* maps of object which are needed for workflow processing

	* @return true in case that workflow is done without errors otherwise false

	*/
	
	public boolean processWorkflow(Map<String, Object> parameters);


}
