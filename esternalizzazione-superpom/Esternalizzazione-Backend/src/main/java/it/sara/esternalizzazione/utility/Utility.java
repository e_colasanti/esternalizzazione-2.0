package it.sara.esternalizzazione.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import it.sara.esternalizzazione.db.bean.TipoTask;
import it.sara.esternalizzazione.web.bean.UtenteWeb;

public class Utility {



	//formatta l'id che arriva con una string '0000000'+int
	public static String formattaId(int idEsternalizzazione) {
		String pattern= "000000000";
		DecimalFormat myFormatter = new DecimalFormat(pattern);
		String output = myFormatter.format(idEsternalizzazione);
		return output;
	}
	//formatta l'id '0000000'+int che arriva con una string 'int'
	public static String formattaId(String str) {

		if (str.length ()> 0) {

			if (str.charAt (0) == '0') {

				return formattaId (str.substring (1));

			}

		}		
		return str;	
	}

	public static String getStringFromDate(Date parametro) {

		String result = null;
		if(parametro==null)
			return "";
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		result = formatter.format(parametro); 

		return result;

	}

	//formatta un stringa Data in un date 
	public static Date getDateFromString(String data) {
		Date myDate = null;
		try {
			DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ITALY);
			myDate = dateFormat.parse(data);
			System.out.println(dateFormat.format(myDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return myDate;
	}

	public static String clobStringConversion(Clob clb){

		if (clb == null)
			return  "";

		StringBuffer str = new StringBuffer();
		String strng;

		BufferedReader bufferRead;
		try {
			bufferRead = new BufferedReader(clb.getCharacterStream());
			while ((strng=bufferRead .readLine())!=null)
				str.append(strng);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return str.toString();
	}  
	/***
	 * Ritorna per reflection tutti gli attributi del bean rilievo
	 * @param fields
	 * @param type
	 * @return
	 */
	public static List<Field> getAllFields(List<Field> fields, Class<?> type) {
		fields.addAll(Arrays.asList(type.getDeclaredFields()));

		if (type.getSuperclass() != null) {
			getAllFields(fields, type.getSuperclass());
		}

		return fields;
	}

	public static String returnStringaVuota(String input) {
		if (input == null) return "";
		return input;
	}


	public static TipoTask getTipoTask(List<TipoTask> tipoTaskList,String expression) {
		TipoTask tipoTask=null;

		for (TipoTask tT : tipoTaskList) {
			if(tT.getTipoTask().equals(expression))
				tipoTask=tT;
			break;
		} 


		return tipoTask;
	}


	public static UtenteWeb cloneUtenteWeb(UtenteWeb utenteWeb,String env ) throws CloneNotSupportedException {
		UtenteWeb utenteWebClone=null;	
		if(env.equals(WestCostants.ENVIRONMENT_SVIL) || env.equals(WestCostants.ENVIRONMENT_COLL))
		{
			utenteWebClone=(UtenteWeb)utenteWeb.clone();
			if(utenteWebClone.getUsername().equals("l.vassallo"))
				utenteWebClone.setUsername("t.dipendente");
			else if(utenteWebClone.getUsername().equals("s.chessa"))
				utenteWebClone.setUsername("t2.dipendente");
		}
		return utenteWebClone;	
	}




}
