package it.sara.esternalizzazione.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.sara.esternalizzazione.business.GestioneEsternalizzazione;
import it.sara.esternalizzazione.business.GestioneFaseScadenzaOutSourcing;
import it.sara.esternalizzazione.business.GestioneFaseValutazioneOutSourcing;
import it.sara.esternalizzazione.business.GestioneTipologicheEsternalizzazione;
import it.sara.esternalizzazione.business.GestioneUtente;
import it.sara.esternalizzazione.business.GestioneWSDL;
import it.sara.esternalizzazione.utility.Utility;
import it.sara.esternalizzazione.web.bean.AzioniCorrettiveWeb;
import it.sara.esternalizzazione.web.bean.BeneficiConseguibiliWeb;
import it.sara.esternalizzazione.web.bean.EsitiControlloWeb;
import it.sara.esternalizzazione.web.bean.EsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.PaginaEsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.PeriodicitaWeb;
import it.sara.esternalizzazione.web.bean.PianiReinternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.RischiBCWeb;
import it.sara.esternalizzazione.web.bean.RischiositaEsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.TipiControlloWeb;
import it.sara.esternalizzazione.web.bean.TipoEsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.UtenteWeb;
import it.sara.esternalizzazione.web.bean.VerificheConflittiInteressiWeb;
import it.sara.esternalizzazione.ws.bean.ZwestDatifornit;

@RestController
@RequestMapping("/rest/esternalizzazione")
@CrossOrigin(origins="*")
public class EsternalizzazioneController extends BaseController{
	private Logger logger = LoggerFactory.getLogger(EsternalizzazioneController.class);
	
	@Autowired
	GestioneUtente gestioneUtente;
	
	@Autowired
	GestioneEsternalizzazione gestioneEsternalizzazione;

	@Autowired
	GestioneFaseValutazioneOutSourcing gestioneFaseValutazioneOutSourcing;

	
	@Autowired
	GestioneFaseScadenzaOutSourcing gestioneFaseScadenzaOutSourcing;

	
	
	
	@Autowired
	GestioneWSDL gestione;
	
	@Autowired	
	GestioneTipologicheEsternalizzazione gestionetipologicheEsternalizzazione;
	
	
	@RequestMapping(value={"/lista", "/lista/{stato}", "/lista/{stato}/{pageNumber}", "/lista/{stato}/{pageNumber}/{righePerPagina}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public PaginaEsternalizzazioneWeb listaEsternalizzazione(@PathVariable(value="stato", required=false) Integer stato, 
			@PathVariable(value="pageNumber", required=false) Integer pageNumber,
			@PathVariable(value="righePerPagina", required=false) Integer righePerPagina,
			HttpServletResponse response,
			HttpServletRequest request){
		
		int numeroPagina = ((pageNumber==null)?0:pageNumber);
		int righePagina = ((righePerPagina==null)?5:righePerPagina);
		
		List<Integer> elencoStati = cercaStati(stato, response);
		UtenteWeb utente=getUtente();
		
		PaginaEsternalizzazioneWeb paginaEsternalizzazioneWeb=null;
		
		if(stato==4)
		{
			paginaEsternalizzazioneWeb=gestioneFaseValutazioneOutSourcing.getListaEsternalizzazioni(utente, elencoStati, numeroPagina, righePagina);				
		}
		else if(stato==5)
		{
			paginaEsternalizzazioneWeb=gestioneFaseScadenzaOutSourcing.getListaEsternalizzazioni(utente, elencoStati, numeroPagina, righePagina);						
		}
		else if(stato==6)
		{
			paginaEsternalizzazioneWeb=gestioneEsternalizzazione.getListaEsternalizzazioni(utente,elencoStati,numeroPagina,righePagina);						
		}		
		else
			paginaEsternalizzazioneWeb=gestioneEsternalizzazione.getListaEsternalizzazioni(utente,elencoStati,numeroPagina,righePagina);
			
		
		return paginaEsternalizzazioneWeb;
	
	}


	private List<Integer> cercaStati(Integer stato, HttpServletResponse response) {
		Collection<? extends GrantedAuthority> ga= SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		List<Integer> elencoStati = gestioneUtente.elencoStati(ga);
		if (stato != null && stato != -1) {
			boolean statoTrovato = false;
			for (Integer s:elencoStati)if(stato.equals(s))statoTrovato=true;
			if (!statoTrovato) {
				try {
					response.sendError(403,"Utente non autorizzato");
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}else {
				//se lo stato è diverso da null il filtro è lo stato
				elencoStati = new LinkedList<Integer>();
				elencoStati.add(stato);
			}
		}
		return elencoStati;
	}
	

	@RequestMapping(value={"/cerca/{pageNumber}/{righePerPagina}","/cerca/{stato}/{pageNumber}/{righePerPagina}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public PaginaEsternalizzazioneWeb cercaEsternalizzazioni(@PathVariable(value="stato", required=false) Integer stato, 
											@PathVariable(value="pageNumber", required=false) Integer pageNumber, 
											@PathVariable(value="righePerPagina", required=false) Integer righePerPagina, 
								            @RequestParam(value="query", required=false) String query,
											HttpServletResponse response){

		int numeroPagina = ((pageNumber==null)?0:pageNumber);
		int righePagina = ((righePerPagina==null)?5:righePerPagina);

		//elenco degli stati abilitati per i ruoli dell'utente.
		//se lo stato verso il quale si vuole navigare non è uno accessibile all'utente errore
		List<Integer> elencoStati = cercaStati(stato, response);
		return gestioneEsternalizzazione.cercaEsternalizzazioni(elencoStati,query,((pageNumber==null)?0:pageNumber),((righePerPagina==null)?5:righePerPagina));
	}
	
	@RequestMapping(value={"/passaListaEsternalizzazioneStato"},  method=RequestMethod.POST)
	public PaginaEsternalizzazioneWeb passaListaEsternalizzazioniStato(@RequestParam(value="statoSuccessivo", required=false) String statoSuccessivo
																	  ,@RequestParam(value="statoCorrente", required=false) Integer statoCorrente
																	  ,@RequestParam(value="idEsternalizzazione", required=false) Integer[] idEsternalizzazione
																	  ,@RequestParam(value="pagina", required=false) Integer pagina
																	  ,@RequestParam(value="righePerPagina", required=false) Integer righePerPagina
																	  ,HttpServletResponse response
																	  ,HttpServletRequest request){
		logger.debug("lista esternalizzazioni: " + idEsternalizzazione);
		logger.debug("stato corrente: " + statoCorrente);
		logger.debug("stato successivo: " + statoSuccessivo);
		logger.debug("pagina: " + pagina);
		logger.debug("Righe per pagina: " + righePerPagina);
		
		String[] risultato = new String[idEsternalizzazione.length];
		if (statoSuccessivo.equals("2"))
			for (int i = 0; i < idEsternalizzazione.length;i++) 
				risultato[i]= gestioneEsternalizzazione.passaConfermata(""+i);

//		try {
//			this.gestioneMail.inviaMailConferma(risultato);
//		}catch(Exception e) {
//			logger.error("Errore nell'invio delle mail di conferma dell'ispezione");
//		}
		
		return listaEsternalizzazione(statoCorrente,pagina,righePerPagina,response,request);
	}
	
	@RequestMapping(value={"/salva"},  method=RequestMethod.POST, consumes="application/json")
	public EsternalizzazioneWeb salvaEsternalizzazione(@RequestBody  EsternalizzazioneWeb esternalizzazioneWeb){
		logger.debug("salva esternalizzazione: " + esternalizzazioneWeb);
		gestioneEsternalizzazione.salvaEsternalizzazione(esternalizzazioneWeb);
		return getEsternalizzazione(esternalizzazioneWeb.getIdEsternalizzazione());
	}
	
	@RequestMapping(value={"/get/{id}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public EsternalizzazioneWeb getEsternalizzazione(@PathVariable(name="id")  String id){
		logger.debug("get esternalizzazione: " + id);
		return gestioneEsternalizzazione.getEsternalizzazione(id);
	}
	
	@RequestMapping(value={"/getEsternalizzazionePrecedente/{id}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public EsternalizzazioneWeb getEsternalizzazionePrecedente(@PathVariable(name="id")  String id){
		logger.debug("getEsternalizzazionePrecedente: ");
		
		UtenteWeb utente = getUtente();
		return gestioneEsternalizzazione.getEsternalizzazionePrecedente(utente,id);
	}
	
	@RequestMapping(value={"/new"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public EsternalizzazioneWeb getNewEsternalizzazione(){
		logger.debug("get new esternalizzazione: " );
		EsternalizzazioneWeb esternalizzazioneWeb =new EsternalizzazioneWeb();
		esternalizzazioneWeb.setStato("1");
		esternalizzazioneWeb.setDataCreazioneEsternalizzazione(Utility.getStringFromDate(new Date()));
		esternalizzazioneWeb.setDataModificaTipologiaEsternalizzazione(Utility.getStringFromDate(new Date()));
		return esternalizzazioneWeb;
	}
	
	
	@RequestMapping(value={"/newRda/{idEsternalizzazione}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public EsternalizzazioneWeb getNewRda(){
		logger.debug("get new rda: " );
		EsternalizzazioneWeb esternalizzazioneWeb =new EsternalizzazioneWeb();
//		esternalizzazioneWeb.setStato("1");
//		esternalizzazioneWeb.setDataCreazioneEsternalizzazione(Utility.getStringFromDate(new Date()));
//		esternalizzazioneWeb.setDataModificaTipologiaEsternalizzazione(Utility.getStringFromDate(new Date()));
//		esternalizzazioneWeb.setNumeroRDA(numeroRDA);
		return esternalizzazioneWeb;
	}
	
	@RequestMapping(value={"/listaTipiEsternalizzazione"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<TipoEsternalizzazioneWeb> getTipiEsternalizzazione(){
		logger.debug("get Tipi Esternalizzazione...");
		return gestionetipologicheEsternalizzazione.getTipiEsternalizzazione();
	}

	@RequestMapping(value={"/listaBenefici"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<BeneficiConseguibiliWeb> getBenefici(){
		logger.debug("get Benefici Conseguibili...");
		return gestionetipologicheEsternalizzazione.getBenefici();
	}
	
	@RequestMapping(value={"/listaRischiositaEsternalizzazione"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<RischiositaEsternalizzazioneWeb> getRischiositaEsternalizzazione(){
		logger.debug("get Valutazione Rischi...");
		return gestionetipologicheEsternalizzazione.getRischiositaEsternalizzazione();
	}
	
	@RequestMapping(value={"/listaConflittiInteressi"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<VerificheConflittiInteressiWeb> getVerificaConflittiInteressi(){
		logger.debug("get Verifica Conflitti Interessi...");
		return gestionetipologicheEsternalizzazione.getVerificaConflittiInteressi();
	}
	
	@RequestMapping(value={"/listaRischiBC"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<RischiBCWeb> getRischiBC(){
		logger.debug("get RischiBC...");
		return gestionetipologicheEsternalizzazione.getRischiBC();
	}
	
	@RequestMapping(value={"/listaPianiReinternalizzazione"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<PianiReinternalizzazioneWeb> getPianiReinternalizzazione(){
		logger.debug("get PianiReinternalizzazione...");
		return gestionetipologicheEsternalizzazione.getPianiReinternalizzazione();
	}
	
	@RequestMapping(value={"/listaTipiControllo"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<TipiControlloWeb> getTipiControllo(){
		logger.debug("get TipiControllo...");
		return gestionetipologicheEsternalizzazione.getTipiControllo();
	}
	
	@RequestMapping(value={"/listaPeriodicita"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<PeriodicitaWeb> getPeriodicita(){
		logger.debug("get Periodicita...");
		return gestionetipologicheEsternalizzazione.getPeriodicita();
	}
	
	@RequestMapping(value={"/listaAzioniCorrettive"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<AzioniCorrettiveWeb> getAzioniCorrettive(){
		logger.debug("get Azioni Correttive...");
		return gestionetipologicheEsternalizzazione.getAzioniCorrettive();
	}
	
	@RequestMapping(value={"/listaEsitiControllo"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<EsitiControlloWeb> getEsitiControllo(){
		logger.debug("get Esiti Controllo...");
		return gestionetipologicheEsternalizzazione.getEsitiControllo();
	}
	
	@RequestMapping(value={"/listaFornitori/{ragioneSociale1}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<ZwestDatifornit> getFornitori(@PathVariable("ragioneSociale1") String ragioneSociale1){
		logger.debug("get lista fornitori...");
		return gestione.getListaFornitori(ragioneSociale1);
	}
	
	
	@RequestMapping(value={"/listaConoRE"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<UtenteWeb> getListaConoRE(){
		logger.debug("get lista Responsabili...");
		return gestione.getListaConoRE(getUtente());
	}
	
	@RequestMapping(value={"/esternalizzazioneConfermata/{id}"},  method=RequestMethod.POST, consumes="application/json")
	public EsternalizzazioneWeb esternalizzazioneConfermata(@PathVariable(name="id")  String id){
		logger.debug("valida esternalizzazione: " + id);
		String idEsternalizzazione = gestioneEsternalizzazione.passaConfermata(id);
		if (idEsternalizzazione==null || !idEsternalizzazione.equals(""))return null;
		
//		try {
//			this.gestioneMail.inviaMailConferma(new Integer[] {idEsternalizzazione});
//		}catch(Exception e) {
//			logger.error("Errore nell'invio delle mail di conferma dell'isezione");
//		}

		return getEsternalizzazione(idEsternalizzazione);
	}
	
	@RequestMapping(value={"/assegnaEsternalizzazione/{id}/{responsabileControllo}/"},  method=RequestMethod.POST, consumes="application/json")
	public EsternalizzazioneWeb assegnaEsternalizzazione(@PathVariable(name="id")  String id,@PathVariable(name="responsabileControllo")  String responsabileControllo){
		logger.debug("assegna esternalizzazione: " + id+" responsabileControllo:  "+responsabileControllo);
		String userName=getUtente().getUsername();
		boolean modificata = gestioneEsternalizzazione.assegnaEsternalizzazione(id,responsabileControllo);
		if (modificata==false)return null;
		return getEsternalizzazione(id+"");
	}
}
