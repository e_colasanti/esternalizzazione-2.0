package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

public class TipoListaWeb implements Serializable {

		
	/**
	 * 
	 */
	private static final long serialVersionUID = -6506843789706048917L;
		
	private String codice;
	private String descrizione;

	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
}
