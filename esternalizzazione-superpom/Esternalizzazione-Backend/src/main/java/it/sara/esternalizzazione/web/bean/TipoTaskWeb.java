package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import it.sara.esternalizzazione.db.bean.TipoTask;

public class TipoTaskWeb extends TipoListaWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8292739570210674942L;
	
	
	public TipoTaskWeb(TipoTask tipoTask) {
		this.setCodice(tipoTask.getIdTipoTask()+"");
		this.setDescrizione(tipoTask.getDescTipoTask());
	}

	

}
