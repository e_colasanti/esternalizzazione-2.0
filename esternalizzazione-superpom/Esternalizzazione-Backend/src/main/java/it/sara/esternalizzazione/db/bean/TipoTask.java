package it.sara.esternalizzazione.db.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="WEST_TIPO_TASK")
public class TipoTask implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1088801714330141749L;
	
	

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GENERA_ID")
    @SequenceGenerator(name="SEQ_GENERA_ID", sequenceName="SEQ_GENERA_ID", allocationSize=1)
	@Column(name="ID_TIPO_TASK")
	Integer idTipoTask;
	
	@Column(name="TIPO_TASK")
	String tipoTask;
	
	
	@Column(name="DESC_TIPO_TASK")
	String descTipoTask;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_UTENTE")
	TipoUtente tipoUtente;
	
	@Column(name="OGGETTO")
	String oggetto;
	

	/**
	 * @return the idTipoTask
	 */
	public Integer getIdTipoTask() {
		return idTipoTask;
	}


	/**
	 * @param idTipoTask the idTipoTask to set
	 */
	public void setIdTipoTask(Integer idTipoTask) {
		this.idTipoTask = idTipoTask;
	}


	/**
	 * @return the tipoTask
	 */
	public String getTipoTask() {
		return tipoTask;
	}


	/**
	 * @param tipoTask the tipoTask to set
	 */
	public void setTipoTask(String tipoTask) {
		this.tipoTask = tipoTask;
	}


	/**
	 * @return the descTipoTask
	 */
	public String getDescTipoTask() {
		return descTipoTask;
	}


	/**
	 * @param descTipoTask the descTipoTask to set
	 */
	public void setDescTipoTask(String descTipoTask) {
		this.descTipoTask = descTipoTask;
	}


	/**
	 * @return the tipoUtente
	 */
	public TipoUtente getTipoUtente() {
		return tipoUtente;
	}


	/**
	 * @param tipoUtente the tipoUtente to set
	 */
	public void setTipoUtente(TipoUtente tipoUtente) {
		this.tipoUtente = tipoUtente;
	}


	/**
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}


	/**
	 * @param oggetto the oggetto to set
	 */
	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}


	


}
