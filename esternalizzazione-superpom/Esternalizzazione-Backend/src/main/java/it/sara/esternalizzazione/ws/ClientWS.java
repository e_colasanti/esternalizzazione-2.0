package it.sara.esternalizzazione.ws;


import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import it.sara.esternalizzazione.utility.ObjectToXml;
import it.sara.esternalizzazione.ws.bean.ObjectFactory;
import it.sara.esternalizzazione.ws.bean.TableOfZwestDatifornit;
import it.sara.esternalizzazione.ws.bean.TableOfZwsConoDirezione;
import it.sara.esternalizzazione.ws.bean.TableOfZwsDirezioneDescrizione;
import it.sara.esternalizzazione.ws.bean.ZwsWestConoResponsabile;
import it.sara.esternalizzazione.ws.bean.ZwsWestConoResponsabileResponse;
import it.sara.esternalizzazione.ws.bean.ZwsWestDatiFornitore;
import it.sara.esternalizzazione.ws.bean.ZwsWestDatiFornitoreResponse;
import it.sara.esternalizzazione.ws.bean.ZwsWestUtente;
import it.sara.esternalizzazione.ws.bean.ZwsWestUtenteResponse;


@Component
public class ClientWS extends WebServiceGatewaySupport{

	@Autowired
	@Qualifier("webServiceTemplateUtente")
	WebServiceTemplate wsTempUtente;
	
	@Autowired
	@Qualifier("webServiceTemplateFornitori")
	WebServiceTemplate wsTempFornitori;
	
	@Autowired
	@Qualifier("webServiceTemplateConoResponsabile")
	WebServiceTemplate wsTempConoRE;

	@Autowired
	@Qualifier("environmentType") 
	String environment;

	public ZwsWestUtenteResponse loadUtente(String userid){
		
		ObjectFactory obj = new ObjectFactory();
		ZwsWestUtente zwsWestUtente = obj.createZwsWestUtente();
		zwsWestUtente.setUsrid(userid);
		zwsWestUtente.setTbRichiedente(new TableOfZwsDirezioneDescrizione());
		ZwsWestUtenteResponse response =null;
		
		if(environment.equals("SVIL"))
			try {
				response=(ZwsWestUtenteResponse)ObjectToXml.read("U",userid);
			} catch (JAXBException | FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   
		else       
		    response =  (ZwsWestUtenteResponse) wsTempUtente.marshalSendAndReceive(zwsWestUtente);
		
		
		
//		try {
//			ObjectToXml.convert(response, "U");
//		} catch (JAXBException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		return response;
	}
	
	
	public ZwsWestDatiFornitoreResponse loadDatiFornitori(String ragioneSociale1){
		
		ObjectFactory obj = new ObjectFactory();
		ZwsWestDatiFornitore zwsWestDatiFornitore = obj.createZwsWestDatiFornitore();
		zwsWestDatiFornitore.setRagSoc1(ragioneSociale1);
		zwsWestDatiFornitore.setTbWest(new TableOfZwestDatifornit());	
		ZwsWestDatiFornitoreResponse response =null;
		if(environment.equals("SVIL"))
			try {
				response=(ZwsWestDatiFornitoreResponse)ObjectToXml.read("F","");
			} catch (JAXBException | FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else       
            response =  (ZwsWestDatiFornitoreResponse) wsTempFornitori.marshalSendAndReceive(zwsWestDatiFornitore);
		
//		try {
//			ObjectToXml.convert(response, "F");
//		} catch (JAXBException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
		return response;
	}
	
	public ZwsWestConoResponsabileResponse loadDatiConoRE(String userid,String zzenteacq){
		
		if(userid.equals("l.vassallo"))
			userid="t.dipendente";
		if(userid.equals("s.chessa"))
			userid="t2.dipendente";
		
		
		
		ObjectFactory obj = new ObjectFactory();
		ZwsWestConoResponsabile zwsWestConoResponsabile = obj.createZwsWestConoResponsabile();
		zwsWestConoResponsabile.setUsrid(userid);
		zwsWestConoResponsabile.setZzenteacq(zzenteacq);
		zwsWestConoResponsabile.setTbCono(new TableOfZwsConoDirezione());	
	
		ZwsWestConoResponsabileResponse response =null;
		if(environment.equals("SVIL"))
			try {
				response=(ZwsWestConoResponsabileResponse)ObjectToXml.read("C","");
			} catch (JAXBException | FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else 
		
		response =  (ZwsWestConoResponsabileResponse) wsTempConoRE.marshalSendAndReceive(zwsWestConoResponsabile);
		
		
//		try {
//			ObjectToXml.convert(response, "C");
//		} catch (JAXBException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
		return response;
	}
	

}
