package it.sara.esternalizzazione.business.task;

import java.util.Date;
import java.util.Random;

import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.bean.TipoTask;
import it.sara.esternalizzazione.db.bean.TipoUtente;
import it.sara.esternalizzazione.utility.WestCostants;

@Component("TaskRischiBC")
public class TaskRischiBC extends AbstractTaskFactory{

	public TaskRischiBC() {}

	/* (non-Javadoc)
	 * @see it.sara.esternalizzazione.business.task.AbstractTaskFactory#createTask(it.sara.esternalizzazione.db.bean.Esternalizzazione)
	 */
	@Override
	public Task createTask(Esternalizzazione esternalizzazione) {
		Task task=new Task();
		task.setEsternalizzazione(esternalizzazione);
		TipoTask  tipoTask=gestioneTipoTask.getTipoTaskByTipoTask(WestCostants.RVBC);
		task.setTipoTask(tipoTask);	
		task.setTipoCampoAttivatoEst(WestCostants.RISCHI_BC);
		task.setProgLavTask(new Random().nextInt());
		task.setDataCreazione(new Date());       
		task.setDataFine(null);
		task.setStato("A");
		TipoUtente tipoUtente=gestioneTipoUtente.getByDescTipoUtente(WestCostants.BC);
		task.setTipoUtente(tipoUtente);										
		task.setUtenteOwner(esternalizzazione.getUtenteModificaEsternalizzazione());
		task.setUtenteResponsabile(esternalizzazione.getCompilataDa());
		task.setUtenteDelegato(esternalizzazione.getResponsabileSingoloControllo());
		task.setFaseEsternalizzazione(WestCostants.FASEVALUTAZIONE);
		task.setCodiceDirezione(tipoUtente.getCodiceDirezione());					
		taskDao.save(task);
		gestioneMail.inviaMail(task);
		return task;
	}

	
	
	
	
	
}
