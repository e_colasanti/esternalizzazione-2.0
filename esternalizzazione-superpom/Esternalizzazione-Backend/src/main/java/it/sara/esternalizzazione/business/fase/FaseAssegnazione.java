package it.sara.esternalizzazione.business.fase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.business.controlli.AssegnazioneRegole;
import it.sara.esternalizzazione.business.controlli.ControlloOperativoManager;
import it.sara.esternalizzazione.db.bean.ControlloOperativo;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.dao.ControlloOperativoDao;
import it.sara.esternalizzazione.db.dao.EsternalizzazioneDao;
import it.sara.esternalizzazione.web.bean.EsternalizzazioneWeb;


@Component("FaseAssegnazione")
public class FaseAssegnazione {
	
	private Logger logger = LoggerFactory.getLogger(FaseAssegnazione.class);
	
	
	@Autowired
	EsternalizzazioneDao esternalizzazioneDao;
	
	@Autowired
	ControlloOperativoManager controlloOperativoManager;
	
	@Autowired
	AssegnazioneRegole assegnazioneRegole; 
	

	@Autowired
	ControlloOperativoDao controlloOperativoDao;
	
	public FaseAssegnazione() {}
	
	
	public boolean assegnaEsternalizzazione(String id, String responsabileControllo) {

		boolean assegnato = false;
		Integer idFormattato=Integer.parseInt(id);
		
		Esternalizzazione esternalizzazione = this.esternalizzazioneDao.findByIdEsternalizzazione(idFormattato);

		EsternalizzazioneWeb esternalizzazioneWeb = new EsternalizzazioneWeb(esternalizzazione);
		ControlloOperativo controlloOperativo = this.controlloOperativoDao.findByRegistrazioneAndEsternalizzazione(responsabileControllo,idFormattato);
		if(controlloOperativo== null) {
			
			esternalizzazioneWeb.setIdRC(esternalizzazione.getIdRC());
			esternalizzazioneWeb.setResponsabileControlloEsternalizzazione(responsabileControllo);
			esternalizzazioneWeb.setResponsabileSingoloControllo(esternalizzazione.getResponsabileSingoloControllo());
			controlloOperativo = controlloOperativoManager.creaControlloOperativo(esternalizzazioneWeb);
		    if(controlloOperativo!=null)	
			logger.debug("ID Controllo Operativo: "+controlloOperativo.getIdControlloOperativo());

			assegnato= true;
		}
		else {
			assegnato= true;
		}
		if(assegnato) {
			esternalizzazione.setResponsabileControlloEsternalizzazione(responsabileControllo);
			esternalizzazione.setResponsabileSingoloControllo(esternalizzazioneWeb.getResponsabileSingoloControllo());			
			esternalizzazioneDao.save(esternalizzazione);
			assegnazioneRegole.updateTask(esternalizzazione);
			assegnazioneRegole.taskToActivate(esternalizzazione);
		}
		
		return assegnato;
	}
	
	
	

}
