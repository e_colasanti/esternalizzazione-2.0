package it.sara.esternalizzazione.controller;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import it.sara.esternalizzazione.business.GestioneUtente;
import it.sara.esternalizzazione.web.bean.UtenteWeb;


public abstract class  BaseController {
	
	@Autowired
	GestioneUtente gestioneUtente;
	@Autowired
	private HttpSession session;
	
	private Logger logger = LoggerFactory.getLogger(BaseController.class);
	
	
	protected UtenteWeb getUtente() {
		String username  = SecurityContextHolder.getContext().getAuthentication().getName();
		logger.debug("USERNAME: "+username+" SESSION ID: "+session.getId()+" CREATION TIME: "+new Date(session.getCreationTime())+" LAST ACCESSED TIME: "+new Date(session.getLastAccessedTime()));
		UtenteWeb utente=(UtenteWeb)session.getAttribute("UtenteWeb");		
		
		if(utente==null) {
			logger.debug("#####Utente non presente in Sessione####");
		       utente = gestioneUtente.popolaUtente(username);		
		       session.setAttribute("UtenteWeb", utente);    
				logger.debug("#####Utente in Sessione: "+session.getAttribute("UtenteWeb")+" #######");
		}
		return utente;
	}
}
