/**
 * 
 */
package it.sara.esternalizzazione.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.db.bean.TipoTask;
import it.sara.esternalizzazione.db.dao.TipoTaskDao;
import it.sara.esternalizzazione.utility.WestCostants;
import it.sara.esternalizzazione.web.bean.TipoTaskWeb;

/**
 * @author f.vescovi
 *
 */
@Component("GestioneTipoTask")
public class GestioneTipoTask {
	
	@Autowired
	TipoTaskDao tipoTaskDao;
	
	
	public TipoTask getTipoTaskByIdTipoEsternalizzazione(int idEsternalizzazione) 
	{
       TipoTask tipoTask=null;		
		
       switch (idEsternalizzazione) {
	case 2:
	    tipoTask=tipoTaskDao.findTipoTaskByTipoUtenteAndDescTipoTask(Integer.valueOf(4), WestCostants.CO);		
		break;		
	case 4:
		tipoTask=tipoTaskDao.findTipoTaskByTipoUtenteAndDescTipoTask(Integer.valueOf(4), WestCostants.AR);		
		break;
		

	default:
		break;
	}
  
       return tipoTask;
	}
	
	
	
	public List<TipoTaskWeb> getAll(){
		
		List<TipoTaskWeb> list=new ArrayList<>();
		
		List<TipoTask> lTask=tipoTaskDao.findAll();
		for (TipoTask tipoTask : lTask) {
		
			TipoTaskWeb tipoTaskWeb=new TipoTaskWeb(tipoTask);
			list.add(tipoTaskWeb);
		
		}	
		return list;
		
	}
	
	
	public TipoTask getTipoTaskByTipoTask(String tipo )
	{ TipoTask tTask=null;
	List<TipoTask> lTask=tipoTaskDao.findAll();
	for (TipoTask tipoTask : lTask) {

		if(tipoTask.getDescTipoTask().equals(tipo)) {
			tTask=tipoTask;
		    break;
		}
	}	    
	
		
	   return tTask;	
	}
	
	
}
