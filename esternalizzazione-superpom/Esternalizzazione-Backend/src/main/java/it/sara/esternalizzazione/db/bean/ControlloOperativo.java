package it.sara.esternalizzazione.db.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author io
 *
 */
@Entity
@Table(name="WEST_CONTROLLO_OPERATIVI")
public class ControlloOperativo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4966860746868757646L;
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_CONTROLLO_OPERATIVI")
    @SequenceGenerator(name="SEQ_CONTROLLO_OPERATIVI", sequenceName="SEQ_CONTROLLO_OPERATIVI", allocationSize=1)
	@Column(name="ID_CONT_OP")
	Integer idControlloOperativo;
	
	@Column(name="NOME")
	String nome;
	
	@Column(name="COGNOME")
	String cognome;
	
	@Column(name="REGISTRAZIONE")
	String registrazione;
	
	@Column(name="CONTROLLO")
	Integer controllo;
	
	@Column(name="OPERATIVO")
	Integer operativo;
	
	@Column(name="ID_ESTERNALIZZAZIONE")
	Integer esternalizzazione;
	
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="ID_ESTERNALIZZAZIONE")
//	Esternalizzazione esternalizzazione;
//	
//	

	public Integer getIdControlloOperativo() {
		return idControlloOperativo;
	}

	public void setIdControlloOperativo(Integer idControlloOperativo) {
		this.idControlloOperativo = idControlloOperativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getRegistrazione() {
		return registrazione;
	}

	public void setRegistrazione(String registrazione) {
		this.registrazione = registrazione;
	}

	public Integer getControllo() {
		return controllo;
	}

	public void setControllo(Integer controllo) {
		this.controllo = controllo;
	}

	public Integer getOperativo() {
		return operativo;
	}

	public void setOperativo(Integer operativo) {
		this.operativo = operativo;
	}

	public Integer getEsternalizzazione() {
		return esternalizzazione;
	}

	public void setEsternalizzazione(Integer esternalizzazione) {
		this.esternalizzazione = esternalizzazione;
	}

	@Override
	public String toString() {
		return "ControlloOperativo [idControlloOperativo=" + idControlloOperativo + ", nome=" + nome + ", cognome="
				+ cognome + ", registrazione=" + registrazione + ", controllo=" + controllo + ", operativo=" + operativo
				+ ", esternalizzazione=" + esternalizzazione + "]";
	}
}
