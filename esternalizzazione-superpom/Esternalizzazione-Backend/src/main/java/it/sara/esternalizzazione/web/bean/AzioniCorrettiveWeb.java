package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import it.sara.esternalizzazione.db.bean.AzioneCorrettiva;

public class AzioniCorrettiveWeb extends TipoListaWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2468613729409952042L;

	public AzioniCorrettiveWeb(AzioneCorrettiva azioneCorrettiva) {
		this.setCodice(azioneCorrettiva.getIdAzioneCorrettiva()+"");
		this.setDescrizione(azioneCorrettiva.getDescrizioneAzioneCorrettiva());
	}
}
