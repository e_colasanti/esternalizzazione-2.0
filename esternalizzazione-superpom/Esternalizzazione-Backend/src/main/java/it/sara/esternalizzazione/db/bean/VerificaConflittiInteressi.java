package it.sara.esternalizzazione.db.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="WEST_VERIFICA_CONFL_INT")
public class VerificaConflittiInteressi implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8942105439890563033L;
	
	@Id
	@Column(name="ID_VERIFICA_CONFL_INT")
	Integer idVerificaConflittiInteressi;
	
	@Column(name="DESCR_VERIFICA_CONFL_INT")
	String descrizioneVerificaConflittiInteressi;

	
	@Column(name="DATA_INSERIMENTO")
	Date dataInserimento;

	
	@Column(name="DATA_AGGIORNAMENTO")
	String dataAggiornamento;

	
	@Column(name="UTENTE_INSERIMENTO")
	String utenteInserimento;

	
	@Column(name="UTENTE_AGGIORNAMENTO")
	String utenteAggiornamento;


	@Column(name="DATA_INIZIO_VALIDITA")
	Date dataInizioValidita;
	

	@Column(name="DATA_FINE_VALIDITA")
	Date dataFineValidita;	
	
	public Integer getIdVerificaConflittiInteressi() {
		return idVerificaConflittiInteressi;
	}


	public void setIdVerificaConflittiInteressi(Integer idVerificaConflittiInteressi) {
		this.idVerificaConflittiInteressi = idVerificaConflittiInteressi;
	}


	public String getDescrizioneVerificaConflittiInteressi() {
		return descrizioneVerificaConflittiInteressi;
	}


	public void setDescrizioneVerificaConflittiInteressi(String descrizioneVerificaConflittiInteressi) {
		this.descrizioneVerificaConflittiInteressi = descrizioneVerificaConflittiInteressi;
	}


	public Date getDataInserimento() {
		return dataInserimento;
	}


	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}


	public String getDataAggiornamento() {
		return dataAggiornamento;
	}


	public void setDataAggiornamento(String dataAggiornamento) {
		this.dataAggiornamento = dataAggiornamento;
	}


	public String getUtenteInserimento() {
		return utenteInserimento;
	}


	public void setUtenteInserimento(String utenteInserimento) {
		this.utenteInserimento = utenteInserimento;
	}


	public String getUtenteAggiornamento() {
		return utenteAggiornamento;
	}


	public void setUtenteAggiornamento(String utenteAggiornamento) {
		this.utenteAggiornamento = utenteAggiornamento;
	}


	public Date getDataInizioValidita() {
		return dataInizioValidita;
	}


	public void setDataInizioValidita(Date dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}


	public Date getDataFineValidita() {
		return dataFineValidita;
	}


	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}


	@Override
	public String toString() {
		return "VerificaConflittiInteressi [idVerificaConflittiInteressi=" + idVerificaConflittiInteressi
				+ ", descrizioneVerificaConflittiInteressi=" + descrizioneVerificaConflittiInteressi
				+ ", dataInserimento=" + dataInserimento + ", dataAggiornamento=" + dataAggiornamento
				+ ", utenteInserimento=" + utenteInserimento + ", utenteAggiornamento=" + utenteAggiornamento
				+ ", dataInizioValidita=" + dataInizioValidita + ", dataFineValidita=" + dataFineValidita + "]";
	}
}
