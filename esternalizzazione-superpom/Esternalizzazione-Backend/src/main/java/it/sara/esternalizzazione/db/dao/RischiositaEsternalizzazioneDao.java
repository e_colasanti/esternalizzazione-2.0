package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.RischiositaEsternalizzazione;

public interface RischiositaEsternalizzazioneDao extends CrudRepository<RischiositaEsternalizzazione,Integer>{
	
	public RischiositaEsternalizzazione findByIdRischiositaEsternalizzazione(Integer idRischiositaEsternalizzazione);
	public List<RischiositaEsternalizzazione> findAll();
}
