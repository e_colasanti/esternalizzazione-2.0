package it.sara.esternalizzazione.business.fase;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.business.GestioneAzioneCorrettiva;
import it.sara.esternalizzazione.business.controlli.ControlloOperativoManager;
import it.sara.esternalizzazione.business.controlli.PredisposizioneRegole;
import it.sara.esternalizzazione.db.bean.AzioneCorrettiva;
import it.sara.esternalizzazione.db.bean.EsitoControllo;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Periodicita;
import it.sara.esternalizzazione.db.bean.PianoReinternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.bean.TipoControllo;
import it.sara.esternalizzazione.db.bean.TipoEsternalizzazione;
import it.sara.esternalizzazione.db.dao.AzioneCorrettivaDao;
import it.sara.esternalizzazione.db.dao.ControlloOperativoDao;
import it.sara.esternalizzazione.db.dao.EsitoControlloDao;
import it.sara.esternalizzazione.db.dao.EsternalizzazioneDao;
import it.sara.esternalizzazione.db.dao.PeriodicitaDao;
import it.sara.esternalizzazione.db.dao.PianoReinternalizzazioneDao;
import it.sara.esternalizzazione.db.dao.TipoControlloDao;
import it.sara.esternalizzazione.web.bean.EsternalizzazioneWeb;

@Component("FasePredisposizione")
public class FasePredisposizione {
	
	
	private Logger logger = LoggerFactory.getLogger(FaseValutazione.class);



	@Autowired
	ControlloOperativoDao controlloOperativoDao;
	
	@Autowired
	TipoControlloDao tipoControlloDao;
	

	@Autowired
	PeriodicitaDao periodicitaDao;
	
	@Autowired
	AzioneCorrettivaDao azioneCorrettivaDao;

	@Autowired
	EsitoControlloDao esitoControlloDao;
	
	
	@Autowired
	PianoReinternalizzazioneDao pianoReinternalizzazioneDao;
	
	@Autowired
	EsternalizzazioneDao esternalizzazioneDao;
	
	@Autowired
	ControlloOperativoManager controlloOperativoManager;
	
	@Autowired
	PredisposizioneRegole predisposizioneRegole; 
	
	
	@Autowired
    GestioneAzioneCorrettiva gestioneAzioneCorrettiva;	
	
	
	
	public Esternalizzazione predisposizioneEsternalizzazione(Esternalizzazione esternalizzazione, EsternalizzazioneWeb esternalizzazioneWeb,TipoEsternalizzazione tipoEsternalizzazione) {

		PianoReinternalizzazione pianoReinternalizzazione = null;
		if (esternalizzazioneWeb.getPianoReinternalizzazione()!=null)		
			pianoReinternalizzazione = this.pianoReinternalizzazioneDao.findByIdPianoReinternalizzazione(new Integer(esternalizzazioneWeb.getPianoReinternalizzazione()));


		TipoControllo tipoControllo= null;
		if (esternalizzazioneWeb.getTipoControllo()!=null)		
			tipoControllo = this.tipoControlloDao.findByIdTipoControllo(new Integer(esternalizzazioneWeb.getTipoControllo()));

		Periodicita periodicita= null;
		if (esternalizzazioneWeb.getPeriodicita()!=null)		
			periodicita = this.periodicitaDao.findByIdPeriodicita(new Integer(esternalizzazioneWeb.getPeriodicita()));

		EsitoControllo esitoControllo = null;
		if (esternalizzazioneWeb.getEsitoControllo()!=null)		
			esitoControllo = this.esitoControlloDao.findByIdEsitoControllo(new Integer(esternalizzazioneWeb.getEsitoControllo()));

		AzioneCorrettiva azioneCorrettiva = null;
		if (esternalizzazioneWeb.getAzioneCorrettiva()!=null || !("").equals(esternalizzazioneWeb.getAzioneCorrettiva()))		
			azioneCorrettiva = this.azioneCorrettivaDao.findByIdAzioneCorrettiva(new Integer(esternalizzazioneWeb.getAzioneCorrettiva()));

		esternalizzazione.setPianoReinternalizzazione(pianoReinternalizzazione);
		esternalizzazione.setTipoControllo(tipoControllo);
		esternalizzazione.setPeriodicita(periodicita);
		esternalizzazione.setEsitoControllo(esitoControllo);
		esternalizzazione.setAzioneCorrettiva(azioneCorrettiva);

		esternalizzazioneWeb.toEsternalizzazioneDb(esternalizzazione,tipoEsternalizzazione, "predisposizione");
		
		
	    List<Task>  taskToActivate= predisposizioneRegole.taskToActivate(esternalizzazione);
	    if(!taskToActivate.isEmpty()) {
	    	logger.debug("Attivati "+taskToActivate.size()+" per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());
	    	esternalizzazioneWeb.setListaTask(taskToActivate);
	    }
	
		
        esternalizzazioneDao.save(esternalizzazione);
		
		
		
		return esternalizzazione;
	}

}
