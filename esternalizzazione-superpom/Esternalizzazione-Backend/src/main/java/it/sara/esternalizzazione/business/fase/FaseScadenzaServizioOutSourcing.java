/**
 * 
 */
package it.sara.esternalizzazione.business.fase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author f.vescovi
 *
 */
@Component("FaseScadenzaServizioOutSourcing")
public class FaseScadenzaServizioOutSourcing {
	
	private Logger logger = LoggerFactory.getLogger(FaseScadenzaServizioOutSourcing.class);
	
	
	public FaseScadenzaServizioOutSourcing() {}

}
