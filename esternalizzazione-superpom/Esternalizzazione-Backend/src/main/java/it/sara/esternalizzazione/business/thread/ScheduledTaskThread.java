/**
 * 
 */
package it.sara.esternalizzazione.business.thread;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Remind;
import it.sara.esternalizzazione.db.dao.RemindDao;
import it.sara.esternalizzazione.db.dao.TipoUtenteDao;

/**
 * @author f.vescovi
 *
 */
public class ScheduledTaskThread extends Thread {


	protected RemindDao remindDao; 
	
	protected List<Esternalizzazione> listaEsternalizzazione;
	
	protected TipoUtenteDao tipoUtenteDao;
	
	protected String oggettoRemind;

	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(ScheduledTaskThread.class);


	public ScheduledTaskThread(List<Esternalizzazione> listaEsternalizzazione,RemindDao remindDao,TipoUtenteDao tipoUtenteDao,String oggettoRemind) {
		this.listaEsternalizzazione=listaEsternalizzazione;
		this.remindDao=remindDao;
		this.tipoUtenteDao=tipoUtenteDao;
		this.oggettoRemind=oggettoRemind;
	}


	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		
	
		for (Esternalizzazione esternalizzazione : listaEsternalizzazione) {

			logger.debug("ID: esternalizzazione: "+esternalizzazione.getIdEsternalizzazione()+" Responsabile: "+esternalizzazione.getResponsabileSingoloControllo());
			createRemind(esternalizzazione);
		}

		super.run();
	}


	protected void createRemind(Esternalizzazione esternalizzazione) throws RuntimeException {
		
		List<Remind> list=  remindDao.findByIdEsternalizzazione(esternalizzazione.getIdEsternalizzazione());
		Remind remind =null;
		if(!list.isEmpty()) {
			remind=list.get(0);
			logger.debug("Remind presente nel Database");
		}
		if(remind==null) {
			remind=new Remind();
			logger.debug("Remind non presente nel Database");
        remind.setTipoRemind("MAIL");
        remind.setOggetto(this.oggettoRemind);
		remind.setEsternalizzazione(esternalizzazione);
		remind.setTipoUtente(tipoUtenteDao.findByIdTipoUtente(1));
		remind.setDataInizio(null);
		remind.setDataFine(null);
		Remind newRemind=remindDao.save(remind);
		logger.debug("Nuovo Remind creato: "+newRemind.getIdRemind());
		}

		
	}


}
