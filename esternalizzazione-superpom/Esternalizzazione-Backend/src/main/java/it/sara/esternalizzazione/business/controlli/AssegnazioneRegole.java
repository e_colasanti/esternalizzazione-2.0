/**
 * 
 */
package it.sara.esternalizzazione.business.controlli;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.business.GestioneMail;
import it.sara.esternalizzazione.business.GestioneTipoTask;
import it.sara.esternalizzazione.business.GestioneTipoUtente;
import it.sara.esternalizzazione.business.GestioneUtente;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.bean.TipoTask;
import it.sara.esternalizzazione.db.bean.TipoUtente;
import it.sara.esternalizzazione.db.dao.TaskDao;
import it.sara.esternalizzazione.utility.WestCostants;
import it.sara.esternalizzazione.web.bean.UtenteWeb;

/**
 * @author f.vescovi
 *
 */
@Component("AssegnazioneRegoled")
public class AssegnazioneRegole implements IRegole{
	
	
	private Logger logger = LoggerFactory.getLogger(AssegnazioneRegole.class);
	
	protected List<Task> taskList=new ArrayList<>();

	protected List<TipoTask> tipotaskList=null;
	
	@Autowired
	TaskDao taskDao;
	
	@Autowired
	GestioneTipoTask gestioneTipoTask;
	
	@Autowired
	GestioneTipoUtente gestioneTipoUtente;
	
	@Autowired
	GestioneMail gestioneMail;
	
	@Autowired
	GestioneUtente gestioneUtente;


	@Override
	public boolean checkMandatoryMinimumFields(Esternalizzazione esternalizzazione) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Task> taskToActivate(Esternalizzazione esternalizzazione) {	
		
		Task task=null;
		task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.RESPONSABILE_CONTROLLO_ESTERNALIZZAZIONE);	

		if(task==null)
		{
			task=new Task();
			task.setEsternalizzazione(esternalizzazione);
			TipoTask  tipoTask=gestioneTipoTask.getTipoTaskByTipoTask(WestCostants.RRC);
			task.setTipoTask(tipoTask);
			task.setTipoCampoAttivatoEst(WestCostants.RESPONSABILE_CONTROLLO_ESTERNALIZZAZIONE);
			task.setProgLavTask(new Random().nextInt());
			task.setDataCreazione(new Date());       
			task.setDataFine(null);
			task.setStato("A");
			TipoUtente tipoUtente=gestioneTipoUtente.getByDescTipoUtente(WestCostants.RC);
			UtenteWeb utente=gestioneUtente.popolaUtente(esternalizzazione.getResponsabileControlloEsternalizzazione());			
			task.setTipoUtente(tipoUtente);
			task.setUtenteOwner(esternalizzazione.getIdRC());
			task.setUtenteResponsabile(esternalizzazione.getResponsabileControlloEsternalizzazione());
			task.setUtenteDelegato(esternalizzazione.getResponsabileControlloEsternalizzazione());
			task.setFaseEsternalizzazione(WestCostants.FASEINSERIMENTO);
			task.setCodiceDirezione(tipoUtente.getCodiceDirezione());
			taskDao.save(task);
			gestioneMail.setDestinatario(utente.getUsername());
			gestioneMail.inviaMail(task);				
			this.taskList.add(task);			
			logger.debug("Attivato Task su Responsabile Controllo esternalizzazione per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());			
		}
		
		
		
		
		return null;
	}

	@Override
	public boolean checkFaseCompletata(Esternalizzazione esternalizzazione) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	public void richiestaAutorizzazioneResponsabile(Esternalizzazione esternalizzazione) {

		if(checkFaseCompletata(esternalizzazione))
			if(esternalizzazione.getResponsabileSingoloControllo()!=null) {

				Task task=new Task();
				task.setEsternalizzazione(esternalizzazione);
				TipoTask  tipoTask= new TipoTask();
				tipoTask.setTipoTask(WestCostants.RAA);
				TipoUtente tipoUtente=new TipoUtente();
				tipoUtente.setAcTipoUtente(WestCostants.RE);
				tipoUtente.setIndirizzoEmail("#########"); 
				tipoTask.setTipoUtente(tipoUtente);
				task.setTipoTask(tipoTask);			
				this.taskList.add(task);		

			} 

	}
	
	
	public void updateTask(Esternalizzazione esternalizzazione) {

		Task task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), esternalizzazione.getTipoEsternalizzazione().getDescrizioneTipoEsternalizzazione());
		if(task!=null)
		{
           String responsabile=esternalizzazione.getResponsabileControlloEsternalizzazione();
			if(responsabile.contains("@"))
				responsabile.substring(0,responsabile.lastIndexOf("@") );
			
			task.setUtenteDelegato(responsabile);
			taskDao.save(task);
		}



	}
	
	

}
