package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import it.sara.esternalizzazione.db.bean.VerificaConflittiInteressi;

public class VerificheConflittiInteressiWeb extends TipoListaWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2689264740627710608L;
	
	public VerificheConflittiInteressiWeb(VerificaConflittiInteressi verificaConflittiInteressi) {
		this.setCodice(verificaConflittiInteressi.getIdVerificaConflittiInteressi()+"");
		this.setDescrizione(verificaConflittiInteressi.getDescrizioneVerificaConflittiInteressi());
	}
}
