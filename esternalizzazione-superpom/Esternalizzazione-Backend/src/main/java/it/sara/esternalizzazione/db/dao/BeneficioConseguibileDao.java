package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.BeneficioConseguibile;

public interface BeneficioConseguibileDao extends CrudRepository<BeneficioConseguibile,Integer>{
	
	public BeneficioConseguibile findByIdBeneficioConseguibile(Integer idBeneficioConseguibile);
	public List<BeneficioConseguibile> findAll();
}
