package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.AzioneCorrettiva;

public interface AzioneCorrettivaDao extends CrudRepository<AzioneCorrettiva,Integer>{
	
	public AzioneCorrettiva findByIdAzioneCorrettiva(Integer idAzioneCorrettiva);
	public List<AzioneCorrettiva> findAll();
}
