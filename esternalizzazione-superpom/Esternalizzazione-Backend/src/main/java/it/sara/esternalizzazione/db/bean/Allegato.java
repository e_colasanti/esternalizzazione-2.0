package it.sara.esternalizzazione.db.bean;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="WEST_ALLEGATO")
public class Allegato {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_ALLEGATO")
    @SequenceGenerator(name="SEQ_ALLEGATO", sequenceName="SEQ_ALLEGATO", allocationSize=1)
	@Column(name="ID_ALLEGATO")
	Integer idAllegato;

	@Column(name="NOME_ALLEGATO")
	String nomeAllegato;
	
	@Column(name="MIME_TYPES")
	String mimeType;
	
	@Column(name="SIZE_ALLEGATO")
	String sizeAllegato;
	
	@Column(name="TIPO_ALLEGATO")
	String tipoAllegato;
	
	@Column(name="CONTENUTO")
	byte[] contenuto;

	@Column(name="DATA_INSERIMENTO")
	Date dataInserimento;

	@Column(name="UTENTE_INSERIMENTO")
	String utenteInserimento;
	
	@Column(name="TIPO_AMBITO")
	String tipoAmbito;

	
	@Column(name="AMBITO")
	String ambito;
	

	@Column(name="CONTESTO")
	String contesto;

	
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ESTERNALIZZAZIONE")
	Esternalizzazione esternalizzazione;


	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TASK")
	Task task;


	
	public Integer getIdAllegato() {
		return idAllegato;
	}

	public void setIdAllegato(Integer idAllegato) {
		this.idAllegato = idAllegato;
	}

	public String getNomeAllegato() {
		return nomeAllegato;
	}

	public void setNomeAllegato(String nomeAllegato) {
		this.nomeAllegato = nomeAllegato;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getSizeAllegato() {
		return sizeAllegato;
	}

	public void setSizeAllegato(String sizeAllegato) {
		this.sizeAllegato = sizeAllegato;
	}

	public String getTipoAllegato() {
		return tipoAllegato;
	}

	public void setTipoAllegato(String tipoAllegato) {
		this.tipoAllegato = tipoAllegato;
	}

	public byte[] getContenuto() {
		return contenuto;
	}

	public void setContenuto(byte[] contenuto) {
		this.contenuto = contenuto;
	}

	public Date getDataInserimento() {
		return dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public String getUtenteInserimento() {
		return utenteInserimento;
	}

	public void setUtenteInserimento(String utenteInserimento) {
		this.utenteInserimento = utenteInserimento;
	}

		
	public String getTipoAmbito() {
		return tipoAmbito;
	}

	public void setTipoAmbito(String tipoAmbito) {
		this.tipoAmbito = tipoAmbito;
	}
	
	

	/**
	 * @return the ambito
	 */
	public String getAmbito() {
		return ambito;
	}

	/**
	 * @param ambito the ambito to set
	 */
	public void setAmbito(String ambito) {
		this.ambito = ambito;
	}

	/**
	 * @return the contesto
	 */
	public String getContesto() {
		return contesto;
	}

	/**
	 * @param contesto the contesto to set
	 */
	public void setContesto(String contesto) {
		this.contesto = contesto;
	}

	/**
	 * @return the esternalizzazione
	 */
	public Esternalizzazione getEsternalizzazione() {
		return esternalizzazione;
	}


	/**
	 * @param esternalizzazione the esternalizzazione to set
	 */
	public void setEsternalizzazione(Esternalizzazione esternalizzazione) {
		this.esternalizzazione = esternalizzazione;
	}
	

	/**
	 * @return the task
	 */
	public Task getTask() {
		return task;
	}

	/**
	 * @param task the task to set
	 */
	public void setTask(Task task) {
		this.task = task;
	}

	@Override
	public String toString() {
		return "Allegato [idAllegato=" + idAllegato + ", nomeAllegato=" + nomeAllegato
				+ ", mimeType=" + mimeType + ", sizeAllegato=" + sizeAllegato  + ", tipoAllegato=" + tipoAllegato + 
				", dataInserimento=" + dataInserimento + ", utenteInserimento="
				+ utenteInserimento + "]";
	}
	
	

}
