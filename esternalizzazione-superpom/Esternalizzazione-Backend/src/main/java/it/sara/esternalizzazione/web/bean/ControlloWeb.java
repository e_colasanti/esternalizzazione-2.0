package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

public class ControlloWeb  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4104341023068046783L;

	
	String idControllo;
	
	String tipoControllo;
	
	String descrizioneControllo;
	
	String dataInizioControllo;
	
	String dataFineControllo;
	
	String periodicita;
	
	String vincoloPagamentoFattura;

	String responsabileSingoloControllo;
	
	String esitoControllo;
	
	String descrizioneEsitoControllo;
	
	String azioneCorrettiva;
	
	String notaAzioneCorrettiva;
	
	String idEsternalizzazione;

	
	
	
	public ControlloWeb() {
       super();
	}




	public ControlloWeb(String idControllo, String tipoControllo, String descrizioneControllo,
			String dataInizioControllo, String dataFineControllo, String periodicita, String vincoloPagamentoFattura,
			String responsabileSingoloControllo, String esitoControllo, String descrizioneEsitoControllo,
			String azioneCorrettiva, String notaAzioneCorrettiva, String idEsternalizzazione) {
		super();
		this.idControllo = idControllo;
		this.tipoControllo = tipoControllo;
		this.descrizioneControllo = descrizioneControllo;
		this.dataInizioControllo = dataInizioControllo;
		this.dataFineControllo = dataFineControllo;
		this.periodicita = periodicita;
		this.vincoloPagamentoFattura = vincoloPagamentoFattura;
		this.responsabileSingoloControllo = responsabileSingoloControllo;
		this.esitoControllo = esitoControllo;
		this.descrizioneEsitoControllo = descrizioneEsitoControllo;
		this.azioneCorrettiva = azioneCorrettiva;
		this.notaAzioneCorrettiva = notaAzioneCorrettiva;
		this.idEsternalizzazione = idEsternalizzazione;
	}




	/**
	 * @return the idControllo
	 */
	public String getIdControllo() {
		return idControllo;
	}




	/**
	 * @param idControllo the idControllo to set
	 */
	public void setIdControllo(String idControllo) {
		this.idControllo = idControllo;
	}




	/**
	 * @return the tipoControllo
	 */
	public String getTipoControllo() {
		return tipoControllo;
	}




	/**
	 * @param tipoControllo the tipoControllo to set
	 */
	public void setTipoControllo(String tipoControllo) {
		this.tipoControllo = tipoControllo;
	}




	/**
	 * @return the descrizioneControllo
	 */
	public String getDescrizioneControllo() {
		return descrizioneControllo;
	}




	/**
	 * @param descrizioneControllo the descrizioneControllo to set
	 */
	public void setDescrizioneControllo(String descrizioneControllo) {
		this.descrizioneControllo = descrizioneControllo;
	}




	/**
	 * @return the dataInizioControllo
	 */
	public String getDataInizioControllo() {
		return dataInizioControllo;
	}




	/**
	 * @param dataInizioControllo the dataInizioControllo to set
	 */
	public void setDataInizioControllo(String dataInizioControllo) {
		this.dataInizioControllo = dataInizioControllo;
	}




	/**
	 * @return the dataFineControllo
	 */
	public String getDataFineControllo() {
		return dataFineControllo;
	}




	/**
	 * @param dataFineControllo the dataFineControllo to set
	 */
	public void setDataFineControllo(String dataFineControllo) {
		this.dataFineControllo = dataFineControllo;
	}




	/**
	 * @return the periodicita
	 */
	public String getPeriodicita() {
		return periodicita;
	}




	/**
	 * @param periodicita the periodicita to set
	 */
	public void setPeriodicita(String periodicita) {
		this.periodicita = periodicita;
	}




	/**
	 * @return the vincoloPagamentoFattura
	 */
	public String getVincoloPagamentoFattura() {
		return vincoloPagamentoFattura;
	}




	/**
	 * @param vincoloPagamentoFattura the vincoloPagamentoFattura to set
	 */
	public void setVincoloPagamentoFattura(String vincoloPagamentoFattura) {
		this.vincoloPagamentoFattura = vincoloPagamentoFattura;
	}




	/**
	 * @return the responsabileSingoloControllo
	 */
	public String getResponsabileSingoloControllo() {
		return responsabileSingoloControllo;
	}




	/**
	 * @param responsabileSingoloControllo the responsabileSingoloControllo to set
	 */
	public void setResponsabileSingoloControllo(String responsabileSingoloControllo) {
		this.responsabileSingoloControllo = responsabileSingoloControllo;
	}




	/**
	 * @return the esitoControllo
	 */
	public String getEsitoControllo() {
		return esitoControllo;
	}




	/**
	 * @param esitoControllo the esitoControllo to set
	 */
	public void setEsitoControllo(String esitoControllo) {
		this.esitoControllo = esitoControllo;
	}




	/**
	 * @return the descrizioneEsitoControllo
	 */
	public String getDescrizioneEsitoControllo() {
		return descrizioneEsitoControllo;
	}




	/**
	 * @param descrizioneEsitoControllo the descrizioneEsitoControllo to set
	 */
	public void setDescrizioneEsitoControllo(String descrizioneEsitoControllo) {
		this.descrizioneEsitoControllo = descrizioneEsitoControllo;
	}




	/**
	 * @return the azioneCorrettiva
	 */
	public String getAzioneCorrettiva() {
		return azioneCorrettiva;
	}




	/**
	 * @param azioneCorrettiva the azioneCorrettiva to set
	 */
	public void setAzioneCorrettiva(String azioneCorrettiva) {
		this.azioneCorrettiva = azioneCorrettiva;
	}




	/**
	 * @return the notaAzioneCorrettiva
	 */
	public String getNotaAzioneCorrettiva() {
		return notaAzioneCorrettiva;
	}




	/**
	 * @param notaAzioneCorrettiva the notaAzioneCorrettiva to set
	 */
	public void setNotaAzioneCorrettiva(String notaAzioneCorrettiva) {
		this.notaAzioneCorrettiva = notaAzioneCorrettiva;
	}




	/**
	 * @return the idEsternalizzazione
	 */
	public String getIdEsternalizzazione() {
		return idEsternalizzazione;
	}




	/**
	 * @param idEsternalizzazione the idEsternalizzazione to set
	 */
	public void setIdEsternalizzazione(String idEsternalizzazione) {
		this.idEsternalizzazione = idEsternalizzazione;
	}
	
	
	
	
	
	
}
