/**
 * 
 */
package it.sara.esternalizzazione.db.dao;

import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.Controllo;

/**
 * @author f.vescovi
 *
 */
public interface ControlloDao extends CrudRepository<Controllo, Integer> {

}
