package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;
import java.util.List;


public class PaginaEsternalizzazioneWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8312626303421898158L;
	List<EsternalizzazioneWeb> listaEsternalizzazioni;
	private Integer totalePagine;
	private int paginaCorrente;
	private Long totaleRecord;
	private Integer recordPerPagina;
	
	
	public List<EsternalizzazioneWeb> getListaEsternalizzazioni() {
		return listaEsternalizzazioni;
	}
	public void setListaEsternalizzazioni(List<EsternalizzazioneWeb> listaEsternalizzazioni) {
		this.listaEsternalizzazioni = listaEsternalizzazioni;
	}
	public Integer getTotalePagine() {
		return totalePagine;
	}
	public void setTotalePagine(Integer totalePagine) {
		this.totalePagine = totalePagine;
	}
	public int getPaginaCorrente() {
		return paginaCorrente;
	}
	public void setPaginaCorrente(int paginaCorrente) {
		this.paginaCorrente = paginaCorrente;
	}
	public Long getTotaleRecord() {
		return totaleRecord;
	}
	public void setTotaleRecord(Long totaleRecord) {
		this.totaleRecord = totaleRecord;
	}
	public Integer getRecordPerPagina() {
		return recordPerPagina;
	}
	public void setRecordPerPagina(Integer recordPerPagina) {
		this.recordPerPagina = recordPerPagina;
	}
	@Override
	public String toString() {
		return "PaginaEsternalizzazioneWeb [listaEsternalizzazioni=" + listaEsternalizzazioni + ", totalePagine=" + totalePagine
				+ ", paginaCorrente=" + paginaCorrente + ", totaleRecord=" + totaleRecord + ", recordPerPagina="
				+ recordPerPagina + "]";
	}
}
