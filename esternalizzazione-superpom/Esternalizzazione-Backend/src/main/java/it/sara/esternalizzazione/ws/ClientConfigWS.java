package it.sara.esternalizzazione.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;

@Configuration
@PropertySource("classpath:config.properties")
public class ClientConfigWS {

	@Autowired
	Environment enviroment;
	
	private final static String PATH_WS_UTENTE_FROM_CONFIG = "pathWSUtente";
	private final static String PATH_WS_FORNITORE_FROM_CONFIG = "pathWSFornitore";
	private final static String PATH_WS_CONO_FROM_CONFIG = "pathWSConoRE";
	private final static String ENVIRONMENT = "environment";
	
	
	
	@Bean
	Jaxb2Marshaller jaxb2Marshaller(){
		
		Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
	    jaxb2Marshaller.setContextPath("it.sara.esternalizzazione.ws.bean");
	    return jaxb2Marshaller;
	}
	
	@Bean(name="webServiceTemplateUtente")
	public WebServiceTemplate webServiceTemplate(){
		
		WebServiceTemplate wsTemp = new WebServiceTemplate();
		String uri = enviroment.getProperty(PATH_WS_UTENTE_FROM_CONFIG);
		wsTemp.setMarshaller(jaxb2Marshaller());
		wsTemp.setUnmarshaller(jaxb2Marshaller());
		wsTemp.setDefaultUri(uri);
		
		return wsTemp;
	}
	
	@Bean(name="webServiceTemplateFornitori")
	public WebServiceTemplate webServiceTemplateFornitori(){
		
		WebServiceTemplate wsTemp = new WebServiceTemplate();
		String uri = enviroment.getProperty(PATH_WS_FORNITORE_FROM_CONFIG);
		wsTemp.setMarshaller(jaxb2Marshaller());
		wsTemp.setUnmarshaller(jaxb2Marshaller());
		wsTemp.setDefaultUri(uri);
		
		return wsTemp;
	}
	

	@Bean(name="webServiceTemplateConoResponsabile")
	public WebServiceTemplate webServiceTemplateConoResponsabile(){
		
		WebServiceTemplate wsTemp = new WebServiceTemplate();
		String uri = enviroment.getProperty(PATH_WS_CONO_FROM_CONFIG);
		wsTemp.setMarshaller(jaxb2Marshaller());
		wsTemp.setUnmarshaller(jaxb2Marshaller());
		wsTemp.setDefaultUri(uri);
		
		return wsTemp;
	}
	
	@Bean(name="environmentType")	
	public String getSaraEnvironment() {
		String env=null;
		env = enviroment.getProperty(ENVIRONMENT);
		return env;
	}
	
	
}
