/**
 * 
 */
package it.sara.esternalizzazione.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.db.bean.TipoUtente;
import it.sara.esternalizzazione.db.dao.TipoUtenteDao;
import it.sara.esternalizzazione.web.bean.TipoUtenteWeb;

/**
 * @author f.vescovi
 *
 */
@Component("GestioneTipoUtente")
public class GestioneTipoUtente {
	
	
	@Autowired
	TipoUtenteDao tipoUtenteDao;
	
	
	public List<TipoUtenteWeb> getAll(){
		
		List<TipoUtenteWeb> list=new ArrayList<>();
		
		List<TipoUtente> l=tipoUtenteDao.findAll();
		
		
		for (TipoUtente tipoUtente : l) {
		     
			TipoUtenteWeb t=new TipoUtenteWeb(tipoUtente);
			list.add(t);	
		}	
		return list;
	}
	
	
	public TipoUtente getByAcTipoUtente(String acTipoUtente) {
		
		TipoUtente tu=tipoUtenteDao.findByIdAcTipoUtente(acTipoUtente);
		
		return tu;
	}
	
	
	public TipoUtente getByDescTipoUtente(String descTipoUtente) {
		
		TipoUtente tu=tipoUtenteDao.findByDescTipoUtente(descTipoUtente);
		
		return tu;
	}	
	
	

}
