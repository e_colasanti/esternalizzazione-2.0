package it.sara.esternalizzazione.business;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.db.bean.Task;

@Component("GestioneMail")
public class GestioneMail {

	private Logger logger = LoggerFactory.getLogger(GestioneMail.class);

	@Value("${serverSMTP}")
	String mailServer;

	@Value("${mittenteMail}")
	String mittenteMail;
	
	
	@Value("${destinatario}")
	String destinatario;

	@Value("${emailIsEnabled}")
	String emailIsEnabled;
	
	
//String[] to, String subject, String body	
	public boolean inviaMail(Task task) {

		boolean checked=false;
		
		String subject="Test Subject";
		
		
		if(emailIsEnabled.equals("false"))
			return false;
		
		if (task.getTipoUtente().getIndirizzoEmail()==null ||task.getTipoUtente().getIndirizzoEmail().isEmpty())return false;

		// Get system properties
		Properties properties = new Properties();

//		properties.put("mail.smtp.auth", "true");
//		properties.put("mail.smtp.starttls.enable", "true");
//		properties.put("mail.smtp.host", "smtp.gmail.com");
//		properties.put("mail.smtp.port", "587");


	      // Setup mail server
	      properties.setProperty("mail.smtp.host", mailServer);

	      // Get the default Session object.
	      Session session = Session.getDefaultInstance(properties);
			

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(mittenteMail));

			// Set To: header field of the header.
			//for (int i = 0; i < to.length;i++)
			
			
			if(!("").equals(destinatario))
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
			else
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(task.getTipoUtente().getIndirizzoEmail()));

			// Set Subject: header field
			message.setSubject(subject);
			
			
			message.setText(task.getTipoTask().getOggetto());

			Object body=new String("Esternalizzazione n°: "+task.getEsternalizzazione().getIdEsternalizzazione()+" Responsabile: "+task.getEsternalizzazione().getCompilataDa()+" Oggetto: "+task.getTipoTask().getOggetto());
			// Now set the actual message
			message.setContent(body, "text/html");

			// Send message
			Transport.send(message);
			checked=true;
			logger.info("Messaggio inviato correttamente....");
		} catch (MessagingException mex) {	
			checked=false;
			mex.printStackTrace();
		}
		
		return checked;
	}


	/**
	 * @return the destinatario
	 */
	public String getDestinatario() {
		return destinatario;
	}


	/**
	 * @param destinatario the destinatario to set
	 */
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	
	
	
	
	
	
}
