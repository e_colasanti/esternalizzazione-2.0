/**
 * 
 */
package it.sara.esternalizzazione.db.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author f.vescovi
 *
 */


@Entity
@Table(name="WEST_TIPO_UTENTE")
public class TipoUtente implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3372789878392527440L;


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GENERA_ID")
	@SequenceGenerator(name="SEQ_GENERA_ID", sequenceName="SEQ_GENERA_ID", allocationSize=1)
	@Column(name="ID_TIPO_UTENTE")
	Integer idTipoUtente;


	@Column(name="AC_TIPO_UTENTE")
	String acTipoUtente;
	
	
	@Column(name="DESC_TIPO_UTENTE")	
	String descTipoUtente;
	
	
	@Column(name="INDIRIZZO_EMAIL")
	String indirizzoEmail;
	
	
	@Column(name="CODICE_DIREZIONE")
	String codiceDirezione;
	


	/**
	 * @return the idTipoUtente
	 */
	public Integer getIdTipoUtente() {
		return idTipoUtente;
	}


	/**
	 * @param idTipoUtente the idTipoUtente to set
	 */
	public void setIdTipoUtente(Integer idTipoUtente) {
		this.idTipoUtente = idTipoUtente;
	}


	/**
	 * @return the acTipoUtente
	 */
	public String getAcTipoUtente() {
		return acTipoUtente;
	}


	/**
	 * @param acTipoUtente the acTipoUtente to set
	 */
	public void setAcTipoUtente(String acTipoUtente) {
		this.acTipoUtente = acTipoUtente;
	}


	/**
	 * @return the descTipoUtente
	 */
	public String getDescTipoUtente() {
		return descTipoUtente;
	}


	/**
	 * @param descTipoUtente the descTipoUtente to set
	 */
	public void setDescTipoUtente(String descTipoUtente) {
		this.descTipoUtente = descTipoUtente;
	}


	/**
	 * @return the indirizzoEmail
	 */
	public String getIndirizzoEmail() {
		return indirizzoEmail;
	}


	/**
	 * @param indirizzoEmail the indirizzoEmail to set
	 */
	public void setIndirizzoEmail(String indirizzoEmail) {
		this.indirizzoEmail = indirizzoEmail;
	}


	/**
	 * @return the codiceDirezione
	 */
	public String getCodiceDirezione() {
		return codiceDirezione;
	}


	/**
	 * @param codiceDirezione the codiceDirezione to set
	 */
	public void setCodiceDirezione(String codiceDirezione) {
		this.codiceDirezione = codiceDirezione;
	}
	
	

	

}
