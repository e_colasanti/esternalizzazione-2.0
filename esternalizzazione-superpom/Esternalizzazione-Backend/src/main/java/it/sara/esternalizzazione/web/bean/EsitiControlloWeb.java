package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;
import it.sara.esternalizzazione.db.bean.EsitoControllo;

public class EsitiControlloWeb extends TipoListaWeb implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6688176239512768255L;

	public EsitiControlloWeb(EsitoControllo esitoControllo) {
		this.setCodice(esitoControllo.getIdEsitoControllo()+"");
		this.setDescrizione(esitoControllo.getDescrizioneEsitoControllo());
	}
}
