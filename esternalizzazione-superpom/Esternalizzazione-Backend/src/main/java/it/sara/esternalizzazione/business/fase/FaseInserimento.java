/**
 * 
 */
package it.sara.esternalizzazione.business.fase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.business.controlli.InserimentoRegole;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.bean.TipoEsternalizzazione;
import it.sara.esternalizzazione.db.dao.EsternalizzazioneDao;
import it.sara.esternalizzazione.utility.WestCostants;
import it.sara.esternalizzazione.web.bean.EsternalizzazioneWeb;

/**
 * @author f.vescovi
 *
 */
@Component("FaseInserimento")
public class FaseInserimento{
	
	private Logger logger = LoggerFactory.getLogger(FaseInserimento.class);

	
	@Autowired
	EsternalizzazioneDao esternalizzazioneDao;
	
	@Autowired
	InserimentoRegole inserimentoRegole;
	
	
	private boolean isDelegated;	
	
	private Task task;
	
	
	public FaseInserimento() {
	}



	/**
	 * @return the isDelegated
	 */
	public boolean isDelegated(Esternalizzazione esternalizzazione) {
		
		if(esternalizzazione.getDataModificaTipologiaEsternalizzazione()!=null) {
		       task=new Task();
		       //task.setTipoTask(Utility.getTipoTask(tipoTaskList, "AR"));
		}
			
		return isDelegated;
	}


	
	/**
	 * @return the tipoEsternalizzazione
	 */
	public void checkTipoEsternalizzazione(Esternalizzazione esternalizzazione) {

		task=new Task();

		if( esternalizzazione.getTipoEsternalizzazione().getIdTipoEsternalizzazione()==1 || 
			esternalizzazione.getTipoEsternalizzazione().getIdTipoEsternalizzazione()==2) {
			task.setDescrizione("Invio delle comunicazioni obbligatorie");
		       //task.setTipoTask(Utility.getTipoTask(tipoTaskList, "FL"));

		}
		else if( esternalizzazione.getTipoEsternalizzazione().getIdTipoEsternalizzazione()==4) {
			task.setDescrizione("In attesa di chiusura attività ");
		       //task.setTipoTask(Utility.getTipoTask(tipoTaskList, "FL"));

		}
		
		
	}


	
	public Esternalizzazione inserimentoEsternalizzazione(EsternalizzazioneWeb esternalizzazioneWeb,TipoEsternalizzazione tipoEsternalizzazione) {
		
		Esternalizzazione esternalizzazione = new Esternalizzazione();
	
		esternalizzazioneWeb.toEsternalizzazioneDb(esternalizzazione,tipoEsternalizzazione, "nuova");

 		esternalizzazione.setStato(2);
		esternalizzazioneWeb.setStato("2");		
 		
        this.esternalizzazioneDao.save(esternalizzazione);
        
        inserimentoRegole.taskToActivate(esternalizzazione);

		return esternalizzazione;
	}
	
	

}
