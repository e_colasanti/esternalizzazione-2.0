/**
 * 
 */
package it.sara.esternalizzazione.db.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author f.vescovi
 *
 */
@Entity
@Table(name="WEST_TIPO_FASE")
public class TipoFase implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2958381392433868085L;


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GENERA_ID")
	@SequenceGenerator(name="SEQ_GENERA_ID", sequenceName="SEQ_GENERA_ID", allocationSize=1)
	@Column(name="ID_TIPO_FASE")
	Integer idTipoFase;
	
	
	@Column(name="DESC_TIPO_FASE")
	String descTipoFase;


	/**
	 * @return the idTipoFase
	 */
	public Integer getIdTipoFase() {
		return idTipoFase;
	}


	/**
	 * @param idTipoFase the idTipoFase to set
	 */
	public void setIdTipoFase(Integer idTipoFase) {
		this.idTipoFase = idTipoFase;
	}


	/**
	 * @return the descTipoFase
	 */
	public String getDescTipoFase() {
		return descTipoFase;
	}


	/**
	 * @param descTipoFase the descTipoFase to set
	 */
	public void setDescTipoFase(String descTipoFase) {
		this.descTipoFase = descTipoFase;
	}
	
	
	
	
	
	
	
	
	

}
