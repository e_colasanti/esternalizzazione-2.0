package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.PianoReinternalizzazione;

public interface PianoReinternalizzazioneDao extends CrudRepository<PianoReinternalizzazione,Integer>{

	
	public PianoReinternalizzazione findByIdPianoReinternalizzazione(Integer idPianoReinternalizzazione);
	public List<PianoReinternalizzazione> findAll();
}
