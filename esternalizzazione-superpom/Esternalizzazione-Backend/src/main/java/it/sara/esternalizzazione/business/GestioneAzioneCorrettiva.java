/**
 * 
 */
package it.sara.esternalizzazione.business;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.business.task.TaskAzioneCorrettiva;
import it.sara.esternalizzazione.db.bean.AzioneCorrettiva;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.bean.TipoTask;
import it.sara.esternalizzazione.db.dao.TaskDao;
import it.sara.esternalizzazione.db.dao.TipoTaskDao;
import it.sara.esternalizzazione.utility.WestCostants;

/**
 * @author f.vescovi
 *
 */
@Component("GestioneAzioneCorrettiva")
public class GestioneAzioneCorrettiva {


	private Logger logger = LoggerFactory.getLogger(GestioneAzioneCorrettiva.class);


	@Autowired
	TaskDao taskDao;

	@Autowired
	TipoTaskDao tipoTaskDao;
	
	
	
	@Autowired
	TaskAzioneCorrettiva taskAzioneCorrettiva;


	public GestioneAzioneCorrettiva() {}



	public void attivazioneTaskCorrettive(AzioneCorrettiva azioneCorrettiva,Esternalizzazione esternalizzazione) {

		if(azioneCorrettiva!=null)
		{
			if(azioneCorrettiva.getIdAzioneCorrettiva()==2)//Variazione Contratto 
			{
				TipoTask tipoTask=tipoTaskDao.findByTipoTask(WestCostants.RFAVC); 
				if(tipoTask!=null)
				{
                   List<Task> taskList= taskDao.findTaskByEstAndTipoTask(esternalizzazione.getIdEsternalizzazione(),tipoTask.getIdTipoTask());
                   if(taskList.isEmpty())
                   {                     
                
                	   taskAzioneCorrettiva.createTask(esternalizzazione,WestCostants.RFAVC,WestCostants.FA);
                      
                   }
				}
			}
			else if(azioneCorrettiva.getIdAzioneCorrettiva()==3)//Cessazione Contratto 
			{

         	   taskAzioneCorrettiva.createTask(esternalizzazione,WestCostants.RFADCC,WestCostants.FA);

         	   taskAzioneCorrettiva.createTask(esternalizzazione,WestCostants.RFLCIVASS,WestCostants.FA);
				
			}
			else//Nessuna Variazione 
			{
				TipoTask tipoTask=tipoTaskDao.findByTipoTask(WestCostants.RRAC); 
				if(tipoTask!=null)
				{
                   Task task= taskDao.findTaskByTipoTask(tipoTask.getIdTipoTask());
                   if(task!=null)
                   {
                      if(!task.getStato().equals(WestCostants.TASK_COMPLETATO))
                      {
                    	  task.setStato(WestCostants.TASK_COMPLETATO);
                          taskDao.save(task);
                      }  
                   }
				}

			}


		}





	}


}
