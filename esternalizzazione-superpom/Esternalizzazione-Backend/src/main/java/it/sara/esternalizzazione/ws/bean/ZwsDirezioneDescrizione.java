//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.11 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.08.07 alle 11:52:51 AM CEST 
//


package it.sara.esternalizzazione.ws.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ZwsDirezioneDescrizione complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ZwsDirezioneDescrizione"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Zzenteacq" type="{urn:sap-com:document:sap:rfc:functions}char3"/&gt;
 *         &lt;element name="Text" type="{urn:sap-com:document:sap:rfc:functions}char50"/&gt;
 *         &lt;element name="UserWest" type="{urn:sap-com:document:sap:soap:functions:mc-style}char2"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZwsDirezioneDescrizione", propOrder = {
    "zzenteacq",
    "text",
    "userWest"
})
public class ZwsDirezioneDescrizione {

    @XmlElement(name = "Zzenteacq", required = true)
    protected String zzenteacq;
    @XmlElement(name = "Text", required = true)
    protected String text;
    @XmlElement(name = "UserWest", required = true)
    protected String userWest;

    /**
     * Recupera il valore della proprietà zzenteacq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZzenteacq() {
        return zzenteacq;
    }

    /**
     * Imposta il valore della proprietà zzenteacq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZzenteacq(String value) {
        this.zzenteacq = value;
    }

    /**
     * Recupera il valore della proprietà text.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getText() {
        return text;
    }

    /**
     * Imposta il valore della proprietà text.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setText(String value) {
        this.text = value;
    }

    /**
     * Recupera il valore della proprietà userWest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserWest() {
        return userWest;
    }

    /**
     * Imposta il valore della proprietà userWest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserWest(String value) {
        this.userWest = value;
    }

}
