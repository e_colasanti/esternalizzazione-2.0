/**
 * 
 */
package it.sara.esternalizzazione.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.sara.esternalizzazione.business.GestioneTask;
import it.sara.esternalizzazione.business.GestioneTipoTask;
import it.sara.esternalizzazione.business.GestioneTipoUtente;
import it.sara.esternalizzazione.web.bean.PaginaTaskWeb;
import it.sara.esternalizzazione.web.bean.TaskWeb;
import it.sara.esternalizzazione.web.bean.TipoTaskWeb;
import it.sara.esternalizzazione.web.bean.TipoUtenteWeb;
import it.sara.esternalizzazione.web.bean.UtenteWeb;

/**
 * @author f.vescovi
 *
 */
@RestController
@RequestMapping("/rest/task")
@CrossOrigin(origins="*")
public class TaskController extends BaseController {
	
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(TaskController.class);
	
	@Autowired
	GestioneTipoUtente gestioneTipoUtente;
	
	@Autowired
	GestioneTask gestioneTask;
	
	
	@Autowired
	GestioneTipoTask gestioneTipoTask;
	
	
	
	@RequestMapping(value={"/lista", "/lista/{stato}", "/lista/{stato}/{pageNumber}", "/lista/{stato}/{pageNumber}/{righePerPagina}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public PaginaTaskWeb listaAttivita(@PathVariable(value="stato", required=false) Integer stato, 
			@PathVariable(value="pageNumber", required=false) Integer pageNumber,
			@PathVariable(value="righePerPagina", required=false) Integer righePerPagina,
			HttpServletResponse response,
			HttpServletRequest request){
		
		logger.debug(request.getProtocol()+request.getServerName()+request.getLocalPort()+request.getContextPath());
		int numeroPagina = ((pageNumber==null)?0:pageNumber);
		int righePagina = ((righePerPagina==null)?5:righePerPagina);
		
		List<Integer> elencoStati = cercaStati(stato, response);
		UtenteWeb utente = getUtente();
		
		return gestioneTask.getListaAttivita(utente,elencoStati,numeroPagina,righePagina);
	
	}
	
	
	@RequestMapping(value={"/get/{id}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public TaskWeb getTask(@PathVariable(name="id")  String id){
		logger.debug("get esternalizzazione: " + id);
		return gestioneTask.getTaskById(Integer.valueOf(id));
	}
	
	
	private List<Integer> cercaStati(Integer stato, HttpServletResponse response) {
		Collection<? extends GrantedAuthority> ga= SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		List<Integer> elencoStati = gestioneUtente.elencoStati(ga);
		if (stato != null && stato != -1) {
			boolean statoTrovato = false;
			for (Integer s:elencoStati)if(stato.equals(s))statoTrovato=true;
			if (!statoTrovato) {
				try {
					response.sendError(403,"Utente non autorizzato");
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}else {
				//se lo stato è diverso da null il filtro è lo stato
				elencoStati = new LinkedList<Integer>();
				elencoStati.add(stato);
			}
		}
		return elencoStati;
	}
	
	@RequestMapping(value={"/cerca/{pageNumber}/{righePerPagina}","/cerca/{stato}/{pageNumber}/{righePerPagina}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public PaginaTaskWeb cercaTask(@PathVariable(value="stato", required=false) Integer stato, 
											@PathVariable(value="pageNumber", required=false) Integer pageNumber, 
											@PathVariable(value="righePerPagina", required=false) Integer righePerPagina, 
								            @RequestParam(value="query", required=false) String query,
											HttpServletResponse response){

		int numeroPagina = ((pageNumber==null)?0:pageNumber);
		int righePagina = ((righePerPagina==null)?5:righePerPagina);

		List<Integer> elencoStati = cercaStati(stato, response);
		return gestioneTask.cercaTask(elencoStati,query,((pageNumber==null)?0:pageNumber),((righePerPagina==null)?5:righePerPagina));
	}
	
	@RequestMapping(value={"/salva"},  method=RequestMethod.POST, consumes="application/json")
	public TaskWeb salvaTask(@RequestBody  TaskWeb taskWeb){
		logger.debug("salva task: " + taskWeb);
		gestioneTask.salvaTask(taskWeb);
		return getTask(Integer.valueOf(taskWeb.getIdTask()));
	}
	
	
	@RequestMapping(value={"/listaTipiTask"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<TipoTaskWeb> getTipiTask(){
		logger.debug("get Tipi Task...");
		return gestioneTipoTask.getAll();
	}
	
	
	@RequestMapping(value={"/listaTipiUtente"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<TipoUtenteWeb> getTipiUtente(){
		logger.debug("get Tipi Utente...");
		return gestioneTipoUtente.getAll();
	}
	
	
	@RequestMapping(value={"/getTask/{idTask}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public TaskWeb getTask(@PathVariable(value="idTask", required=false) Integer idTask){
		logger.debug("getTask...");
		return gestioneTask.getTaskById(idTask);
	}

}
