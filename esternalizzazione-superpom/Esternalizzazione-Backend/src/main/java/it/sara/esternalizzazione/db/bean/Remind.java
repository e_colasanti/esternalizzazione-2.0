package it.sara.esternalizzazione.db.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="WEST_REMIND")
public class Remind {


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_REMIND")
	@SequenceGenerator(name="SEQ_REMIND", sequenceName="SEQ_REMIND", allocationSize=1)
	@Column(name="ID_REMIND")
	Integer idRemind;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_UTENTE")
	TipoUtente tipoUtente;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ESTERNALIZZAZIONE")
	Esternalizzazione esternalizzazione;


	@Column(name="TIPO_REMIND")
	String tipoRemind;
	
	
	@Column(name="OGGETTO")	
	String oggetto;


	@Column(name="PERIODICITA")	
	Integer periodicita;


	@Column(name="DATA_INIZIO")	
	Date dataInizio;
	
	@Column(name="DATA_FINE")	
	Date dataFine;
	
	@Column(name="stato")	
	String stato;
	
	

	/**
	 * @return the idRemind
	 */
	public Integer getIdRemind() {
		return idRemind;
	}

	/**
	 * @param idRemind the idRemind to set
	 */
	public void setIdRemind(Integer idRemind) {
		this.idRemind = idRemind;
	}


	/**
	 * @return the tipoRemind
	 */
	public String getTipoRemind() {
		return tipoRemind;
	}

	/**
	 * @param tipoRemind the tipoRemind to set
	 */
	public void setTipoRemind(String tipoRemind) {
		this.tipoRemind = tipoRemind;
	}

	/**
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * @param oggetto the oggetto to set
	 */
	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * @return the tipoUtente
	 */
	public TipoUtente getTipoUtente() {
		return tipoUtente;
	}

	/**
	 * @param tipoUtente the tipoUtente to set
	 */
	public void setTipoUtente(TipoUtente tipoUtente) {
		this.tipoUtente = tipoUtente;
	}
	

	/**
	 * @return the esternalizzazione
	 */
	public Esternalizzazione getEsternalizzazione() {
		return esternalizzazione;
	}

	/**
	 * @param esternalizzazione the esternalizzazione to set
	 */
	public void setEsternalizzazione(Esternalizzazione esternalizzazione) {
		this.esternalizzazione = esternalizzazione;
	}

	/**
	 * @return the periodicita
	 */
	public Integer getPeriodicita() {
		return periodicita;
	}

	/**
	 * @param periodicita the periodicita to set
	 */
	public void setPeriodicita(Integer periodicita) {
		this.periodicita = periodicita;
	}

	/**
	 * @return the dataInizio
	 */
	public Date getDataInizio() {
		return dataInizio;
	}

	/**
	 * @param dataInizio the dataInizio to set
	 */
	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	/**
	 * @return the dataFine
	 */
	public Date getDataFine() {
		return dataFine;
	}

	/**
	 * @param dataFine the dataFine to set
	 */
	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}

	/**
	 * @return the stato
	 */
	public String getStato() {
		return stato;
	}

	/**
	 * @param stato the stato to set
	 */
	public void setStato(String stato) {
		this.stato = stato;
	}



}
