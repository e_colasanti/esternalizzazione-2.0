package it.sara.esternalizzazione.business.controlli;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.business.GestioneAllegato;
import it.sara.esternalizzazione.business.GestioneMail;
import it.sara.esternalizzazione.business.GestioneTipoTask;
import it.sara.esternalizzazione.business.GestioneTipoUtente;
import it.sara.esternalizzazione.business.GestioneWSDL;
import it.sara.esternalizzazione.business.task.TaskFornitore;
import it.sara.esternalizzazione.business.task.TaskFunzioneAcquisti;
import it.sara.esternalizzazione.business.task.TaskFunzioneLegale;
import it.sara.esternalizzazione.business.task.TaskResponsabileControllo;
import it.sara.esternalizzazione.business.task.TaskRischiBC;
import it.sara.esternalizzazione.business.task.TaskSEE;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.dao.TaskDao;
import it.sara.esternalizzazione.utility.WestCostants;
import it.sara.esternalizzazione.web.bean.AllegatoWeb;
import it.sara.esternalizzazione.ws.bean.ZwsWestDatiFornitoreResponse;

@Component("ValutazioneRegole")
public class ValutazioneRegole implements IRegole {


	private Logger logger = LoggerFactory.getLogger(ValutazioneRegole.class);
	protected List<Task> taskList=new ArrayList<>();

	@Autowired
	TaskDao taskDao;

	@Autowired
	GestioneMail gestioneMail;


	@Autowired
	GestioneTipoUtente gestioneTipoUtente;
	
	@Autowired
	GestioneTipoTask gestioneTipoTask;

	
	@Autowired
	GestioneWSDL gestione;

	
	@Autowired
	GestioneAllegato gestioneAllegato;
	
	
	@Autowired
	TaskFornitore  taskFornitore;
	
	
	@Autowired
	TaskRischiBC taskRischiBC;
	
	@Autowired
	TaskSEE taskSEE;
	
	@Autowired
	TaskResponsabileControllo taskResponsabileControllo;

	@Autowired
	TaskFunzioneAcquisti taskFunzioneAcquisti;
	
	@Autowired
	TaskFunzioneLegale taskFunzioneLegale;

	public ValutazioneRegole() {
	}


	@Override
	public boolean checkMandatoryMinimumFields(Esternalizzazione esternalizzazione) {
		boolean control=true;

		if(esternalizzazione.getNomeFornitore()==null )
			return false;
		if(esternalizzazione.getRischiositaEsternalizzazione()==null )
			return false;
		if(esternalizzazione.getVerificaConflittiInteressi()==null )
			return false;
		if(esternalizzazione.getRischiBC()==null )
			return false;
		if(esternalizzazione.getSpazioEconomicoEuropeo()==null )
			return false;		
		if(esternalizzazione.getSlaContrattualmenteDefiniti()==null )
			return false;		
		if(esternalizzazione.getResponsabileSingoloControllo()==null )
			return false;	
		if(esternalizzazione.getPianoReinternalizzazione()==null )
			return false;	
		if(esternalizzazione.getRispettoRemunerazione()==null )
			return false;				
		return control;
	}

	@SuppressWarnings("unlikely-arg-type")
	@Override
	public List<Task> taskToActivate(Esternalizzazione esternalizzazione) {

		if(esternalizzazione.getStato()==2)
		{

			if(esternalizzazione.getNomeFornitore()!=null && !("").equals(esternalizzazione.getNomeFornitore()) )
			{
				ZwsWestDatiFornitoreResponse fornitore=null;				
				Task task=null;

				fornitore= gestione.loadDatiFornitori(esternalizzazione.getNomeFornitore());				    
				if(fornitore.getTbWest().getItem().isEmpty()) {
					task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.NOME_FORNITORE);	

					if(task==null)
					{
						task=taskFornitore.createTask(esternalizzazione);
						this.taskList.add(task);			
						logger.debug("Attivato Task su Nome Fornitore per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());
					}
					else
						this.taskList.add(task);			
						
						
				}
			}


			if(esternalizzazione.getRischiBC()!=null && !("").equals(esternalizzazione.getRischiBC()) )
			{
				Task task=null;
				task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.RISCHI_BC);	

				if(task==null)
				{
					task=taskRischiBC.createTask(esternalizzazione); 
					this.taskList.add(task);			
					logger.debug("Attivato Task su Rischi Biologici per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());
				}
				else
					this.taskList.add(task);			

			}


			if(esternalizzazione.getSpazioEconomicoEuropeo()!=null ) 
			{
				if(esternalizzazione.getSpazioEconomicoEuropeo().equals("NO"))
				{
					Task task=null;
					task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.SPAZIO_ECONOMICO_EUROPEO);	

					if(task==null)
					{
                       task= taskSEE.createTask(esternalizzazione);   
						this.taskList.add(task);
						logger.debug("Attivato Task su Spazio Economico Europeo per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());
					}
					else
						this.taskList.add(task);			

				}
			}

			if(esternalizzazione.getResponsabileSingoloControllo()!=null && !("").equals(esternalizzazione.getResponsabileSingoloControllo()) )
			{
				Task task=null;
				task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.RESPONSABILE_CONTROLLO_ESTERNALIZZAZIONE);	

				if(task==null)
				{

					task=taskResponsabileControllo.createTask(esternalizzazione);
					this.taskList.add(task);			
					logger.debug("Attivato Task su Responsabile del Controllo per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());				
				}
				else
					this.taskList.add(task);			

			}


		}

		return taskList;
	}

	@Override
	public boolean checkFaseCompletata(Esternalizzazione esternalizzazione) {

		boolean control=true;


		if(esternalizzazione.getPianoReinternalizzazione()!=null )//bloccante
			if(esternalizzazione.getPianoReinternalizzazione().getDescrizionePianoReinternalizzazione().equals("Presente"))
			{
			 List<AllegatoWeb> listaAllegati=gestioneAllegato.findAllegatiByIdEsternalizzazioneAndTipoAmbito(esternalizzazione.getIdEsternalizzazione(), WestCostants.TIPO_AMBITO_ALLEGATO_PIANO_REINTERNALIZZAZIONE);

			 if(listaAllegati.isEmpty()) {
				 logger.debug("Il Piano di reinternalizzazione non presenta allegati"); 
				 control=false;
			 }
			}
		
		if(esternalizzazione.getRispettoRemunerazione()==null )//bloccante
			return false;

		//		if(!esternalizzazione.getSlaContrattualmenteDefiniti().equals("NO") )//bloccante
		//			return false;
		
		
		List<Task> listaTask=  taskDao.findTaskByEsternalizzazione(esternalizzazione.getIdEsternalizzazione());
		
		if(!listaTask.isEmpty())
		{
			for (Task task : listaTask) {
				if(task.getStato().equals("A") || task.getStato().equals("V"))
					{
					 control=false;
					 break;
					}
			}
			
		}
			
		return control;
	}


	public void updateTask(Esternalizzazione esternalizzazione) {

		Task task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), esternalizzazione.getTipoEsternalizzazione().getDescrizioneTipoEsternalizzazione());
		if(task!=null)
		{
           String responsabile=esternalizzazione.getResponsabileControlloEsternalizzazione();
			if(responsabile.contains("@"))
				responsabile.substring(0,responsabile.lastIndexOf("@") );
			
			task.setUtenteDelegato(responsabile);
			taskDao.save(task);
		}

	}
	
	/**
	 * Metodo per la crezione del Task relativo all'inserimento
	 * delle informazioni per la fase di predisposizione dell'esternalizzazione
	 * da parte della Funzione Acquisti
	 * @param esternalizzazione
	 */
	public void creaTaskFunzioneAcquisti(Esternalizzazione esternalizzazione)
	{
		Task task=null;
		task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.INSERIMENTO_FUNZIONE_ACQUISTI);	

		if(task==null)
		{
			
			task=taskFunzioneAcquisti.createTask(esternalizzazione);
			this.taskList.add(task);			
			logger.debug("Attivato Task verso la Funzione Acquisti per l'inserimento delle informazioni relative alla fase di predisposizione dell'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());
		}		
		else
			this.taskList.add(task);			

		
	}

	
	/**
	 * Metodo per la crezione del Task relativo all'inserimento
	 * delle informazioni per la fase di predisposizione dell'esternalizzazione 
	 * da parte della Funzione Legale 
	 * @param esternalizzazione
	 */	
	public void creaTaskFunzioneLegale(Esternalizzazione esternalizzazione)
	{
		Task task=null;
		task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.INSERIMENTO_FUNZIONE_LEGALE);	

		if(task==null)
		{

			task=taskFunzioneLegale.createTask(esternalizzazione);
			this.taskList.add(task);			
			logger.debug("Attivato Task verso la Funzione Acquisti per l'inserimento delle informazioni relative alla fase di predisposizione dell'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());
		}
		else
		this.taskList.add(task);			

		
		
	}	


}
