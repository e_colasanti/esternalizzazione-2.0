package it.sara.esternalizzazione.business;

import java.util.List;

import it.sara.esternalizzazione.web.bean.UtenteWeb;
import it.sara.esternalizzazione.ws.bean.ZwestDatifornit;
import it.sara.esternalizzazione.ws.bean.ZwsWestConoResponsabileResponse;
import it.sara.esternalizzazione.ws.bean.ZwsWestDatiFornitoreResponse;
import it.sara.esternalizzazione.ws.bean.ZwsWestUtenteResponse;

public interface IGestioneWSDL {
	
	public ZwsWestUtenteResponse loadUtente(String userid);
	public ZwsWestDatiFornitoreResponse loadDatiFornitori(String ragioneSociale1);
	public ZwsWestConoResponsabileResponse loadDatiConoRE(String userid, String zzenteacq);
	public  List<ZwestDatifornit> getListaFornitori(String ragioneSociale1);
	public List<UtenteWeb> getListaConoRE(UtenteWeb utente);
}
