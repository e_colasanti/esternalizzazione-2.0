package it.sara.esternalizzazione.business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.utility.ObjectToXml;
import it.sara.esternalizzazione.utility.Utility;
import it.sara.esternalizzazione.web.bean.UtenteWeb;
import it.sara.esternalizzazione.ws.bean.TableOfZwsDirezioneDescrizione;
import it.sara.esternalizzazione.ws.bean.ZwsDirezioneDescrizione;
import it.sara.esternalizzazione.ws.bean.ZwsWestUtenteResponse;

@Component("gestioneUtente")
public class GestioneUtente {
	@Autowired
	GestioneWSDL gestione;

	@Value("${RE}")
	public String gruppoResponsabile;


	@Autowired
	@Qualifier("environmentType")	
	String saraEnvironment;


	//ritorna vero se ha solo quel ruolo e solo quello
	public boolean hasSoloRuolo(UtenteWeb utente) {
		List<String> elencoRuoli = utente.getRuoli();
		//ha più di un ruolo
		if (elencoRuoli.size()>1)return false;
		//ha un solo ruolo ritorna true ha quello
		for (String s : elencoRuoli) {
			if (s.equals(utente.getUserWest()) || s.equals(utente.getResponsabile()))return true;
		}
		return false;
	}

	//questo metodo è necessario per capire seun utente ha un determinato ruolo a prescindere da come è chiamato su LDAP
	//Tenendo presente che i ruoli applicativi sono 8
	//- responsabile
	//- ispettore
	//- commerciale
	//- approvatore
	//- direttore generale
	//- rva
	//- rvz
	//- country manager
	public boolean hasRole(String nomeRuolo, Collection<? extends GrantedAuthority> elencoGruppi) {
		if (elencoGruppi==null || nomeRuolo == null)return false;

		for (GrantedAuthority ga : elencoGruppi) {
			if (ga.getAuthority().equals("ROLE_"+gruppoResponsabile) && nomeRuolo.equals("RE")) return true;
		}
		return false;
	}

	//solo la commerciale vede le esternalizzazioni a direzione commerciale e chiuse tutti gli altri vedono tutti gli stati
	public List<Integer> elencoStati(Collection<? extends GrantedAuthority> elencoGruppi) {
		if (elencoGruppi==null )return null;
		LinkedList<Integer> risultato = new LinkedList<Integer>();
		Hashtable<Integer, Integer> statiHash = new Hashtable<Integer, Integer>();
		for (GrantedAuthority ga : elencoGruppi) {
			if (ga.getAuthority().equals("ROLE_"+gruppoResponsabile)) { 
				statiHash.put(1,1);
				statiHash.put(2,2);
				statiHash.put(3,3);
				statiHash.put(4,4);
				statiHash.put(5,5);
				statiHash.put(6,6);
				statiHash.put(7,7);
				statiHash.put(8,8);
				statiHash.put(11,11);
				statiHash.put(12,12);

			}
		}
		for (Enumeration<Integer> e= statiHash.keys();e.hasMoreElements();)
			risultato.add(e.nextElement());
		return risultato;
	}

	public UtenteWeb popolaUtente(String userid) {
		UtenteWeb utente=null;
		utente=caricaUtenteWSDL(userid);
		return utente;
	}

	private UtenteWeb caricaUtenteWSDL( String userid) {
		ZwsWestUtenteResponse responseWSUtente = new ZwsWestUtenteResponse();		
		responseWSUtente = gestione.loadUtente(userid);
		
		TableOfZwsDirezioneDescrizione tableRichiedente = responseWSUtente.getTbRichiedente();
		if(responseWSUtente.getEsito().equals("0")) {
			return null;
		}	
		UtenteWeb utente=new UtenteWeb();
		List<String> ruoli = new ArrayList<>();
		utente.setUsername(responseWSUtente.getAenam().toLowerCase());
		utente.setNome(responseWSUtente.getNome());
		utente.setCognome(responseWSUtente.getCognome());
		utente.setSesso(responseWSUtente.getSesso());
		utente.setLivello(responseWSUtente.getLivello());
		utente.setDescrLivello(responseWSUtente.getDescrLivello());
		utente.setResponsabile(responseWSUtente.getResponsabile());
		utente.setEsito(responseWSUtente.getEsito());

		for(ZwsDirezioneDescrizione zwsDir:tableRichiedente.getItem()) {
			utente.setZzenteacq(zwsDir.getZzenteacq());
			utente.setText(zwsDir.getText());
			utente.setUserWest(zwsDir.getUserWest());
			ruoli.add(utente.getUserWest());
		}
		if(utente.getResponsabile()!= null && !("").equals(utente.getResponsabile()))
			ruoli.add(utente.getResponsabile());

		utente.setRuoli(ruoli);		
		System.out.println("utente: " + utente.getNome()+ " "+ utente.getCognome()+" Responsabile: "+utente.getResponsabile());


		try {
			utente=Utility.cloneUtenteWeb(utente, saraEnvironment);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}


		return utente;
	}
}
