package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.Stato;

public interface StatoDao extends CrudRepository<Stato, Integer> {
	public Stato findByIdStato(Integer idStato);
	public List<Stato> findByDescrizioneStato(String descrizione);
}
