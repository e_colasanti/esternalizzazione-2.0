package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.VerificaConflittiInteressi;

public interface VerificaConflittiInteressiDao extends CrudRepository<VerificaConflittiInteressi,Integer>{
	
	public VerificaConflittiInteressi findByIdVerificaConflittiInteressi(Integer idVerificaConflittiInteressi);
	public List<VerificaConflittiInteressi> findAll();
}
