/**
 * 
 */
package it.sara.esternalizzazione.business.controlli;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.business.GestioneWSDL;
import it.sara.esternalizzazione.db.bean.ControlloOperativo;
import it.sara.esternalizzazione.db.dao.ControlloOperativoDao;
import it.sara.esternalizzazione.web.bean.EsternalizzazioneWeb;
import it.sara.esternalizzazione.ws.bean.ZwsWestUtenteResponse;

/**
 * @author f.vescovi
 *
 */
@Component("ControlloOperativoManager")
public class ControlloOperativoManager {

	private Logger logger = LoggerFactory.getLogger(ControlloOperativoManager.class);
	
	@Autowired
	ControlloOperativoDao controlloOperativoDao;

	@Autowired
	GestioneWSDL gestione;


	public ControlloOperativoManager() {}
	
	
	public ControlloOperativo creaControlloOperativo(EsternalizzazioneWeb esternalizzazioneWeb) {		

		ControlloOperativo controlloOperativo =null;
		
		int idEsternalizzazione = Integer.parseInt(esternalizzazioneWeb.getIdEsternalizzazione());
		//se entrambi sono valorizzati
		if(esternalizzazioneWeb.getResponsabileControlloEsternalizzazione()!= null) {			
				controlloOperativo = insertOnlyOneRow(esternalizzazioneWeb, idEsternalizzazione);
		}else {
			// se è valorizzato RC inserisco solo quello
			if(esternalizzazioneWeb.getIdRC()!= null && esternalizzazioneWeb.getResponsabileControlloEsternalizzazione()== null ) {
				controlloOperativo = valorizzoControllo(esternalizzazioneWeb, idEsternalizzazione);
			}
			//se è volarizzato solo UO inserisco solo quello
			else if(esternalizzazioneWeb.getIdRC()== null && esternalizzazioneWeb.getIdUO()!= null) {
				controlloOperativo =valorizzoOperatore(esternalizzazioneWeb, idEsternalizzazione);
			}					

			this.controlloOperativoDao.save(controlloOperativo);
		}	
		return controlloOperativo;
	}
	
	
	
	public ControlloOperativo insertIfDifferentUsers(EsternalizzazioneWeb esternalizzazioneWeb,int idEsternalizzazione) {
		ControlloOperativo controlloOperativo;
		controlloOperativo = this.controlloOperativoDao.findByRegistrazioneAndEsternalizzazione(esternalizzazioneWeb.getIdUO(),idEsternalizzazione);


		if(controlloOperativo == null || (controlloOperativo != null && controlloOperativo.getOperativo()== 0)) {
			controlloOperativo = caricamentoControlloOperativo(esternalizzazioneWeb,esternalizzazioneWeb.getIdUO());
			controlloOperativo.setRegistrazione(esternalizzazioneWeb.getIdUO());
			controlloOperativo.setOperativo(1);
			controlloOperativo.setControllo(0);				
			this.controlloOperativoDao.save(controlloOperativo);
		}	
		controlloOperativo = null;
		controlloOperativo = this.controlloOperativoDao.findByRegistrazioneAndEsternalizzazione(esternalizzazioneWeb.getIdRC(), idEsternalizzazione);

		if(controlloOperativo == null  || (controlloOperativo != null && controlloOperativo.getControllo()== 0)) {			

			controlloOperativo = caricamentoControlloOperativo(esternalizzazioneWeb,esternalizzazioneWeb.getIdRC());
			controlloOperativo.setRegistrazione(esternalizzazioneWeb.getIdRC());
			controlloOperativo.setOperativo(0);
			controlloOperativo.setControllo(1);				
			this.controlloOperativoDao.save(controlloOperativo);
		}
		return controlloOperativo;
	}

	public ControlloOperativo caricamentoControlloOperativo(EsternalizzazioneWeb esternalizzazioneWeb,String responsabileControllo) {
		ControlloOperativo controlloOperativo;
		int idEsternalizzazione = Integer.parseInt(esternalizzazioneWeb.getIdEsternalizzazione());

		
		if(responsabileControllo.contains("@"))
			responsabileControllo.substring(0,responsabileControllo.lastIndexOf('@'));
		

		ZwsWestUtenteResponse utenteUO= gestione.loadUtente(responsabileControllo);
		controlloOperativo= new ControlloOperativo();
		List<ControlloOperativo> list=controlloOperativoDao.findAll();
		int id=0;
		if(!list.isEmpty())
	     id= controlloOperativoDao.findMaxId()+1;

		
		controlloOperativo.setIdControlloOperativo(id);
		controlloOperativo.setNome(utenteUO.getNome());
		controlloOperativo.setCognome(utenteUO.getCognome());
		controlloOperativo.setEsternalizzazione(idEsternalizzazione);
		
		return controlloOperativo;
	}

	
	
	public ControlloOperativo insertOnlyOneRow(EsternalizzazioneWeb esternalizzazioneWeb, int idEsternalizzazione) {
		ControlloOperativo controlloOperativo;
		controlloOperativo = this.controlloOperativoDao.findByRegistrazioneAndEsternalizzazione(esternalizzazioneWeb.getResponsabileControlloEsternalizzazione(),idEsternalizzazione);
		//se non c'è nulla inserisco una riga soltanto
		if(controlloOperativo == null) {
			controlloOperativo = caricamentoControlloOperativo(esternalizzazioneWeb,esternalizzazioneWeb.getResponsabileControlloEsternalizzazione());
			controlloOperativo.setRegistrazione(esternalizzazioneWeb.getResponsabileControlloEsternalizzazione());
			controlloOperativo.setOperativo(1);
			controlloOperativo.setControllo(1);
			this.controlloOperativoDao.save(controlloOperativo);
		}else if(controlloOperativo != null && controlloOperativo.getOperativo() == 0 ) {	
			//controlloOperativo = caricamentoControlloOperativo(esternalizzazioneWeb,esternalizzazioneWeb.getIdUO());
			controlloOperativo.setRegistrazione(esternalizzazioneWeb.getResponsabileControlloEsternalizzazione());
			controlloOperativo.setOperativo(1);
			this.controlloOperativoDao.save(controlloOperativo);
		}
		else if(controlloOperativo != null && controlloOperativo.getControllo() == 0 ) {	
			//controlloOperativo = caricamentoControlloOperativo(esternalizzazioneWeb,esternalizzazioneWeb.getIdUO());
			controlloOperativo.setRegistrazione(esternalizzazioneWeb.getResponsabileControlloEsternalizzazione());
			controlloOperativo.setControllo(1);
			this.controlloOperativoDao.save(controlloOperativo);
		}
		return controlloOperativo;
	}
	
	
	public ControlloOperativo valorizzoOperatore(EsternalizzazioneWeb esternalizzazioneWeb, int idEsternalizzazione) {
		ControlloOperativo controlloOperativo;
		controlloOperativo = this.controlloOperativoDao.findByRegistrazioneAndEsternalizzazione(esternalizzazioneWeb.getResponsabileSingoloControllo(), idEsternalizzazione);
		if(controlloOperativo == null  ) {
			controlloOperativo = caricamentoControlloOperativo(esternalizzazioneWeb,esternalizzazioneWeb.getResponsabileControlloEsternalizzazione());
			controlloOperativo.setRegistrazione(esternalizzazioneWeb.getResponsabileSingoloControllo());
			controlloOperativo.setControllo(0);
			controlloOperativo.setOperativo(1);
		}else {

			if(controlloOperativo.getOperativo()== 0){
				controlloOperativo.setRegistrazione(esternalizzazioneWeb.getResponsabileControlloEsternalizzazione());
				controlloOperativo.setOperativo(1);
				//controlloOperativo.setOperativo(0);
			}

		}
		return controlloOperativo;
	}

	public ControlloOperativo valorizzoControllo(EsternalizzazioneWeb esternalizzazioneWeb, int idEsternalizzazione) {
		ControlloOperativo controlloOperativo;
		controlloOperativo = this.controlloOperativoDao.findByRegistrazioneAndEsternalizzazione(esternalizzazioneWeb.getResponsabileControlloEsternalizzazione(), idEsternalizzazione);

		if(controlloOperativo == null  ) {
			controlloOperativo = caricamentoControlloOperativo(esternalizzazioneWeb,esternalizzazioneWeb.getResponsabileControlloEsternalizzazione());
			controlloOperativo.setRegistrazione(esternalizzazioneWeb.getResponsabileSingoloControllo());
			controlloOperativo.setControllo(1);
			controlloOperativo.setOperativo(0);
		}else {

			if(controlloOperativo.getControllo()== 0){
				controlloOperativo.setRegistrazione(esternalizzazioneWeb.getResponsabileControlloEsternalizzazione());
				controlloOperativo.setControllo(1);
				//controlloOperativo.setOperativo(0);
			}

		}
		return controlloOperativo;
	}

	

}
