package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.esternalizzazione.db.bean.TipoTask;
import it.sara.esternalizzazione.web.bean.TipoTaskWeb;

public interface TipoTaskDao extends CrudRepository<TipoTask, Integer> {
	
	public TipoTask findByIdTipoTask(Integer idTipoTask);

	public TipoTask findByTipoTask(String tipoTask);
	
	@Query("select tt from TipoTask tt where tt.tipoUtente.idTipoUtente=:idTipoUtente" )
	TipoTask findTipoTaskByTipoUtente(@Param("idTipoUtente")Integer idTipoUtente);
	
	@Query("select tt from TipoTask tt where tt.tipoUtente.idTipoUtente=:idTipoUtente and tt.descTipoTask=:descTipoTask" )
	TipoTask findTipoTaskByTipoUtenteAndDescTipoTask(@Param("idTipoUtente")Integer idTipoUtente,@Param("descTipoTask")String descTipoTask);		
	
	public List<TipoTask> findAll();
	

}
