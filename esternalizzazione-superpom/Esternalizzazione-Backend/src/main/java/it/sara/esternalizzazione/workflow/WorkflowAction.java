/**
 * 
 */
package it.sara.esternalizzazione.workflow;

/**
 * @author f.vescovi
 *
 */
public interface WorkflowAction {
	
	/**

	* Execute action.

	*

	* @param context

	* @throws Exception

	*/

	public void doAction(Context context) throws Exception;
	

}
