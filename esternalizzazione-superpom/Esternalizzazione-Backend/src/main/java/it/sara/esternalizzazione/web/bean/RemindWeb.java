package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import it.sara.esternalizzazione.db.bean.Remind;
import it.sara.esternalizzazione.utility.Utility;

public class RemindWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6615062491012729715L;


	String idRemind;

	String tipoUtente;

	String esternalizzazione;

	String tipoRemind;

	String oggetto;

	String periodicita;

	String dataInizio;

	String dataFine;
	
	


	public RemindWeb(String idRemind, String tipoUtente, String esternalizzazione, String tipoRemind, String oggetto,
			String periodicita, String dataInizio, String dataFine) {
		super();
		this.idRemind = idRemind;
		this.tipoUtente = tipoUtente;
		this.esternalizzazione = esternalizzazione;
		this.tipoRemind = tipoRemind;
		this.oggetto = oggetto;
		this.periodicita = periodicita;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
	}

	
	public RemindWeb(Remind remind) {
		super();
		this.idRemind =Utility.formattaId(remind.getIdRemind());
		this.tipoUtente = remind.getTipoUtente()==null?null:remind.getTipoUtente().getIdTipoUtente()+"";
		this.esternalizzazione = remind.getEsternalizzazione()==null?null:remind.getEsternalizzazione().getIdEsternalizzazione()+"";
		this.tipoRemind = remind.getTipoRemind();
		this.oggetto = remind.getOggetto();
		this.periodicita = remind.getPeriodicita()==null?"":remind.getPeriodicita()+"";
		this.dataInizio = Utility.getStringFromDate(remind.getDataInizio());
		this.dataFine = Utility.getStringFromDate(remind.getDataFine());
	}
	
	
	
	
	/**
	 * @return the idRemind
	 */
	public String getIdRemind() {
		return idRemind;
	}

	/**
	 * @param idRemind the idRemind to set
	 */
	public void setIdRemind(String idRemind) {
		this.idRemind = idRemind;
	}

	/**
	 * @return the tipoUtente
	 */
	public String getTipoUtente() {
		return tipoUtente;
	}

	/**
	 * @param tipoUtente the tipoUtente to set
	 */
	public void setTipoUtente(String tipoUtente) {
		this.tipoUtente = tipoUtente;
	}

	/**
	 * @return the esternalizzazione
	 */
	public String getEsternalizzazione() {
		return esternalizzazione;
	}

	/**
	 * @param esternalizzazione the esternalizzazione to set
	 */
	public void setEsternalizzazione(String esternalizzazione) {
		this.esternalizzazione = esternalizzazione;
	}

	/**
	 * @return the tipoRemind
	 */
	public String getTipoRemind() {
		return tipoRemind;
	}

	/**
	 * @param tipoRemind the tipoRemind to set
	 */
	public void setTipoRemind(String tipoRemind) {
		this.tipoRemind = tipoRemind;
	}

	/**
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * @param oggetto the oggetto to set
	 */
	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * @return the periodicita
	 */
	public String getPeriodicita() {
		return periodicita;
	}

	/**
	 * @param periodicita the periodicita to set
	 */
	public void setPeriodicita(String periodicita) {
		this.periodicita = periodicita;
	}

	/**
	 * @return the dataInizio
	 */
	public String getDataInizio() {
		return dataInizio;
	}

	/**
	 * @param dataInizio the dataInizio to set
	 */
	public void setDataInizio(String dataInizio) {
		this.dataInizio = dataInizio;
	}

	/**
	 * @return the dataFine
	 */
	public String getDataFine() {
		return dataFine;
	}

	/**
	 * @param dataFine the dataFine to set
	 */
	public void setDataFine(String dataFine) {
		this.dataFine = dataFine;
	}
	
	
	
	
	



}
