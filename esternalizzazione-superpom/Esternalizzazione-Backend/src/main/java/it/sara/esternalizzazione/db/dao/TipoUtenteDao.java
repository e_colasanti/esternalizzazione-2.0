package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import it.sara.esternalizzazione.db.bean.TipoUtente;

public interface TipoUtenteDao extends Repository<TipoUtente, Integer> {
	
	public TipoUtente findByIdTipoUtente(Integer idTipoUtente);
	
	@Query("select tu from TipoUtente tu where tu.acTipoUtente =:acTipoUtente" )
	public TipoUtente findByIdAcTipoUtente(@Param("acTipoUtente")String acTipoUtente);
	
	@Query("select tu from TipoUtente tu where tu.codiceDirezione =:codiceDirezione" )
	public TipoUtente findByCodDirezione(@Param("codiceDirezione")String codiceDirezione);
	
	@Query("select tu from TipoUtente tu where tu.descTipoUtente =:descTipoUtente" )
	public TipoUtente findByDescTipoUtente(@Param("descTipoUtente")String descTipoUtente);
	
	public List<TipoUtente> findAll();

}
