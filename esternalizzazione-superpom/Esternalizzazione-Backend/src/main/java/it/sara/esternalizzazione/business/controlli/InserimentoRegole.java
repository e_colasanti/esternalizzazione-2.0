package it.sara.esternalizzazione.business.controlli;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.business.GestioneMail;
import it.sara.esternalizzazione.business.GestioneTipoTask;
import it.sara.esternalizzazione.business.GestioneTipoUtente;
import it.sara.esternalizzazione.business.task.TaskFunzioneAttivitaEssenziale;
import it.sara.esternalizzazione.business.task.TaskFunzioneFondamentale;
import it.sara.esternalizzazione.business.task.TaskFunzioneLegale;
import it.sara.esternalizzazione.business.task.TaskRischiBC;
import it.sara.esternalizzazione.business.task.TaskSupportoLegale;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.bean.TipoTask;
import it.sara.esternalizzazione.db.bean.TipoUtente;
import it.sara.esternalizzazione.db.dao.TaskDao;
import it.sara.esternalizzazione.utility.WestCostants;

@Component("InserimentoRegole")
public class InserimentoRegole implements IRegole{


	private Logger logger = LoggerFactory.getLogger(InserimentoRegole.class);


	protected List<Task> taskList=new ArrayList<>();

	protected List<TipoTask> tipotaskList=null;
	protected String role=null;


	@Autowired
	TaskDao taskDao;

	@Autowired
	GestioneMail gestioneMail;

	@Autowired
	GestioneTipoTask gestioneTipoTask;


	@Autowired
	GestioneTipoUtente gestioneTipoUtente;

	@Autowired
	TaskFunzioneFondamentale taskFunzioneFondamentale;

	@Autowired
	TaskFunzioneAttivitaEssenziale taskFunzioneAttivitaEssenziale;

	@Autowired
	TaskSupportoLegale taskSupportoLegale;	
	
	@Autowired
	TaskRischiBC taskRischiBC;


	public InserimentoRegole() {}


	@Override	
	public boolean checkMandatoryMinimumFields(Esternalizzazione esternalizzazione)
	{
		boolean control=true;

		if(esternalizzazione.getCompilataDa()==null )
			return false;
		if(esternalizzazione.getDataCreazioneEsternalizzazione()==null )
			return false;
		if(esternalizzazione.getTitoloEsternalizzazione()==null )
			return false;
		if(esternalizzazione.getDataModificaTipologiaEsternalizzazione()==null )
			return false;
		if(esternalizzazione.getMotivazioneEsternalizzazione()==null )
			return false;		


		return control;
	}


	/* (non-Javadoc)
	 * @see it.sara.esternalizzazione.business.controlli.IRegole#taskToActivate()
	 */
	@SuppressWarnings("unlikely-arg-type")
	@Override
	public List<Task> taskToActivate(Esternalizzazione esternalizzazione) {


		if(esternalizzazione.getTipoEsternalizzazione().getDescrizioneTipoEsternalizzazione().equals(WestCostants.FUNZIONEFONDAMENTALE) )
		{	
			Task task=null;
			task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.FUNZIONEFONDAMENTALE);	

			if(task==null)
			{
				task=taskFunzioneFondamentale.createTask(esternalizzazione);
				this.taskList.add(task);
				logger.debug("Attivato Task su FUNZIONEFONDAMENTALE per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());			
			}
			else
				this.taskList.add(task);
				
		}
		if(esternalizzazione.getTipoEsternalizzazione().getDescrizioneTipoEsternalizzazione().equals(WestCostants.FUNZIONEATTIVITAESSENZIALEIMPORTANTE) )
		{	
			Task task=null;
			task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.FUNZIONEATTIVITAESSENZIALEIMPORTANTE);	

			if(task==null)
			{
				task=taskFunzioneAttivitaEssenziale.createTask(esternalizzazione);
				this.taskList.add(task);
				logger.debug("Attivato Task su FUNZIONEATTIVITAESSENZIALEIMPORTANTE per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());			
			}
			else
				this.taskList.add(task);

		}
		if(esternalizzazione.getTipoEsternalizzazione().getDescrizioneTipoEsternalizzazione().equals(WestCostants.RICHIESTOSUPPORTOLEGALE))
		{

			Task task=null;
			task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.RICHIESTOSUPPORTOLEGALE);	

			if(task==null)
			{
				task=taskSupportoLegale.createTask(esternalizzazione);
				this.taskList.add(task);			
				logger.debug("Attivato Task su FUNZIONEFONDAMENTALE per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());			
			}
			else
				this.taskList.add(task);			

		}


		Task taskRischiBC=createRischiBCTask(esternalizzazione);
		if(taskRischiBC!=null)
			this.taskList.add(taskRischiBC);



		return taskList;
	}

	/* (non-Javadoc)
	 * @see it.sara.esternalizzazione.business.controlli.IRegole#checkFaseCompletata()
	 */
	@Override
	public boolean checkFaseCompletata(Esternalizzazione esternalizzazione) {

		return false;
	}


	public Task createRischiBCTask(Esternalizzazione esternalizzazione)
	{
		Task task=null;
		task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.RISCHI_BC);	

		if(task==null)
		{
             task=taskRischiBC.createTask(esternalizzazione);			
			this.taskList.add(task);			
			logger.debug("Attivato Task su BUSINESS CONTINUITY per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());			
		}
		else
			this.taskList.add(task);			

		
		return task;

	}	}
