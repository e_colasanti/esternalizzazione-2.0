/**
 * 
 */
package it.sara.esternalizzazione.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.util.ResourceUtils;

import it.sara.esternalizzazione.ws.bean.ZwsWestConoResponsabileResponse;
import it.sara.esternalizzazione.ws.bean.ZwsWestDatiFornitoreResponse;
import it.sara.esternalizzazione.ws.bean.ZwsWestUtenteResponse;

/**
 * @author f.vescovi
 *
 */
public class ObjectToXml {
	

	private ObjectToXml() {}


	public static void convert(Object obj,String filter) throws JAXBException, FileNotFoundException {

		File file =null;
		JAXBContext jaxbContext =null;
		
		
		if(filter.equals("U"))
		{
			file = ResourceUtils.getFile("classpath:xml/utente.xml");
			jaxbContext = JAXBContext.newInstance(obj.getClass());	
		}	
		else if(filter.equals("F"))
		{
			file = ResourceUtils.getFile("classpath:xml/fornitore.xml");
			jaxbContext = JAXBContext.newInstance(obj.getClass());					
		}
		else if(filter.equals("C"))
		{
			file = ResourceUtils.getFile("classpath:xml/conoRE.xml");
			jaxbContext = JAXBContext.newInstance(obj.getClass());								
		}


		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(obj, file);// this line create xml file in specified path.


		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(obj, sw);
		String xmlString = sw.toString();

		System.out.println(xmlString);
	}


	
	public static Object read(String filter,String userid) throws JAXBException, FileNotFoundException {
		
		File file =null;
		JAXBContext jaxbContext =null;
        Object obj=null;
        Unmarshaller jaxbUnmarshaller = null;
        
//
//		ZwsWestUtenteResponse zwsWestUtenteResponse=new ZwsWestUtenteResponse();
//
//		ZwsWestConoResponsabileResponse zwsWestConoResponsabileResponse = new ZwsWestConoResponsabileResponse();	
//
//		ZwsWestDatiFornitoreResponse zwsWestDatiFornitoreResponse = new ZwsWestDatiFornitoreResponse();

		if(filter.equals("U") && !("").equals(userid))
		{
			file = ResourceUtils.getFile("classpath:xml/utente - "+userid+".xml");
			jaxbContext = JAXBContext.newInstance(ZwsWestUtenteResponse.class);
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			ZwsWestUtenteResponse zwsWestUtenteResponse = (ZwsWestUtenteResponse) jaxbUnmarshaller.unmarshal(file);
			System.out.println(zwsWestUtenteResponse);
			
			return zwsWestUtenteResponse;
		}	
		else if(filter.equals("F"))
		{
			file = ResourceUtils.getFile("classpath:xml/fornitore.xml");
			jaxbContext = JAXBContext.newInstance(ZwsWestDatiFornitoreResponse.class);
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			ZwsWestDatiFornitoreResponse zwsWestDatiFornitoreResponse = (ZwsWestDatiFornitoreResponse) jaxbUnmarshaller.unmarshal(file);
			System.out.println(zwsWestDatiFornitoreResponse);
			return zwsWestDatiFornitoreResponse;		
		}
		else if(filter.equals("C"))
		{
			file = ResourceUtils.getFile("classpath:xml/conoRE.xml");
			jaxbContext = JAXBContext.newInstance(ZwsWestConoResponsabileResponse.class);
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			ZwsWestConoResponsabileResponse zwsWestConoResponsabileResponse = (ZwsWestConoResponsabileResponse) jaxbUnmarshaller.unmarshal(file);			
			System.out.println(zwsWestConoResponsabileResponse);
			return zwsWestConoResponsabileResponse;
		}	
		return obj;		
		
	}
	


}
