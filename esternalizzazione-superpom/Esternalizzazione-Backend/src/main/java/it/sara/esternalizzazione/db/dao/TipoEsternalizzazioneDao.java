package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.TipoEsternalizzazione;

public interface TipoEsternalizzazioneDao extends CrudRepository<TipoEsternalizzazione,Integer>{
	
	public TipoEsternalizzazione findByIdTipoEsternalizzazione(Integer idTipoEsternalizzazione);
	public List<TipoEsternalizzazione> findAll();
}
