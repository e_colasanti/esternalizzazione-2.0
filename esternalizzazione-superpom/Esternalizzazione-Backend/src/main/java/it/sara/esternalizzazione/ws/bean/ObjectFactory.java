//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.11 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.08.07 alle 11:52:51 AM CEST 
//


package it.sara.esternalizzazione.ws.bean;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.sara.esternalizzazione.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.sara.esternalizzazione.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ZwsWestUtente }
     * 
     */
    public ZwsWestUtente createZwsWestUtente() {
        return new ZwsWestUtente();
    }

    /**
     * Create an instance of {@link TableOfZwsDirezioneDescrizione }
     * 
     */
    public TableOfZwsDirezioneDescrizione createTableOfZwsDirezioneDescrizione() {
        return new TableOfZwsDirezioneDescrizione();
    }

    /**
     * Create an instance of {@link ZwsWestUtenteResponse }
     * 
     */
    public ZwsWestUtenteResponse createZwsWestUtenteResponse() {
        return new ZwsWestUtenteResponse();
    }

    /**
     * Create an instance of {@link ZwsWestDatiFornitore }
     * 
     */
    public ZwsWestDatiFornitore createZwsWestDatiFornitore() {
        return new ZwsWestDatiFornitore();
    }

    /**
     * Create an instance of {@link TableOfZwestDatifornit }
     * 
     */
    public TableOfZwestDatifornit createTableOfZwestDatifornit() {
        return new TableOfZwestDatifornit();
    }

    /**
     * Create an instance of {@link ZwsWestDatiFornitoreResponse }
     * 
     */
    public ZwsWestDatiFornitoreResponse createZwsWestDatiFornitoreResponse() {
        return new ZwsWestDatiFornitoreResponse();
    }

    /**
     * Create an instance of {@link ZwsDirezioneDescrizione }
     * 
     */
    public ZwsDirezioneDescrizione createZwsDirezioneDescrizione() {
        return new ZwsDirezioneDescrizione();
    }

    /**
     * Create an instance of {@link ZwestDatifornit }
     * 
     */
    public ZwestDatifornit createZwestDatifornit() {
        return new ZwestDatifornit();
    }
    
    /**
     * Create an instance of {@link ZwsWestConoResponsabile }
     * 
     */
    public ZwsWestConoResponsabile createZwsWestConoResponsabile() {
        return new ZwsWestConoResponsabile();
    }

    /**
     * Create an instance of {@link TableOfZwsConoDirezione }
     * 
     */
    public TableOfZwsConoDirezione createTableOfZwsConoDirezione() {
        return new TableOfZwsConoDirezione();
    }

    /**
     * Create an instance of {@link ZwsWestConoResponsabileResponse }
     * 
     */
    public ZwsWestConoResponsabileResponse createZwsWestConoResponsabileResponse() {
        return new ZwsWestConoResponsabileResponse();
    }

    /**
     * Create an instance of {@link ZwsConoDirezione }
     * 
     */
    public ZwsConoDirezione createZwsConoDirezione() {
        return new ZwsConoDirezione();
    }

}
