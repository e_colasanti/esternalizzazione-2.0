package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.ControlloOperativo;

public interface ControlloOperativoDao extends CrudRepository<ControlloOperativo,Integer> {
	public ControlloOperativo findByIdControlloOperativo(Integer idControlloOperativo);
	
	public ControlloOperativo findByRegistrazioneAndEsternalizzazione(String idUO, Integer idEsternalizzazione);
	
	public List<ControlloOperativo> findAll();
	
	@Query("select c from ControlloOperativo c where c.controllo=1 order by c.cognome,c.nome desc" )
	public List<ControlloOperativo> findAllRC();
	@Query("select c from ControlloOperativo c where c.operativo=1 order by c.cognome,c.nome desc" )
	public List<ControlloOperativo> findAllUO();
	@Query("select max(c.idControlloOperativo) from ControlloOperativo c")
	public Integer findMaxId();
	
}
