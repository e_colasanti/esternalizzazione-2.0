package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.esternalizzazione.db.bean.Allegato;


public interface AllegatoDao extends CrudRepository<Allegato, Integer> {
	

	@Query("select a from Allegato a where a.esternalizzazione.idEsternalizzazione=:idEsternalizzazione and a.nomeAllegato=:nomeAllegato " )	
	public List<Allegato> findAllegatiByIdEstAndNome(@Param("idEsternalizzazione")Integer idEsternalizzazione,@Param("nomeAllegato")String nomeAllegato);
	
	@Query("select a from Allegato a where a.esternalizzazione.idEsternalizzazione=:idOggetto " )	
	public List<Allegato> findAllegatiByIdEsternalizzazione(@Param("idOggetto")Integer idOggetto);

	@Query("select a from Allegato a where a.task.idTask=:idOggetto " )	
	public List<Allegato> findAllegatiByIdTask(@Param("idOggetto")Integer idOggetto);
	
	@Query("select a from Allegato a where  a.esternalizzazione.idEsternalizzazione=:idEsternalizzazione and a.tipoAmbito like(:tipoAmbito)" )	
	public List<Allegato> findAllegatiByIdEsternalizzazioneAndTipoAmbito(@Param("idEsternalizzazione")Integer idEsternalizzazione,@Param("tipoAmbito")String tipoAmbito);
	
	@Query("select a from Allegato a where  a.task.idTask=:idTask " )		
	public List<Allegato> findByAllegatoByIdTask(@Param("idTask")Integer idTask);
	
	@Query("select a from Allegato a where  a.task.idTask=:idTask and a.tipoAmbito=:tipoAmbito" )		
	public List<Allegato> findAllegatoByIdTaskAndTipoAmbito(@Param("idTask")Integer idTask,@Param("tipoAmbito")String tipoAmbito);

	
}
