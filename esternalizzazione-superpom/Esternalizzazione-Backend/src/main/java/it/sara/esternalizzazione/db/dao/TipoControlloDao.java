package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.TipoControllo;

public interface TipoControlloDao extends CrudRepository<TipoControllo,Integer>{
	
	public TipoControllo findByIdTipoControllo(Integer idTipoControllo);
	public List<TipoControllo> findAll();
}
