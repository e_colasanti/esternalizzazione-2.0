package it.sara.esternalizzazione.business.controlli;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.business.GestioneMail;
import it.sara.esternalizzazione.business.GestioneTipoTask;
import it.sara.esternalizzazione.business.GestioneTipoUtente;
import it.sara.esternalizzazione.business.GestioneUtente;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.bean.TipoTask;
import it.sara.esternalizzazione.db.bean.TipoUtente;
import it.sara.esternalizzazione.db.dao.TaskDao;
import it.sara.esternalizzazione.utility.WestCostants;
import it.sara.esternalizzazione.web.bean.UtenteWeb;

@Component("PredisposizioneRegole")
public class PredisposizioneRegole implements IRegole{

	private Logger logger = LoggerFactory.getLogger(ValutazioneRegole.class);

	protected List<Task> taskList=new ArrayList<>();
	protected List<TipoTask> tipotaskList=null;

	@Autowired
	TaskDao taskDao;

	@Autowired
	GestioneMail gestioneMail;


	@Autowired
	GestioneTipoTask gestioneTipoTask;


	@Autowired
	GestioneTipoUtente gestioneTipoUtente;
	
	
	@Autowired
	GestioneUtente gestioneUtente;
	


	public PredisposizioneRegole() {
	}

	@Override
	public boolean checkMandatoryMinimumFields(Esternalizzazione esternalizzazione) {
		boolean control=true;

		if(esternalizzazione.getResponsabileSingoloControllo().equals(WestCostants.FA))
		{		
			if(esternalizzazione.getDataInizioEfficaciaContratto()==null)
				return false;
			if(esternalizzazione.getPeriodoPreavvisoContrattuale()==null)
				return false;
			if(esternalizzazione.getTipologiaRinnovo()==null)
				return false;
			if(esternalizzazione.getNumeroODA()==null)
				return false;
			if(esternalizzazione.getNumeroProtocolloContrattuale()==null)
				return false;
			if(esternalizzazione.getPeriodicita()==null)
				return false;
			if(esternalizzazione.getSlaContrattualmenteDefiniti()==null)
				return false;
			if(esternalizzazione.getValutazioneRischiBC()==null)
				return false;
		}

		if(esternalizzazione.getTipoEsternalizzazione().getIdTipoEsternalizzazione().equals(WestCostants.FUNZIONEATTIVITAESSENZIALEIMPORTANTE) || 
				esternalizzazione.getTipoEsternalizzazione().getIdTipoEsternalizzazione().equals(WestCostants.FUNZIONEFONDAMENTALE))
		{	
			if(esternalizzazione.getResponsabileSingoloControllo().equals(WestCostants.FL))
			{
				if(esternalizzazione.getNumeroComunicazioneIVASS()==null)
					return false;
			}
		}



		return control;
	}

	@Override
	public List<Task> taskToActivate(Esternalizzazione esternalizzazione) {

		if(esternalizzazione.getTipoEsternalizzazione().getIdTipoEsternalizzazione().equals(WestCostants.FUNZIONEATTIVITAESSENZIALEIMPORTANTE) || 
				esternalizzazione.getTipoEsternalizzazione().getIdTipoEsternalizzazione().equals(WestCostants.FUNZIONEFONDAMENTALE))
		{			

			Task task=null;
			task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.COMUNICAZIONE_IVASS);	

			if(task==null)
			{
				task=new Task();
				task.setEsternalizzazione(esternalizzazione);
				TipoTask tipoTask=gestioneTipoTask.getTipoTaskByTipoTask(WestCostants.INCIVASS);
				task.setTipoTask(tipoTask);	
				task.setTipoCampoAttivatoEst(WestCostants.COMUNICAZIONE_IVASS);
				task.setProgLavTask(new Random().nextInt());
				task.setDataCreazione(new Date());       
				task.setDataFine(null);
				task.setStato("A");
				TipoUtente tipoUtente=gestioneTipoUtente.getByDescTipoUtente(WestCostants.FL);
				task.setTipoUtente(tipoUtente);											
				task.setUtenteOwner(esternalizzazione.getUtenteModificaEsternalizzazione());
				task.setUtenteResponsabile(esternalizzazione.getCompilataDa());
				task.setUtenteDelegato(esternalizzazione.getResponsabileSingoloControllo());
				task.setFaseEsternalizzazione(WestCostants.FASEPREDISPOSIZIONE); 					
				taskDao.save(task);
				gestioneMail.inviaMail(task);
				this.taskList.add(task);			
				logger.debug("Attivato Task su Comunicazioni IVASS per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());
			}

		}


		if(checkMandatoryMinimumFields(esternalizzazione))
				{
			Task task=null;
			task=taskDao.findTaskByEstAndTipoCampoAttivatoEst(esternalizzazione.getIdEsternalizzazione(), WestCostants.VERIFICA_SLA);	

			if(task==null)
			{
				task=new Task();
				task.setEsternalizzazione(esternalizzazione);
				TipoTask tipoTask=gestioneTipoTask.getTipoTaskByTipoTask(WestCostants.RVSLA);
				task.setTipoTask(tipoTask);	
				task.setTipoCampoAttivatoEst(WestCostants.VERIFICA_SLA);
				task.setProgLavTask(new Random().nextInt());
				task.setDataCreazione(new Date());       
				task.setDataFine(null);
				task.setStato("A");
				TipoUtente tipoUtente=gestioneTipoUtente.getByDescTipoUtente(WestCostants.RE);
				UtenteWeb utente=gestioneUtente.popolaUtente(esternalizzazione.getResponsabileControlloEsternalizzazione());
				task.setTipoUtente(tipoUtente);											
				task.setUtenteOwner(esternalizzazione.getUtenteModificaEsternalizzazione());
				task.setUtenteResponsabile(esternalizzazione.getCompilataDa());
				task.setUtenteDelegato(esternalizzazione.getResponsabileSingoloControllo());
				task.setFaseEsternalizzazione(WestCostants.FASEPREDISPOSIZIONE);					
				taskDao.save(task);
				gestioneMail.setDestinatario(utente.getUsername());
				gestioneMail.inviaMail(task);
				this.taskList.add(task);			
				logger.debug("Attivato Task per verifica Sla per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());
			}
			

				}


		return this.taskList;
	}

	/* (non-Javadoc)
	 * @see it.sara.esternalizzazione.business.controlli.IRegole#checkFaseCompletata()
	 */
	@Override
	public boolean checkFaseCompletata(Esternalizzazione esternalizzazione) {

		boolean control=false; 

		if(checkMandatoryMinimumFields(esternalizzazione))
			if(esternalizzazione.getNumeroComunicazioneIVASS()!=null) {
				List<Task> list=taskDao.findTaskByEstAndTipoTask(esternalizzazione.getIdEsternalizzazione(), 3);
				if(!list.isEmpty()) {
					if(list.get(0).getStato().equals("A"))
						return true;
				}
			}
		if(esternalizzazione.getSlaContrattualmenteDefiniti()==null )//bloccante
			return false;


		return control;
	}






}
