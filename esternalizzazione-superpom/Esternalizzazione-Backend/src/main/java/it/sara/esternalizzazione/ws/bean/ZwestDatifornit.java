//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.11 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.08.07 alle 11:52:51 AM CEST 
//


package it.sara.esternalizzazione.ws.bean;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ZwestDatifornit complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ZwestDatifornit"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ForniId" type="{urn:sap-com:document:sap:rfc:functions}char10"/&gt;
 *         &lt;element name="RagSoc1" type="{urn:sap-com:document:sap:rfc:functions}char35"/&gt;
 *         &lt;element name="RagSoc2" type="{urn:sap-com:document:sap:rfc:functions}char35"/&gt;
 *         &lt;element name="RagSoc3" type="{urn:sap-com:document:sap:rfc:functions}char35"/&gt;
 *         &lt;element name="PartitaIva" type="{urn:sap-com:document:sap:rfc:functions}char11"/&gt;
 *         &lt;element name="CodiceFiscale" type="{urn:sap-com:document:sap:rfc:functions}char16"/&gt;
 *         &lt;element name="OggettoSociale" type="{urn:sap-com:document:sap:rfc:functions}char255"/&gt;
 *         &lt;element name="CapitaleSociale" type="{urn:sap-com:document:sap:rfc:functions}curr20.2"/&gt;
 *         &lt;element name="TotDipendenti" type="{urn:sap-com:document:sap:rfc:functions}char18"/&gt;
 *         &lt;element name="NumImpiegati" type="{urn:sap-com:document:sap:rfc:functions}char18"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZwestDatifornit", propOrder = {
    "forniId",
    "ragSoc1",
    "ragSoc2",
    "ragSoc3",
    "partitaIva",
    "codiceFiscale",
    "oggettoSociale",
    "capitaleSociale",
    "totDipendenti",
    "numImpiegati"
})
public class ZwestDatifornit {

    @XmlElement(name = "ForniId", required = true)
    protected String forniId;
    @XmlElement(name = "RagSoc1", required = true)
    protected String ragSoc1;
    @XmlElement(name = "RagSoc2", required = true)
    protected String ragSoc2;
    @XmlElement(name = "RagSoc3", required = true)
    protected String ragSoc3;
    @XmlElement(name = "PartitaIva", required = true)
    protected String partitaIva;
    @XmlElement(name = "CodiceFiscale", required = true)
    protected String codiceFiscale;
    @XmlElement(name = "OggettoSociale", required = true)
    protected String oggettoSociale;
    @XmlElement(name = "CapitaleSociale", required = true)
    protected BigDecimal capitaleSociale;
    @XmlElement(name = "TotDipendenti", required = true)
    protected String totDipendenti;
    @XmlElement(name = "NumImpiegati", required = true)
    protected String numImpiegati;

    /**
     * Recupera il valore della proprietà forniId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForniId() {
        return forniId;
    }

    /**
     * Imposta il valore della proprietà forniId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForniId(String value) {
        this.forniId = value;
    }

    /**
     * Recupera il valore della proprietà ragSoc1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRagSoc1() {
        return ragSoc1;
    }

    /**
     * Imposta il valore della proprietà ragSoc1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRagSoc1(String value) {
        this.ragSoc1 = value;
    }

    /**
     * Recupera il valore della proprietà ragSoc2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRagSoc2() {
        return ragSoc2;
    }

    /**
     * Imposta il valore della proprietà ragSoc2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRagSoc2(String value) {
        this.ragSoc2 = value;
    }

    /**
     * Recupera il valore della proprietà ragSoc3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRagSoc3() {
        return ragSoc3;
    }

    /**
     * Imposta il valore della proprietà ragSoc3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRagSoc3(String value) {
        this.ragSoc3 = value;
    }

    /**
     * Recupera il valore della proprietà partitaIva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartitaIva() {
        return partitaIva;
    }

    /**
     * Imposta il valore della proprietà partitaIva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartitaIva(String value) {
        this.partitaIva = value;
    }

    /**
     * Recupera il valore della proprietà codiceFiscale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    /**
     * Imposta il valore della proprietà codiceFiscale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceFiscale(String value) {
        this.codiceFiscale = value;
    }

    /**
     * Recupera il valore della proprietà oggettoSociale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggettoSociale() {
        return oggettoSociale;
    }

    /**
     * Imposta il valore della proprietà oggettoSociale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggettoSociale(String value) {
        this.oggettoSociale = value;
    }

    /**
     * Recupera il valore della proprietà capitaleSociale.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCapitaleSociale() {
        return capitaleSociale;
    }

    /**
     * Imposta il valore della proprietà capitaleSociale.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCapitaleSociale(BigDecimal value) {
        this.capitaleSociale = value;
    }

    /**
     * Recupera il valore della proprietà totDipendenti.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotDipendenti() {
        return totDipendenti;
    }

    /**
     * Imposta il valore della proprietà totDipendenti.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotDipendenti(String value) {
        this.totDipendenti = value;
    }

    /**
     * Recupera il valore della proprietà numImpiegati.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumImpiegati() {
        return numImpiegati;
    }

    /**
     * Imposta il valore della proprietà numImpiegati.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumImpiegati(String value) {
        this.numImpiegati = value;
    }

}
