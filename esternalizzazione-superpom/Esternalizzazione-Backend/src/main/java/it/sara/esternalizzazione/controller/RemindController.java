/**
 * 
 */
package it.sara.esternalizzazione.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.sara.esternalizzazione.business.GestioneRemind;
import it.sara.esternalizzazione.business.GestioneTipoUtente;
import it.sara.esternalizzazione.web.bean.PaginaRemindWeb;
import it.sara.esternalizzazione.web.bean.RemindWeb;
import it.sara.esternalizzazione.web.bean.TipoUtenteWeb;

/**
 * @author f.vescovi
 *
 */
@RestController
@RequestMapping("/rest/remind")
@CrossOrigin(origins="*")
public class RemindController extends BaseController {
	
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(RemindController.class);
	
	@Autowired
	GestioneTipoUtente gestioneTipoUtente;
	
	@Autowired
	GestioneRemind gestioneRemind;
	
	
	
	@RequestMapping(value={"/lista", "/lista/{stato}", "/lista/{stato}/{pageNumber}", "/lista/{stato}/{pageNumber}/{righePerPagina}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public PaginaRemindWeb listaRemind(@PathVariable(value="stato", required=false) Integer stato, 
			@PathVariable(value="pageNumber", required=false) Integer pageNumber,
			@PathVariable(value="righePerPagina", required=false) Integer righePerPagina,
			HttpServletResponse response,
			HttpServletRequest request){
		
		logger.debug(request.getProtocol()+request.getServerName()+request.getLocalPort()+request.getContextPath());
		int numeroPagina = ((pageNumber==null)?0:pageNumber);
		int righePagina = ((righePerPagina==null)?5:righePerPagina);
		
		List<Integer> elencoStati = cercaStati(stato, response);
		String username = getUtente().getUsername();
		
		return gestioneRemind.getListaRemind(getUtente(),elencoStati,numeroPagina,righePagina);
	
	}
	
	
	private List<Integer> cercaStati(Integer stato, HttpServletResponse response) {
		Collection<? extends GrantedAuthority> ga= SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		List<Integer> elencoStati = gestioneUtente.elencoStati(ga);
		if (stato != null && stato != -1) {
			boolean statoTrovato = false;
			for (Integer s:elencoStati)if(stato.equals(s))statoTrovato=true;
			if (!statoTrovato) {
				try {
					response.sendError(403,"Utente non autorizzato");
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}else {
				//se lo stato è diverso da null il filtro è lo stato
				elencoStati = new LinkedList<Integer>();
				elencoStati.add(stato);
			}
		}
		return elencoStati;
	}
	
	@RequestMapping(value={"/cerca/{pageNumber}/{righePerPagina}","/cerca/{stato}/{pageNumber}/{righePerPagina}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public PaginaRemindWeb cercaRemind(@PathVariable(value="stato", required=false) Integer stato, 
											@PathVariable(value="pageNumber", required=false) Integer pageNumber, 
											@PathVariable(value="righePerPagina", required=false) Integer righePerPagina, 
								            @RequestParam(value="query", required=false) String query,
											HttpServletResponse response){

		int numeroPagina = ((pageNumber==null)?0:pageNumber);
		int righePagina = ((righePerPagina==null)?5:righePerPagina);

		//elenco degli stati abilitati per i ruoli dell'utente.
		//se lo stato verso il quale si vuole navigare non è uno accessibile all'utente errore
		List<Integer> elencoStati = cercaStati(stato, response);
		return gestioneRemind.cercaRemind(query,((pageNumber==null)?0:pageNumber),((righePerPagina==null)?5:righePerPagina));
	}
	
	
	
	@RequestMapping(value={"/listaTipiUtente"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<TipoUtenteWeb> getTipiUtente(){
		logger.debug("get Tipi Utente...");
		return gestioneTipoUtente.getAll();
	}
	
	
	@RequestMapping(value={"/getRemind/{idRemind}"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public RemindWeb getRemind(@PathVariable(value="idRemind", required=false) Integer idRemind){
		logger.debug("getRemind...");
		return gestioneRemind.getRemindById(idRemind);
	}

}
