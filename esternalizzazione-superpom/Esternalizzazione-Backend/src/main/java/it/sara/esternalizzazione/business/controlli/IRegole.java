package it.sara.esternalizzazione.business.controlli;

import java.util.List;

import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;

public interface IRegole {

	/**
	 * Controllo sui campi obbligatori
	 * @return
	 */
	public boolean checkMandatoryMinimumFields(Esternalizzazione esternalizzazione);

	/**
	 * Lista dei task da attivare sulla fase
	 * @return
	 */
	public List<Task>  taskToActivate(Esternalizzazione esternalizzazione);
	
	/**
	 * Test per fase completata
	 * @return
	 */
	public boolean checkFaseCompletata(Esternalizzazione esternalizzazione);
	

}
