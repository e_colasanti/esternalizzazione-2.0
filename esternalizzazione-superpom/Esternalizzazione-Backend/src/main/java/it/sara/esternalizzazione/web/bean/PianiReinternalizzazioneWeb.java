package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import it.sara.esternalizzazione.db.bean.PianoReinternalizzazione;

public class PianiReinternalizzazioneWeb extends TipoListaWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1323872586421758806L;
	
	public PianiReinternalizzazioneWeb(PianoReinternalizzazione pianoReinternalizzazione) {
		this.setCodice(pianoReinternalizzazione.getIdPianoReinternalizzazione()+"");
		this.setDescrizione(pianoReinternalizzazione.getDescrizionePianoReinternalizzazione());
	}

}
