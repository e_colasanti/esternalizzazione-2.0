package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;
import java.util.List;

public class PaginaTaskWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5841835291723924007L;
	
	List<TaskWeb> listaAttivita;
	private Integer totalePagine;
	private int paginaCorrente;
	private Long totaleRecord;
	private Integer recordPerPagina;
	
	
	public List<TaskWeb> getListaAttivita() {
		return listaAttivita;
	}
	public void setListaAttivita(List<TaskWeb> listaAttivita) {
		this.listaAttivita = listaAttivita;
	}
	public Integer getTotalePagine() {
		return totalePagine;
	}
	public void setTotalePagine(Integer totalePagine) {
		this.totalePagine = totalePagine;
	}
	public int getPaginaCorrente() {
		return paginaCorrente;
	}
	public void setPaginaCorrente(int paginaCorrente) {
		this.paginaCorrente = paginaCorrente;
	}
	public Long getTotaleRecord() {
		return totaleRecord;
	}
	public void setTotaleRecord(Long totaleRecord) {
		this.totaleRecord = totaleRecord;
	}
	public Integer getRecordPerPagina() {
		return recordPerPagina;
	}
	public void setRecordPerPagina(Integer recordPerPagina) {
		this.recordPerPagina = recordPerPagina;
	}
	@Override
	public String toString() {
		return "PaginaTaskWeb [listaAttivita=" + listaAttivita + ", totalePagine=" + totalePagine
				+ ", paginaCorrente=" + paginaCorrente + ", totaleRecord=" + totaleRecord + ", recordPerPagina="
				+ recordPerPagina + "]";
	}
	
	

}
