package it.sara.esternalizzazione.db.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.esternalizzazione.db.bean.Esternalizzazione;

public interface EsternalizzazioneDao extends CrudRepository<Esternalizzazione,Integer>{
	
	Esternalizzazione findByIdEsternalizzazione (Integer idEsternalizzazione);
	List<Esternalizzazione> findAll();	
	Page<Esternalizzazione> findAll(Pageable p);
	
	List<Esternalizzazione> findByStato(Integer stato);
	Page<Esternalizzazione> findByStatoIn(List<Integer> stato,Pageable p);
	
//	@Query("select e from Esternalizzazione e where e.stato in :stato and e.responsabileControlloEsternalizzazione:responsabileControlloEsternalizzazione and e.responsabileSingoloControllo:responsabileSingoloControllo and e.codiceDirezione:codiceDirezione ")
//	Page<Esternalizzazione> findEstrazioni(@Param("stato") List<Integer> stato,@Param("responsabileControlloEsternalizzazione") String responsabileControlloEsternalizzazione,@Param("responsabileSingoloControllo") String responsabileSingoloControllo,@Param("codiceDirezione") String codiceDirezione,Pageable p);

	@Query("select e from Esternalizzazione e where e.stato in :stato and lower(e.compilataDa) like :query")
	Page<Esternalizzazione> findByStatoInLike(@Param("stato") List<Integer> stato,@Param("query") String query,Pageable p);	
	
	@Query("select e from Esternalizzazione e where e.stato in :stato and e.codiceDirezione in :codiceDirezione")
	Page<Esternalizzazione> findAllByUtenteResponsabile(@Param("codiceDirezione")List<String> codiceDirezione, @Param("stato") List<Integer> stato,Pageable p);
	
	@Query("select e from Esternalizzazione e where e.stato in :stato and e.codiceDirezione =:codiceDirezione and lower(e.responsabileControlloEsternalizzazione) like :username ")
	Page<Esternalizzazione> findAllByUtente(@Param("username")String username, @Param("codiceDirezione") String codiceDirezione,@Param("stato") List<Integer> stato,Pageable p);

	@Query("select e from Esternalizzazione e where e.stato in :stato and e.codiceDirezione =:codiceDirezione ")
	Page<Esternalizzazione> findAllByUtenteAndCodDirezione(@Param("codiceDirezione") String codiceDirezione,@Param("stato") List<Integer> stato,Pageable p);	
	
	Page<Esternalizzazione> findByCompilataDaAndStatoIn(String username, List<Integer> stato,Pageable p);
	
	@Query("select e from Esternalizzazione e where e.dataCreazioneEsternalizzazione < :dataCreazioneEsternalizzazione and e.codiceDirezione in(:codiceDirezione) order by e.dataCreazioneEsternalizzazione desc" )
	List<Esternalizzazione> findByIdEsternalizzazionePrecedenteResponsabile(@Param("codiceDirezione")String codiceDirezione, @Param("dataCreazioneEsternalizzazione") Date dataCreazioneEsternalizzazione);
	
	@Query("select e from Esternalizzazione e where e.dataCreazioneEsternalizzazione < :dataCreazioneEsternalizzazione and e.compilataDa in(:username) and e.codiceDirezione in(:codiceDirezione) order by e.dataCreazioneEsternalizzazione desc" )
	List<Esternalizzazione> findByIdEsternalizzazionePrecedente(@Param("username")String username, @Param("codiceDirezione") String codiceDirezione, @Param("dataCreazioneEsternalizzazione") Date dataCreazioneEsternalizzazione);


	@Query("select e from Esternalizzazione e where e.tipoEsternalizzazione = 2 and e.rischiositaEsternalizzazione=4 " )
	List<Esternalizzazione> findByTipoEstAndRischiosita();


	@Query("select e from Esternalizzazione e where e.responsabileControlloEsternalizzazione is null and ( TRUNC(MONTHS_BETWEEN (TO_DATE (sysdate, 'yyyy/mm/dd'), TO_DATE (e.dataInizioEfficaciaContratto, 'yyyy/mm/dd')))=1 or TRUNC(MONTHS_BETWEEN (TO_DATE (sysdate, 'yyyy/mm/dd'), TO_DATE (e.dataCreazioneEsternalizzazione, 'yyyy/mm/dd') ))=1) order by e.dataCreazioneEsternalizzazione desc " )
	List<Esternalizzazione> findByRCNull();
	
	@Query("select e from Esternalizzazione e where e.pianoReinternalizzazione is null order by e.dataCreazioneEsternalizzazione desc" )
	List<Esternalizzazione> findByPianoReinternalizzazioneNull();
	
	
	@Query("select e from Esternalizzazione e where e.periodicita is not null order by e.dataCreazioneEsternalizzazione desc" )
	List<Esternalizzazione> findByPeriodicita();
		
	
	@Query("select e from Esternalizzazione e where e.stato in :stato and (e.dataFineControllo + TO_NUMBER('e.periodoPreavvisoContrattuale','99')+30)=sysdate " )
	Page<Esternalizzazione> findByScadenza(@Param("stato") List<Integer> stato,Pageable p);
	
	@Query("select e from Esternalizzazione e where e.stato in :stato and e.codiceDirezione in :codiceDirezione and (e.dataFineControllo + TO_NUMBER('e.periodoPreavvisoContrattuale','99')+30)=sysdate " )
	Page<Esternalizzazione> findByScadenzaAndCodice(@Param("codiceDirezione")List<String> codiceDirezione,@Param("stato") List<Integer> stato,Pageable p);

	@Query("select e from Esternalizzazione e where e.stato in :stato and e.codiceDirezione =:codiceDirezione and lower(e.responsabileControlloEsternalizzazione) like :username and (e.dataFineControllo + TO_NUMBER('e.periodoPreavvisoContrattuale','99')+30)=sysdate " )
	Page<Esternalizzazione> findByScadenzaAndUser(@Param("username")String username,@Param("codiceDirezione") String codiceDirezione,@Param("stato") List<Integer> stato,Pageable p);

	
	@Query("select e from Esternalizzazione e where e.periodicita is not null " )
	Page<Esternalizzazione> findByValutazione(Pageable p);
	
	@Query("select e from Esternalizzazione e where e.codiceDirezione in :codiceDirezione and e.periodicita is not null " )
	Page<Esternalizzazione> findByValutazioneAndCodice(@Param("codiceDirezione")List<String> codiceDirezione,Pageable p);

	@Query("select e from Esternalizzazione e where e.codiceDirezione =:codiceDirezione and lower(e.responsabileControlloEsternalizzazione) like :username and e.periodicita is not null " )
	Page<Esternalizzazione> findByValutazioneAndUser(@Param("username")String username,@Param("codiceDirezione") String codiceDirezione,Pageable p);

}
