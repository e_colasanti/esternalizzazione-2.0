package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import it.sara.esternalizzazione.db.bean.TipoEsternalizzazione;


public class TipoEsternalizzazioneWeb extends TipoListaWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1137002908005031388L;
	
	public TipoEsternalizzazioneWeb(TipoEsternalizzazione tipoEsternalizzazione) {
		this.setCodice(tipoEsternalizzazione.getIdTipoEsternalizzazione()+"");
		this.setDescrizione(tipoEsternalizzazione.getDescrizioneTipoEsternalizzazione());
	}
}
