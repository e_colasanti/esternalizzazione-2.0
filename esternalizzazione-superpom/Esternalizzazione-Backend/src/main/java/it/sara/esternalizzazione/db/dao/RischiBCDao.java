package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.RischiBC;

public interface RischiBCDao extends CrudRepository<RischiBC,Integer>{
	
	public RischiBC findByIdRischiBC(Integer idRischiBC);
	public List<RischiBC> findAll();
}
