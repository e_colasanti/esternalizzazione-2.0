package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import it.sara.esternalizzazione.db.bean.BeneficioConseguibile;

public class BeneficiConseguibiliWeb extends TipoListaWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3699397813047652618L;
	
	public BeneficiConseguibiliWeb(BeneficioConseguibile beneficioConseguibile) {
		this.setCodice(beneficioConseguibile.getIdBeneficioConseguibile()+"");
		this.setDescrizione(beneficioConseguibile.getDescrizioneBeneficioConseguibile());
	}
}
