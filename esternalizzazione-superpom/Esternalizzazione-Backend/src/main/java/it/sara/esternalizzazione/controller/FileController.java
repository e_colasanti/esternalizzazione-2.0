package it.sara.esternalizzazione.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.core.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import it.sara.esternalizzazione.business.GestioneAllegato;
import it.sara.esternalizzazione.db.bean.Allegato;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.web.bean.AllegatoWeb;
import it.sara.esternalizzazione.web.bean.WebFile;



@RestController
@RequestMapping("/rest/file")
@CrossOrigin(origins="*")
public class FileController {

	private Logger logger = LoggerFactory.getLogger(FileController.class);

	@Autowired 
	GestioneAllegato gestioneAllegato;



	  @RequestMapping(value = "/upload", method = RequestMethod.POST)
	@CrossOrigin(origins = "http://localhost:4200/", maxAge = 3600)
	public ResponseEntity<String> upload(@RequestPart("uploadFile") MultipartFile file,
			@RequestPart("descrizioneOggetto") String descrizioneOggetto,
			@RequestPart("idEsternalizzazione") String idEsternalizzazione,
			@RequestPart("ambito") String ambito) {
		String message=null;

		try {
			System.out.println("Nome del file: "+file.getOriginalFilename());
			System.out.println("idEsternalizzazione: "+idEsternalizzazione);
			System.out.println("Descrizione Oggetto: "+descrizioneOggetto);			
			
			AllegatoWeb allegato=gestioneAllegato.salvaAllegato(file,Integer.valueOf(idEsternalizzazione),descrizioneOggetto,ambito);			
			

			return ResponseEntity.status(HttpStatus.OK).body("File: "+allegato.getNomeFileOrigine()+" salvato correttamente");
		} catch (Exception e) {
			message = "Errore durante il salvataggio del file: " + file.getOriginalFilename() + "!";
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
		}       

	}


	@RequestMapping(value={"/remove/{idAllegato}"}, method = RequestMethod.GET)
	public ResponseEntity<String> remove(@PathVariable(name="idAllegato") String idAllegato) {
		String message;
		try {
			logger.debug("FileController.remove, cancellazione del file. idAllegato: "+ idAllegato);
			gestioneAllegato.rimuoviAllegato(new Integer(idAllegato));
			return ResponseEntity.status(HttpStatus.OK).body("File cancellato correttamente");
		} catch (Exception e) {
			message = "FAIL to remove allegato " + idAllegato + "!";
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("errore durante la cancellazione del file");
		}       
	}

	@RequestMapping(value={"/download/{idAllegato}"}, method = RequestMethod.GET)
	public void download(@PathVariable(name="idAllegato") String idAllegato,
			HttpServletResponse response) {
		try {
			logger.debug("FileController.download, get del file da DB");
			Allegato a = gestioneAllegato.getAllegatoById(new Integer(idAllegato));
			if (a==null)return;
			response.setContentType(a.getMimeType());
			response.setHeader("Content-Disposition", "attachment; filename=\""+a.getNomeAllegato()+"\"");

			FileCopyUtils.copy(a.getContenuto(), response.getOutputStream());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	@RequestMapping(value={"/lista/{idOggetto}/{ambito}"}, method = RequestMethod.GET)
	public List<AllegatoWeb> listaFile( 
			@PathVariable(name="idOggetto") Integer idOggetto,
			@PathVariable(name="ambito") String ambito
			) {
		logger.debug("FileController.download, get del file da DB");
		List<AllegatoWeb> l=null;
		try {
			l = gestioneAllegato.getAllAllegati(idOggetto,ambito);
		} catch (NumberFormatException e) {
			logger.error(e.getMessage());			} 
		return l;
	}

}
