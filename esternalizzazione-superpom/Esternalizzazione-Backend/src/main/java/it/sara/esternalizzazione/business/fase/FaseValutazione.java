package it.sara.esternalizzazione.business.fase;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.business.controlli.ControlloOperativoManager;
import it.sara.esternalizzazione.business.controlli.ValutazioneRegole;
import it.sara.esternalizzazione.db.bean.BeneficioConseguibile;
import it.sara.esternalizzazione.db.bean.ControlloOperativo;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.PianoReinternalizzazione;
import it.sara.esternalizzazione.db.bean.RischiBC;
import it.sara.esternalizzazione.db.bean.RischiositaEsternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.bean.TipoEsternalizzazione;
import it.sara.esternalizzazione.db.bean.VerificaConflittiInteressi;
import it.sara.esternalizzazione.db.dao.BeneficioConseguibileDao;
import it.sara.esternalizzazione.db.dao.ControlloOperativoDao;
import it.sara.esternalizzazione.db.dao.EsternalizzazioneDao;
import it.sara.esternalizzazione.db.dao.PianoReinternalizzazioneDao;
import it.sara.esternalizzazione.db.dao.RischiBCDao;
import it.sara.esternalizzazione.db.dao.RischiositaEsternalizzazioneDao;
import it.sara.esternalizzazione.db.dao.VerificaConflittiInteressiDao;
import it.sara.esternalizzazione.web.bean.EsternalizzazioneWeb;


@Component("FaseValutazione")
public class FaseValutazione {

	private Logger logger = LoggerFactory.getLogger(FaseValutazione.class);

	@Autowired
	BeneficioConseguibileDao beneficioConseguibileDao;

	@Autowired
	RischiositaEsternalizzazioneDao rischiositaEsternalizzazioneDao;

	@Autowired
	VerificaConflittiInteressiDao verificaConflittiInteressiDao;

	@Autowired
	RischiBCDao rischiBCDao;

	@Autowired
	ControlloOperativoDao controlloOperativoDao;
	
	
	@Autowired
	PianoReinternalizzazioneDao pianoReinternalizzazioneDao;
	
	@Autowired
	EsternalizzazioneDao esternalizzazioneDao;
	
	@Autowired
	ControlloOperativoManager controlloOperativoManager;
	
	@Autowired
	ValutazioneRegole valutazioneRegole; 
	

	public FaseValutazione() {}



	public Esternalizzazione valutazioneEsternalizzazione(Esternalizzazione esternalizzazione,EsternalizzazioneWeb esternalizzazioneWeb,TipoEsternalizzazione tipoEsternalizzazione) {

		BeneficioConseguibile beneficioConseguibile = null;
		if (esternalizzazioneWeb.getBeneficioConseguibile()!=null)		
			beneficioConseguibile = this.beneficioConseguibileDao.findByIdBeneficioConseguibile(new Integer(esternalizzazioneWeb.getBeneficioConseguibile()));
    
	    
		VerificaConflittiInteressi verificaConflittiInteressi= null;
		if (esternalizzazioneWeb.getVerificaConflittiInteressi()!=null)	
			verificaConflittiInteressi = this.verificaConflittiInteressiDao.findByIdVerificaConflittiInteressi(new Integer(esternalizzazioneWeb.getVerificaConflittiInteressi()));

			
		RischiositaEsternalizzazione rischiositaEsternalizzazione= null;
		if (esternalizzazioneWeb.getRischiositaEsternalizzazione()!=null)	
			rischiositaEsternalizzazione = this.rischiositaEsternalizzazioneDao.findByIdRischiositaEsternalizzazione(new Integer(esternalizzazioneWeb.getRischiositaEsternalizzazione()));

		PianoReinternalizzazione pianoReinternalizzazione= null;
		if (esternalizzazioneWeb.getPianoReinternalizzazione()!=null)	
			pianoReinternalizzazione = this.pianoReinternalizzazioneDao.findByIdPianoReinternalizzazione(new Integer(esternalizzazioneWeb.getPianoReinternalizzazione()));

		
		
		RischiBC rischiBC = null;
		if (esternalizzazioneWeb.getRischiBC()!=null)	
			rischiBC = this.rischiBCDao.findByIdRischiBC(new Integer(esternalizzazioneWeb.getRischiBC()));
		
		if(esternalizzazioneWeb.getResponsabileControlloEsternalizzazione()!=null && esternalizzazioneWeb.getIdRC()!=null) {

			ControlloOperativo controlloOperativo = controlloOperativoManager.creaControlloOperativo(esternalizzazioneWeb);			
		    if(controlloOperativo!=null)	
			logger.debug("ID Controllo Operativo: "+controlloOperativo.getIdControlloOperativo());
	
		}
		
		esternalizzazione.setBeneficioConseguibile(beneficioConseguibile);
	    esternalizzazione.setVerificaConflittiInteressi(verificaConflittiInteressi);
	    esternalizzazione.setRischiositaEsternalizzazione(rischiositaEsternalizzazione);
	    esternalizzazione.setPianoReinternalizzazione(pianoReinternalizzazione);
	    esternalizzazione.setRischiBC(rischiBC);
	    
		esternalizzazioneWeb.toEsternalizzazioneDb(esternalizzazione,tipoEsternalizzazione, "valutazione");
	    
	    
	    
	    List<Task>  taskToActivate= valutazioneRegole.taskToActivate(esternalizzazione);
	    if(!taskToActivate.isEmpty()) {
	    	logger.debug("Attivati "+taskToActivate.size()+" per l'esternalizzazione numero: "+esternalizzazione.getIdEsternalizzazione());
	    	esternalizzazioneWeb.setListaTask(taskToActivate);
	    }
			 
	    if(valutazioneRegole.checkFaseCompletata(esternalizzazione))
	    {
		    esternalizzazione.setStato(3);//TODO per demo
		    esternalizzazioneWeb.setStato("3");//TODO per demo
	    }	      
	   
        Esternalizzazione readyForPredisposizione=esternalizzazioneDao.save(esternalizzazione); 
        
        if(readyForPredisposizione!=null)
        {
        	if(readyForPredisposizione.getStato()==3) {
        	valutazioneRegole.creaTaskFunzioneAcquisti(readyForPredisposizione);
        	valutazioneRegole.creaTaskFunzioneLegale(readyForPredisposizione);
        	}
        }
		
		return esternalizzazione;
	}
	
	
	public boolean assegnaEsternalizzazione(String id, String responsabileControllo) {

		boolean assegnato = false;
		Integer idFormattato=Integer.parseInt(id);
		
		Esternalizzazione esternalizzazione = this.esternalizzazioneDao.findByIdEsternalizzazione(idFormattato);

		EsternalizzazioneWeb esternalizzazioneWeb = new EsternalizzazioneWeb(esternalizzazione);
		ControlloOperativo controlloOperativo = this.controlloOperativoDao.findByRegistrazioneAndEsternalizzazione(responsabileControllo,idFormattato);
		if(controlloOperativo== null) {
			
			esternalizzazioneWeb.setIdRC(esternalizzazione.getIdRC());
			esternalizzazioneWeb.setResponsabileControlloEsternalizzazione(responsabileControllo);
			esternalizzazioneWeb.setResponsabileSingoloControllo(esternalizzazione.getResponsabileSingoloControllo());
			controlloOperativo = controlloOperativoManager.creaControlloOperativo(esternalizzazioneWeb);
		    if(controlloOperativo!=null)	
			logger.debug("ID Controllo Operativo: "+controlloOperativo.getIdControlloOperativo());

			assegnato= true;
		}
		else {
			assegnato= true;
		}
		if(assegnato) {
			esternalizzazione.setResponsabileControlloEsternalizzazione(responsabileControllo);
			esternalizzazione.setResponsabileSingoloControllo(esternalizzazioneWeb.getResponsabileSingoloControllo());			
			esternalizzazioneDao.save(esternalizzazione);
			valutazioneRegole.updateTask(esternalizzazione);
			valutazioneRegole.taskToActivate(esternalizzazione);
		}
		
		return assegnato;
	}
	
	


}
