/**
 * 
 */
package it.sara.esternalizzazione.business;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.dao.EsternalizzazioneDao;
import it.sara.esternalizzazione.web.bean.EsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.PaginaEsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.UtenteWeb;

/**
 * @author f.vescovi
 *
 */
@Component("GestioneFaseScadenzaOutSourcing")
public class GestioneFaseScadenzaOutSourcing {
	
	private Logger logger = LoggerFactory.getLogger(GestioneFaseScadenzaOutSourcing.class);

	@Autowired
	EsternalizzazioneDao esternalizzazioneDao;
	
	
	
	public PaginaEsternalizzazioneWeb getListaEsternalizzazioni(UtenteWeb utente, List<Integer> stati, int numeroPagina,int righePagina) {

		Sort lSort = new Sort(new Sort.Order(Sort.Direction.DESC, "dataCreazioneEsternalizzazione"));
		Pageable p = new PageRequest(numeroPagina, righePagina, lSort);
		Page<Esternalizzazione> l =null;
		boolean isVisibleToFunction=false;

		if(("057").equals(utente.getZzenteacq()) || 
				("008").equals(utente.getZzenteacq()) ||					
				("024").equals(utente.getZzenteacq()) || 
				("052").equals(utente.getZzenteacq()) ||
				("059").equals(utente.getZzenteacq()) 
				)
			isVisibleToFunction=true;


		if(utente.getResponsabile()!= null && !("").equals(utente.getResponsabile())) {
			List<String> codici =new ArrayList<>();
			codici.add(utente.getZzenteacq());
			l = esternalizzazioneDao.findByScadenzaAndCodice(codici, stati, p);
		}else {
			String user=utente.getUsername();
			if(utente.getUsername().contains("@")) 
				user=utente.getUsername().substring(0, utente.getUsername().lastIndexOf("@"));



			if(!isVisibleToFunction)
				l = esternalizzazioneDao.findByScadenzaAndUser(user, utente.getZzenteacq(), stati, p);
			else
				l = esternalizzazioneDao.findByScadenza(stati, p);    				  

		}
		if (l==null)return null;

		List<EsternalizzazioneWeb> lista = new LinkedList<EsternalizzazioneWeb>();
		for (Esternalizzazione esternalizzazione : l) {		
			lista.add(new EsternalizzazioneWeb(esternalizzazione));
		}
		PaginaEsternalizzazioneWeb risultato = new PaginaEsternalizzazioneWeb();
		risultato.setListaEsternalizzazioni(lista);
		risultato.setPaginaCorrente(l.getNumber());
		risultato.setRecordPerPagina(l.getNumberOfElements());
		risultato.setTotalePagine(l.getTotalPages());
		risultato.setTotaleRecord(l.getTotalElements());
		return risultato;
	}

	
	
	

}
