/**
 * 
 */
package it.sara.esternalizzazione.db.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;

/**
 * @author f.vescovi
 *
 */
public interface TaskDao extends CrudRepository<Task, Integer> {

	Task findByIdTask (Integer idTask);
	List<Task> findAll();	
	Page<Task> findAll(Pageable p);
	
	List<Task> findByStato(String stato);
	Page<Task> findByStatoIn(List<String> stato,Pageable p);
	
	@Query("select e from Task e where e.stato in :stato and lower(e.utenteOwner) like :query")
	Page<Task> findByStatoInLike(@Param("stato") List<Integer> stato,@Param("query") String query,Pageable p);
		
	@Query("select t from Task t where t.stato in :stato and t.utenteOwner =:username ")
	Page<Task> findAllByUtenteAndStatus( @Param("stato") String stato,@Param("username") String username,Pageable p);

	@Query("select t from Task t where t.utenteDelegato =:username ")
	Page<Task> findAllByResponsabileControllo( @Param("username") String username,Pageable p);

	@Query("select t from Task t where t.utenteResponsabile =:username ")
	Page<Task> findAllByResponsabile( @Param("username") String username,Pageable p);
	
	@Query("select t from Task t where t.stato=:stato")
	Page<Task> findAllByStato(@Param("stato") String stato,Pageable p);
	
//	Page<Task> findByCompilataDaAndStatoIn(String username, List<String> stato,Pageable p);
	
	@Query("select e from Task e where e.dataCreazione < :dataCreazione  order by e.dataCreazione desc" )
	List<Task> findByIdTaskPrecedenteResponsabile(@Param("dataCreazione") Date dataCreazioneTask);
	
	@Query("select e from Task e where e.dataCreazione < :dataCreazione and e.utenteOwner in(:username)  order by e.dataCreazione desc" )
	List<Task> findByIdTaskPrecedente(@Param("username")String username, @Param("dataCreazione") Date dataCreazioneTask);

	@Query("select max(progLavTask) from Task t where t.stato='C' " )
	Integer findTipoTaskByLastClosed();
	
	@Query("select t from Task t where t.esternalizzazione.idEsternalizzazione=:idEsternalizzazione" )
	List<Task> findTaskByEsternalizzazione(@Param("idEsternalizzazione") Integer idEsternalizzazione);
	

	@Query("select t from Task t where t.esternalizzazione.idEsternalizzazione=:idEsternalizzazione and t.tipoTask.idTipoTask=:idTipoTask" )
	List<Task> findTaskByEstAndTipoTask(@Param("idEsternalizzazione") Integer idEsternalizzazione,@Param("idTipoTask") Integer idTipoTask);
	
	@Query("select t from Task t where t.esternalizzazione.idEsternalizzazione=:idEsternalizzazione and t.tipoCampoAttivatoEst=:tipoCampoAttivatoEst" )
	Task findTaskByEstAndTipoCampoAttivatoEst(@Param("idEsternalizzazione") Integer idEsternalizzazione,@Param("tipoCampoAttivatoEst") String tipoCampoAttivatoEst);
	
	@Query("select t from Task t where t.tipoTask.idTipoTask=:idTipoTask" )
	Task findTaskByTipoTask(@Param("idTipoTask") Integer idTipoTask);
	
}
