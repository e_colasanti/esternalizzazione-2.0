package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import javax.persistence.Column;

import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.utility.Utility;


public class TaskWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2803484977811020579L;
	
	
	String idTask;
	
	String progLavTask;
	
	String idTipoTask;

	String utenteOwner;
	
	String utenteResponsabile;
	
	String dataCreazione;
	
	String dataAssegnazione;
	
	String dataFine;
	
	String stato;
	
	String descrizione;
	
    String idEsternalizzazione;
    
    String utenteDelegato;

    String idTipoUtente;

    byte[] allegati;    

	String tipoCampoAttivatoEst;
	
	String faseEsternalizzazione; 
	
	String codiceDirezione; 

	String bloccante;
	
	String faseBloccante;
    
    
    public TaskWeb() {
    	
    	super();
    }


	public TaskWeb(String idTask, String progLavTask, String idTipoTask, String utenteOwner, String utenteResponsabile,
			String dataCreazione, String dataAssegnazione, String dataFine, String stato, String descrizione,
			String idEsternalizzazione, String utenteDelegato, String idTipoUtente, byte[] allegati,
			String tipoCampoAttivatoEst,String faseEsternalizzazione,String codiceDirezione,String bloccante,String faseBloccante) {
		super();
		this.idTask = idTask;
		this.progLavTask = progLavTask;
		this.idTipoTask = idTipoTask;
		this.utenteOwner = utenteOwner;
		this.utenteResponsabile = utenteResponsabile;
		this.dataCreazione = dataCreazione;
		this.dataAssegnazione = dataAssegnazione;
		this.dataFine = dataFine;
		this.stato = stato;
		this.descrizione = descrizione;
		this.idEsternalizzazione = idEsternalizzazione;
		this.utenteDelegato = utenteDelegato;
		this.idTipoUtente = idTipoUtente;
		this.allegati = allegati;
		this.tipoCampoAttivatoEst=tipoCampoAttivatoEst;
		this.faseEsternalizzazione=faseEsternalizzazione;
		this.codiceDirezione=codiceDirezione;
		this.bloccante=bloccante;
		this.faseBloccante=faseBloccante;
	}

	public TaskWeb(Task task) {
		this.idTask = String.valueOf(task.getIdTask());
		this.progLavTask = String.valueOf(task.getProgLavTask());
		this.idTipoTask = task.getTipoTask()==null?null:String.valueOf(task.getTipoTask().getIdTipoTask());
		this.utenteOwner = task.getUtenteOwner();
		this.utenteResponsabile = task.getUtenteResponsabile();
		this.dataAssegnazione = Utility.getStringFromDate(task.getDataAssegnazione());		
		this.dataCreazione = Utility.getStringFromDate(task.getDataCreazione());
		this.dataFine = Utility.getStringFromDate(task.getDataFine());
		this.stato = task.getStato();
		this.descrizione = task.getDescrizione();
		this.idEsternalizzazione =String.valueOf(task.getEsternalizzazione().getIdEsternalizzazione());
		this.utenteDelegato = task.getUtenteDelegato();
		this.idTipoUtente =  task.getTipoUtente()==null?null:String.valueOf(task.getTipoUtente().getIdTipoUtente());
		this.allegati = task.getAllegati();
		this.tipoCampoAttivatoEst=task.getTipoCampoAttivatoEst();
		this.faseEsternalizzazione=task.getFaseEsternalizzazione();	
		this.codiceDirezione=task.getCodiceDirezione();
        bloccante=task.getBloccante();
		faseBloccante=task.getFaseBloccante();
	}

	/**
	 * @return the idTask
	 */
	public String getIdTask() {
		return idTask;
	}

	/**
	 * @param idTask the idTask to set
	 */
	public void setIdTask(String idTask) {
		this.idTask = idTask;
	}

	/**
	 * @return the progLavTask
	 */
	public String getProgLavTask() {
		return progLavTask;
	}

	/**
	 * @param progLavTask the progLavTask to set
	 */
	public void setProgLavTask(String progLavTask) {
		this.progLavTask = progLavTask;
	}



	/**
	 * @return the idTipoTask
	 */
	public String getIdTipoTask() {
		return idTipoTask;
	}

	/**
	 * @param idTipoTask the idTipoTask to set
	 */
	public void setIdTipoTask(String idTipoTask) {
		this.idTipoTask = idTipoTask;
	}

	/**
	 * @return the utenteOwner
	 */
	public String getUtenteOwner() {
		return utenteOwner;
	}

	/**
	 * @param utenteOwner the utenteOwner to set
	 */
	public void setUtenteOwner(String utenteOwner) {
		this.utenteOwner = utenteOwner;
	}

	/**
	 * @return the dataCreazione
	 */
	public String getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * @param dataCreazione the dataCreazione to set
	 */
	public void setDataCreazione(String dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	/**
	 * @return the dataFine
	 */
	public String getDataFine() {
		return dataFine;
	}

	/**
	 * @param dataFine the dataFine to set
	 */
	public void setDataFine(String dataFine) {
		this.dataFine = dataFine;
	}

	/**
	 * @return the stato
	 */
	public String getStato() {
		return stato;
	}

	/**
	 * @param stato the stato to set
	 */
	public void setStato(String stato) {
		this.stato = stato;
	}

	/**
	 * @return the descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * @param descrizione the descrizione to set
	 */
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * @return the idEsternalizzazione
	 */
	public String getIdEsternalizzazione() {
		return idEsternalizzazione;
	}

	/**
	 * @param idEsternalizzazione the idEsternalizzazione to set
	 */
	public void setIdEsternalizzazione(String idEsternalizzazione) {
		this.idEsternalizzazione = idEsternalizzazione;
	}

	/**
	 * @return the utenteResponsabile
	 */
	public String getUtenteResponsabile() {
		return utenteResponsabile;
	}

	/**
	 * @param utenteResponsabile the utenteResponsabile to set
	 */
	public void setUtenteResponsabile(String utenteResponsabile) {
		this.utenteResponsabile = utenteResponsabile;
	}

	/**
	 * @return the dataAssegnazione
	 */
	public String getDataAssegnazione() {
		return dataAssegnazione;
	}

	/**
	 * @param dataAssegnazione the dataAssegnazione to set
	 */
	public void setDataAssegnazione(String dataAssegnazione) {
		this.dataAssegnazione = dataAssegnazione;
	}

	/**
	 * @return the utenteDelegato
	 */
	public String getUtenteDelegato() {
		return utenteDelegato;
	}

	/**
	 * @param utenteDelegato the utenteDelegato to set
	 */
	public void setUtenteDelegato(String utenteDelegato) {
		this.utenteDelegato = utenteDelegato;
	}

	/**
	 * @return the idTipoUtente
	 */
	public String getIdTipoUtente() {
		return idTipoUtente;
	}

	/**
	 * @param idTipoUtente the idTipoUtente to set
	 */
	public void setIdTipoUtente(String idTipoUtente) {
		this.idTipoUtente = idTipoUtente;
	}

	/**
	 * @return the allegati
	 */
	public byte[] getAllegati() {
		return allegati;
	}

	/**
	 * @param allegati the allegati to set
	 */
	public void setAllegati(byte[] allegati) {
		this.allegati = allegati;
	}

	/**
	 * @return the tipoCampoAttivatoEst
	 */
	public String getTipoCampoAttivatoEst() {
		return tipoCampoAttivatoEst;
	}

	/**
	 * @param tipoCampoAttivatoEst the tipoCampoAttivatoEst to set
	 */
	public void setTipoCampoAttivatoEst(String tipoCampoAttivatoEst) {
		this.tipoCampoAttivatoEst = tipoCampoAttivatoEst;
	}

	/**
	 * @return the faseEsternalizzazione
	 */
	public String getFaseEsternalizzazione() {
		return faseEsternalizzazione;
	}

	/**
	 * @param faseEsternalizzazione the faseEsternalizzazione to set
	 */
	public void setFaseEsternalizzazione(String faseEsternalizzazione) {
		this.faseEsternalizzazione = faseEsternalizzazione;
	}


	public String getCodiceDirezione() {
		return codiceDirezione;
	}


	public void setCodiceDirezione(String codiceDirezione) {
		this.codiceDirezione = codiceDirezione;
	}


	/**
	 * @return the bloccante
	 */
	public String getBloccante() {
		return bloccante;
	}


	/**
	 * @param bloccante the bloccante to set
	 */
	public void setBloccante(String bloccante) {
		this.bloccante = bloccante;
	}


	/**
	 * @return the faseBloccante
	 */
	public String getFaseBloccante() {
		return faseBloccante;
	}


	/**
	 * @param faseBloccante the faseBloccante to set
	 */
	public void setFaseBloccante(String faseBloccante) {
		this.faseBloccante = faseBloccante;
	}
	
	
	

}
