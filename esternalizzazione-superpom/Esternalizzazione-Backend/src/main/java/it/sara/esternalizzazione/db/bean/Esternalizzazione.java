package it.sara.esternalizzazione.db.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="WEST_ESTERNALIZZAZIONE")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Esternalizzazione implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6509098158247632760L;
	
	/*fase inserimento*/
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GENERA_ID")
    @SequenceGenerator(name="SEQ_GENERA_ID", sequenceName="SEQ_GENERA_ID", allocationSize=1)
	@Column(name="ID_ESTERNALIZZAZIONE")
	Integer idEsternalizzazione;
	
	@Column(name="DATA_CREAZIONE_EST")
	Date dataCreazioneEsternalizzazione;
	
	@Column(name="COMPILATA_DA")
	String compilataDa;
	
	@Column(name="CODICE_DIREZIONE")
	String codiceDirezione;
	
	@Column(name="TITOLO_ESTERNALIZZAZIONE")
	String titoloEsternalizzazione;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_ESTERNALIZZAZIONE")
	TipoEsternalizzazione tipoEsternalizzazione;
		
		
	@Column(name="ID_RC")
	String idRC;
	
	
	/*fase valutazione*/
	
	@Column(name="APPROVAZIONE_CDA")
	Date approvazioneCDA;
	
	@Column(name="DATA_MODIFICA_TIPOLOGIA_EST")
	Date dataModificaTipologiaEsternalizzazione;
	
	@Column(name="UTENTE_MODIFICA_EST")
	String utenteModificaEsternalizzazione;
	
	@Column(name="MOTIVAZIONE_ESTERNALIZZAZIONE")
	String motivazioneEsternalizzazione;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BENEFICIO_CONSEGUIBILE")
	BeneficioConseguibile beneficioConseguibile;
	
	@Column(name="DATA_PROBABILE_INIZIO_EST")
	Date dataProbabileInizioEsternalizzazione;
	
	@Column(name="NOME_FORNITORE")
	String nomeFornitore;
	
	@Column(name="SPAZIO_ECONOMICO_EUROPEO")
	String spazioEconomicoEuropeo;
	
	@Column(name="AFFIDABILITA_COMP_FORNITORE")
	String affidabilitaCompetenzaFornitore;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RISCHIOSITA_EST")
	RischiositaEsternalizzazione rischiositaEsternalizzazione;
	
	@Column(name="VALUTAZIONE_RISCHIO_EST")
	String valutazioneRischioEsternalizzazione;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VERIFICA_CONFL_INT")
	VerificaConflittiInteressi verificaConflittiInteressi;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RISCHI_BC")
	RischiBC rischiBC;
	
	/*fase di predisposizione*/
	
	@Column(name="VALUTAZIONE_RISCHI_BC")
	String valutazioneRischiBC;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PIANO_REINTERNALIZZAZIONE")
	PianoReinternalizzazione pianoReinternalizzazione;
	
	@Column(name="RISPETTO_REMUNERAZIONE")
	String rispettoRemunerazione;
	
	@Column(name="DATA_INIZIO_EFFICACIA_CONTR")
	Date dataInizioEfficaciaContratto;
	
	@Column(name="NUMERO_COM_IVASS")
	String numeroComunicazioneIVASS;
	
	@Column(name="NUMERO_RDA")
	String numeroRDA;
	
	@Column(name="NUMERO_ODA")
	String numeroODA;
	
	@Column(name="NUMERO_PROT_CONTRATTUALE")
	String numeroProtocolloContrattuale;
	
	@Column(name="PERIODO_PREAVV_CONTRATTUALE")
	String periodoPreavvisoContrattuale;
	
	@Column(name="TIPOLOGIA_RINNOVO")
	String tipologiaRinnovo;
	
	@Column(name="SLA_CONTRATTUALMENTE_DEFINITI")
	String slaContrattualmenteDefiniti;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_CONTROLLO")
	TipoControllo tipoControllo;
	
	
	@Column(name="DESCRIZIONE_CONTROLLO")
	String descrizioneControllo;

	
	@Column(name="DATA_INIZIO_CONTROLLO")
	Date dataInizioControllo;
	
	@Column(name="DATA_FINE_CONTROLLO")
	Date dataFineControllo;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PERIODICITA")
	Periodicita periodicita;
	
	@Column(name="VINCOLO_PAGAMENTO_FATTURA")
	String vincoloPagamentoFattura;
	
	@Column(name="RESPONSABILE_CONTROLLO_EST")
	String responsabileControlloEsternalizzazione;
	
	@Column(name="RESPONSABILE_SINGOLO_CONTROLLO")
	String responsabileSingoloControllo;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ESITO_CONTROLLO")
	EsitoControllo esitoControllo;
	
	@Column(name="DESCRIZIONE_ESITO_CONTROLLO")
	String descrizioneEsitoControllo;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_AZIONE_CORRETTIVA")
	AzioneCorrettiva azioneCorrettiva;
	
	
	@Column(name="NOTA_AZIONE_CORRETTIVA")
	String notaAzioneCorrettiva;
	

	@OneToMany(mappedBy="esternalizzazione",targetEntity = Controllo.class,cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Controllo> controlli;
	
	
	
	@Column(name="ID_STATO")
	Integer stato;	
	
	
	@Column(name="OGGETTO_ATTIVITA")
	String oggettoAttivita;
		
	
	/**
	 * @return the oggettoAttivita
	 */
	public String getOggettoAttivita() {
		return oggettoAttivita;
	}

	/**
	 * @param oggettoAttivita the oggettoAttivita to set
	 */
	public void setOggettoAttivita(String oggettoAttivita) {
		this.oggettoAttivita = oggettoAttivita;
	}

	/**
	 * @return the controllo
	 */
	public List<Controllo> getControlli() {
		return controlli;
	}

	/**
	 * @param controllo the controllo to set
	 */
	public void setControllo(List<Controllo> controlli) {
		this.controlli = controlli;
	}

	public Integer getIdEsternalizzazione() {
		return idEsternalizzazione;
	}

	public void setIdEsternalizzazione(Integer idEsternalizzazione) {
		this.idEsternalizzazione = idEsternalizzazione;
	}

	public Date getDataCreazioneEsternalizzazione() {
		return dataCreazioneEsternalizzazione;
	}

	public void setDataCreazioneEsternalizzazione(Date dataCreazioneEsternalizzazione) {
		this.dataCreazioneEsternalizzazione = dataCreazioneEsternalizzazione;
	}

	public String getCompilataDa() {
		return compilataDa;
	}

	public void setCompilataDa(String compilataDa) {
		this.compilataDa = compilataDa;
	}

	public String getCodiceDirezione() {
		return codiceDirezione;
	}

	public void setCodiceDirezione(String codiceDirezione) {
		this.codiceDirezione = codiceDirezione;
	}

	public String getTitoloEsternalizzazione() {
		return titoloEsternalizzazione;
	}

	public void setTitoloEsternalizzazione(String titoloEsternalizzazione) {
		this.titoloEsternalizzazione = titoloEsternalizzazione;
	}

	public TipoEsternalizzazione getTipoEsternalizzazione() {
		return tipoEsternalizzazione;
	}

	public void setTipoEsternalizzazione(TipoEsternalizzazione tipoEsternalizzazione) {
		this.tipoEsternalizzazione = tipoEsternalizzazione;
	}
	

	public String getIdRC() {
		return idRC;
	}

	public void setIdRC(String idRC) {
		this.idRC = idRC;
	}

	public Date getApprovazioneCDA() {
		return approvazioneCDA;
	}

	public void setApprovazioneCDA(Date approvazioneCDA) {
		this.approvazioneCDA = approvazioneCDA;
	}

	public Date getDataModificaTipologiaEsternalizzazione() {
		return dataModificaTipologiaEsternalizzazione;
	}

	public void setDataModificaTipologiaEsternalizzazione(Date dataModificaTipologiaEsternalizzazione) {
		this.dataModificaTipologiaEsternalizzazione = dataModificaTipologiaEsternalizzazione;
	}

	public String getUtenteModificaEsternalizzazione() {
		return utenteModificaEsternalizzazione;
	}

	public void setUtenteModificaEsternalizzazione(String utenteModificaEsternalizzazione) {
		this.utenteModificaEsternalizzazione = utenteModificaEsternalizzazione;
	}

	public String getMotivazioneEsternalizzazione() {
		return motivazioneEsternalizzazione;
	}

	public void setMotivazioneEsternalizzazione(String motivazioneEsternalizzazione) {
		this.motivazioneEsternalizzazione = motivazioneEsternalizzazione;
	}

	public BeneficioConseguibile getBeneficioConseguibile() {
		return beneficioConseguibile;
	}

	public void setBeneficioConseguibile(BeneficioConseguibile beneficioConseguibile) {
		this.beneficioConseguibile = beneficioConseguibile;
	}

	public Date getDataProbabileInizioEsternalizzazione() {
		return dataProbabileInizioEsternalizzazione;
	}

	public void setDataProbabileInizioEsternalizzazione(Date dataProbabileInizioEsternalizzazione) {
		this.dataProbabileInizioEsternalizzazione = dataProbabileInizioEsternalizzazione;
	}

	public String getNomeFornitore() {
		return nomeFornitore;
	}

	public void setNomeFornitore(String nomeFornitore) {
		this.nomeFornitore = nomeFornitore;
	}

	public String getSpazioEconomicoEuropeo() {
		return spazioEconomicoEuropeo;
	}

	public void setSpazioEconomicoEuropeo(String spazioEconomicoEuropeo) {
		this.spazioEconomicoEuropeo = spazioEconomicoEuropeo;
	}

	public String getAffidabilitaCompetenzaFornitore() {
		return affidabilitaCompetenzaFornitore;
	}

	public void setAffidabilitaCompetenzaFornitore(String affidabilitaCompetenzaFornitore) {
		this.affidabilitaCompetenzaFornitore = affidabilitaCompetenzaFornitore;
	}

	public RischiositaEsternalizzazione getRischiositaEsternalizzazione() {
		return rischiositaEsternalizzazione;
	}

	public void setRischiositaEsternalizzazione(RischiositaEsternalizzazione rischiositaEsternalizzazione) {
		this.rischiositaEsternalizzazione = rischiositaEsternalizzazione;
	}

	public String getValutazioneRischioEsternalizzazione() {
		return valutazioneRischioEsternalizzazione;
	}

	public void setValutazioneRischioEsternalizzazione(String valutazioneRischioEsternalizzazione) {
		this.valutazioneRischioEsternalizzazione = valutazioneRischioEsternalizzazione;
	}

	public VerificaConflittiInteressi getVerificaConflittiInteressi() {
		return verificaConflittiInteressi;
	}

	public void setVerificaConflittiInteressi(VerificaConflittiInteressi verificaConflittiInteressi) {
		this.verificaConflittiInteressi = verificaConflittiInteressi;
	}

	public RischiBC getRischiBC() {
		return rischiBC;
	}

	public void setRischiBC(RischiBC rischiBC) {
		this.rischiBC = rischiBC;
	}

	public String getValutazioneRischiBC() {
		return valutazioneRischiBC;
	}

	public void setValutazioneRischiBC(String valutazioneRischiBC) {
		this.valutazioneRischiBC = valutazioneRischiBC;
	}

	public PianoReinternalizzazione getPianoReinternalizzazione() {
		return pianoReinternalizzazione;
	}

	public void setPianoReinternalizzazione(PianoReinternalizzazione pianoReinternalizzazione) {
		this.pianoReinternalizzazione = pianoReinternalizzazione;
	}

	public String getRispettoRemunerazione() {
		return rispettoRemunerazione;
	}

	public void setRispettoRemunerazione(String rispettoRemunerazione) {
		this.rispettoRemunerazione = rispettoRemunerazione;
	}

	
	public Date getDataInizioEfficaciaContratto() {
		return dataInizioEfficaciaContratto;
	}

	public void setDataInizioEfficaciaContratto(Date dataInizioEfficaciaContratto) {
		this.dataInizioEfficaciaContratto = dataInizioEfficaciaContratto;
	}

	public String getNumeroComunicazioneIVASS() {
		return numeroComunicazioneIVASS;
	}

	public void setNumeroComunicazioneIVASS(String numeroComunicazioneIVASS) {
		this.numeroComunicazioneIVASS = numeroComunicazioneIVASS;
	}

	public String getNumeroRDA() {
		return numeroRDA;
	}

	public void setNumeroRDA(String numeroRDA) {
		this.numeroRDA = numeroRDA;
	}

	public String getNumeroODA() {
		return numeroODA;
	}

	public void setNumeroODA(String numeroODA) {
		this.numeroODA = numeroODA;
	}

	public String getNumeroProtocolloContrattuale() {
		return numeroProtocolloContrattuale;
	}

	public void setNumeroProtocolloContrattuale(String numeroProtocolloContrattuale) {
		this.numeroProtocolloContrattuale = numeroProtocolloContrattuale;
	}

	public String getPeriodoPreavvisoContrattuale() {
		return periodoPreavvisoContrattuale;
	}

	public void setPeriodoPreavvisoContrattuale(String periodoPreavvisoContrattuale) {
		this.periodoPreavvisoContrattuale = periodoPreavvisoContrattuale;
	}

	public String getTipologiaRinnovo() {
		return tipologiaRinnovo;
	}

	public void setTipologiaRinnovo(String tipologiaRinnovo) {
		this.tipologiaRinnovo = tipologiaRinnovo;
	}

	/**
	 * @return the slaContrattualmenteDefiniti
	 */
	public String getSlaContrattualmenteDefiniti() {
		return slaContrattualmenteDefiniti;
	}

	/**
	 * @param slaContrattualmenteDefiniti the slaContrattualmenteDefiniti to set
	 */
	public void setSlaContrattualmenteDefiniti(String slaContrattualmenteDefiniti) {
		this.slaContrattualmenteDefiniti = slaContrattualmenteDefiniti;
	}

	public String getDescrizioneControllo() {
		return descrizioneControllo;
	}

	public void setDescrizioneControllo(String descrizioneControllo) {
		this.descrizioneControllo = descrizioneControllo;
	}

	public TipoControllo getTipoControllo() {
		return tipoControllo;
	}

	public void setTipoControllo(TipoControllo tipoControllo) {
		this.tipoControllo = tipoControllo;
	}

	public Date getDataInizioControllo() {
		return dataInizioControllo;
	}

	public void setDataInizioControllo(Date dataInizioControllo) {
		this.dataInizioControllo = dataInizioControllo;
	}

	public Date getDataFineControllo() {
		return dataFineControllo;
	}

	public void setDataFineControllo(Date dataFineControllo) {
		this.dataFineControllo = dataFineControllo;
	}

	public Periodicita getPeriodicita() {
		return periodicita;
	}

	public void setPeriodicita(Periodicita periodicita) {
		this.periodicita = periodicita;
	}

	public String getVincoloPagamentoFattura() {
		return vincoloPagamentoFattura;
	}

	public void setVincoloPagamentoFattura(String vincoloPagamentoFattura) {
		this.vincoloPagamentoFattura = vincoloPagamentoFattura;
	}

	public String getResponsabileControlloEsternalizzazione() {
		return responsabileControlloEsternalizzazione;
	}

	public void setResponsabileControlloEsternalizzazione(String responsabileControlloEsternalizzazione) {
		this.responsabileControlloEsternalizzazione = responsabileControlloEsternalizzazione;
	}	
	
	public String getResponsabileSingoloControllo() {
		return responsabileSingoloControllo;
	}

	public void setResponsabileSingoloControllo(String responsabileSingoloControllo) {
		this.responsabileSingoloControllo = responsabileSingoloControllo;
	}

	public EsitoControllo getEsitoControllo() {
		return esitoControllo;
	}

	public void setEsitoControllo(EsitoControllo esitoControllo) {
		this.esitoControllo = esitoControllo;
	}

	public String getDescrizioneEsitoControllo() {
		return descrizioneEsitoControllo;
	}

	public void setDescrizioneEsitoControllo(String descrizioneEsitoControllo) {
		this.descrizioneEsitoControllo = descrizioneEsitoControllo;
	}

	public AzioneCorrettiva getAzioneCorrettiva() {
		return azioneCorrettiva;
	}

	public void setAzioneCorrettiva(AzioneCorrettiva azioneCorrettiva) {
		this.azioneCorrettiva = azioneCorrettiva;
	}

	public Integer getStato() {
		return stato;
	}

	public void setStato(Integer stato) {
		this.stato = stato;
	}

	public String getNotaAzioneCorrettiva() {
		return notaAzioneCorrettiva;
	}

	public void setNotaAzioneCorrettiva(String notaAzioneCorrettiva) {
		this.notaAzioneCorrettiva = notaAzioneCorrettiva;
	}
	
		
	
//	public ControlloOperativo getRCEsternalizzazione() {
//		return RCEsternalizzazione;
//	}
//
//	public void setRCEsternalizzazione(ControlloOperativo rCEsternalizzazione) {
//		RCEsternalizzazione = rCEsternalizzazione;
//	}
//
//	public ControlloOperativo getUOEsternalizzazione() {
//		return UOEsternalizzazione;
//	}
//
//	public void setUOEsternalizzazione(ControlloOperativo uOEsternalizzazione) {
//		UOEsternalizzazione = uOEsternalizzazione;
//	}



	@Override
	public String toString() {
		return "Esternalizzazione [idEsternalizzazione=" + idEsternalizzazione + ", dataCreazioneEsternalizzazione="
				+ dataCreazioneEsternalizzazione + ", compilataDa=" + compilataDa + ", codiceUtente=" + codiceDirezione+ ", titoloEsternalizzazione="
				+ titoloEsternalizzazione + ", tipoEsternalizzazione=" + tipoEsternalizzazione + ", approvazioneCDA="
				+ approvazioneCDA + ", dataModificaTipologiaEsternalizzazione=" + dataModificaTipologiaEsternalizzazione
				+ ", utenteModificaEsternalizzazione=" + utenteModificaEsternalizzazione
				+ ", motivazioneEsternalizzazione=" + motivazioneEsternalizzazione + ", beneficioConseguibile="
				+ beneficioConseguibile + ", dataProbabileInizioEsternalizzazione="
				+ dataProbabileInizioEsternalizzazione + ", nomeFornitore=" + nomeFornitore
				+ ", spazioEconomicoEuropeo=" + spazioEconomicoEuropeo + ", affidabilitaCompetenzaFornitore="
				+ affidabilitaCompetenzaFornitore + ", rischiositaEsternalizzazione=" + rischiositaEsternalizzazione
				+ ", valutazioneRischioEsternalizzazione=" + valutazioneRischioEsternalizzazione
				+ ", verificaConflittiInteressi=" + verificaConflittiInteressi + ", rischiBC=" + rischiBC
				+ ", valutazioneRischiBC=" + valutazioneRischiBC + ", pianoReinternalizzazione="
				+ pianoReinternalizzazione + ", rispettoRemunerazione=" + rispettoRemunerazione
//				+ ", RCEsternalizzazione=" + RCEsternalizzazione + ", UOEsternalizzazione=" + UOEsternalizzazione 
				+", dataInizioEfficaciaContratto="+ dataInizioEfficaciaContratto + ", numeroComunicazioneIVASS=" + numeroComunicazioneIVASS
				+ ", numeroRDA=" + numeroRDA + ", numeroODA=" + numeroODA + ", numeroProtocolloContrattuale="
				+ numeroProtocolloContrattuale + ", periodoPreavvisoContrattual=" + periodoPreavvisoContrattuale
				+ ", tipologiaRinnovo=" + tipologiaRinnovo + ", SLAContrattualmenteDefiniti="+ slaContrattualmenteDefiniti + ", descrizioneControllo=" + descrizioneControllo + ", tipoControllo="
				+ tipoControllo + ", dataInizioControllo=" + dataInizioControllo + ", dataFineControllo="
				+ dataFineControllo + ", periodicita=" + periodicita + ", vincoloPagamentoFattura="
				+ vincoloPagamentoFattura + ", responsabileSingoloControllo=" + responsabileSingoloControllo
				+ ", esitoControllo=" + esitoControllo + ", descrizioneEsitoControllo=" + descrizioneEsitoControllo
				+ ", azioneCorrettiva=" + azioneCorrettiva + ", stato=" + stato + ", notaAzioneCorrettiva="
				+ notaAzioneCorrettiva + "]";
	}	
}
