/**
 * 
 */
package it.sara.esternalizzazione.web.bean;

import it.sara.esternalizzazione.db.bean.TipoUtente;

/**
 * @author f.vescovi
 *
 */
public class TipoUtenteWeb extends TipoListaWeb {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TipoUtenteWeb(TipoUtente tipoUtente) {
		this.setCodice(tipoUtente.getIdTipoUtente()+"");
		this.setDescrizione(tipoUtente.getDescTipoUtente());
	}
	
	
	
	
	

}
