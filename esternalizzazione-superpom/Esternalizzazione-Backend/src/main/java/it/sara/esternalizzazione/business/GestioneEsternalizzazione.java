package it.sara.esternalizzazione.business;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import it.sara.esternalizzazione.business.fase.FaseAssegnazione;
import it.sara.esternalizzazione.business.fase.FaseInserimento;
import it.sara.esternalizzazione.business.fase.FasePredisposizione;
import it.sara.esternalizzazione.business.fase.FaseValutazione;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.TipoEsternalizzazione;
import it.sara.esternalizzazione.db.dao.EsternalizzazioneDao;
import it.sara.esternalizzazione.db.dao.TipoEsternalizzazioneDao;
import it.sara.esternalizzazione.utility.Utility;
import it.sara.esternalizzazione.web.bean.EsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.PaginaEsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.UtenteWeb;

@Component("GestioneEsternalizzazione")
public class GestioneEsternalizzazione {

	private Logger logger = LoggerFactory.getLogger(GestioneEsternalizzazione.class);

	@Autowired
	EsternalizzazioneDao esternalizzazioneDao;

	@Autowired
	TipoEsternalizzazioneDao tipoEsternalizzazioneDao;


	@Autowired
	FaseValutazione faseValutazione;

	@Autowired
	FaseInserimento faseInserimento;

	@Autowired
	FasePredisposizione fasePredisposizione;

	@Autowired
	FaseAssegnazione faseAssegnazione;

	public PaginaEsternalizzazioneWeb getListaEsternalizzazioni(UtenteWeb utente, List<Integer> stati, int numeroPagina,int righePagina) {

		Sort lSort = new Sort(new Sort.Order(Sort.Direction.DESC, "dataCreazioneEsternalizzazione"));
		Pageable p = new PageRequest(numeroPagina, righePagina, lSort);
		Page<Esternalizzazione> l =null;
		boolean isVisibleToFunction=false;

		if(("057").equals(utente.getZzenteacq()) || 
				("008").equals(utente.getZzenteacq()) ||					
				("024").equals(utente.getZzenteacq()) || 
				("052").equals(utente.getZzenteacq()) ||
				("059").equals(utente.getZzenteacq()) 
				)
			isVisibleToFunction=true;


		if(utente.getResponsabile()!= null && !("").equals(utente.getResponsabile())) {
			List<String> codici =new ArrayList<>();
			codici.add(utente.getZzenteacq());
			l = esternalizzazioneDao.findAllByUtenteResponsabile(codici, stati, p);
		}else {
			String user=utente.getUsername();
			if(utente.getUsername().contains("@")) 
				user=utente.getUsername().substring(0, utente.getUsername().lastIndexOf("@"));



			if(!isVisibleToFunction)
				l = esternalizzazioneDao.findAllByUtente(user, utente.getZzenteacq(), stati, p);
			else
				l = esternalizzazioneDao.findByStatoIn(stati, p);    				  

		}
		if (l==null)return null;

		List<EsternalizzazioneWeb> lista = new LinkedList<EsternalizzazioneWeb>();
		for (Esternalizzazione esternalizzazione : l) {		
			lista.add(new EsternalizzazioneWeb(esternalizzazione));
		}
		PaginaEsternalizzazioneWeb risultato = new PaginaEsternalizzazioneWeb();
		risultato.setListaEsternalizzazioni(lista);
		risultato.setPaginaCorrente(l.getNumber());
		risultato.setRecordPerPagina(l.getNumberOfElements());
		risultato.setTotalePagine(l.getTotalPages());
		risultato.setTotaleRecord(l.getTotalElements());
		return risultato;
	}

	private List<EsternalizzazioneWeb> caricaListaEsternalizzazioneWeb(List<Integer> stati, String query, Integer pageNumber, Integer righePerPagina)throws UnsupportedEncodingException {
		Sort lSort = new Sort(new Sort.Order(Sort.Direction.DESC, "dataCreazioneEsternalizzazione"));
		Pageable p = new PageRequest(pageNumber, righePerPagina, lSort);

		String queryDecoded = URLDecoder.decode(query, "UTF-8");

		Page<Esternalizzazione> listaEsternalizzazioni = esternalizzazioneDao.findByStatoInLike(stati,"%"+queryDecoded.toLowerCase()+"%",p);
		if (listaEsternalizzazioni==null)return null;

		List<EsternalizzazioneWeb> lista = new LinkedList<EsternalizzazioneWeb>();
		for(Esternalizzazione esternalizzazzione:listaEsternalizzazioni) {
			lista.add(new EsternalizzazioneWeb(esternalizzazzione));			
		}
		return lista;
	}


	//con questo metodo l'Esternalizzazione passa allo stato da validare(2) ci si dovrebbe arrivare dallo stato bozza(1)
	@Transactional(rollbackFor=Exception.class)
	public String  passaConfermata(String id){
		if (id==null)return null;

		try {
			String idDaFormattare = "";
			int idFormattato= 0;
			if(id!= null && !id.equals(""))
				idDaFormattare = Utility.formattaId(id);
			if(idDaFormattare != null && !idDaFormattare.equals(""))
				idFormattato =Integer.parseInt(idDaFormattare);

			Esternalizzazione esternalizzazione = this.esternalizzazioneDao.findByIdEsternalizzazione(idFormattato);

			return Utility.formattaId(""+esternalizzazione.getIdEsternalizzazione());

		}catch (Exception e) {
			logger.error("Errore durante il passaggio dell'esternalizzazione ");
			e.printStackTrace();
		}
		return null;
	}

	public PaginaEsternalizzazioneWeb cercaEsternalizzazioni(List<Integer> stati, String query, Integer pageNumber, Integer righePerPagina) {

		try {	
			List<EsternalizzazioneWeb> lista =  caricaListaEsternalizzazioneWeb(stati,query,  pageNumber,  righePerPagina);

			PaginaEsternalizzazioneWeb risultato = new PaginaEsternalizzazioneWeb();
			risultato.setListaEsternalizzazioni(lista);
			risultato.setPaginaCorrente(1);
			risultato.setRecordPerPagina(10);
			risultato.setTotalePagine(1);
			risultato.setTotaleRecord(new Long(lista.size()));
			return risultato;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}


	public PaginaEsternalizzazioneWeb cercaEstrazioni(List<Integer> stati, String responsabileControlloEsternalizzazione,String responsabileSingoloControllo,String codiceDirezione, Integer pageNumber, Integer righePerPagina) {

		try {

			//List<EsternalizzazioneWeb> lista =  caricaListaEsternalizzazioneWeb(stati,query,  pageNumber,  righePerPagina);

			Sort lSort = new Sort(new Sort.Order(Sort.Direction.DESC, "dataCreazioneEsternalizzazione"));
			Pageable p = new PageRequest(pageNumber, righePerPagina, lSort);


			Page<Esternalizzazione> listaEsternalizzazioni = esternalizzazioneDao.findAll(p);

			if (listaEsternalizzazioni==null)return null;

			List<EsternalizzazioneWeb> lista = new LinkedList<EsternalizzazioneWeb>();
			for(Esternalizzazione esternalizzazzione:listaEsternalizzazioni) {

				if(esternalizzazzione.getResponsabileControlloEsternalizzazione().equals(responsabileControlloEsternalizzazione) || esternalizzazzione.getResponsabileSingoloControllo().equals(responsabileSingoloControllo) || esternalizzazzione.getCodiceDirezione().equals(codiceDirezione))
				{
					lista.add(new EsternalizzazioneWeb(esternalizzazzione));			
				} 

			}


			PaginaEsternalizzazioneWeb risultato = new PaginaEsternalizzazioneWeb();
			risultato.setListaEsternalizzazioni(lista);
			risultato.setPaginaCorrente(1);
			risultato.setRecordPerPagina(10);
			risultato.setTotalePagine(1);
			risultato.setTotaleRecord(new Long(lista.size()));
			return risultato;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Transactional(rollbackFor=Exception.class)
	public void salvaEsternalizzazione(EsternalizzazioneWeb esternalizzazioneWeb){

		try {

			if (esternalizzazioneWeb.getIdEsternalizzazione() == null){

				TipoEsternalizzazione tipoEsternalizzazione = null;
				if (esternalizzazioneWeb.getTipoEsternalizzazione()!=null)		
					tipoEsternalizzazione = this.tipoEsternalizzazioneDao.findByIdTipoEsternalizzazione(new Integer(esternalizzazioneWeb.getTipoEsternalizzazione()));

				Esternalizzazione newEsternalizzazione  = faseInserimento.inserimentoEsternalizzazione(esternalizzazioneWeb, tipoEsternalizzazione);							

				esternalizzazioneWeb.setIdEsternalizzazione(newEsternalizzazione.getIdEsternalizzazione()+"");

			}else {

				String idDaFormattare = "";
				int idFormattato= 0;
				if(esternalizzazioneWeb.getIdEsternalizzazione()!= null && !esternalizzazioneWeb.getIdEsternalizzazione().equals(""))
					idDaFormattare = Utility.formattaId(esternalizzazioneWeb.getIdEsternalizzazione());
				if(idDaFormattare != null && !idDaFormattare.equals(""))
					idFormattato =Integer.parseInt(idDaFormattare);
				Esternalizzazione esternalizzazione = this.esternalizzazioneDao.findByIdEsternalizzazione(idFormattato);
				TipoEsternalizzazione tipoEsternalizzazione = null;
				if (esternalizzazioneWeb.getTipoEsternalizzazione()!=null)		
					tipoEsternalizzazione = this.tipoEsternalizzazioneDao.findByIdTipoEsternalizzazione(new Integer(esternalizzazioneWeb.getTipoEsternalizzazione()));


				if(esternalizzazioneWeb.getStato()!= null && !esternalizzazioneWeb.getStato().equals(""))
					esternalizzazione.setStato(new Integer(esternalizzazioneWeb.getStato()));

				if(esternalizzazione.getStato()==2)								    	
					esternalizzazione = faseValutazione.valutazioneEsternalizzazione(esternalizzazione, esternalizzazioneWeb, tipoEsternalizzazione);
				else if(esternalizzazione.getStato()==3)	
					esternalizzazione =fasePredisposizione.predisposizioneEsternalizzazione(esternalizzazione, esternalizzazioneWeb, tipoEsternalizzazione);

				esternalizzazioneWeb.setIdEsternalizzazione(esternalizzazione.getIdEsternalizzazione()+"");	

			}
		}catch (Exception e) {
			logger.error("Errore durante il salvataggio dell'esternalizzazione");
			e.printStackTrace();
		}
	}





	public EsternalizzazioneWeb getEsternalizzazione(String id) {
		EsternalizzazioneWeb risultato = null;
		Esternalizzazione esternalizzazione = this.esternalizzazioneDao.findByIdEsternalizzazione(new Integer(id));
		risultato = new EsternalizzazioneWeb(esternalizzazione);

		return risultato;
	}	

	public EsternalizzazioneWeb getEsternalizzazionePrecedente(UtenteWeb utente,String id) {
		EsternalizzazioneWeb risultato = null;
		Esternalizzazione esternalizzazione = this.esternalizzazioneDao.findByIdEsternalizzazione(new Integer(id));
		List<Esternalizzazione> listaEsternalizzazioni =null;
		if(utente.getResponsabile()!= null && !("").equals(utente.getResponsabile())) {
			listaEsternalizzazioni =  this.esternalizzazioneDao.findByIdEsternalizzazionePrecedenteResponsabile(utente.getZzenteacq(), esternalizzazione.getDataCreazioneEsternalizzazione());
		}else
			listaEsternalizzazioni =  this.esternalizzazioneDao.findByIdEsternalizzazionePrecedente(utente.getUsername(), utente.getZzenteacq(), esternalizzazione.getDataCreazioneEsternalizzazione()); 

		Esternalizzazione esternalizzazionePrecedente = null;

		if(listaEsternalizzazioni == null || listaEsternalizzazioni.size() == 0) return null;

		for(Esternalizzazione estePrec : listaEsternalizzazioni) {
			esternalizzazionePrecedente = new Esternalizzazione();
			risultato = new EsternalizzazioneWeb(estePrec);
			esternalizzazionePrecedente.setIdEsternalizzazione(estePrec.getIdEsternalizzazione());
			break;
		}	

		return risultato;
	}



	@Transactional(rollbackFor=Exception.class)	
	public boolean assegnaEsternalizzazione(String id, String responsabileControllo) {		
		return faseAssegnazione.assegnaEsternalizzazione(id,responsabileControllo);
	}	
}
