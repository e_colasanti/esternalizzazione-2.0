package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.esternalizzazione.db.bean.Remind;

public interface RemindDao extends CrudRepository<Remind, Integer> {
	
	public Remind findByIdRemind(Integer idRemind);
	public List<Remind> findAll();	
	Page<Remind> findAll(Pageable p);

	List<Remind> findByTipoRemind(String tipoRemind);
	Page<Remind> findByPeriodicita(List<String> periodicita,Pageable p);
	
	@Query("select r from Remind r where r.esternalizzazione.idEsternalizzazione = :idEsternalizzazione " )
	List<Remind> findByIdEsternalizzazione(@Param("idEsternalizzazione")Integer idEsternalizzazione);
	

	@Query("select r from Remind r where lower(r.oggetto) like :query")
	Page<Remind> findByTipoRemindInLike(@Param("query") String query,Pageable p);
	
	
	@Query("select r from Remind r where r.tipoUtente.idTipoUtente =:tipoUtente ")
	Page<Remind> findAllByTipoUtente(@Param("tipoUtente") Integer tipoUtente,Pageable p);
	


}
