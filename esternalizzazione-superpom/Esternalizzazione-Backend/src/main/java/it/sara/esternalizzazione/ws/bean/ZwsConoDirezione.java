//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.11 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.08.08 alle 02:27:44 PM CEST 
//


package it.sara.esternalizzazione.ws.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ZwsConoDirezione complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ZwsConoDirezione"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Nome" type="{urn:sap-com:document:sap:rfc:functions}char40"/&gt;
 *         &lt;element name="Cognome" type="{urn:sap-com:document:sap:rfc:functions}char40"/&gt;
 *         &lt;element name="Usrid" type="{urn:sap-com:document:sap:rfc:functions}char30"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZwsConoDirezione", propOrder = {
    "nome",
    "cognome",
    "usrid"
})
public class ZwsConoDirezione {

    @XmlElement(name = "Nome", required = true)
    protected String nome;
    @XmlElement(name = "Cognome", required = true)
    protected String cognome;
    @XmlElement(name = "Usrid", required = true)
    protected String usrid;

    /**
     * Recupera il valore della proprietà nome.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Imposta il valore della proprietà nome.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Recupera il valore della proprietà cognome.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCognome() {
        return cognome;
    }

    /**
     * Imposta il valore della proprietà cognome.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCognome(String value) {
        this.cognome = value;
    }

    /**
     * Recupera il valore della proprietà usrid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsrid() {
        return usrid;
    }

    /**
     * Imposta il valore della proprietà usrid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsrid(String value) {
        this.usrid = value;
    }

}
