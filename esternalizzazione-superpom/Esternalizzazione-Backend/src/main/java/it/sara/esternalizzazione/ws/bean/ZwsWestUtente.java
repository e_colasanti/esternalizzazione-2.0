//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.11 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.08.07 alle 11:52:51 AM CEST 
//


package it.sara.esternalizzazione.ws.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TbRichiedente" type="{urn:sap-com:document:sap:soap:functions:mc-style}TableOfZwsDirezioneDescrizione" minOccurs="0"/&gt;
 *         &lt;element name="Usrid" type="{urn:sap-com:document:sap:rfc:functions}char30"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tbRichiedente",
    "usrid"
})
@XmlRootElement(name = "ZwsWestUtente")
public class ZwsWestUtente {

    @XmlElement(name = "TbRichiedente")
    protected TableOfZwsDirezioneDescrizione tbRichiedente;
    @XmlElement(name = "Usrid", required = true)
    protected String usrid;

    /**
     * Recupera il valore della proprietà tbRichiedente.
     * 
     * @return
     *     possible object is
     *     {@link TableOfZwsDirezioneDescrizione }
     *     
     */
    public TableOfZwsDirezioneDescrizione getTbRichiedente() {
        return tbRichiedente;
    }

    /**
     * Imposta il valore della proprietà tbRichiedente.
     * 
     * @param value
     *     allowed object is
     *     {@link TableOfZwsDirezioneDescrizione }
     *     
     */
    public void setTbRichiedente(TableOfZwsDirezioneDescrizione value) {
        this.tbRichiedente = value;
    }

    /**
     * Recupera il valore della proprietà usrid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsrid() {
        return usrid;
    }

    /**
     * Imposta il valore della proprietà usrid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsrid(String value) {
        this.usrid = value;
    }

}
