/**
 * 
 */
package it.sara.esternalizzazione.business;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.sara.esternalizzazione.db.bean.AzioneCorrettiva;
import it.sara.esternalizzazione.db.bean.BeneficioConseguibile;
import it.sara.esternalizzazione.db.bean.ControlloOperativo;
import it.sara.esternalizzazione.db.bean.EsitoControllo;
import it.sara.esternalizzazione.db.bean.Periodicita;
import it.sara.esternalizzazione.db.bean.PianoReinternalizzazione;
import it.sara.esternalizzazione.db.bean.RischiBC;
import it.sara.esternalizzazione.db.bean.RischiositaEsternalizzazione;
import it.sara.esternalizzazione.db.bean.TipoControllo;
import it.sara.esternalizzazione.db.bean.TipoEsternalizzazione;
import it.sara.esternalizzazione.db.bean.VerificaConflittiInteressi;
import it.sara.esternalizzazione.db.dao.AzioneCorrettivaDao;
import it.sara.esternalizzazione.db.dao.BeneficioConseguibileDao;
import it.sara.esternalizzazione.db.dao.ControlloOperativoDao;
import it.sara.esternalizzazione.db.dao.EsitoControlloDao;
import it.sara.esternalizzazione.db.dao.PeriodicitaDao;
import it.sara.esternalizzazione.db.dao.PianoReinternalizzazioneDao;
import it.sara.esternalizzazione.db.dao.RischiBCDao;
import it.sara.esternalizzazione.db.dao.RischiositaEsternalizzazioneDao;
import it.sara.esternalizzazione.db.dao.TipoControlloDao;
import it.sara.esternalizzazione.db.dao.TipoEsternalizzazioneDao;
import it.sara.esternalizzazione.db.dao.VerificaConflittiInteressiDao;
import it.sara.esternalizzazione.web.bean.AzioniCorrettiveWeb;
import it.sara.esternalizzazione.web.bean.BeneficiConseguibiliWeb;
import it.sara.esternalizzazione.web.bean.ControlloOperativoWeb;
import it.sara.esternalizzazione.web.bean.EsitiControlloWeb;
import it.sara.esternalizzazione.web.bean.PeriodicitaWeb;
import it.sara.esternalizzazione.web.bean.PianiReinternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.RischiBCWeb;
import it.sara.esternalizzazione.web.bean.RischiositaEsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.TipiControlloWeb;
import it.sara.esternalizzazione.web.bean.TipoEsternalizzazioneWeb;
import it.sara.esternalizzazione.web.bean.VerificheConflittiInteressiWeb;

/**
 * @author f.vescovi
 *
 */
@Component("GestioneTipologicheEsternalizzazione")
public class GestioneTipologicheEsternalizzazione {
	

	@Autowired
	TipoEsternalizzazioneDao tipoEsternalizzazioneDao;
	
	@Autowired
	BeneficioConseguibileDao beneficioConseguibileDao;
	
	@Autowired
	RischiositaEsternalizzazioneDao rischiositaEsternalizzazioneDao;
	
	@Autowired
	VerificaConflittiInteressiDao verificaConflittiInteressiDao;
	
	@Autowired
	RischiBCDao rischiBCDao;
	
	@Autowired
	PianoReinternalizzazioneDao pianoReinternalizzazioneDao;
	
	@Autowired
	TipoControlloDao tipoControlloDao;
	
	@Autowired
	PeriodicitaDao periodicitaDao;
	
	@Autowired
	AzioneCorrettivaDao azioneCorrettivaDao;

	@Autowired
	EsitoControlloDao esitoControlloDao;
	
	@Autowired
	ControlloOperativoDao controlloOperativoDao;
	
	
	public List<TipoEsternalizzazioneWeb> getTipiEsternalizzazione(){
		List<TipoEsternalizzazione> l =  this.tipoEsternalizzazioneDao.findAll();
		List<TipoEsternalizzazioneWeb> lista = new LinkedList<TipoEsternalizzazioneWeb>();
		for (TipoEsternalizzazione t : l) {
			if (lista == null)lista = new LinkedList<TipoEsternalizzazioneWeb>();
			lista.add(new TipoEsternalizzazioneWeb(t));
		}
		return lista;
	}
	
	
	public List<BeneficiConseguibiliWeb> getBenefici(){

		List<BeneficioConseguibile> l =  this.beneficioConseguibileDao.findAll();
		List<BeneficiConseguibiliWeb> lista = new LinkedList<BeneficiConseguibiliWeb>();
		for (BeneficioConseguibile t : l) {
			if (lista == null)lista = new LinkedList<BeneficiConseguibiliWeb>();
			lista.add(new BeneficiConseguibiliWeb(t));
		}	
		return lista;
	}

	
	

	public List<RischiositaEsternalizzazioneWeb> getRischiositaEsternalizzazione() {

		List<RischiositaEsternalizzazione> l =  this.rischiositaEsternalizzazioneDao.findAll();
		List<RischiositaEsternalizzazioneWeb> lista = new LinkedList<RischiositaEsternalizzazioneWeb>();
		for (RischiositaEsternalizzazione t : l) {
			if (lista == null)lista = new LinkedList<RischiositaEsternalizzazioneWeb>();
			lista.add(new RischiositaEsternalizzazioneWeb(t));
		}
		return lista;
	}


	

	public List<VerificheConflittiInteressiWeb> getVerificaConflittiInteressi() {
		List<VerificaConflittiInteressi> l =  this.verificaConflittiInteressiDao.findAll();
		List<VerificheConflittiInteressiWeb> lista = new LinkedList<VerificheConflittiInteressiWeb>();
		for (VerificaConflittiInteressi t : l) {
			if (lista == null)lista = new LinkedList<VerificheConflittiInteressiWeb>();
			lista.add(new VerificheConflittiInteressiWeb(t));
		}
		return lista;
	}



	public List<RischiBCWeb> getRischiBC() {
		List<RischiBC> l =  this.rischiBCDao.findAll();
		List<RischiBCWeb> lista = new LinkedList<RischiBCWeb>();
		for (RischiBC t : l) {
			if (lista == null)lista = new LinkedList<RischiBCWeb>();
			lista.add(new RischiBCWeb(t));
		}
		return lista;
	}



	public List<PianiReinternalizzazioneWeb> getPianiReinternalizzazione() {
		List<PianoReinternalizzazione> l =  this.pianoReinternalizzazioneDao.findAll();		
		List<PianiReinternalizzazioneWeb> lista = new LinkedList<PianiReinternalizzazioneWeb>();
		for (PianoReinternalizzazione t : l) {
			if (lista == null)lista = new LinkedList<PianiReinternalizzazioneWeb>();
			lista.add(new PianiReinternalizzazioneWeb(t));
		}
		return lista;
	}



	public List<TipiControlloWeb> getTipiControllo() {
		List<TipoControllo> l =  this.tipoControlloDao.findAll();
		List<TipiControlloWeb> lista = new LinkedList<TipiControlloWeb>();
		for (TipoControllo t : l) {
			if (lista == null)lista = new LinkedList<TipiControlloWeb>();
			lista.add(new TipiControlloWeb(t));
		}
		return lista;
	}



	public List<PeriodicitaWeb> getPeriodicita() {
		List<Periodicita> l =  this.periodicitaDao.findAll();
		List<PeriodicitaWeb> lista = new LinkedList<PeriodicitaWeb>();
		for (Periodicita t : l) {
			if (lista == null)lista = new LinkedList<PeriodicitaWeb>();
			lista.add(new PeriodicitaWeb(t));
		}
		return lista;
	}



	public List<AzioniCorrettiveWeb> getAzioniCorrettive() {
		List<AzioneCorrettiva> l =  this.azioneCorrettivaDao.findAll();
		List<AzioniCorrettiveWeb> lista = new LinkedList<AzioniCorrettiveWeb>();
		for (AzioneCorrettiva t : l) {
			if (lista == null)lista = new LinkedList<AzioniCorrettiveWeb>();
			lista.add(new AzioniCorrettiveWeb(t));
		}
		return lista;
	}



	public List<EsitiControlloWeb> getEsitiControllo() {
		List<EsitoControllo> l =  this.esitoControlloDao.findAll();
		List<EsitiControlloWeb> lista = new LinkedList<EsitiControlloWeb>();
		for (EsitoControllo t : l) {
			if (lista == null)lista = new LinkedList<EsitiControlloWeb>();
			lista.add(new EsitiControlloWeb(t));
		}
		return lista;
	}

	
	public List<ControlloOperativoWeb> getListaRC() {
		List<ControlloOperativo> l =  this.controlloOperativoDao.findAllRC();
		List<ControlloOperativoWeb> lista = new LinkedList<ControlloOperativoWeb>();
		for (ControlloOperativo t : l) {
			if (lista == null)lista = new LinkedList<ControlloOperativoWeb>();
			lista.add(new ControlloOperativoWeb(t));
		}
		return lista;
	}

	public List<ControlloOperativoWeb> getListaUO() {
		List<ControlloOperativo> l =  this.controlloOperativoDao.findAllUO();
		List<ControlloOperativoWeb> lista = new LinkedList<ControlloOperativoWeb>();
		for (ControlloOperativo t : l) {
			if (lista == null)lista = new LinkedList<ControlloOperativoWeb>();
			lista.add(new ControlloOperativoWeb(t));
		}
		return lista;
	}

	

}
