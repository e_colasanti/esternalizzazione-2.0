//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.11 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.08.07 alle 11:52:51 AM CEST 
//


package it.sara.esternalizzazione.ws.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RagSoc1" type="{urn:sap-com:document:sap:rfc:functions}char35"/&gt;
 *         &lt;element name="TbWest" type="{urn:sap-com:document:sap:soap:functions:mc-style}TableOfZwestDatifornit" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ragSoc1",
    "tbWest"
})
@XmlRootElement(name = "ZwsWestDatiFornitore")
public class ZwsWestDatiFornitore {

    @XmlElement(name = "RagSoc1", required = true)
    protected String ragSoc1;
    @XmlElement(name = "TbWest")
    protected TableOfZwestDatifornit tbWest;

    /**
     * Recupera il valore della proprietà ragSoc1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRagSoc1() {
        return ragSoc1;
    }

    /**
     * Imposta il valore della proprietà ragSoc1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRagSoc1(String value) {
        this.ragSoc1 = value;
    }

    /**
     * Recupera il valore della proprietà tbWest.
     * 
     * @return
     *     possible object is
     *     {@link TableOfZwestDatifornit }
     *     
     */
    public TableOfZwestDatifornit getTbWest() {
        return tbWest;
    }

    /**
     * Imposta il valore della proprietà tbWest.
     * 
     * @param value
     *     allowed object is
     *     {@link TableOfZwestDatifornit }
     *     
     */
    public void setTbWest(TableOfZwestDatifornit value) {
        this.tbWest = value;
    }

}
