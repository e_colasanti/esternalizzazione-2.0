/**
 * 
 */
package it.sara.esternalizzazione.business.fase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author f.vescovi
 *
 */
@Component("FaseValutazioneServizioOutSourcing")
public class FaseValutazioneServizioOutSourcing {
	
	
	private Logger logger = LoggerFactory.getLogger(FaseValutazioneServizioOutSourcing.class);
	
	public FaseValutazioneServizioOutSourcing() {}

}
