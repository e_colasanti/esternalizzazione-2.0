package it.sara.esternalizzazione.db.bean;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
//@Table(name="WEST_OGGETTO")
public class Oggetto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6128298639066296902L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GENERA_ID")
    @SequenceGenerator(name="SEQ_GENERA_ID", sequenceName="SEQ_GENERA_ID", allocationSize=1)
	@Column(name="ID_OGGETTO")
	Integer idOggetto;
	
	@Column(name="DESCRIZIONE_OGGETTO")
	String descrizioneOggetto;

	public Integer getIdOggetto() {
		return idOggetto;
	}

	public void setIdOggetto(Integer idOggetto) {
		this.idOggetto = idOggetto;
	}

	public String getDescrizioneOggetto() {
		return descrizioneOggetto;
	}

	public void setDescrizioneOggetto(String descrizioneOggetto) {
		this.descrizioneOggetto = descrizioneOggetto;
	}

	@Override
	public String toString() {
		return "Oggetto [idOggetto=" + idOggetto + ", descrizioneOggetto=" + descrizioneOggetto + "]";
	}
	

}
