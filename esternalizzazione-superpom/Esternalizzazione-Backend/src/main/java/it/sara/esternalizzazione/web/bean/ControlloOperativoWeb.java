package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import it.sara.esternalizzazione.db.bean.ControlloOperativo;

public class ControlloOperativoWeb  extends TipoListaWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4104341023068046783L;

	public ControlloOperativoWeb(ControlloOperativo controlloOperativo) {
		this.setCodice(controlloOperativo.getIdControlloOperativo()+"");
		this.setDescrizione(controlloOperativo.getRegistrazione());
	}
}
