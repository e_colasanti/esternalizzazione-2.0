package it.sara.esternalizzazione.business.task;

import org.springframework.beans.factory.annotation.Autowired;

import it.sara.esternalizzazione.business.GestioneMail;
import it.sara.esternalizzazione.business.GestioneTipoTask;
import it.sara.esternalizzazione.business.GestioneTipoUtente;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.dao.TaskDao;


public abstract class AbstractTaskFactory {
	
	@Autowired
	protected TaskDao taskDao;

	@Autowired
	protected GestioneMail gestioneMail;


	@Autowired
	protected GestioneTipoUtente gestioneTipoUtente;
	
	@Autowired
	protected GestioneTipoTask gestioneTipoTask;

	
	public Task createTask(Esternalizzazione esternalizzazione) {

		return null; 
	}
	
	
	public Task createTask(Esternalizzazione esternalizzazione,String descTask,String tipoUtente) {

		return null; 
	}
	
	

}
