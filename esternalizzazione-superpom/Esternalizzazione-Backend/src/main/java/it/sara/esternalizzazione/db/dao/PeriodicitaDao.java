package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.sara.esternalizzazione.db.bean.Periodicita;

public interface PeriodicitaDao extends CrudRepository<Periodicita,Integer>{
	public Periodicita findByIdPeriodicita(Integer idPeriodicita);
	public List<Periodicita> findAll();

}
