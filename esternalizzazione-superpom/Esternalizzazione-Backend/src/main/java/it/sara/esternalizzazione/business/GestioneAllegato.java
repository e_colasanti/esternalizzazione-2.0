package it.sara.esternalizzazione.business;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import it.sara.esternalizzazione.db.bean.Allegato;
import it.sara.esternalizzazione.db.bean.Esternalizzazione;
import it.sara.esternalizzazione.db.bean.Task;
import it.sara.esternalizzazione.db.dao.AllegatoDao;
import it.sara.esternalizzazione.db.dao.EsternalizzazioneDao;
import it.sara.esternalizzazione.db.dao.TaskDao;
import it.sara.esternalizzazione.utility.Utility;
import it.sara.esternalizzazione.utility.WestCostants;
import it.sara.esternalizzazione.web.bean.AllegatoWeb;



@Component("gestioneAllegato")
public class GestioneAllegato {

	private Logger logger = LoggerFactory.getLogger(GestioneAllegato.class);


	@Autowired
	AllegatoDao allegatoDao;

	@Autowired
	EsternalizzazioneDao esternalizzazioneDao;

	@Autowired
	TaskDao taskDao;

	@Transactional(rollbackFor=Exception.class)
	public AllegatoWeb salvaAllegato(MultipartFile fileCaricato,
			Integer idEsternalizzazione,
			String descrizioneOggetto,
			String ambito) {
		AllegatoWeb risultato = null;
		Esternalizzazione esternalizzazione = null;
		Task task = null;
		List<Allegato>	list=null;
		String contesto=null;

		try {


			if(idEsternalizzazione!=0)
			{

				esternalizzazione=esternalizzazioneDao.findByIdEsternalizzazione(idEsternalizzazione);
				list= allegatoDao.findAllegatiByIdEsternalizzazione(idEsternalizzazione);	
			}




			if(!fileCaricato.isEmpty())
			{

				Allegato alle=null;

				logger.debug("Set info dell'allegato...");
				alle = new Allegato();
				alle.setMimeType(fileCaricato.getContentType());
				alle.setNomeAllegato(fileCaricato.getOriginalFilename());
				alle.setSizeAllegato(fileCaricato.getSize()+"");
				alle.setContenuto(fileCaricato.getBytes());
				alle.setDataInserimento(new Date());
				alle.setTipoAllegato(null);
				alle.setUtenteInserimento(SecurityContextHolder.getContext().getAuthentication().getName());
				alle.setEsternalizzazione(esternalizzazione);
				alle.setTask(task);
				if(ambito.contains("PIANO"))
					alle.setTipoAmbito(WestCostants.TIPO_AMBITO_ALLEGATO_PIANO_REINTERNALIZZAZIONE);
				else if(ambito.contains("CONTROLLO"))	
					alle.setTipoAmbito(WestCostants.TIPO_AMBITO_ALLEGATO_CONTROLLO);					
				else if(ambito.contains("SLA"))	
					alle.setTipoAmbito(WestCostants.TIPO_AMBITO_ALLEGATO_SLA);
				else if(ambito.contains("NOMINA"))	
					alle.setTipoAmbito(WestCostants.TIPO_AMBITO_ALLEGATO_LETTERA_NOMINA_DIPENDENTE);
				else if(ambito.contains("TASK"))	
					alle.setTipoAmbito(WestCostants.TIPO_AMBITO_ALLEGATO_TASK);	
				else if(ambito.contains("CONTRATTO"))
					alle.setTipoAmbito(WestCostants.TIPO_AMBITO_ALLEGATO_CONTRATTO);
				else if(ambito.contains("OGGETTO"))
					alle.setTipoAmbito(WestCostants.TIPO_AMBITO_ALLEGATO_OGGETTO_ATTIVITA);
				else if(ambito.contains("MOTIVAZIONE"))
					alle.setTipoAmbito(WestCostants.TIPO_AMBITO_ALLEGATO_MOTIVAZIONE);
				
				

				alle.setContesto(descrizioneOggetto);
				alle.setAmbito(ambito);


				logger.debug("Salvataggio allegato su db: " + alle.toString());
				allegatoDao.save(alle);


				risultato = new AllegatoWeb();
				risultato.setDimensione(new Long(alle.getSizeAllegato()));
				risultato.setNomeFileOrigine(alle.getNomeAllegato());
				risultato.setContentType(alle.getMimeType());
				risultato.setIdAllegato(alle.getIdAllegato());
				risultato.setContesto(alle.getContesto());
				risultato.setAmbito(alle.getAmbito());

				logger.debug("Return dell'oggetto web...");
			}



		} catch (IllegalStateException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());			
		}
		return risultato;

	}


	@Transactional(rollbackFor=Exception.class)
	public void rimuoviAllegato(Integer idAllegato) {

		Allegato alle = null;
		logger.debug("Recupero dell'allegato con idAllegato: "+idAllegato);
		logger.debug("Get dell'allegato...");
		alle = allegatoDao.findOne(idAllegato);
		allegatoDao.delete(alle);
	}


	public Allegato getAllegatoById(Integer idAllegato) {

		logger.debug("Recupero dell'allegato con idAllegato: "+idAllegato);
		logger.debug("Get dell'allegato...");
		return allegatoDao.findOne(idAllegato);
	}



	public List<AllegatoWeb> findAllegatiByIdEsternalizzazioneAndTipoAmbito(Integer idEsternalizzazione,String tipoAmbito) {

		List<Allegato> alle = null;

		logger.debug("Get della lista degli allegati per id Esternalizzazione...: "+idEsternalizzazione);

		alle=allegatoDao.findAllegatiByIdEsternalizzazioneAndTipoAmbito(idEsternalizzazione, tipoAmbito);

		if (alle == null)return null;

		logger.debug("Trovati "+alle.size()+" allegati.");
		List<AllegatoWeb> risultato = new LinkedList<AllegatoWeb>();
		for (Allegato a: alle) {
			AllegatoWeb aw = new AllegatoWeb();
			aw.setContentType(a.getMimeType());
			aw.setDimensione(new Long(a.getSizeAllegato()));
			aw.setIdAllegato(a.getIdAllegato());
			aw.setNomeFileOrigine(a.getNomeAllegato());
			risultato.add(aw);
		}

		return risultato;
	}


	public List<AllegatoWeb> getListaAllegatiByIdTask(Integer idTask,String tipoAmbito) {

		List<Allegato> alle = null;

		logger.debug("Get della lista degli allegati per id Task ...: "+idTask);

		alle=allegatoDao.findAllegatoByIdTaskAndTipoAmbito(idTask, tipoAmbito);

		if (alle == null)return null;

		logger.debug("Trovati "+alle.size()+" allegati");
		List<AllegatoWeb> risultato = new LinkedList<AllegatoWeb>();
		for (Allegato a: alle) {
			AllegatoWeb aw = new AllegatoWeb();
			aw.setContentType(a.getMimeType());
			aw.setDimensione(new Long(a.getSizeAllegato()));
			aw.setIdAllegato(a.getIdAllegato());
			aw.setNomeFileOrigine(a.getNomeAllegato());
			risultato.add(aw);
		}
		return risultato;
	}


	public List<AllegatoWeb> getAllAllegati(Integer idOggetto,String ambito) {

		List<Allegato> alle = null;
		List<AllegatoWeb> risultato =null;

		if(idOggetto!=0)
		{
			if(ambito.equals(WestCostants.ALLEGATO_ESTERNALIZZAZIONE))
				alle=allegatoDao.findAllegatiByIdEsternalizzazione(idOggetto);
			else if(ambito.equals(WestCostants.ALLEGATO_TASK))				
				alle=allegatoDao.findAllegatiByIdTask(idOggetto);

			if (alle == null)return null;

			logger.debug("Trovati "+alle.size()+" allegati.");
			risultato = new LinkedList<AllegatoWeb>();
			for (Allegato a: alle) {
				AllegatoWeb aw = new AllegatoWeb();
				aw.setContentType(a.getMimeType());
				aw.setDimensione(new Long(a.getSizeAllegato()));
				aw.setIdAllegato(a.getIdAllegato());
				aw.setNomeFileOrigine(a.getNomeAllegato());
				aw.setAmbito(a.getAmbito());
				aw.setContesto(a.getContesto());
				risultato.add(aw);
			}



		}


		return risultato;
	}


}
