package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import it.sara.esternalizzazione.db.bean.Periodicita;

public class PeriodicitaWeb extends TipoListaWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3247841556995981232L;

	public PeriodicitaWeb(Periodicita periodicita) {
		this.setCodice(periodicita.getIdPeriodicita()+"");
		this.setDescrizione(periodicita.getDescrizionePeriodicita());
	}
	
}
