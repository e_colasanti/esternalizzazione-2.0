package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import it.sara.esternalizzazione.db.bean.RischiBC;

public class RischiBCWeb extends TipoListaWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7061291329450031460L;
	
	public RischiBCWeb(RischiBC rischiBC) {
		this.setCodice(rischiBC.getIdRischiBC()+"");
		this.setDescrizione(rischiBC.getDescrizioneRischiBC());
	}
}
