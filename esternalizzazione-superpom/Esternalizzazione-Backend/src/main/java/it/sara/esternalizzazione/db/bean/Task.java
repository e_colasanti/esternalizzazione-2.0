package it.sara.esternalizzazione.db.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="WEST_TASK")
public class Task implements Serializable {
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7327987280181896661L;
	/**
	 * 
	 */


	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_TASK")
    @SequenceGenerator(name="SEQ_TASK", sequenceName="SEQ_TASK", allocationSize=1)
	@Column(name="ID_TASK")
	Integer idTask;
	
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GENERA_PROG_LAV")
    @SequenceGenerator(name="SEQ_GENERA_PROG_LAV", sequenceName="SEQ_GENERA_PROG_LAV", allocationSize=1)
	@Column(name="PROG_LAV_TASK")
	Integer progLavTask;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_TASK")
	TipoTask tipoTask;
	
	@Column(name="UTENTE_OWNER")
	String utenteOwner;
	
	
	@Column(name="UTENTE_RESPONSABILE")
	String utenteResponsabile;
	
	
	@Column(name="DATA_CREAZIONE")
	Date dataCreazione;
	
	
	@Column(name="DATA_FINE")
	Date dataFine;
	

	@Column(name="DATA_ASSEGNAZIONE")
	Date dataAssegnazione;

	
	@Column(name="STATO")
	String stato;
	
	
	@Column(name="DESCRIZIONE")
	String descrizione;
	
	@Column(name="UTENTE_DELEGATO")
	String utenteDelegato;
	
	
	@Column(name="TIPO_CAMPO_ATTIVATO_EST")
	String tipoCampoAttivatoEst;
	
	@Column(name="FASE_ESTERNALIZZAZIONE")
	String faseEsternalizzazione;
	
	@Column(name="CODICE_DIREZIONE")
	String codiceDirezione;
	
		
	@Column(name="BLOCCANTE")
	String bloccante;
	
	@Column(name="FASE_BLOCCANTE")
	String faseBloccante;
	
	
	 @Lob
	 @Column(name = "ALLEGATI", columnDefinition="BLOB")
	 byte[] allegati; 
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ESTERNALIZZAZIONE")
	Esternalizzazione esternalizzazione;


	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_UTENTE")
	TipoUtente tipoUtente;



	/**
	 * @return the idTask
	 */
	public Integer getIdTask() {
		return idTask;
	}


	/**
	 * @param idTask the idTask to set
	 */
	public void setIdTask(Integer idTask) {
		this.idTask = idTask;
	}


	/**
	 * @return the progLavTask
	 */
	public Integer getProgLavTask() {
		
		
		return progLavTask;
	}


	/**
	 * @param progLavTask the progLavTask to set
	 */
	public void setProgLavTask(Integer progLavTask) {
		this.progLavTask = progLavTask;
	}


	/**
	 * @return the tipoTask
	 */
	public TipoTask getTipoTask() {
		return tipoTask;
	}


	/**
	 * @param tipoTask the tipoTask to set
	 */
	public void setTipoTask(TipoTask tipoTask) {
		this.tipoTask = tipoTask;
	}


	/**
	 * @return the utenteOwner
	 */
	public String getUtenteOwner() {
		return utenteOwner;
	}


	/**
	 * @param utenteOwner the utenteOwner to set
	 */
	public void setUtenteOwner(String utenteOwner) {
		this.utenteOwner = utenteOwner;
	}


	/**
	 * @return the dataCreazione
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}


	/**
	 * @param dataCreazione the dataCreazione to set
	 */
	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}


	/**
	 * @return the dataFine
	 */
	public Date getDataFine() {
		return dataFine;
	}


	/**
	 * @param dataFine the dataFine to set
	 */
	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}


	/**
	 * @return the stato
	 */
	public String getStato() {
		return stato;
	}


	/**
	 * @param stato the stato to set
	 */
	public void setStato(String stato) {
		this.stato = stato;
	}



	/**
	 * @return the descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}


	/**
	 * @param descrizione the descrizione to set
	 */
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}


	/**
	 * @return the esternalizzazione
	 */
	public Esternalizzazione getEsternalizzazione() {
		return esternalizzazione;
	}


	/**
	 * @param esternalizzazione the esternalizzazione to set
	 */
	public void setEsternalizzazione(Esternalizzazione esternalizzazione) {
		this.esternalizzazione = esternalizzazione;
	}


	public String getUtenteResponsabile() {
		return utenteResponsabile;
	}


	public void setUtenteResponsabile(String utenteResponsabile) {
		this.utenteResponsabile = utenteResponsabile;
	}


	public Date getDataAssegnazione() {
		return dataAssegnazione;
	}


	public void setDataAssegnazione(Date dataAssegnazione) {
		this.dataAssegnazione = dataAssegnazione;
	}


	/**
	 * @return the utenteDelegato
	 */
	public String getUtenteDelegato() {
		return utenteDelegato;
	}


	/**
	 * @param utenteDelegato the utenteDelegato to set
	 */
	public void setUtenteDelegato(String utenteDelegato) {
		this.utenteDelegato = utenteDelegato;
	}


	/**
	 * @return the allegati
	 */
	public byte[] getAllegati() {
		return allegati;
	}


	/**
	 * @param allegati the allegati to set
	 */
	public void setAllegati(byte[] allegati) {
		this.allegati = allegati;
	}


	/**
	 * @return the tipoUtente
	 */
	public TipoUtente getTipoUtente() {
		return tipoUtente;
	}


	/**
	 * @param tipoUtente the tipoUtente to set
	 */
	public void setTipoUtente(TipoUtente tipoUtente) {
		this.tipoUtente = tipoUtente;
	}


	/**
	 * @return the tipoCampoAttivatoEst
	 */
	public String getTipoCampoAttivatoEst() {
		return tipoCampoAttivatoEst;
	}


	/**
	 * @param tipoCampoAttivatoEst the tipoCampoAttivatoEst to set
	 */
	public void setTipoCampoAttivatoEst(String tipoCampoAttivatoEst) {
		this.tipoCampoAttivatoEst = tipoCampoAttivatoEst;
	}


	/**
	 * @return the faseEsternalizzazione
	 */
	public String getFaseEsternalizzazione() {
		return faseEsternalizzazione;
	}


	/**
	 * @param faseEsternalizzazione the faseEsternalizzazione to set
	 */
	public void setFaseEsternalizzazione(String faseEsternalizzazione) {
		this.faseEsternalizzazione = faseEsternalizzazione;
	}


	public String getCodiceDirezione() {
		return codiceDirezione;
	}


	public void setCodiceDirezione(String codiceDirezione) {
		this.codiceDirezione = codiceDirezione;
	}


	/**
	 * @return the bloccante
	 */
	public String getBloccante() {
		return bloccante;
	}


	/**
	 * @param bloccante the bloccante to set
	 */
	public void setBloccante(String bloccante) {
		this.bloccante = bloccante;
	}


	/**
	 * @return the faseBloccante
	 */
	public String getFaseBloccante() {
		return faseBloccante;
	}


	/**
	 * @param faseBloccante the faseBloccante to set
	 */
	public void setFaseBloccante(String faseBloccante) {
		this.faseBloccante = faseBloccante;
	}
	
	
	

}
