package it.sara.esternalizzazione.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import  org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.sara.esternalizzazione.business.GestioneUtente;
import it.sara.esternalizzazione.web.bean.UtenteWeb;


@RestController
@RequestMapping("/rest/utente")
@CrossOrigin(origins="*")
public class UtenteController extends BaseController{
	
	private Logger logger = LoggerFactory.getLogger(UtenteController.class);

	@Autowired 
	GestioneUtente gestioneUtente;
//	
	@SuppressWarnings("unchecked")
	@RequestMapping(value={"/username"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public UtenteWeb userSession( HttpServletResponse response,
			HttpServletRequest request){
		logger.debug("Autentication user: "+SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		logger.debug("Username: "+SecurityContextHolder.getContext().getAuthentication().getName());
		UtenteWeb utenteWeb = getUtente();
				
//		if (utenteWeb == null) {
//            logger.error("utente non connesso");
//            return new ResponseEntity(new CustomErrorType("Utente non connesso"), HttpStatus.NOT_FOUND);
//        }
		logger.debug("##### COOKIE: "+request.getHeader("Cookie"));
		String cookie=request.getHeader("Cookie");
		if(cookie!=null || !("").equals(cookie))
		{
			utenteWeb.setCookie(cookie);
			utenteWeb.setSessionID(request.getSession().getId());
		}
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Cookie",request.getHeader("Cookie"));
		return utenteWeb;
		//return new ResponseEntity<UtenteWeb>(utenteWeb,responseHeaders ,HttpStatus.OK);
	}

	@RequestMapping(value={"/roleUtente"},  method=RequestMethod.GET, produces= {MediaType.APPLICATION_JSON_VALUE})
	public List<String> ruoliUtente(){

		List<String> risultato = getUtente().getRuoli();
		logger.debug("Ruoli utente: "+risultato);
		return risultato;
	}	
}
