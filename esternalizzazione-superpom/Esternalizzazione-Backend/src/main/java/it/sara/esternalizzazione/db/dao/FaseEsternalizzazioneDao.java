package it.sara.esternalizzazione.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.sara.esternalizzazione.db.bean.FaseEsternalizzazione;

public interface FaseEsternalizzazioneDao extends  CrudRepository<FaseEsternalizzazione, Integer> {
	
	
	public FaseEsternalizzazione findByIdFaseEsternalizzazione(Integer idFaseEsternalizzazione);
	public List<FaseEsternalizzazione> findAll();
	
	@Query("select fe from FaseEsternalizzazione fe where fe.esternalizzazione.idEsternalizzazione=:idEsternalizzazione and fe.tipoFase.descTipoFase=:descTipoFase" )	
	public FaseEsternalizzazione findEsternalizzazioneByTipoFase(@Param("idEsternalizzazione")Integer idEsternalizzazione,@Param("descTipoFase")String descTipoFase);

	
	@Query("select fe from FaseEsternalizzazione fe where fe.utenteResponsabile=:utenteResponsabile" )
	List<FaseEsternalizzazione> findFaseEsternalizzazioneByUtenteResponsabile(@Param("utenteResponsabile")String utenteResponsabile);


	@Query("select fe from FaseEsternalizzazione fe where fe.utenteResponsabile=:utenteResponsabile and fe.utenteDelegato=:utenteDelegato" )
	List<FaseEsternalizzazione> findFaseEsternalizzazioneByUtenteResponsabileAndUtenteDelegato(@Param("utenteResponsabile")String utenteResponsabile,@Param("utenteDelegato")String utenteDelegato );
	
	
	@Query("select fe from FaseEsternalizzazione fe where fe.esternalizzazione.idEsternalizzazione=:idEsternalizzazione and fe.utenteDelegato=:utenteDelegato" )
	List<FaseEsternalizzazione> findFaseEsternalizzazioneByIdEsternalizzazioneAndUtenteDelegato(@Param("idEsternalizzazione")Integer idEsternalizzazione,@Param("utenteDelegato")String utenteDelegato );

	
	
}
