/**
 * 
 */
package it.sara.esternalizzazione.db.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author f.vescovi
 *
 */
@Entity
@Table(name="WEST_CONTROLLO")
public class Controllo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8599707102564743529L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_CONTROLLO")
    @SequenceGenerator(name="SEQ_CONTROLLO", sequenceName="SEQ_CONTROLLO", allocationSize=1)
	@Column(name="ID_CONTROLLO")
	Integer idControllo;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_CONTROLLO")
	TipoControllo tipoControllo;
	
	
	@Column(name="DESCRIZIONE_CONTROLLO")
	String descrizioneControllo;

	
	@Column(name="DATA_INIZIO_CONTROLLO")
	Date dataInizioControllo;
	
	@Column(name="DATA_FINE_CONTROLLO")
	Date dataFineControllo;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PERIODICITA")
	Periodicita periodicita;
	
	@Column(name="VINCOLO_PAGAMENTO_FATTURA")
	String vincoloPagamentoFattura;


	@Column(name="RESPONSABILE_SINGOLO_CONTROLLO")
	String responsabileSingoloControllo;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ESITO_CONTROLLO")
	EsitoControllo esitoControllo;
	
	@Column(name="DESCRIZIONE_ESITO_CONTROLLO")
	String descrizioneEsitoControllo;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_AZIONE_CORRETTIVA")
	AzioneCorrettiva azioneCorrettiva;
	
	
	@Column(name="NOTA_AZIONE_CORRETTIVA")
	String notaAzioneCorrettiva;
	

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ESTERNALIZZAZIONE")
	Esternalizzazione esternalizzazione;
	
	public Integer getIdControllo() {
		return idControllo;
	}


	public void setIdControllo(Integer idControllo) {
		this.idControllo = idControllo;
	}


	public TipoControllo getTipoControllo() {
		return tipoControllo;
	}


	public void setTipoControllo(TipoControllo tipoControllo) {
		this.tipoControllo = tipoControllo;
	}


	public String getDescrizioneControllo() {
		return descrizioneControllo;
	}


	public void setDescrizioneControllo(String descrizioneControllo) {
		this.descrizioneControllo = descrizioneControllo;
	}


	public Date getDataInizioControllo() {
		return dataInizioControllo;
	}


	public void setDataInizioControllo(Date dataInizioControllo) {
		this.dataInizioControllo = dataInizioControllo;
	}


	public Date getDataFineControllo() {
		return dataFineControllo;
	}


	public void setDataFineControllo(Date dataFineControllo) {
		this.dataFineControllo = dataFineControllo;
	}


	public Periodicita getPeriodicita() {
		return periodicita;
	}


	public void setPeriodicita(Periodicita periodicita) {
		this.periodicita = periodicita;
	}


	public String getVincoloPagamentoFattura() {
		return vincoloPagamentoFattura;
	}


	public void setVincoloPagamentoFattura(String vincoloPagamentoFattura) {
		this.vincoloPagamentoFattura = vincoloPagamentoFattura;
	}


	public String getResponsabileSingoloControllo() {
		return responsabileSingoloControllo;
	}


	public void setResponsabileSingoloControllo(String responsabileSingoloControllo) {
		this.responsabileSingoloControllo = responsabileSingoloControllo;
	}


	public EsitoControllo getEsitoControllo() {
		return esitoControllo;
	}


	public void setEsitoControllo(EsitoControllo esitoControllo) {
		this.esitoControllo = esitoControllo;
	}


	public String getDescrizioneEsitoControllo() {
		return descrizioneEsitoControllo;
	}


	public void setDescrizioneEsitoControllo(String descrizioneEsitoControllo) {
		this.descrizioneEsitoControllo = descrizioneEsitoControllo;
	}


	public AzioneCorrettiva getAzioneCorrettiva() {
		return azioneCorrettiva;
	}


	public void setAzioneCorrettiva(AzioneCorrettiva azioneCorrettiva) {
		this.azioneCorrettiva = azioneCorrettiva;
	}


	public String getNotaAzioneCorrettiva() {
		return notaAzioneCorrettiva;
	}


	public void setNotaAzioneCorrettiva(String notaAzioneCorrettiva) {
		this.notaAzioneCorrettiva = notaAzioneCorrettiva;
	}

	
	/**
	 * @return the esternalizzazione
	 */
	public Esternalizzazione getEsternalizzazione() {
		return esternalizzazione;
	}


	/**
	 * @param esternalizzazione the esternalizzazione to set
	 */
	public void setEsternalizzazione(Esternalizzazione esternalizzazione) {
		this.esternalizzazione = esternalizzazione;
	}
	
	

}
