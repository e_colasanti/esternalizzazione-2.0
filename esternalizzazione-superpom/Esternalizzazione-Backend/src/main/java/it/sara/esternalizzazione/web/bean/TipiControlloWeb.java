package it.sara.esternalizzazione.web.bean;

import java.io.Serializable;

import it.sara.esternalizzazione.db.bean.TipoControllo;

public class TipiControlloWeb extends TipoListaWeb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4360676113501366693L;
	
	public TipiControlloWeb(TipoControllo tipoControllo) {
		this.setCodice(tipoControllo.getIdTipoControllo()+"");
		this.setDescrizione(tipoControllo.getDescrizioneTipoControllo());
	}

}
