import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaseValutazioneComponent } from './Components/fase-valutazione/fase-valutazione.component';
import { FasePredisposizioneComponent } from './Components/fase-predisposizione/fase-predisposizione.component';
import { FaseScadOutsourcingComponent } from './Components/fase-scad-outsourcing/fase-scad-outsourcing.component';
import { EstrazioneComponent } from './Components/estrazione/estrazione.component';
import { HomeComponent } from './Components/home/home.component';
import { FaseValOutsourcingComponent } from './Components/fase-val-outsourcing/fase-val-outsourcing.component';
import { ListaTaskApplicativiComponent } from './components/lista-task-applicativi/lista-task-applicativi.component';
import { DettaglioEsternalizzazioneComponent } from './components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component';
import { DettaglioTaskComponent } from './components/task/dettaglio-task/dettaglio-task.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  {path: '' ,                           component: HomeComponent                        },
  {path: 'home',                        component: HomeComponent                        },
  {path: 'fase-valutazione',            component: FaseValutazioneComponent             },
  {path: 'fase-predisposizione',        component: FasePredisposizioneComponent         },
  {path: 'fase-val-outsourcing',        component: FaseValOutsourcingComponent          },
  {path: 'fase-scad-outsourcing',       component: FaseScadOutsourcingComponent         },
  {path: 'estrazione',                  component: EstrazioneComponent                  },
  {path: 'lista-task-applicativi',      component: ListaTaskApplicativiComponent        },
  {path: 'esternalizzazione-dett/:id',  component: DettaglioEsternalizzazioneComponent  },
  {path: 'esternalizzazione-new',       component: DettaglioEsternalizzazioneComponent  },
  {path: 'task-details/:id' ,           component: DettaglioTaskComponent               },
  {path: 'login',                       component: LoginComponent                       }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
