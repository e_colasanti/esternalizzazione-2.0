import { Component } from '@angular/core';
import { FornitoriService } from './services/esternalizzazione/fornitori/fornitori.service';
import { VariabiliGlobali } from './variabili-globali/variabili-globali';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Esternalizzazione';
  log: boolean;

  cookieValue = '';

  constructor (private fornitoriService: FornitoriService , private varG: VariabiliGlobali, private cookieService: CookieService) {
    this.log = false;
    this.getListaFornitori('a');

  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit(): void {

    this.cookieValue = this.cookieService.get('Cookie');
    console.log('@@@@@@ COOKIE VALUE: ' + this.cookieValue);
  }

  logIn(login: boolean) {
    this.log = login;
  }

  getListaFornitori (ambito: string) {
    this.fornitoriService.getListaFornitori(ambito).subscribe( ( res ) => {
      this.varG.setFornitori(res);
    });
  }

}
