import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstrazioneComponent } from './estrazione.component';

describe('EstrazioneComponent', () => {
  let component: EstrazioneComponent;
  let fixture: ComponentFixture<EstrazioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstrazioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstrazioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
