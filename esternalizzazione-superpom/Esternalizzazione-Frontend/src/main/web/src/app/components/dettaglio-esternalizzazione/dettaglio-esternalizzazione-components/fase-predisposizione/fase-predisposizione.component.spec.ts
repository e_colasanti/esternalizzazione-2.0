import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FasePredisposizioneDettComponent } from './fase-predisposizione.component';

describe('FasePredisposizioneComponent', () => {
  let component: FasePredisposizioneDettComponent;
  let fixture: ComponentFixture<FasePredisposizioneDettComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FasePredisposizioneDettComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FasePredisposizioneDettComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
