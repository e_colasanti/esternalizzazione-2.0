        import { Component, OnInit } from '@angular/core';
        import { UserService } from 'src/app/services/user-service/user.service';
        import { Utente } from 'src/app/interfaces/utente';
        import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';
        import { CookieService } from 'ngx-cookie-service';

        @Component({
        selector: 'app-user',
        templateUrl: './user.component.html',
        styleUrls: ['./user.component.css']
        })
        export class UserComponent implements OnInit {
        user: Utente;
        resp: boolean;

        constructor(private userService: UserService , private cookie: CookieService, private varG: VariabiliGlobali) {
            console.log('UserService');
            this.resp = false;
            this.getUser();
        }

        ngOnInit() {
        }

        getUser() {
            this.userService.getUser().subscribe( ( res ) => {

            if (this.user === undefined) {
            console.log('Utente nuovo connesso in sessione ');
            this.user = res;
            this.varG.setUtente(this.user);
            this.cookie.set('Cookie', '');
            this.cookie.set('Cookie', this.user.cookie);
            this.resp = true;
            } else {
            if ( this.user.username === res.username && this.user.sessionID !== res.sessionID) {
            this.user.sessionID = res.sessionID;
            this.cookie.set('Cookie', '');
            this.cookie.set('Cookie', res.cookie);
            } else { console.log('Utente connesso: ' + res.username ); }
            }
            });



        }

        }
