import { Component, OnInit } from '@angular/core';
import { Utente } from 'src/app/interfaces/utente';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
  selector: 'app-fase-val-outsourcing',
  templateUrl: './fase-val-outsourcing.component.html',
  styleUrls: ['./fase-val-outsourcing.component.css']
})
export class FaseValOutsourcingComponent implements OnInit {

    user: Utente;
    view: boolean;
    constructor(private userService: UserService) {

    this.view = false;
    this.userService.getUser().subscribe( (res) => {
      this.user = res;
      this.view = true;
    });
  }

  ngOnInit() {
  }

}
