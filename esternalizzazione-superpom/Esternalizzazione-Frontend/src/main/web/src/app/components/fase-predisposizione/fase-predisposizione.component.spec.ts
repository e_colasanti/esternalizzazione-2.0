import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FasePredisposizioneComponent } from './fase-predisposizione.component';

describe('FasePredisposizioneComponent', () => {
  let component: FasePredisposizioneComponent;
  let fixture: ComponentFixture<FasePredisposizioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FasePredisposizioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FasePredisposizioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
