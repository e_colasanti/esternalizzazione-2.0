import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseValOutsourcingComponent } from './fase-val-outsourcing.component';

describe('FaseValOutsourcingComponent', () => {
  let component: FaseValOutsourcingComponent;
  let fixture: ComponentFixture<FaseValOutsourcingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaseValOutsourcingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaseValOutsourcingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
