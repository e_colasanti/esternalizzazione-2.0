import { Component, OnInit } from '@angular/core';
import { Utente } from 'src/app/interfaces/utente';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
  selector: 'app-fase-predisposizione',
  templateUrl: './fase-predisposizione.component.html',
  styleUrls: ['./fase-predisposizione.component.css']
})
export class FasePredisposizioneComponent implements OnInit {

  routerLink: string;
  view: boolean;
  user: Utente;

  constructor(private userService: UserService) {
    this.view = false;

    this.userService.getUser().subscribe( (res) => {
      this.user = res;
      this.view = true;
    });
  }

  ngOnInit() {
  }


}
