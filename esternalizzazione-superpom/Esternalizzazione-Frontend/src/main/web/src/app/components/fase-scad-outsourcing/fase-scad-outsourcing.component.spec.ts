import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseScadOutsourcingComponent } from './fase-scad-outsourcing.component';

describe('FaseScadOutsourcingComponent', () => {
  let component: FaseScadOutsourcingComponent;
  let fixture: ComponentFixture<FaseScadOutsourcingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaseScadOutsourcingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaseScadOutsourcingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
