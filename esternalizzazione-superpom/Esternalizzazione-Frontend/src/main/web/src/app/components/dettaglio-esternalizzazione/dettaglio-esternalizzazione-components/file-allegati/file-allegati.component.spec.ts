import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileAllegatiComponent } from './file-allegati.component';

describe('FileAllegatiComponent', () => {
  let component: FileAllegatiComponent;
  let fixture: ComponentFixture<FileAllegatiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileAllegatiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileAllegatiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
