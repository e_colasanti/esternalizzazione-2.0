import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';

@Component({
  selector: 'app-actions-bar-task',
  templateUrl: './actions-bar-task.component.html',
  styleUrls: ['./actions-bar-task.component.css']
})
export class ActionsBarTaskComponent implements OnInit {

  edit: boolean;
  routerLink: string;

  @Output() editOutput = new EventEmitter<boolean>();

  constructor(private varG: VariabiliGlobali ) {
    this.edit = true;
    this.routerLink = this.varG.getRouterLink().valueOf();
   }

  ngOnInit() {
  }

  activeEditMode() {
    this.edit = false;
    this.editOutput.emit(false);
  }

  saveChanges() {
    this.edit = true;
    this.editOutput.emit(true);
  }
}
