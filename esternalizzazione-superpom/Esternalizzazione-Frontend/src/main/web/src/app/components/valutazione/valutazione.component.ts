import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { PageEvent, MatPaginator, MatTableDataSource } from '@angular/material';
import { Esternalizzazioni } from 'src/app/interfaces/esternalizzazioni';
import { FaseValutazioneService } from 'src/app/services/fase-valutazione/fase-valutazione.service';
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';
import { Esternalizzazione } from 'src/app/interfaces/refactoredInterfaces/esternalizzazione';

@Component({
  selector: 'app-valutazione',
  templateUrl: './valutazione.component.html',
  styleUrls: ['./valutazione.component.css']
})
export class ValutazioneComponent implements OnInit {
  @Output()page: EventEmitter<PageEvent>;

  listaEsternalizzazioni: Esternalizzazioni;

  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: Number;
  length: Number;

  displayedColumns: string[] = ['ID', 'DATA CREAZIONE', 'TITOLO', 'RESPONSABILE', 'nuovaRda'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  view: boolean;
  resp: boolean;
  notResp: boolean;

  constructor(private service: FaseValutazioneService , private varG: VariabiliGlobali) {
    this.view = false;
    this.resp = false;
    this.notResp = false;
  }

  dataSource: MatTableDataSource<Esternalizzazione> = null;

  ngOnInit() {
    this.getValutazione(null);
  }

  getValutazione(event?: PageEvent) {

    if (event === null) {
      this.pageIndex = 0;
      this.pageSize = 5;
      this.firstTimeGet();
    } else {
      this.pageIndex = event.pageIndex;
      this.pageSize = event.pageSize;
      this.get();
    }

  }

  viewDetails() {
    this.varG.setPosizione('valutazione');
  }

  firstTimeGet() {
    setTimeout( () => {
      this.get();
    }, 1000);
  }

  get() {
    this.service.getFaseValutazione( this.pageIndex, this.pageSize ).subscribe(res => {
      this.listaEsternalizzazioni = res;

      this.view = true;

      if (this.listaEsternalizzazioni.totaleRecord === 0) {
          this.notResp = true;
      } else {
        this.resp = true;
      }

      if (this.dataSource === null) {
          this.dataSource = new MatTableDataSource<Esternalizzazione>(this.listaEsternalizzazioni.listaEsternalizzazioni);
          this.dataSource.paginator = this.paginator;
          this.pageIndex = this.listaEsternalizzazioni.paginaCorrente;
          this.length = this.listaEsternalizzazioni.totaleRecord;
      } else {
          this.pageIndex = this.listaEsternalizzazioni.paginaCorrente;
          this.length = this.listaEsternalizzazioni.totaleRecord;
          this.dataSource.data = this.listaEsternalizzazioni.listaEsternalizzazioni;
          this._updatePaginator(this.paginator.length , this.pageIndex);
        }
    });
  }

  _updatePaginator(filteredDataLength: number, pageIndex: number) {
    Promise.resolve().then(() => {
      this.paginator.pageIndex = pageIndex;

      if (!this.paginator) { return; }
      this.paginator.length = filteredDataLength;
      // If the page index is set beyond the page, reduce it to the last page.
      if (this.paginator.pageIndex > 0) {
        const lastPageIndex = Math.ceil(this.paginator.length / this.paginator.pageSize) - 1 || 0;
        this.paginator.pageIndex = Math.min(this.paginator.pageIndex, lastPageIndex);
      }
    });
  }

}
