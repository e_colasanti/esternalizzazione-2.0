import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { FormBuilder , FormGroup } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output() logged = new EventEmitter<boolean>();

  login: FormGroup;
  constructor(private fb: FormBuilder) {
   }

  ngOnInit() {
    this.login = this.fb.group({
      username: '',
      password: ''
    });
  }

  checkLog() {
    this.logged.emit(true);
  }

}
