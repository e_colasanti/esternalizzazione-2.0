import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseValutazioneComponent } from './fase-valutazione.component';

describe('FaseValutazioneComponent', () => {
  let component: FaseValutazioneComponent;
  let fixture: ComponentFixture<FaseValutazioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaseValutazioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaseValutazioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
