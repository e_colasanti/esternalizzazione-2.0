import { Component, OnInit, Input } from '@angular/core';
import { GetTaskService } from 'src/app/services/lista-task/get-task/get-task.service';
import { ListaAttivita } from '../../../interfaces/lista-attivita';
import { TipologicheTaskSelectService } from 'src/app/services/lista-task/tipologiche-task/tipologiche-task.service';
import { ListaWeb } from 'src/app/interfaces/lista-web';
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';
import { FileTaskService } from 'src/app/services/File-Service/file-task/file-task.service';
import { File } from 'src/app/interfaces/file';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dettaglio-task',
  templateUrl: './dettaglio-task.component.html',
  styleUrls: ['./dettaglio-task.component.css']
})
export class DettaglioTaskComponent implements OnInit {
    task: ListaAttivita;
    view: boolean;
    listaTipologiaTask: ListaWeb[];
    listaTipiUtente: ListaWeb[];

    edit: boolean;
    fileTask: File[];
    id: string;

  constructor(private taskService: GetTaskService , private tipologicheService: TipologicheTaskSelectService ,
              private varG: VariabiliGlobali , private fileService: FileTaskService, private route: ActivatedRoute) {

      this.fileTask = [];
      this.edit = true;
      this.view = false;
   }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');

        if (this.varG.getPosizione().valueOf() === 'home') {
          this.varG.setRouterLink('/home');
      } else if (this.varG.getPosizione().valueOf() === 'lista-task-app') {
          this.varG.setRouterLink('/lista-task-applicativi');
      }
    this.getListaTipiUtente();
    this.getListaTipologiaTask();
    this.getFileTask();
    this.getTask();
  }


  getFileTask() {
    this.fileService.getListaFile().subscribe( ( res ) => {
      this.fileTask = res;
    });
  }

  getTask() {
      this.taskService.getTask(this.id).subscribe( (res) => {
          this.task = res;
          this.view = true;
      });
  }

  getListaTipologiaTask() {
    this.tipologicheService.getListaTipologiaTask().subscribe( (res) => {
      this.listaTipologiaTask = res;
    });
  }

  getListaTipiUtente() {
    this.tipologicheService.getListaTipiUtente().subscribe( (res) => {
      this.listaTipiUtente = res;
    });
  }
}
