import { Component, OnInit} from '@angular/core';
import { FindByIdEService } from 'src/app/services/findById/esternalizzazione/find-by-id-e.service';

// tslint:disable-next-line:max-line-length
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';
import { HttpService } from 'src/app/services/http-service/http.service';
import { Esternalizzazione } from 'src/app/interfaces/refactoredInterfaces/esternalizzazione';
import { ActivatedRoute } from '@angular/router';

const viewO = 'view';
const editO = 'edit';
const saveO = 'salva';

@Component({
  selector: 'app-dettaglio-esternalizzazione',
  templateUrl: './dettaglio-esternalizzazione.component.html',
  styleUrls: ['./dettaglio-esternalizzazione.component.css']
})

export class DettaglioEsternalizzazioneComponent implements OnInit {

  esternalizzazione: Esternalizzazione;
  view: boolean;

  saveNewButton: boolean;

  button: boolean;

  posizione: string;

  edit: string;

  selectedIndex: number;

  routerLink: string;
  id: string;


  constructor(private service: FindByIdEService ,
    private varG: VariabiliGlobali , private http: HttpService , private route: ActivatedRoute ) {

    this.button = true;
    this.view = false;
    this.edit = viewO;
    this.saveNewButton = false;
    this.routerLink = '/home';

    this.posizione = this.varG.getPosizione();

  }

  ngOnInit() {

    this.id = this.route.snapshot.paramMap.get('id');

    if (this.id === 'new') {
      this.selectedIndex = 0;
      this.routerLink = '/home';
      this.button = false;
      this.saveNewButton = true;
      this.edit = editO;
      this.getNewEsternalizzazione();
    } else {
      if (this.posizione.valueOf() === 'home') {
        this.routerLink = '/home';
      } else if (this.posizione.valueOf() === 'valutazione') {
        this.routerLink = '/fase-valutazione';
      } else if (this.posizione.valueOf() === 'predisposizione') {
        this.routerLink = '/fase-predisposizione';
      }
      this.getEsternalizzazione();
    }
  }

  selectIndex() {
    if (this.esternalizzazione.stato === '1') {
      this.selectedIndex = 0;
    } else if (this.esternalizzazione.stato === '2') {
      this.selectedIndex = 1;
    } else {
      this.selectedIndex = 2;
    }
  }

  activeEditMode() {
    if (this.esternalizzazione.stato === '3') {
      if ((this.varG.getUtente().zzenteacq === '052' || this.varG.getUtente().zzenteacq === '059')) {
      this.button = false;
      this.edit = editO;
      } else {
        this.button = true;
        this.edit = viewO;
        alert('permesso negato');
        }
      } else {
      this.button = false;
      this.edit = editO;
    }

  }

  saveChanges() {
    this.button = true;
    this.edit = saveO;
  }

  saveNewChanges() {
    this.edit = saveO;
    setTimeout(() => {
      this.edit = '';
    }, 500);
  }

  getNewEsternalizzazione() {
    this.http.doGet('rest/esternalizzazione/new').subscribe( (res) => {
      this.esternalizzazione = res;
      this.view = true;
      this.edit = editO;
    });
  }

  getEsternalizzazione() {
   setTimeout(() => {
    this.service.findEsternalizzazione(this.id).subscribe( (res) => {
      this.esternalizzazione = res;
      this.selectIndex();
      this.view = true;
     });
   }, 300);
  }
}


