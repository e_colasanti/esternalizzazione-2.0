import { Component, OnInit, Input, SimpleChanges, ViewChild } from '@angular/core';
import { TipologicheSelectService } from 'src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service';
import { ListaWeb } from 'src/app/interfaces/lista-web';
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';
import { Utente } from 'src/app/interfaces/utente';
import { SalvaEsternalizzazioneService } from 'src/app/services/esternalizzazione/salva-esternalizzazione.service';
import { Esternalizzazione } from 'src/app/interfaces/refactoredInterfaces/esternalizzazione';
import { Controllo } from 'src/app/interfaces/refactoredInterfaces/controllo';
import { ListaConoREService } from 'src/app/services/lista-conoRE/lista-cono-re.service';
import { ListaConoRE } from 'src/app/interfaces/lista-cono-re';

@Component({
  selector: 'app-fase-predisposizione-dett',
  templateUrl: './fase-predisposizione.component.html',
  styleUrls: ['./fase-predisposizione.component.css']
})
export class FasePredisposizioneDettComponent implements OnInit {

  @Input() public esternalizzazione: Esternalizzazione;

  @Input() public edit: string;

  singoloControllo: Controllo = {
    azioneCorrettive: '',
    dataFineControllo: '',
    dataInizioControllo: '',
    descrizioneAzioneCorrettiva: '',
    descrizioneEsitoControllo: '',
    esitoControllo: '',
    notaAzioneCorrettiva: '',
    responsabileSingoloControllo: '',
    tipoControllo: '',
    vincoloPagamentoFattura: '',
    descrEsitoControllo: '',
    descrizioneTipoControllo: '',
    idControllo: '',
    idEsternalizzazione: '',
    descrizioneControllo: ''
  };

  displayedColumns: string[] = ['TIPO', 'DATA INIZIO' , 'DATA FINE' ,
    'VINCOLO PER IL PAGAMENTO FATTURA' , 'RESPONSABILE SINGOLO CONTROLLO' , 'AZIONI CORRETTIVE' , 'NOTA AZIONI CORRETTIVE'];

  // (utente.zzenteacq=='052' || utente.zzenteacq=='059')

  listaPeriodicita: ListaWeb[];
  editCombinato: boolean;
  utente: Utente;
  saved: boolean;
  gif: boolean;
  savedMessage: boolean;
  listaControlli: boolean;
  listaConoRE: ListaConoRE[];
  listaTipiControllo: ListaWeb[];
  listaEsitoControllo: ListaWeb[];
  listaAzioniCorrettice: ListaWeb[];

  constructor(private tipologicheService: TipologicheSelectService ,
    private varG: VariabiliGlobali , private salvaEsternalizzazioneService: SalvaEsternalizzazioneService,
    private conoREService: ListaConoREService) {

    this.listaControlli = true;
    this.utente = this.varG.getUtente();
    this.editCombinato = true;
    this.getListaAzioniCorrettive();
    this.getListaEsitoControllo();
    this.getlistaConoRE();
    this.getListaTipiControllo();
    this.getListaPeriodicita();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges) {
    if (this.esternalizzazione.stato.valueOf() === '3') {
      switch (this.edit) {
        case 'view': {
          this.editCombinato = true;
          console.log('view');
          break;
        }
        case 'edit' : {
         if (this.utente.zzenteacq === '052' || this.utente.zzenteacq === '059') {
          this.editCombinato = false;
          console.log('edit');
         } else {
          this.editCombinato = true;
         }
          break;
        }
        case 'salva' : {
         console.log('salva');
          this.editCombinato = true && (this.utente.zzenteacq === '052' || this.utente.zzenteacq === '059');
          if (this.editCombinato === false) {
            return;
          }
          this.salvaEsternalizzazione();
          break;
        }
        case '': {
          break;
        }
      }
    } else {
      this.editCombinato = true;
    }
 }

  ngOnInit() {
  }

  getListaAzioniCorrettive() {
    this.tipologicheService.getListaAzioniCorrettive().subscribe( (res) => {
      this.listaAzioniCorrettice = res;
    });
  }

  getListaEsitoControllo() {
    this.tipologicheService.getListaEsitiControllo().subscribe( ( res ) => {
      this.listaEsitoControllo = res;
    });
  }

  getListaTipiControllo() {
    this.tipologicheService.getListaTipiControllo().subscribe( ( res ) => {
      this.listaTipiControllo = res;
    });
  }

  getlistaConoRE() {
    this.conoREService.getListaConoRE().subscribe( ( res ) => {
      this.listaConoRE = res;
    });
  }

  getListaPeriodicita() {
    this.tipologicheService.getListaPeriodicita().subscribe( ( res ) => {
      this.listaPeriodicita = res;
    });
  }

  add() {
    this.listaControlli = false;
    this.singoloControllo = {
      azioneCorrettive: '',
      dataFineControllo: '',
      dataInizioControllo: '',
      descrizioneAzioneCorrettiva: '',
      descrizioneEsitoControllo: '',
      esitoControllo: '',
      notaAzioneCorrettiva: '',
      responsabileSingoloControllo: '',
      tipoControllo: '',
      vincoloPagamentoFattura: '',
      descrEsitoControllo: '',
      descrizioneTipoControllo: '',
      idControllo: '',
      idEsternalizzazione: '',
      descrizioneControllo: ''
    };
  }

  salvaModiche() {
    this.listaControlli = true;
    this.esternalizzazione.listaControlli.push(this.singoloControllo);
  }

  remove( i: number ) {
    console.log(i);
    this.esternalizzazione.listaControlli.splice( i , 1);
  }

  salvaEsternalizzazione() {
    if (this.esternalizzazione.stato !== '3') { return; }

    // this.inizializzaErrori();

    // if (this.edit.valueOf() === 'approva') {
    //   if (!this.validaCampi()) { return; }
    // }
    this.saved = true;
    this.gif = true;
    console.log('salvo');
    console.log(this.esternalizzazione);
    this.salvaEsternalizzazioneService.salva(this.esternalizzazione).subscribe( (res) => {
      this.gif = false;
      this.savedMessage = true;
      console.log('saved');
    });

  }
}
