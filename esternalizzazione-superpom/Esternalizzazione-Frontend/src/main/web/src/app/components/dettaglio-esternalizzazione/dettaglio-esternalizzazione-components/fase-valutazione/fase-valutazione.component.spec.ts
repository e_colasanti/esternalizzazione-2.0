import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseValutazioneDettComponent } from './fase-valutazione.component';

describe('FaseValutazioneDettComponent', () => {
  let component: FaseValutazioneDettComponent;
  let fixture: ComponentFixture<FaseValutazioneDettComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaseValutazioneDettComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaseValutazioneDettComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
