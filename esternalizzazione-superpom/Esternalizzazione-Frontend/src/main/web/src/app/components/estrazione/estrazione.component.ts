import { Component, OnInit } from '@angular/core';
import { FormBuilder , FormGroup, FormControl, Validators } from '@angular/forms';
import { ListaConoREService } from 'src/app/services/lista-conoRE/lista-cono-re.service';
import { ListaConoRE } from 'src/app/interfaces/lista-cono-re';

@Component({
  selector: 'app-estrazione',
  templateUrl: './estrazione.component.html',
  styleUrls: ['./estrazione.component.css']
})
export class EstrazioneComponent implements OnInit {

  listaConoRE: ListaConoRE[];
  estrazione: FormGroup;

  responsabileControllo = new FormControl('', [Validators.required]);
  responsabileSingoloControllo = new FormControl('', [Validators.required]);
  funzione = new FormControl('', [Validators.required]);


  funzioni: Funzioni [] = [
    {
      id: '008',
      funzione: 'AU'
    },
    {
      id: '057',
      funzione: 'CA'
    },
    {
      id: '052',
      funzione: 'FA'
    },
    {
      id: '059',
      funzione: 'FL'
    },
    {
      id: '024',
      funzione: 'RM'
    }
  ];


  constructor(private fb: FormBuilder , private service: ListaConoREService ) {
    this.get();
   }

  ngOnInit() {
    this.estrazione = this.fb.group({
      responsabileControllo: '',
      responsabileSingoloControllo: '',
      funzione: ''
    });
  }

  get() {
    this.service.getListaConoRE().subscribe( (res) => {
    this.listaConoRE = res;
    } );
  }

}

export interface Funzioni {
  id: string;
  funzione: string;
}
