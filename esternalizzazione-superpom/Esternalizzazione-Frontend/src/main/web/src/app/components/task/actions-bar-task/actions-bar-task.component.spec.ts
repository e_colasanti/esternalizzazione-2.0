import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionsBarTaskComponent } from './actions-bar-task.component';

describe('ActionsBarTaskComponent', () => {
  let component: ActionsBarTaskComponent;
  let fixture: ComponentFixture<ActionsBarTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionsBarTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionsBarTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
