import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScadOutsourcingComponent } from './scad-outsourcing.component';

describe('ScadOutsourcingComponent', () => {
  let component: ScadOutsourcingComponent;
  let fixture: ComponentFixture<ScadOutsourcingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScadOutsourcingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScadOutsourcingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
