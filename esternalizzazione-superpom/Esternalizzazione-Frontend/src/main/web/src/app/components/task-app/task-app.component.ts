import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { ListaTaskAppService } from 'src/app/services/lista-task-app/lista-task-app.service';
import { Attivita } from 'src/app/interfaces/attivita';
import { PageEvent, MatPaginator, MatTableDataSource } from '@angular/material';
import { ListaAttivita } from 'src/app/interfaces/lista-attivita';
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';

@Component({
  selector: 'app-task-app',
  templateUrl: './task-app.component.html',
  styleUrls: ['./task-app.component.css']
})
export class TaskAppComponent implements OnInit {

  listaAttivita: Attivita;
  @Output()page: EventEmitter<PageEvent>;

  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: Number;
  length: Number;

  displayedColumns: string[] = ['ID TASK' , 'DATA CREAZIONE' , 'UTENTE DELEGATO', 'RE'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  view: boolean;
  notResp: boolean;
  resp: boolean;

  constructor(private service: ListaTaskAppService , private varG: VariabiliGlobali) {
    this.view = false;
    this.resp = false;
    this.notResp = false;
  }

  dataSource: MatTableDataSource<ListaAttivita> = null;

  ngOnInit() {
    this.getListaTaskApp(null);
  }

  getListaTaskApp(event?: PageEvent) {

    if (event === null) {
      this.pageIndex = 0;
      this.pageSize = 5;
      this.firstTimeGet();
    } else {
      this.pageIndex = event.pageIndex;
      this.pageSize = event.pageSize;
      this.get();
    }
  }

  viewDetails(details: ListaAttivita) {
    this.varG.setIdTask(details.idTask.valueOf());
    this.varG.setPosizione('lista-task-app');
  }

  firstTimeGet() {
    setTimeout( () => {
      this.get();
    }, 1000);
  }

  get() {
    this.service.getListaTaskApp( this.pageIndex, this.pageSize ).subscribe(res => {
      this.listaAttivita = res;

      this.view = true;

      if (this.listaAttivita.totaleRecord === 0 ) {
        this.notResp = true;
      } else {
        this.resp = true;
      }

      if (this.dataSource === null) {
          this.dataSource = new MatTableDataSource<ListaAttivita>(this.listaAttivita.listaAttivita);
          this.dataSource.paginator = this.paginator;
          this.pageIndex = this.listaAttivita.paginaCorrente;
          this.length = this.listaAttivita.totaleRecord;
      } else {
          this.pageIndex = this.listaAttivita.paginaCorrente;
          this.length = this.listaAttivita.totaleRecord;
          this.dataSource.data = this.listaAttivita.listaAttivita;
          this._updatePaginator(this.paginator.length , this.pageIndex);
        }
    });
  }

  _updatePaginator(filteredDataLength: number, pageIndex: number) {
    Promise.resolve().then(() => {
      this.paginator.pageIndex = pageIndex;

      if (!this.paginator) { return; }
      this.paginator.length = filteredDataLength;
      // If the page index is set beyond the page, reduce it to the last page.
      if (this.paginator.pageIndex > 0) {
        const lastPageIndex = Math.ceil(this.paginator.length / this.paginator.pageSize) - 1 || 0;
        this.paginator.pageIndex = Math.min(this.paginator.pageIndex, lastPageIndex);
      }
    });
  }

}
