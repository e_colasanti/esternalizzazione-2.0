import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseInserimentoComponent } from './fase-inserimento.component';

describe('FaseInserimentoComponent', () => {
  let component: FaseInserimentoComponent;
  let fixture: ComponentFixture<FaseInserimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaseInserimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaseInserimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
