import { Component, OnInit, ElementRef } from '@angular/core';
import { File } from 'src/app/interfaces/file';
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { HttpService } from 'src/app/services/http-service/http.service';
import { ActivatedRoute } from '@angular/router';
import { FileEsternalizzazione } from 'src/app/services/File-Service/file-nomina-responsabile-controllo/file-esternalizzazione.service';


// define the constant url we would be uploading to.
const URL = 'http://localhost:8080/West/rest/file/upload';

@Component({
  selector: 'app-file-allegati',
  templateUrl: './file-allegati.component.html',
  styleUrls: ['./file-allegati.component.css']
})
export class FileAllegatiComponent implements OnInit {

  fileNomina: File[];
  filePiano: File[];
  fileControllo: File[];
  fileContratto: File[];
  fileOggetto: File[];
  fileMotivazione: File[];
  returnedFileList: File[];
  new: boolean;

  view: boolean;

  cookie: string;
  token: string;

  idEsternalizzazione: string;

  constructor(private httpService: HttpService, private el: ElementRef, private route: ActivatedRoute ,
    private fileService: FileEsternalizzazione) {

      this.fileNomina = [];
      this.filePiano = [];
      this.fileControllo = [];
      this.fileContratto = [];
      this.fileOggetto = [];
      this.fileMotivazione = [];
      this.view = false;
      this.new = true;
     }

    // declare a property called fileuploader and assign it to an instance of a new fileUploader.
    // pass in the Url to be uploaded to, and pass the itemAlais, which would be the name of the //file input when sending the post request.
    public uploader: FileUploader;
    // This is the default title property created by the angular cli. Its responsible for the app works

    title = 'app works!';

    ngOnInit() {

            this.idEsternalizzazione = this.route.snapshot.paramMap.get('id');
            console.log('this.idEsternalizzazione: ' + this.idEsternalizzazione);

          if (this.idEsternalizzazione !== 'new') {
            this.getFile();
          } else {
            this.new = false;
          }
    }


     // the function which handles the file upload without using a plugin.
    upload(ambito: string, id: number) {
    // locate the file element meant for the file upload.
        const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#uploadFile' + id );
    // get the total amount of files attached to the file input.
        const fileCount: number = inputEl.files.length;
        console.log('Numero di file: ' + fileCount);

        console.log('AMBITO: ' + ambito);

    // create a new fromdata instance
        const formData = new FormData();
    // check if the filecount is greater than zero, to be sure a file was selected.
                formData.append('uploadFile', inputEl.files.item(0));
                formData.append('idEsternalizzazione', this.idEsternalizzazione );
                formData.append('descrizioneOggetto', 'ESTERNALIZZAZIONE');
                formData.append('ambito', ambito );
                this.uploadFile(formData);
       }


            uploadFile(data: FormData) {
                console.log('FORM DATA: ' + data);

                this.httpService.doPostFile('rest/file/upload' , data).subscribe( (res) => {
                  console.log('SALVATAGGIO FILE:: ' + res.message);
                  console.log('STATUS:: ' + res.status);

                  this.getFile();
                }, (err) => {
                  console.log('ERRORE:: ' + err.message);
                  console.log('STATUS:: ' + err.status);

                });
            }


            rimuoviAllegato(file: File , i: number , ) {
                console.log('remove');
                 this.httpService.doGet('rest/file/remove/' + file.idAllegato).subscribe( (res) => {
                  console.log('RIMOSSO:: ' + res.message);
                    this.getFile();
                }, (err) => {
                  console.log('ERRORE:: ' + err.message);
                });

            }


  getFile() {
    this.view = false;
    this.fileNomina = [];
    this.filePiano = [];
    this.fileControllo = [];
    this.fileContratto = [];
    this.fileOggetto = [];
    this.fileMotivazione = [];

    console.log('get file');
    this.fileService.getListaFile(this.idEsternalizzazione).subscribe( (res) => {
      this.returnedFileList = res;

      this.returnedFileList.forEach(file => {
        switch (file.ambito) {
              case 'NOMINA': {
                this.fileNomina.push(file);
                break;
              }
              case 'PIANO' : {
                this.filePiano.push(file);
                break;
              }
              case 'CONTROLLO' : {
                this.fileControllo.push(file);
                break;
              }
              case 'CONTRATTO' : {
                this.fileContratto.push(file);
                break;
              }
              case 'OGGETTO' : {
                this.fileOggetto.push(file);
                break;
            }
              case 'MOTIVAZIONE' : {
                this.fileMotivazione.push(file);
               break;
            }
            }
        });
        this.view = true;
    });
  }

}
