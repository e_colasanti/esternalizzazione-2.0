import { Component, OnInit } from '@angular/core';
import { Utente } from 'src/app/interfaces/utente';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  routerLink: string;
  user: Utente;
  view: boolean;
  constructor(private userService: UserService) {

    this.view = false;
    this.userService.getUser().subscribe( (res) => {
      this.user = res;
      this.view = true;
    });
  }

  ngOnInit() {
  }
}
