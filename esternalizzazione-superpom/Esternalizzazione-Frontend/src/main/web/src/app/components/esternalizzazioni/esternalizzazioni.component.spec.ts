import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsternalizzazioniComponent } from './esternalizzazioni.component';

describe('EsternalizzazioniComponent', () => {
  let component: EsternalizzazioniComponent;
  let fixture: ComponentFixture<EsternalizzazioniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsternalizzazioniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsternalizzazioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
