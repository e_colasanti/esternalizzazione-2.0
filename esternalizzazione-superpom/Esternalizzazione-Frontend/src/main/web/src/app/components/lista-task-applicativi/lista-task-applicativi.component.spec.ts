import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTaskApplicativiComponent } from './lista-task-applicativi.component';

describe('ListaTaskApplicativiComponent', () => {
  let component: ListaTaskApplicativiComponent;
  let fixture: ComponentFixture<ListaTaskApplicativiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTaskApplicativiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTaskApplicativiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
