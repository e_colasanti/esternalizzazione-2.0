import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredisposizioneComponent } from './predisposizione.component';

describe('PredisposizioneComponent', () => {
  let component: PredisposizioneComponent;
  let fixture: ComponentFixture<PredisposizioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredisposizioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredisposizioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
