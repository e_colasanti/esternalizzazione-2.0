import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DettaglioEsternalizzazioneComponent } from './dettaglio-esternalizzazione.component';

describe('DettaglioEsternalizzazioneComponent', () => {
  let component: DettaglioEsternalizzazioneComponent;
  let fixture: ComponentFixture<DettaglioEsternalizzazioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DettaglioEsternalizzazioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DettaglioEsternalizzazioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
