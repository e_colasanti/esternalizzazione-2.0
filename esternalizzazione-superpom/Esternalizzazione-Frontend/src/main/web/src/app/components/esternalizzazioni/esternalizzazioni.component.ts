import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { ListaEsternalizzazioniService } from 'src/app/services/esternalizzazione/lista-esternalizzazioni.service';
import { Esternalizzazioni } from 'src/app/interfaces/esternalizzazioni';
import { MatPaginator, MatTableDataSource, PageEvent } from '@angular/material';
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';
import { UserService } from '../../services/user-service/user.service';
import { Esternalizzazione } from 'src/app/interfaces/refactoredInterfaces/esternalizzazione';

@Component({
  selector: 'app-esternalizzazioni',
  templateUrl: './esternalizzazioni.component.html',
  styleUrls: ['./esternalizzazioni.component.css']
})
export class EsternalizzazioniComponent implements OnInit {

  @Output()page: EventEmitter<PageEvent>;

  listaEsternalizzazioni: Esternalizzazioni;

  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: Number;
  length: Number;

  routerParam: string;

  displayedColumns: string[] = ['ID', 'DATA CREAZIONE', 'TITOLO', 'RESPONSABILE', 'nuovaRda'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  view: boolean;
  resp: boolean;
  notResp: boolean;

  constructor(private service: ListaEsternalizzazioniService , private varG: VariabiliGlobali, private userService: UserService) {
    this.routerParam = '/esternalizzazione-dett/';
    this.view = false;
    this.resp = false;
    this.notResp = false;
  }

  dataSource: MatTableDataSource<Esternalizzazione> = null;

  ngOnInit() {
    this.userService.getUser();
    this.getEsternalizzazioni(null);
  }

  getEsternalizzazioni(event?: PageEvent) {

    if (event === null) {
      this.pageIndex = 0;
      this.pageSize = 5;
      this.firstTimeGet();
    } else {
      this.pageIndex = event.pageIndex;
      this.pageSize = event.pageSize;
      this.get();
    }

  }

  viewDetails() {
    this.varG.setPosizione('home');
  }

  get() {

    this.service.getEsternalizzazioni( this.pageIndex, this.pageSize ).subscribe( res => {
        this.listaEsternalizzazioni = res;

      this.view = true;

      if (this.listaEsternalizzazioni.totaleRecord === 0) {
          this.notResp = true;
      } else {
        this.resp = true;
      }

      if (this.dataSource === null) {
          this.dataSource = new MatTableDataSource<Esternalizzazione>(this.listaEsternalizzazioni.listaEsternalizzazioni);
          this.dataSource.paginator = this.paginator;
          this.pageIndex = this.listaEsternalizzazioni.paginaCorrente;
          this.length = this.listaEsternalizzazioni.totaleRecord;
      } else {
          this.pageIndex = this.listaEsternalizzazioni.paginaCorrente;
          this.length = this.listaEsternalizzazioni.totaleRecord;
          this.dataSource.data = this.listaEsternalizzazioni.listaEsternalizzazioni;
          this._updatePaginator(this.paginator.length , this.pageIndex);
        }
    });
  }

  firstTimeGet() {
    setTimeout( () => {
      this.get();
    }, 1000);
  }

  _updatePaginator(filteredDataLength: number, pageIndex: number) {
    Promise.resolve().then(() => {
      this.paginator.pageIndex = pageIndex;

      if (!this.paginator) { return; }
      this.paginator.length = filteredDataLength;

      if (this.paginator.pageIndex > 0) {
        const lastPageIndex = Math.ceil(this.paginator.length / this.paginator.pageSize) - 1 || 0;
        this.paginator.pageIndex = Math.min(this.paginator.pageIndex, lastPageIndex);
      }
    });
  }
}

