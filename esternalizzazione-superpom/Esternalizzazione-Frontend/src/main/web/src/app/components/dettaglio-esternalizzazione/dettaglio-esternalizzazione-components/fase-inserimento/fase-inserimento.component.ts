import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { TipologicheSelectService } from 'src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service';
import { ListaWeb } from 'src/app/interfaces/lista-web';
import { SalvaEsternalizzazioneService } from 'src/app/services/esternalizzazione/salva-esternalizzazione.service';
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';
import { Esternalizzazione } from 'src/app/interfaces/refactoredInterfaces/esternalizzazione';
import { UserService } from '../../../../services/user-service/user.service';
import { Utente } from 'src/app/interfaces/utente';


@Component({
  selector: 'app-fase-inserimento',
  templateUrl: './fase-inserimento.component.html',
  styleUrls: ['./fase-inserimento.component.css']
})
export class FaseInserimentoComponent implements OnInit , OnChanges {

  @Input() public esternalizzazione: Esternalizzazione;
  @Input() public edit: string;

  editCombinato: boolean;

  listaTipologiaEsternalizzazioni: ListaWeb[];
  uploadURL: string;
  username: string;

  errMotivazione: boolean;
  errTitolo: boolean;
  errTipo: boolean;
  saved: boolean;
  savedMessage: boolean;

  gif: boolean;

  user: Utente;


  constructor( private tipologicheService: TipologicheSelectService ,
    // tslint:disable-next-line:max-line-length
    private salvaEsternalizzazioneService: SalvaEsternalizzazioneService, private varG: VariabiliGlobali, private userService: UserService ) {

    this.saved = false;
    this.errMotivazione = false;
    this.errTipo = false;
    this.errTitolo = false;
    this.gif = false;
    this.savedMessage = false;

    this.editCombinato = true;
    this.getListaTipologiaEsternalizzazione();
   }

   ngOnChanges(changes: SimpleChanges) {

if (this.varG.getPosizione().valueOf() === 'nuova') {
      this.editCombinato = false;
    }
      switch (this.edit) {
        case 'view': {
          this.editCombinato = true;
          break;
        }
        case 'edit' : {
          if (this.esternalizzazione.stato === '1') {
          this.editCombinato = false;
          } else {
            this.editCombinato = true;
          }
          break;
        }
        case 'salva' : {
          if (this.esternalizzazione.stato === '1') {
              if (this.validaCampi()) {
                this.editCombinato = true;
                this.salvaEsternalizzazioneNew();
              } else {
              this.editCombinato = false;
              console.log('NON COMPILATI I CAMPI OBBLIGATORI');
              }
          }
          break;
        }
        case '': {
          break;
        }
      }

  }

  ngOnInit() {
    this.userService.getUser().subscribe( (res) => {
      this.user = res;
      this.username = this.user.username;
    });
  }

  salvaEsternalizzazioneNew() {
    this.esternalizzazione.codiceDirezione = this.varG.getUtente().zzenteacq;
    console.log('UTENTE: ' + this.username);
    this.esternalizzazione.compilataDa = this.username;

    this.saved = true;
    this.gif = true;
    this.salvaEsternalizzazioneService.salvaNew(this.esternalizzazione).subscribe( (res) => {
      this.gif = false;
      this.savedMessage = true;
      console.log('saved');
    });
  }

  getListaTipologiaEsternalizzazione() {
    this.tipologicheService.getListaTipologiaEsternalizzazione().subscribe( ( res ) => {
      this.listaTipologiaEsternalizzazioni = res;
    });
  }

  validaCampi(): boolean {

    this.errMotivazione = false;
    this.errTipo = false;
    this.errTitolo = false;

    if (this.esternalizzazione.motivazioneEsternalizzazione === null ||
      this.esternalizzazione.motivazioneEsternalizzazione === '') {
        this.errMotivazione = true;
      }
    if (this.esternalizzazione.titoloEsternalizzazione === null ||
      this.esternalizzazione.titoloEsternalizzazione === '') {
        this.errTitolo = true;
      }
    if (this.esternalizzazione.tipoEsternalizzazione === null ||
      this.esternalizzazione.tipoEsternalizzazione === '') {
        this.errTipo = true;
    }

    if (this.errMotivazione && this.errTipo && this.errTitolo) {
      return false;
    } else {
      return true;
    }
  }

}
