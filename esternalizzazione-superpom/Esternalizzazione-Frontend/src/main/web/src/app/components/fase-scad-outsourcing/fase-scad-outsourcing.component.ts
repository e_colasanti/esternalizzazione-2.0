import { Component, OnInit } from '@angular/core';
import { Utente } from 'src/app/interfaces/utente';
import { UserService } from 'src/app/services/user-service/user.service';

@Component({
  selector: 'app-fase-scad-outsourcing',
  templateUrl: './fase-scad-outsourcing.component.html',
  styleUrls: ['./fase-scad-outsourcing.component.css']
})
export class FaseScadOutsourcingComponent implements OnInit {

  user: Utente;
  view: boolean;
  constructor(private userService: UserService) {

    this.view = false;
    this.userService.getUser().subscribe( (res) => {
      this.user = res;
      this.view = true;
    });
  }
  ngOnInit() {
  }

}
