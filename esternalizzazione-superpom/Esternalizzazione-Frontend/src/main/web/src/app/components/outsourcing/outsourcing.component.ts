import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { OutsourcingService } from 'src/app/services/outsourcing/outsourcing.service';
import { MatTableDataSource, PageEvent, MatPaginator } from '@angular/material';
import { Esternalizzazioni } from 'src/app/interfaces/esternalizzazioni';
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';
import { Esternalizzazione } from 'src/app/interfaces/refactoredInterfaces/esternalizzazione';

@Component({
  selector: 'app-outsourcing',
  templateUrl: './outsourcing.component.html',
  styleUrls: ['./outsourcing.component.css']
})
export class OutsourcingComponent implements OnInit {

  @Output()page: EventEmitter<PageEvent>;

  listaEsternalizzazioni: Esternalizzazioni;

  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: Number;
  length: Number;

  displayedColumns: string[] = ['ID', 'DATA CREAZIONE', 'TITOLO', 'RESPONSABILE', 'nuovaRda'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  view: boolean;
  notResp: boolean;
  resp: boolean;

  constructor(private service: OutsourcingService , private varG: VariabiliGlobali) {
    this.view = false;
    this.resp = false;
    this.notResp = false;
  }

  dataSource: MatTableDataSource<Esternalizzazione> = null;

  ngOnInit() {
    this.getOutsourcing(null);
  }

  viewDetails() {
    this.varG.setPosizione('predisposizione');
  }

  getOutsourcing(event?: PageEvent) {

    if (event === null) {
      this.pageIndex = 0;
      this.pageSize = 5;
      this.firstTimeGet();
    } else {
      this.pageIndex = event.pageIndex;
      this.pageSize = event.pageSize;
      this.get();
    }

  }

  firstTimeGet() {
    setTimeout( () => {
      this.get();
    }, 1000);
  }


  get() {
    this.service.getOutsourcing( this.pageIndex, this.pageSize ).subscribe(res => {
      this.listaEsternalizzazioni = res;

      this.view = true;

      if (this.listaEsternalizzazioni.totaleRecord === 0) {
          this.notResp = true;
      } else {
        this.resp = true;
      }

      if (this.dataSource === null) {
          this.dataSource = new MatTableDataSource<Esternalizzazione>(this.listaEsternalizzazioni.listaEsternalizzazioni);
          this.dataSource.paginator = this.paginator;
          this.pageIndex = this.listaEsternalizzazioni.paginaCorrente;
          this.length = this.listaEsternalizzazioni.totaleRecord;
      } else {
          this.pageIndex = this.listaEsternalizzazioni.paginaCorrente;
          this.length = this.listaEsternalizzazioni.totaleRecord;
          this.dataSource.data = this.listaEsternalizzazioni.listaEsternalizzazioni;
          this._updatePaginator(this.paginator.length , this.pageIndex);
        }
    });
  }

  _updatePaginator(filteredDataLength: number, pageIndex: number) {
    Promise.resolve().then(() => {
      this.paginator.pageIndex = pageIndex;

      if (!this.paginator) { return; }
      this.paginator.length = filteredDataLength;
      // If the page index is set beyond the page, reduce it to the last page.
      if (this.paginator.pageIndex > 0) {
        const lastPageIndex = Math.ceil(this.paginator.length / this.paginator.pageSize) - 1 || 0;
        this.paginator.pageIndex = Math.min(this.paginator.pageIndex, lastPageIndex);
      }
    });
  }
}
