import { Component, OnInit } from '@angular/core';
import { Utente } from 'src/app/interfaces/utente';
import { UserService } from 'src/app/services/user-service/user.service';


@Component({
  selector: 'app-fase-valutazione',
  templateUrl: './fase-valutazione.component.html',
  styleUrls: ['./fase-valutazione.component.css']
})
export class FaseValutazioneComponent implements OnInit {


    view: boolean;
    user: Utente;

  constructor(private userService: UserService) {
    this.view = false;

    this.userService.getUser().subscribe( (res) => {
        this.user = res;
        this.view = true;
    });
  }

  ngOnInit() {
  }


}
