import { Component, OnInit, Input } from '@angular/core';
import { TipologicheSelectService } from 'src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service';
import { ListaWeb } from 'src/app/interfaces/lista-web';
import { ListaFornitori } from 'src/app/interfaces/lista-fornitori';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';
import { SalvaEsternalizzazioneService } from 'src/app/services/esternalizzazione/salva-esternalizzazione.service';
import { Esternalizzazione } from 'src/app/interfaces/refactoredInterfaces/esternalizzazione';
import { ListaConoREService } from 'src/app/services/lista-conoRE/lista-cono-re.service';
import { ListaConoRE } from 'src/app/interfaces/lista-cono-re';


@Component({
  selector: 'app-fase-valutazione-dett',
  templateUrl: './fase-valutazione.component.html',
  styleUrls: ['./fase-valutazione.component.css']
})
export class FaseValutazioneDettComponent implements OnInit {

  @Input() public esternalizzazione: Esternalizzazione;
  @Input() public edit: string;



  myControl = new FormControl();
  filteredOptions: Observable<ListaFornitori[]>;

  listaBefici: ListaWeb[];
  listaRischiositaEsternalizzazione: ListaWeb[];
  listaConflittiInteressi: ListaWeb[];
  listaRischiBC: ListaWeb[];
  viewSezioneControlli: boolean;
  editCombinato: boolean;
  listaFornitori: ListaFornitori[];
  value: string;

  gif: boolean;
  saved: boolean;

  errDataProbalibileInizio: boolean;
  errApprovazioneCDA: boolean;
  errNomeFornitore: boolean;
  errRischiosita: boolean;
  errVerificaConflitti: boolean;
  errRischiBC: boolean;
  errSpazioEconomico: boolean;
  errSLA: boolean;
  errRemunerazione: boolean;
  savedMessage: boolean;
  listaConoRE: ListaConoRE[];

  constructor(private tipologicheService: TipologicheSelectService, private listaConoREService: ListaConoREService,
      private varG: VariabiliGlobali , private salvaEsternalizzazioneService: SalvaEsternalizzazioneService) {
    this.listaFornitori = this.varG.getFornitori();
    this.savedMessage = false;
    this.getListaConoRE();
    this.inizializzaErrori();
    this.getListaBenefici();
    this.getListaRischiositaEsternalizzazione();
    this.getListaVerificaConflittiInteressi();
    this.getListaRischiBC();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges() {
     if (this.esternalizzazione.stato.valueOf() === '2') {
      switch (this.edit) {
        case 'view': {
          this.editCombinato = true;
          console.log('view');
          break;
        }
        case 'edit' : {
          this.editCombinato = false;
          console.log('edit');
          break;
        }
        case 'salva' : {
         console.log('salva');
          this.editCombinato = true;
          this.salvaEsternalizzazione();
          break;
        }
        case 'approva': {
          this.editCombinato = true;
          this.salvaEsternalizzazione();
          break;
        }
        case '': {
          break;
        }
      }
     } else {
       this.editCombinato = true;
    }
 }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith<string | ListaFornitori>(''),
        map(value => typeof value === 'string' ? value : value.ragSoc1),
        map(name => name ? this._filter(name) : this.listaFornitori.slice())
      );
  }

  displayFn(fornitore?: string): string | undefined {
    return fornitore ? fornitore : undefined;
  }

  private _filter(name: string): ListaFornitori[] {
    const filterValue = name.toLowerCase();

    return this.listaFornitori.filter(option => option.ragSoc1.toLowerCase().indexOf(filterValue) === 0);
  }

  getListaBenefici() {
    this.tipologicheService.getListaBenefici().subscribe( ( res ) => {
      this.listaBefici = res;
    });
  }

  getListaRischiositaEsternalizzazione() {
    this.tipologicheService.getListaRischiositaEsternalizzazione().subscribe( ( res ) => {
      this.listaRischiositaEsternalizzazione = res;
    });
  }

  getListaConoRE() {
    this.listaConoREService.getListaConoRE().subscribe( (res) => {
      this.listaConoRE = res;
    });
  }

  getListaVerificaConflittiInteressi() {
    this.tipologicheService.getListaConflittiInteressi().subscribe( ( res ) => {
      this.listaConflittiInteressi = res;
    });
  }

  getListaRischiBC() {
    this.tipologicheService.getListaRischiBC().subscribe( ( res ) => {
      this.listaRischiBC = res;
    });
  }

  salvaEsternalizzazione() {
      if (this.esternalizzazione.stato !== '2') { return; }

      this.inizializzaErrori();

      if (this.edit.valueOf() === 'approva') {
        if (!this.validaCampi()) { return; }
      }

      this.saved = true;
      this.gif = true;
      console.log('salvo');
      this.salvaEsternalizzazioneService.salvaNew(this.esternalizzazione).subscribe( (res) => {
        this.gif = false;
        this.savedMessage = true;
        console.log('saved');
      });
  }

  validaCampi(): boolean {
      if (this.esternalizzazione.dataProbabileInizioEsternalizzazione === null ||
        this.esternalizzazione.dataProbabileInizioEsternalizzazione === '') {
          this.errDataProbalibileInizio = true;
        }
      if ( this.esternalizzazione.approvazioneCDA === null ||
        this.esternalizzazione.approvazioneCDA === '') {
          this.errApprovazioneCDA = true;
        }
      if ( this.esternalizzazione.nomeFornitore === null ||
        this.esternalizzazione.nomeFornitore === '') {
          this.errNomeFornitore = true;
        }
      if ( this.esternalizzazione.rischiositaEsternalizzazione === null ||
        this.esternalizzazione.rischiositaEsternalizzazione === '' ) {
          this.errRischiosita = true;
        }
      if (this.esternalizzazione.verificaConflittiInteressi === null ||
        this.esternalizzazione.verificaConflittiInteressi === '') {
          this.errVerificaConflitti = true;
        }
      if (this.esternalizzazione.rischiBC === null ||
        this.esternalizzazione.rischiBC === '') {
          this.errRischiBC = true;
        }
      if (this.esternalizzazione.spazioEconomicoEuropeo ===  null ||
        this.esternalizzazione.spazioEconomicoEuropeo === '') {
          this.errSpazioEconomico = true;
        }
      if (this.esternalizzazione.slaContrattualmenteDefiniti === null ||
        this.esternalizzazione.slaContrattualmenteDefiniti === '') {
          this.errSLA = true;
        }

      if ( this.esternalizzazione.rispettoRemunerazione === null ||
        this.esternalizzazione.rispettoRemunerazione === '') {
          this.errRemunerazione = true;
        }

      if (this.errApprovazioneCDA && this.errDataProbalibileInizio && this.errNomeFornitore &&
        this.errRemunerazione && this.errRischiBC && this.errRischiosita &&
        this.errSLA && this.errSpazioEconomico && this.errVerificaConflitti) {
          return false;
      } else {
        return true;
      }
  }

  inizializzaErrori() {
  this.errDataProbalibileInizio = false;
  this.errApprovazioneCDA = false;
  this.errNomeFornitore = false;
  this.errRischiosita = false;
  this.errVerificaConflitti = false;
  this.errRischiBC = false;
  this.errSpazioEconomico = false;
  this.errSLA = false;
  this.errRemunerazione = false;
  }
}

/*
				!$scope.validaResponsabileSingoloControllo() |
        !$scope.validaRispettoRemunerazione() */
