import { Injectable } from '@angular/core';
import { ListaFornitori } from '../interfaces/lista-fornitori';
import { Utente } from '../interfaces/utente';


@Injectable()
export class VariabiliGlobali {
  private contest = 'http://localhost:8080/West/';


  private idEsternalizzazione = '';

  private idTask = '';
  private posizione = '';
  private routerLink = '';
  private user: Utente;

  private fornitori: ListaFornitori[] = [];

  constructor() {
  }


  setUtente(u: Utente) {
    this.user = u;
  }

  getUtente(): Utente {
    return this.user;
  }

  setFornitori(f: ListaFornitori[]) {
    this.fornitori = f;
  }

  getFornitori(): ListaFornitori[] {
    return this.fornitori;
  }
  setIdTask(id: string) {
      this.idTask = id.valueOf();
  }

  getIdTask(): string {
      return this.idTask;
  }
  getIdEsternalizzazione(): string {
    return this.idEsternalizzazione;
  }

  setIdEsternalizzazione(id: string) {
    this.idEsternalizzazione = id.valueOf();
  }

  setRouterLink(link: string) {
    this.routerLink = link.valueOf();
  }

  getRouterLink(): string {
    return this.routerLink;
  }

  setPosizione(pos: String) {
    this.posizione = pos.valueOf();
  }

  getPosizione(): string {
    return this.posizione;
  }

  getContest(): string {
    return this.contest;
  }

}
