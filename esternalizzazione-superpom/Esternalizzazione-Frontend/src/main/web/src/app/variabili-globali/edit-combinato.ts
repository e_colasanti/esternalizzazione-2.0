import { Injectable } from '@angular/core';


@Injectable()
export class EditCombinato {

    editInserimento = true;
    editValutazione = true;
    editPredisposizione = true;

    setEditCombinato(stato: string) {
        switch (stato) {
            case '1': {
                this.editInserimento = false;
                this.editPredisposizione = true;
                this.editValutazione = true;
                break;
            }
            case '2': {
                this.editInserimento = true;
                this.editPredisposizione = true;
                this.editValutazione = false;
                break;
            }
            case '3': {
                this.editInserimento = true;
                this.editPredisposizione = false;
                this.editValutazione = true;
                break;
            }
        }
    }

    returnEditInserimento(): boolean {
        return this.editInserimento;
    }

    returnEditPredisposizione(): boolean {
        return this.editPredisposizione;
    }

    returnEditValutazione(): boolean {
        return this.editValutazione;
    }
}
