// tslint:disable: max-line-length

// <-- IMPORTAZIONE MODULI -->
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatTabsModule} from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { TaskComponent } from './components/task/task.component';
import { MatPaginatorModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { MatNativeDateModule } from '@angular/material';
import { AngularStickyThingsModule } from '@w11k/angular-sticky-things';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFileUploadModule } from 'angular-material-fileupload';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CookieService } from 'ngx-cookie-service';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';


// <--- IMPORTAZIONE COMPONETS -->
import { AppComponent } from './app.component';
import { FasePredisposizioneComponent } from './Components/fase-predisposizione/fase-predisposizione.component';
import { FaseValOutsourcingComponent } from './Components/fase-val-outsourcing/fase-val-outsourcing.component';
import { FaseScadOutsourcingComponent } from './Components/fase-scad-outsourcing/fase-scad-outsourcing.component';
import { EstrazioneComponent } from './Components/estrazione/estrazione.component';
import { FaseValutazioneComponent } from './Components/fase-valutazione/fase-valutazione.component';
import { HomeComponent } from './Components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { BodyComponent } from './components/body/body.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { LoginComponent } from './components/login/login.component';
import { ListaTaskApplicativiComponent } from './components/lista-task-applicativi/lista-task-applicativi.component';
import { ValutazioneComponent } from './components/valutazione/valutazione.component';
import { PredisposizioneComponent } from './components/predisposizione/predisposizione.component';
import { OutsourcingComponent } from './components/outsourcing/outsourcing.component';
import { ScadOutsourcingComponent } from './components/scad-outsourcing/scad-outsourcing.component';
import { TaskAppComponent } from './components/task-app/task-app.component';
import { DettaglioEsternalizzazioneComponent } from './components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component';
import { FaseInserimentoComponent } from './components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-inserimento/fase-inserimento.component';
import { FaseValutazioneDettComponent } from './components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-valutazione/fase-valutazione.component';
import { FasePredisposizioneDettComponent } from './components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-predisposizione/fase-predisposizione.component';
import { EsternalizzazioniComponent } from './components/esternalizzazioni/esternalizzazioni.component';
import { DettaglioTaskComponent } from './components/task/dettaglio-task/dettaglio-task.component';
import { ActionsBarTaskComponent } from './components/task/actions-bar-task/actions-bar-task.component';
import { UserComponent } from './components/user/user.component';
import { FileAllegatiComponent } from './components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/file-allegati/file-allegati.component';
import { DatePickerComponent } from './components/custom-component/date-picker/date-picker.component';



// <-- FILE TS CONTENENTI VARIBILI USATE DA PIÙ -->
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';
import { EditCombinato } from './variabili-globali/edit-combinato';


@NgModule({
  declarations: [
    AppComponent,
    FaseValutazioneComponent,
    FasePredisposizioneComponent,
    FaseValOutsourcingComponent,
    FaseScadOutsourcingComponent,
    EstrazioneComponent,
    HomeComponent,
    HeaderComponent,
    BodyComponent,
    SearchBarComponent,
    LoginComponent,
    ListaTaskApplicativiComponent,
    EsternalizzazioniComponent,
    TaskComponent,
    ValutazioneComponent,
    PredisposizioneComponent,
    OutsourcingComponent,
    ScadOutsourcingComponent,
    TaskAppComponent,
    DettaglioEsternalizzazioneComponent,
    FaseInserimentoComponent,
    FaseValutazioneDettComponent,
    FasePredisposizioneDettComponent,
    DettaglioTaskComponent,
    ActionsBarTaskComponent,
    UserComponent,
    FileAllegatiComponent,
    DatePickerComponent,
    FileSelectDirective,
    FileDropDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatChipsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTabsModule,
    MatPaginatorModule,
    MatTableModule,
    MatNativeDateModule,
    AngularStickyThingsModule,
    FormsModule,
    MatDatepickerModule,
    MatFileUploadModule,
    MatAutocompleteModule
  ],
  providers: [
    VariabiliGlobali,
    EditCombinato,
    CookieService
  ],
  exports: [
    MatPaginatorModule
  ],
  bootstrap: [
    AppComponent,
    HeaderComponent
  ]
})
export class AppModule { }
