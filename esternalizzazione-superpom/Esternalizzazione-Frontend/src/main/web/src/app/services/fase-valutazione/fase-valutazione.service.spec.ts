import { TestBed } from '@angular/core/testing';

import { FaseValutazioneService } from './fase-valutazione.service';

describe('FaseValutazioneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FaseValutazioneService = TestBed.get(FaseValutazioneService);
    expect(service).toBeTruthy();
  });
});
