import { Injectable } from '@angular/core';
import { ListaConoRE } from 'src/app/interfaces/lista-cono-re';
import { Observable } from 'rxjs';
import { HttpService } from '../http-service/http.service';


@Injectable({
  providedIn: 'root'
})
export class ListaConoREService {

  constructor(private http: HttpService) {
   }

   getListaConoRE(): Observable<ListaConoRE[]> {
    return (this.http.doGet(`rest/esternalizzazione/listaConoRE`));
   }

}
