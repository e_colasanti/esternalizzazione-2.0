import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { File as FileWeb } from 'src/app/interfaces/file';
import { HttpService } from '../../http-service/http.service';

@Injectable({
  providedIn: 'root'
})
export class FileEsternalizzazione {

  constructor(private http: HttpService ) {
   }

   getListaFile(id: string): Observable<FileWeb[]> {
    return (this.http.doGet(`rest/file/lista/${id}/ESTERNALIZZAZIONE`));
   }
}
