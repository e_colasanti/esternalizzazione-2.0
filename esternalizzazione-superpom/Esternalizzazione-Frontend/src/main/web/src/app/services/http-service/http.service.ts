import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { Utente } from '../../interfaces/utente';
import { VariabiliGlobali } from '../../variabili-globali/variabili-globali';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

   private urlContextPath: string;
   private utente: Utente;

   private userSessionIsAuthenticated: boolean;

  constructor(private http: HttpClient, private cookieService: CookieService, private varG: VariabiliGlobali) {

          this.urlContextPath = this.varG.getContest();
   }


  userSessionAuthenticated() {
    return true;
  }


   doGet( url: string  ): Observable<any> {
    return this.http.get<any>(this.urlContextPath + url);
   }


   doPost( url: string, body: any,  contentType: string  ): Observable<any> {

    console.log(' HttpService=>doPost cookie: ' + this.cookieService.get('Cookie') );

    console.log(' X-XSRF-TOKEN: ' + this.cookieService.get('Cookie').split(';')[1].split('=')[1]);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': contentType, // 'application/json; charset=utf-8',
        'Access-Control-Allow-Headers': 'content-type',
        'Access-Control-Allow-Origin': '*',
        'Allow': ' GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH',
        'cookie' : this.cookieService.get('Cookie'),
        'X-XSRF-TOKEN': this.cookieService.get('Cookie').split(';')[1].split('=')[1]
      })
    };

    return this.http.post<any>( this.urlContextPath + url, body, httpOptions);
   }


   doPostFile( url: string, body: any): Observable<any> {

    console.log(' HttpService=> doPost file cookie: ' + this.cookieService.get('Cookie') );

    const token = this.cookieService.get('Cookie').split(';')[1].split('=')[1];

    console.log(' doPostFile X-XSRF-TOKEN: ' + token);

    const httpOptions = {
      headers: new HttpHeaders({
        // 'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With,Accept, Authorization',
        'Allow': ' POST, PUT, DELETE',
        'Accept' : 'application/json',
        'cookie' : this.cookieService.get('Cookie'),
        'X-XSRF-TOKEN': token,
      })
    };

        console.log('doPostFile BODY: ' + body );

    return this.http.post<any>( this.urlContextPath + url, body, httpOptions);
   }



}


