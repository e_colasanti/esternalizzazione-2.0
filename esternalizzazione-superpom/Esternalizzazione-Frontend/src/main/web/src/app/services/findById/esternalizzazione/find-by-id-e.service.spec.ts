import { TestBed } from '@angular/core/testing';

import { FindByIdEService } from './find-by-id-e.service';

describe('FindByIdEService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FindByIdEService = TestBed.get(FindByIdEService);
    expect(service).toBeTruthy();
  });
});
