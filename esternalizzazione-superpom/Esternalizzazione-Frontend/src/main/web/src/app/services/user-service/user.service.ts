import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
// import { ResponseEntity } from 'src/app/interfaces/responseEntity';
import { Utente } from '../../interfaces/utente';
import { HttpService } from '../http-service/http.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url: String;

  constructor(private http: HttpService) {
   }

   getUser(): Observable<Utente> {
    return this.http.doGet(`rest/utente/username`);
}

}
