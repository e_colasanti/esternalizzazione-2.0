import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ListaFornitori } from 'src/app/interfaces/lista-fornitori';
import { HttpService } from '../../http-service/http.service';

@Injectable({
  providedIn: 'root'
})
export class FornitoriService {

  constructor(private http: HttpService) {
  }

  getListaFornitori(ambitoSociale: string): Observable<ListaFornitori[]> {
    return this.http.doGet(`rest/esternalizzazione/listaFornitori/${ambitoSociale}/`);
  }
}
