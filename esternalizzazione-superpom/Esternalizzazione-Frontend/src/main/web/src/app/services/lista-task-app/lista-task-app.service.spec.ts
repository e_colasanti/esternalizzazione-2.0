import { TestBed } from '@angular/core/testing';

import { ListaTaskAppService } from './lista-task-app.service';

describe('ListaTaskAppService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListaTaskAppService = TestBed.get(ListaTaskAppService);
    expect(service).toBeTruthy();
  });
});
