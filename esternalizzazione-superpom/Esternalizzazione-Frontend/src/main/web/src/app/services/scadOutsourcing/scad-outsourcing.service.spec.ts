import { TestBed } from '@angular/core/testing';

import { ScadOutsourcingService } from './scad-outsourcing.service';

describe('ScadOutsourcingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScadOutsourcingService = TestBed.get(ScadOutsourcingService);
    expect(service).toBeTruthy();
  });
});
