import { TestBed } from '@angular/core/testing';

import { FasePredisposizioneService } from './fase-predisposizione.service';

describe('FasePredisposizioneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FasePredisposizioneService = TestBed.get(FasePredisposizioneService);
    expect(service).toBeTruthy();
  });
});
