import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ListaAttivita } from '../../../interfaces/lista-attivita';
import { HttpService } from '../../http-service/http.service';

@Injectable({
  providedIn: 'root'
})
export class GetTaskService {

    private url: string;
    private idTask: string;

  constructor(private http: HttpService) {
   }

   getTask(id): Observable<ListaAttivita> {
    return this.http.doGet(`rest/task/get/${id}`);
   }
}
