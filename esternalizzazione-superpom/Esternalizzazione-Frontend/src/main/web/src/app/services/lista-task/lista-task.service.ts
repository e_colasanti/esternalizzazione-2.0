import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Attivita } from 'src/app/interfaces/attivita';
import { HttpService } from '../http-service/http.service';


@Injectable({
  providedIn: 'root'
})
export class ListaTaskService {


  constructor(private http: HttpService) {
   }

   getListaTask( pageIndex: Number , pageSize: Number ): Observable<Attivita> {
     return (this.http.doGet(`rest/task/lista/-1/${pageIndex}/${pageSize}`));
   }
}
