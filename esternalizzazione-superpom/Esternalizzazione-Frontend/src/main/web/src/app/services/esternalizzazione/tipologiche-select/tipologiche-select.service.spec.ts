import { TestBed } from '@angular/core/testing';

import { TipologicheSelectService } from './tipologiche-select.service';

describe('TipologicheSelectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipologicheSelectService = TestBed.get(TipologicheSelectService);
    expect(service).toBeTruthy();
  });
});
