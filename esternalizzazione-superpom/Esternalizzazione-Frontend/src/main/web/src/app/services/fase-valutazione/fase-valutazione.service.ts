import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Esternalizzazioni } from 'src/app/interfaces/esternalizzazioni';
import { HttpService } from '../http-service/http.service';

@Injectable({
  providedIn: 'root'
})
export class FaseValutazioneService {


  constructor(private http: HttpService , ) {
   }

   getFaseValutazione(pageIndex: Number , pageSize: Number): Observable<Esternalizzazioni> {
    return (this.http.doGet(`rest/esternalizzazione/lista/2/${pageIndex}/${pageSize}`));
   }
}
