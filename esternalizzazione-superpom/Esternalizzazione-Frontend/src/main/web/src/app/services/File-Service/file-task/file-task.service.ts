import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';
import { Observable } from 'rxjs';
import { File } from 'src/app/interfaces/file';

@Injectable({
  providedIn: 'root'
})
export class FileTaskService {

  private url: String;
  private id: string;

  constructor(private http: HttpClient , private varG: VariabiliGlobali) {
    this.url = this.varG.getContest();
   }

   getListaFile(): Observable<File[]> {
    this.id = this.varG.getIdTask();
    return (this.http.get<File[]>(`${this.url}rest/file/lista/${this.id}/TASK`));
   }

   downloadFile(idFile: string): Observable<any> {
    this.id = this.varG.getIdTask();
    return (this.http.get(`${this.url}rest/file/download/${idFile}`));
   }
}
