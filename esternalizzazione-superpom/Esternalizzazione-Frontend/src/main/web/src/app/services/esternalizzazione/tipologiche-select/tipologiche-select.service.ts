import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ListaWeb } from 'src/app/interfaces/lista-web';
import { HttpService } from '../../http-service/http.service';

@Injectable({
  providedIn: 'root'
})
export class TipologicheSelectService {

  constructor(private http: HttpService) {
   }

   getListaRischiBC(): Observable<ListaWeb[]> {
     return (this.http.doGet(`rest/esternalizzazione/listaRischiBC`));
   }

   getListaTipologiaEsternalizzazione(): Observable<ListaWeb[]> {
     return (this.http.doGet(`rest/esternalizzazione/listaTipiEsternalizzazione`));
   }

   getListaBenefici(): Observable<ListaWeb[]> {
     return (this.http.doGet(`rest/esternalizzazione/listaBenefici`));
   }

   getListaRischiositaEsternalizzazione(): Observable<ListaWeb[]> {
     return (this.http.doGet(`rest/esternalizzazione/listaRischiositaEsternalizzazione`));
   }

   getListaConflittiInteressi(): Observable<ListaWeb[]> {
     return (this.http.doGet(`rest/esternalizzazione/listaConflittiInteressi`));
   }

   getListaPianiReinternalizzazione(): Observable<ListaWeb[]> {
     return (this.http.doGet(`rest/esternalizzazione/listaPianiReinternalizzazione`));
   }

   getListaTipiControllo(): Observable<ListaWeb[]> {
    return (this.http.doGet(`rest/esternalizzazione/listaTipiControllo`));
   }

   getListaPeriodicita(): Observable<ListaWeb[]> {
    return (this.http.doGet(`rest/esternalizzazione/listaPeriodicita`));
   }

   getListaAzioniCorrettive(): Observable<ListaWeb[]> {
    return (this.http.doGet(`rest/esternalizzazione/listaAzioniCorrettive`));
   }

   getListaEsitiControllo(): Observable<ListaWeb[]> {
    return (this.http.doGet(`rest/esternalizzazione/listaEsitiControllo`));
   }
}
