import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VariabiliGlobali } from 'src/app/variabili-globali/variabili-globali';
import { Observable } from 'rxjs';
import { ListaWeb } from 'src/app/interfaces/lista-web';

@Injectable({
  providedIn: 'root'
})
export class TipologicheTaskSelectService {

  private url: String;

  constructor(private http: HttpClient , private varG: VariabiliGlobali) {
    this.url = this.varG.getContest();
   }

   getListaTipologiaTask(): Observable<ListaWeb[]> {
    return this.http.get<ListaWeb[]>(`${this.url}rest/task/listaTipiTask`);
   }

   getListaTipiUtente(): Observable<ListaWeb[]> {
    return this.http.get<ListaWeb[]>(`${this.url}rest/task/listaTipiUtente`);
   }
}

