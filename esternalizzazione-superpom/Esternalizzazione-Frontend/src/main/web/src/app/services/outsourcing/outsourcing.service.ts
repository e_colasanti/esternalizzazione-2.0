import { Injectable } from '@angular/core';
import { Esternalizzazioni } from 'src/app/interfaces/esternalizzazioni';
import { Observable } from 'rxjs';
import { HttpService } from '../http-service/http.service';

@Injectable({
  providedIn: 'root'
})
export class OutsourcingService {

  private url: String;

  constructor(private http: HttpService) {
   }


   getOutsourcing(pageIndex: Number , pageSize: Number): Observable<Esternalizzazioni> {
    return (this.http.doGet(`rest/esternalizzazione/lista/4/${pageIndex}/${pageSize}`));
   }

}
