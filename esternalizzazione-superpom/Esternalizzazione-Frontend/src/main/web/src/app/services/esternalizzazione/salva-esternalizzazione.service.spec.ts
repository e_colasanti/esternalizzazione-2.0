import { TestBed } from '@angular/core/testing';

import { SalvaEsternalizzazioneService } from './salva-esternalizzazione.service';

describe('SalvaEsternalizzazioneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SalvaEsternalizzazioneService = TestBed.get(SalvaEsternalizzazioneService);
    expect(service).toBeTruthy();
  });
});
