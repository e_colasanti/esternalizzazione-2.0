import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Esternalizzazione } from 'src/app/interfaces/refactoredInterfaces/esternalizzazione';
import { HttpService } from '../../http-service/http.service';


@Injectable({
  providedIn: 'root'
})
export class FindByIdEService {

  constructor(private http: HttpService) {
   }


  findEsternalizzazione(id: string): Observable<Esternalizzazione> {
    return (this.http.doGet(`rest/esternalizzazione/get/${id}`));
   }
}
