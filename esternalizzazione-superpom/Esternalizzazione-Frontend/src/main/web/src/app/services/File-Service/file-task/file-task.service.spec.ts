import { TestBed } from '@angular/core/testing';

import { FileTaskService } from './file-task.service';

describe('FileTaskService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FileTaskService = TestBed.get(FileTaskService);
    expect(service).toBeTruthy();
  });
});
