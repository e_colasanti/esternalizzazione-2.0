import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ListaEsternalizzazioni } from 'src/app/interfaces/lista-esternalizzazioni';
import { HttpService } from '../http-service/http.service';
import { Esternalizzazione } from 'src/app/interfaces/refactoredInterfaces/esternalizzazione';


@Injectable({
  providedIn: 'root'
})

export class SalvaEsternalizzazioneService {


  constructor(private http: HttpService) {
   }

   salvaNew(esternalizzazione: Esternalizzazione): Observable<Esternalizzazione> {

    return this.http.doPost(`rest/esternalizzazione/salva`, esternalizzazione , 'application/json; charset=utf-8');
    }

   salva(esternalizzazione: Esternalizzazione): Observable<Esternalizzazione> {

    if (esternalizzazione.idEsternalizzazione == null) { return null; } else {
    return this.http.doPost(`rest/esternalizzazione/salva`, esternalizzazione , 'application/json; charset=utf-8');
    }

  }
}

