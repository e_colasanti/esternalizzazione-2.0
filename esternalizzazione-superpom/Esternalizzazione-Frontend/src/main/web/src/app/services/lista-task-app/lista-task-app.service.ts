import { Injectable } from '@angular/core';
import { Attivita } from 'src/app/interfaces/attivita';
import { Observable } from 'rxjs';
import { HttpService } from '../http-service/http.service';

@Injectable({
  providedIn: 'root'
})
export class ListaTaskAppService {

  constructor(private http: HttpService) {
   }


   getListaTaskApp( pageIndex: Number , pageSize: Number ): Observable<Attivita> {
     return (this.http.doGet(`rest/task/lista/12/${pageIndex}/${pageSize}`));
   }
}

