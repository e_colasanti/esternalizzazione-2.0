import { Injectable } from '@angular/core';
import { Esternalizzazioni } from 'src/app/interfaces/esternalizzazioni';
import { Observable } from 'rxjs';
import { HttpService } from '../http-service/http.service';

@Injectable({
  providedIn: 'root'
})
export class FasePredisposizioneService {

  constructor(private http: HttpService) {
   }

   getFaseValutazione(pageIndex: Number , pageSize: Number): Observable<Esternalizzazioni> {
    return (this.http.doGet(`rest/esternalizzazione/lista/3/${pageIndex}/${pageSize}`));
   }
}
