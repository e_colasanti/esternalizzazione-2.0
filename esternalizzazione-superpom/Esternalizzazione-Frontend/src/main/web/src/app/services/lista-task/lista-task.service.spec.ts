import { TestBed } from '@angular/core/testing';

import { ListaTaskService } from './lista-task.service';

describe('ListaTipiTaskService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListaTaskService = TestBed.get(ListaTaskService);
    expect(service).toBeTruthy();
  });
});
