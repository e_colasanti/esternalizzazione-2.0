import { TestBed } from '@angular/core/testing';

import { TipologicheTaskSelectService } from './tipologiche-task.service';

describe('TipologicheTaskService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipologicheTaskSelectService = TestBed.get(TipologicheTaskSelectService);
    expect(service).toBeTruthy();
  });
});
