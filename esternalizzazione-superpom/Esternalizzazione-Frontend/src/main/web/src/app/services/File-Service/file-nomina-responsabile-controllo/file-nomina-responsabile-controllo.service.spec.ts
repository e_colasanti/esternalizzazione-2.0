import { TestBed } from '@angular/core/testing';

import { FileEsternalizzazione } from './file-esternalizzazione.service';

describe('FileNominaResponsabileControlloService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FileEsternalizzazione = TestBed.get(FileEsternalizzazione);
    expect(service).toBeTruthy();
  });
});
