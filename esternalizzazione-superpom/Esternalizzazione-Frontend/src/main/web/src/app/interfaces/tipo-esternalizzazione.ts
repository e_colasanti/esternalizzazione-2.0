export interface TipoEsternalizzazione {
  idTipoEsternalizzazione: Number;
  descrizioneTipoEsternalizzazione: String;
  dataInserimento: Date;
  dataAggiornamento: String;
  utenteInserimento: String;
  utenteAggiornamento: String;
  dataInizioValidita: Date;
  dataFineValidita: Date;
}
