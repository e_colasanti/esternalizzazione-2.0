import { TipoUtente } from './tipo-utente';

export interface TipoTask {
  idTipoTask: Number;
  tipoTask: String;
  descTipoTask: String;
  tipoUtente: TipoUtente;
  oggetto: String;
}
