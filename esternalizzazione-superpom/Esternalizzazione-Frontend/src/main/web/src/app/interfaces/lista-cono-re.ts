export interface ListaConoRE {
  username: String;
  ruoli: String[];
  nome: String;
  cognome: String;
  descrLivello: String;
  esito: String;
  livello: String;
  responsabile: String;
  sesso: String;
  zzenteacq: String;
  text: String;
  userWest: String;
  codiceDirezione: String;
}
