import { Esternalizzazione } from './esternalizzazione';

export interface ListaEsternalizzazioniWeb {
    listaEsternalizzazioni: Esternalizzazione[];
    totalePagine: number;
    paginaCorrente: number;
    totaleRecord: number;
    recordPerPagina: number;
}
