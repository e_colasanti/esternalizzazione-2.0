export interface PianoReinternalizzazione {
  idPianoReinternalizzazione: Number;
  descrizionePianoReinternalizzazione: String;
  dataInserimento: Date;
  dataAggiornamento: String;
  utenteInserimento: String;
  utenteAggiornamento: String;
  dataInizioValidita: Date;
  dataFineValidita: Date;
}
