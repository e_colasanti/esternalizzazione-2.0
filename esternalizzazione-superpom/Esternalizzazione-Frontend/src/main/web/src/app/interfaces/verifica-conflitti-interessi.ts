export interface VerificaConflittiInteressi {
  idVerificaConflittiInteressi: Number;
  descrizioneVerificaConflittiInteressi: String;
  dataInserimento: Date;
  dataAggiornamento: String;
  utenteInserimento: String;
  utenteAggiornamento: String;
  dataInizioValidita: Date;
  dataFineValidita: Date;
}
