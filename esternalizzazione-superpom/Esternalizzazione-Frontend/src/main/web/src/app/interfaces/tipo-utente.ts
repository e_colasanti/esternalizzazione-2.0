export interface TipoUtente {
  idTipoUtente: Number;
  acTipoUtente: String;
  descTipoUtente: String;
  indirizzoEmail: String;
  codiceDirezione: String;
}
