export interface RouterLinks {
  path: String;
  message: String;
  imgUrl: String;
}
