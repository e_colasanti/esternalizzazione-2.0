export interface ListaFornitori {
    forniId: string;
    ragSoc1: string;
    RagSoc2: string;
    ragSoc3: string;
    partitaIva: string;
    codiceFiscale: string;
    oggettoSociale: string;
    capitaleSociale: string;
    totDipendenti: string;
    numImpiegati: string;
}
