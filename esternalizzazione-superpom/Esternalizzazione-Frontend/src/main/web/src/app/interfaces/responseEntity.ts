import { Utente } from './utente';

export interface ResponseEntity {
    utente: Utente;
    headers: Array<String>;
    status: String;
}
