export interface TipoControllo {
  idTipoControllo: Number;
  descrizioneTipoControllo: String;
  dataInserimento: Date;
  dataAggiornamento: String;
  utenteInserimento: String;
  utenteAggiornamento: String;
  dataInizioValidita: Date;
  dataFineValidita: Date;
}
