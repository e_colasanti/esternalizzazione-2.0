export interface Controllo {
    idControllo: string;
    idEsternalizzazione: string;
    tipoControllo: string;
    dataInizioControllo: string;
    dataFineControllo: string;
    vincoloPagamentoFattura: string;
    responsabileSingoloControllo: string;
    esitoControllo: string;
    descrizioneEsitoControllo: string;
    azioneCorrettive: string;
    notaAzioneCorrettiva: string;
    descrizioneAzioneCorrettiva: string;
    descrizioneTipoControllo: string;
    descrEsitoControllo: string;
    descrizioneControllo: string;
}
