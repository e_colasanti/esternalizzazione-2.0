import { Controllo } from './controllo';
import { ListaConoRE } from '../lista-cono-re';
import { Utente } from '../utente';
import { Task } from '../task';

export interface Esternalizzazione {

    idEsternalizzazione: string;
    dataCreazioneEsternalizzazione: string;
    stato: string;
    compilataDa: string;
    titoloEsternalizzazione: string;
    tipoEsternalizzazione: string;
    descrizioneTipoEsternalizzazione: string;
    dataModificaTipologiaEsternalizzazione: string;
    utenteModificaEsternalizzazione: string;
    motivazioneEsternalizzazione: string;
    oggettoAttivita: string;
    codiceDirezione: string;

    /*fase valutazione*/

    approvazioneCDA: string;
    beneficioConseguibile: string;
    descrizioneBeneficioConseguibile: string;
    dataProbabileInizioEsternalizzazione: string;
    nomeFornitore: string;
    spazioEconomicoEuropeo: string;
    affidabilitaCompetenzaFornitore: string;
    rischiositaEsternalizzazione: string;
    descrizioneRischiositaEsternalizzazione: string;
    valutazioneRischioEsternalizzazione: string;
    verificaConflittiInteressi: string;
    descrizioneVerificaConflittiInteressi: string;
    rischiBC: string;
    descrizioneRischiBC: string;
    rispettoRemunerazione: string;
    responsabileControlloEsternalizzazione: string;


    /*fase di predisposizione*/

    valutazioneRischiBC: string;
    dataEfficaciaContratto: string;
    durataEsternalizzazione: string;
    numeroComunicazioneIVASS: string;
    numeroProtocolloContrattuale: string;
    periodoPreavvisoContrattuale: string;
    tipologiaRinnovo: string;
    slaContrattualmenteDefiniti: string;
    periodicita: string;
    dataInvioComunicazioneIVASS: string;

    // controlli

    listaControlli: Controllo[];

      // campi aggiunti per assegnazione utente
    idUO: string;
    idRC: string;
    listaConoRE: ListaConoRE;
    utenteWs: Utente;
    responsabileWs: Utente;
    listaTask: Task;
}
