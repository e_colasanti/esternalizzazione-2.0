export interface File {
  file: string;
  pathFile: string;
  nomeFileOrigine: string;
  idAllegato: string;
  dimensione: string;
  contentType: string;
  contenuto: string;
  ambito: string;
  contesto: string;
}
