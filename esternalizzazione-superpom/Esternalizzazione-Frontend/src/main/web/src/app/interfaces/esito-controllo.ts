export interface EsitoControllo {
  idEsitoControllo: Number;
  descrizioneEsitoControllo: String;
  dataInserimento: Date;
  dataAggiornamento: String;
  utenteInserimento: String;
  utenteAggiornamento: String;
  dataInizioValidita: Date;
  dataFineValidita: Date;
}
