export interface ListaAttivita {
  idTask: String;
  progLavTask: String;
  idTipoTask: String;
  utenteOwner: String;
  utenteResponsabile: String;
  dataCreazione: String;
  dataAssegnazione: String;
  dataFine: String;
  stato: String;
  descrizione: String;
  idEsternalizzazione: String;
  utenteDelegato: String;
  idTipoUtente: String;
  allegati: Number[];
  tipoCampoAttivatoEst: String;
  faseEsternalizzazione: String;
  codiceDirezione: String;
  bloccante: String;
  faseBloccante: String;
}
