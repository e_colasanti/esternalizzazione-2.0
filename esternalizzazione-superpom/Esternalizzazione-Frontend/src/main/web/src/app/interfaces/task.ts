import { TipoUtente } from './tipo-utente';
import { Esternalizzazione } from './refactoredInterfaces/esternalizzazione';

export interface Task {
  idTask: Number;
  progLavTask: Number;
  tipoTask: Number;
  utenteOwner: String;
  utenteResponsabile: String;
  dataCreazione: Date;
  dataFine: Date;
  dataAssegnazione: Date;
  stato: String;
  descrizione: String;
  utenteDelegato: String;
  tipoCampoAttivatoEst: String;
  faseEsternalizzazione: String;
  codiceDirezione: String;
  bloccante: String;
  faseBloccante: String;
  allegati: Number[];
  esternalizzazione: Esternalizzazione ;
  tipoUtente: TipoUtente;
}
