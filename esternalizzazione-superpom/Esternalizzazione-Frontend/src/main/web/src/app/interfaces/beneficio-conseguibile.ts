export interface BeneficioConseguibile {
  idBeneficioConseguibile: Number;
  descrizioneBeneficioConseguibile: String;
  dataInserimento: Date;
  dataAggiornamento: String;
  utenteInserimento: String;
  utenteAggiornamento: String;
  dataInizioValidita: Date;
  dataFineValidita: Date;
}
