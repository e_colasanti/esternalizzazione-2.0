export interface AzioneCorrettiva {
  idAzioneCorrettiva: Number;
  descrizioneAzioneCorrettiva: String;
  dataInserimento: Date;
  dataAggiornamento: String;
  utenteInserimento: String;
  utenteAggiornamento: String;
  dataInizioValidita: Date;
  dataFineValidita: Date;
}
