export interface RischiositaEsternalizzazione {
  idRischiositaEsternalizzazione: Number;
  descrizioneRischiositaEsternalizzazione: String;
  dataInserimento: Date;
  dataAggiornamento: String;
  utenteInserimento: String;
  utenteAggiornamento: String;
  dataInizioValidita: Date;
  dataFineValidita: Date;
}
