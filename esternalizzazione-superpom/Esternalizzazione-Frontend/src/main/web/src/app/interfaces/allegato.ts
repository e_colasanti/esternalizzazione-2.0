import { Task } from './task';
import { ListaEsternalizzazioni } from './lista-esternalizzazioni';

export interface Allegato {
  idAllegato: Number;
  nomeAllegato: String;
  mimeType: String;
  sizeAllegato: String;
  tipoAllegato: String;
  contenuto: Number[];
  dataInserimento: Date;
  utenteInserimento: String;
  tipoAmbito: String;
  ambito: String;
  contesto: String;
  esternalizzazione: ListaEsternalizzazioni;
  task: Task;
}
