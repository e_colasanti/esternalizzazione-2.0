import { Esternalizzazione } from './refactoredInterfaces/esternalizzazione';

export interface Esternalizzazioni {
  listaEsternalizzazioni: Esternalizzazione[];
  totalePagine: Number;
  paginaCorrente: number;
  totaleRecord: Number;
  recordPerPagina: Number;
}
