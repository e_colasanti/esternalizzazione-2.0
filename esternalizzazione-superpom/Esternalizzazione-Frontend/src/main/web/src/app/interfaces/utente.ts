export interface Utente {
  username: string;
  ruoli: string[];
  nome: string;
  cognome: string;
  descrLivello: string;
  esito: string;
  livello: string;
  responsabile: string;
  sesso: string;
  zzenteacq: string;
  text: string;
  userWest: string;
  codiceDirezione: string;
  sessionID: string;
  lastAccessedTime: string ;
  cookie: string;

}
