export interface RischiBC {
  idRischiBC: Number;
  descrizioneRischiBC: String;
  dataInserimento: Date;
  dataAggiornamento: String;
  utenteInserimento: String;
  utenteAggiornamento: String;
  dataInizioValidita: Date;
  dataFineValidita: Date;
}
