import { Task } from './task';
import { TipoFase } from './tipo-fase';
import { ListaEsternalizzazioni } from './lista-esternalizzazioni';

export interface FaseEsternalizzazione {
  idFaseEsternalizzazione: Number;
  dataInserimento: Date;
  dataAggiornamento: Date;
  utenteInserimento: String;
  utenteAggiornamento: String;
  dataInizioValidita: Date;
  dataFineValidita: Date;
  task: Task;
  esternalizzazione: ListaEsternalizzazioni;
  tipoFase: TipoFase;
  dataAssegnazione: Date;
  utenteResponsabile: String;
  utenteDelegato: String;
  stato: String;
  rispostaLegale: String;
}
