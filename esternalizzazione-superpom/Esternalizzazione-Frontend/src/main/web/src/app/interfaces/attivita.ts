import { ListaAttivita } from './lista-attivita';

export interface Attivita {
  listaAttivita: ListaAttivita[];
  totalePagine: Number;
  paginaCorrente: number;
  totaleRecord: Number;
  recordPerPagina: Number;
}
