export interface Stato {
  idStato: Number;
  descrizioneStato: String;
  dataInserimento: Date;
  dataAggiornamento: Date;
  utenteInserimento: String;
  utenteAggiornamento: String;
  dataInizioValidita: Date;
  dataFineValidita: Date;
}
