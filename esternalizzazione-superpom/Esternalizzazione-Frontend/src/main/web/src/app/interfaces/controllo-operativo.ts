export interface ControlloOperativo {
  idControlloOperativo: Number;
  nome: String;
  cognome: String;
  registrazione: String;
  controllo: Number;
  operativo: Number;
  esternalizzazione: Number;
}
