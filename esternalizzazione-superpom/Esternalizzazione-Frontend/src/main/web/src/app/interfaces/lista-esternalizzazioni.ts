import { ListaConoRE } from './lista-cono-re';
import { Utente } from './utente';
import { Task } from './task';

export interface ListaEsternalizzazioni {
  idEsternalizzazione: string;
  dataCreazioneEsternalizzazione: string;
  compilataDa: string;
  codiceDirezione: string;
  titoloEsternalizzazione: string;
  tipoEsternalizzazione: string;
  descrizioneTipoEsternalizzazione: string;
  dataModificaTipologiaEsternalizzazione: string;
  utenteModificaEsternalizzazione: string;
  motivazioneEsternalizzazione: string;

  /*fase valutazione*/

  approvazioneCDA: string;
  beneficioConseguibile: string;
  descrizioneBeneficioConseguibile: string;
  dataProbabileInizioEsternalizzazione: string;
  nomeFornitore: string;
  spazioEconomicoEuropeo: string;
  affidabilitaCompetenzaFornitore: string;
  rischiositaEsternalizzazione: string;
  descrizioneRischiositaEsternalizzazione: string;
  valutazioneRischioEsternalizzazione: string;
  verificaConflittiInteressi: string;
  descrizioneVerificaConflittiInteressi: string;
  rischiBC: string;
  descrizioneRischiBC: string;

  /*fase di predisposizione*/

  valutazioneRischiBC: string;
  pianoReinternalizzazione: string;
  descrizionePianoReinternalizzazione: string;
  rispettoRemunerazione: string;
  responsabileControlloEsternalizzazione: string;
  registrazioneRCEsternalizzazione: string;
  utenteOperativoEsternalizzazione: string;
  registrazioneUOEsternalizzazione: string;
  dataInizioEfficaciaContratto: string;
  numeroComunicazioneIVASS: string;
  numeroRDA: string;
  numeroODA: string;
  numeroProtocolloContrattuale: string;
  periodoPreavvisoContrattuale: string;
  tipologiaRinnovo: string;
  slaContrattualmenteDefiniti: string;
  descrizioneControllo: string;
  tipoControllo: string;
  descrizioneTipoControllo: string;
  dataInizioControllo: string;
  dataFineControllo: string;
  periodicita: string;
  descrizionePeriodicita: string;
  vincoloPagamentoFattura: string;
  responsabileSingoloControllo: string;
  esitoControllo: string;
  descrEsitoControllo: string;
  descrizioneEsitoControllo: string;
  azioneCorrettiva: string;
  descrizioneAzioneCorrettiva: string;
  stato: string;
  notaAzioneCorrettiva: string;

  // campi aggiunti per assegnazione utente
  idUO: string;
  idRC: string;
  listaConoRE: ListaConoRE;
  utenteWs: Utente;
  responsabileWs: Utente;
  listaTask: Task;
}
