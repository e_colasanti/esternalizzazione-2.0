import { TipoUtente } from './tipo-utente';
import { ListaEsternalizzazioni } from './lista-esternalizzazioni';

export interface Remind {
  idRemind: Number;

  tipoUtente: TipoUtente;
  esternalizzazione: ListaEsternalizzazioni;
  tipoRemind: String;
  oggetto: String;
  periodicita: Number;
  dataInizio: Date;
  dataFine: Date;
  stato: String;
}
