(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/Components/estrazione/estrazione.component.css":
/*!****************************************************************!*\
  !*** ./src/app/Components/estrazione/estrazione.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-button:hover {\r\n  border: solid 0.5px;\r\n  background-color: white;\r\n  color: #09357a;\r\n  cursor: pointer;\r\n}\r\n\r\n\r\n.login-button {\r\n  background-color: #09357a;\r\n  color: white;\r\n  margin-top: 5px;\r\n\r\n}\r\n\r\n\r\n.select{\r\n  width: 350px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQ29tcG9uZW50cy9lc3RyYXppb25lL2VzdHJhemlvbmUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsY0FBYztFQUNkLGVBQWU7QUFDakI7OztBQUdBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixlQUFlOztBQUVqQjs7O0FBRUE7RUFDRSxZQUFZO0FBQ2QiLCJmaWxlIjoic3JjL2FwcC9Db21wb25lbnRzL2VzdHJhemlvbmUvZXN0cmF6aW9uZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ2luLWJ1dHRvbjpob3ZlciB7XHJcbiAgYm9yZGVyOiBzb2xpZCAwLjVweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICBjb2xvcjogIzA5MzU3YTtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcblxyXG4ubG9naW4tYnV0dG9uIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDkzNTdhO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBtYXJnaW4tdG9wOiA1cHg7XHJcblxyXG59XHJcblxyXG4uc2VsZWN0e1xyXG4gIHdpZHRoOiAzNTBweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/Components/estrazione/estrazione.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/Components/estrazione/estrazione.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>Estrazione</h2>\r\n\r\n<div style=\"color: #09357a;\" class=\"w-100 flex center\">\r\n  <form [formGroup]=\"estrazione\">\r\n    <br />\r\n\r\n    <mat-form-field class=\"select\">\r\n      <mat-select\r\n        placeholder=\"Responsabile Controllo\"\r\n        [formControl]=\"responsabileControllo\"\r\n        required\r\n      >\r\n        <mat-option>--</mat-option>\r\n        <mat-option *ngFor=\"let RE of listaConoRE\" [value]=\"RE\">\r\n          {{ RE.nome }} {{ RE.cognome }}\r\n        </mat-option>\r\n      </mat-select>\r\n      <mat-error *ngIf=\"responsabileControllo.hasError('required')\"\r\n        >Campo Obbligatorio</mat-error\r\n      >\r\n    </mat-form-field>\r\n\r\n    <br />\r\n\r\n    <mat-form-field class=\"select\" >\r\n      <mat-select\r\n        placeholder=\"Responsabile Singolo Controllo\"\r\n        [formControl]=\"responsabileSingoloControllo\"\r\n        required\r\n      >\r\n        <mat-option>--</mat-option>\r\n        <mat-option *ngFor=\"let RE of listaConoRE\" [value]=\"RE\">\r\n          {{ RE.nome }} {{ RE.cognome }}\r\n        </mat-option>\r\n      </mat-select>\r\n      <mat-error *ngIf=\"responsabileSingoloControllo.hasError('required')\"\r\n        >Campo Obbligatorio</mat-error\r\n      >\r\n    </mat-form-field>\r\n\r\n    <br />\r\n\r\n    <mat-form-field class=\"select\">\r\n      <mat-select placeholder=\"Funzione\" [formControl]=\"funzione\" required>\r\n        <mat-option>--</mat-option>\r\n        <mat-option *ngFor=\"let f of funzioni\" [value]=\"f\">\r\n          {{ f.funzione }}\r\n        </mat-option>\r\n      </mat-select>\r\n      <mat-error *ngIf=\"funzione.hasError('required')\"\r\n        >Campo Obbligatorio</mat-error\r\n      >\r\n    </mat-form-field>\r\n\r\n    <br />\r\n\r\n    <div class=\"flex center\">\r\n      <input type=\"submit\" class=\"btn login-button\" value=\"Ricerca\" />\r\n    </div>\r\n  </form>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/Components/estrazione/estrazione.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/Components/estrazione/estrazione.component.ts ***!
  \***************************************************************/
/*! exports provided: EstrazioneComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EstrazioneComponent", function() { return EstrazioneComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_services_lista_conoRE_lista_cono_re_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/lista-conoRE/lista-cono-re.service */ "./src/app/services/lista-conoRE/lista-cono-re.service.ts");




var EstrazioneComponent = /** @class */ (function () {
    function EstrazioneComponent(fb, service) {
        this.fb = fb;
        this.service = service;
        this.responsabileControllo = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
        this.responsabileSingoloControllo = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
        this.funzione = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
        this.funzioni = [
            {
                id: '008',
                funzione: 'AU'
            },
            {
                id: '057',
                funzione: 'CA'
            },
            {
                id: '052',
                funzione: 'FA'
            },
            {
                id: '059',
                funzione: 'FL'
            },
            {
                id: '024',
                funzione: 'RM'
            }
        ];
        this.get();
    }
    EstrazioneComponent.prototype.ngOnInit = function () {
        this.estrazione = this.fb.group({
            responsabileControllo: '',
            responsabileSingoloControllo: '',
            funzione: ''
        });
    };
    EstrazioneComponent.prototype.get = function () {
        var _this = this;
        this.service.getListaConoRE().subscribe(function (res) {
            _this.listaConoRE = res;
        });
    };
    EstrazioneComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-estrazione',
            template: __webpack_require__(/*! ./estrazione.component.html */ "./src/app/Components/estrazione/estrazione.component.html"),
            styles: [__webpack_require__(/*! ./estrazione.component.css */ "./src/app/Components/estrazione/estrazione.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], src_app_services_lista_conoRE_lista_cono_re_service__WEBPACK_IMPORTED_MODULE_3__["ListaConoREService"]])
    ], EstrazioneComponent);
    return EstrazioneComponent;
}());



/***/ }),

/***/ "./src/app/Components/fase-predisposizione/fase-predisposizione.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/Components/fase-predisposizione/fase-predisposizione.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL0NvbXBvbmVudHMvZmFzZS1wcmVkaXNwb3NpemlvbmUvZmFzZS1wcmVkaXNwb3NpemlvbmUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/Components/fase-predisposizione/fase-predisposizione.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/Components/fase-predisposizione/fase-predisposizione.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"color: #09357a;\" class=\"w-100\">\r\n  <app-search-bar></app-search-bar>\r\n\r\n  <h3>Fase Predisposizione</h3>\r\n\r\n  <div class=\"div-title m-t10\">\r\n    <div>\r\n      <button\r\n        type=\"button\"\r\n        class=\"btn btn-success\"\r\n        style=\"margin-left:20px;margin-top: 10px;\"\r\n      >\r\n        Nuova Esternalizzazione\r\n      </button>\r\n    </div>\r\n  </div>\r\n\r\n  <app-predisposizione></app-predisposizione>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/Components/fase-predisposizione/fase-predisposizione.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/Components/fase-predisposizione/fase-predisposizione.component.ts ***!
  \***********************************************************************************/
/*! exports provided: FasePredisposizioneComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FasePredisposizioneComponent", function() { return FasePredisposizioneComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FasePredisposizioneComponent = /** @class */ (function () {
    function FasePredisposizioneComponent() {
    }
    FasePredisposizioneComponent.prototype.ngOnInit = function () {
    };
    FasePredisposizioneComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fase-predisposizione',
            template: __webpack_require__(/*! ./fase-predisposizione.component.html */ "./src/app/Components/fase-predisposizione/fase-predisposizione.component.html"),
            styles: [__webpack_require__(/*! ./fase-predisposizione.component.css */ "./src/app/Components/fase-predisposizione/fase-predisposizione.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FasePredisposizioneComponent);
    return FasePredisposizioneComponent;
}());



/***/ }),

/***/ "./src/app/Components/fase-scad-outsourcing/fase-scad-outsourcing.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/Components/fase-scad-outsourcing/fase-scad-outsourcing.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL0NvbXBvbmVudHMvZmFzZS1zY2FkLW91dHNvdXJjaW5nL2Zhc2Utc2NhZC1vdXRzb3VyY2luZy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/Components/fase-scad-outsourcing/fase-scad-outsourcing.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/Components/fase-scad-outsourcing/fase-scad-outsourcing.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"color: #09357a;\" class=\"w-100\">\r\n  <app-search-bar></app-search-bar>\r\n\r\n  <h3>Fase Scadenza Outsourcing</h3>\r\n\r\n  <div class=\"div-title m-t10\">\r\n    <div>\r\n      <button\r\n        type=\"button\"\r\n        class=\"btn btn-success\"\r\n        style=\"margin-left:20px;margin-top: 10px;\"\r\n      >\r\n        Nuova Esternalizzazione\r\n      </button>\r\n    </div>\r\n  </div>\r\n\r\n  <app-scad-outsourcing></app-scad-outsourcing>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/Components/fase-scad-outsourcing/fase-scad-outsourcing.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/Components/fase-scad-outsourcing/fase-scad-outsourcing.component.ts ***!
  \*************************************************************************************/
/*! exports provided: FaseScadOutsourcingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaseScadOutsourcingComponent", function() { return FaseScadOutsourcingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FaseScadOutsourcingComponent = /** @class */ (function () {
    function FaseScadOutsourcingComponent() {
    }
    FaseScadOutsourcingComponent.prototype.ngOnInit = function () {
    };
    FaseScadOutsourcingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fase-scad-outsourcing',
            template: __webpack_require__(/*! ./fase-scad-outsourcing.component.html */ "./src/app/Components/fase-scad-outsourcing/fase-scad-outsourcing.component.html"),
            styles: [__webpack_require__(/*! ./fase-scad-outsourcing.component.css */ "./src/app/Components/fase-scad-outsourcing/fase-scad-outsourcing.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FaseScadOutsourcingComponent);
    return FaseScadOutsourcingComponent;
}());



/***/ }),

/***/ "./src/app/Components/fase-val-outsourcing/fase-val-outsourcing.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/Components/fase-val-outsourcing/fase-val-outsourcing.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL0NvbXBvbmVudHMvZmFzZS12YWwtb3V0c291cmNpbmcvZmFzZS12YWwtb3V0c291cmNpbmcuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/Components/fase-val-outsourcing/fase-val-outsourcing.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/Components/fase-val-outsourcing/fase-val-outsourcing.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"color: #09357a;\" class=\"w-100\">\r\n  <app-search-bar></app-search-bar>\r\n\r\n  <h3>Fase Valutazione Outsourcing</h3>\r\n\r\n  <div class=\"div-title m-t10\">\r\n    <div>\r\n      <button\r\n        type=\"button\"\r\n        class=\"btn btn-success\"\r\n        style=\"margin-left:20px;margin-top: 10px;\"\r\n      >\r\n        Nuova Esternalizzazione\r\n      </button>\r\n    </div>\r\n  </div>\r\n\r\n  <app-outsourcing></app-outsourcing>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/Components/fase-val-outsourcing/fase-val-outsourcing.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/Components/fase-val-outsourcing/fase-val-outsourcing.component.ts ***!
  \***********************************************************************************/
/*! exports provided: FaseValOutsourcingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaseValOutsourcingComponent", function() { return FaseValOutsourcingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FaseValOutsourcingComponent = /** @class */ (function () {
    function FaseValOutsourcingComponent() {
    }
    FaseValOutsourcingComponent.prototype.ngOnInit = function () {
    };
    FaseValOutsourcingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fase-val-outsourcing',
            template: __webpack_require__(/*! ./fase-val-outsourcing.component.html */ "./src/app/Components/fase-val-outsourcing/fase-val-outsourcing.component.html"),
            styles: [__webpack_require__(/*! ./fase-val-outsourcing.component.css */ "./src/app/Components/fase-val-outsourcing/fase-val-outsourcing.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FaseValOutsourcingComponent);
    return FaseValOutsourcingComponent;
}());



/***/ }),

/***/ "./src/app/Components/fase-valutazione/fase-valutazione.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/Components/fase-valutazione/fase-valutazione.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL0NvbXBvbmVudHMvZmFzZS12YWx1dGF6aW9uZS9mYXNlLXZhbHV0YXppb25lLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/Components/fase-valutazione/fase-valutazione.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/Components/fase-valutazione/fase-valutazione.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"color: #09357a;\" class=\"w-100\">\r\n  <app-search-bar></app-search-bar>\r\n\r\n  <h3>Fase Valutazione</h3>\r\n\r\n\r\n  <div class=\"div-title m-t10\">\r\n    <div>\r\n      <button\r\n        type=\"button\"\r\n        class=\"btn btn-success\"\r\n        style=\"margin-left:20px;margin-top: 10px;\"\r\n      >\r\n        Nuova Esternalizzazione\r\n      </button>\r\n    </div>\r\n  </div>\r\n\r\n  <app-valutazione></app-valutazione>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/Components/fase-valutazione/fase-valutazione.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/Components/fase-valutazione/fase-valutazione.component.ts ***!
  \***************************************************************************/
/*! exports provided: FaseValutazioneComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaseValutazioneComponent", function() { return FaseValutazioneComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FaseValutazioneComponent = /** @class */ (function () {
    function FaseValutazioneComponent() {
    }
    FaseValutazioneComponent.prototype.ngOnInit = function () {
    };
    FaseValutazioneComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fase-valutazione',
            template: __webpack_require__(/*! ./fase-valutazione.component.html */ "./src/app/Components/fase-valutazione/fase-valutazione.component.html"),
            styles: [__webpack_require__(/*! ./fase-valutazione.component.css */ "./src/app/Components/fase-valutazione/fase-valutazione.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FaseValutazioneComponent);
    return FaseValutazioneComponent;
}());



/***/ }),

/***/ "./src/app/Components/home/home.component.css":
/*!****************************************************!*\
  !*** ./src/app/Components/home/home.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  /* classi css per text-bar */\r\n\r\n  body {\r\n    font-family: Arial;\r\n  }\r\n\r\n  * {\r\n    box-sizing: border-box;\r\n  }\r\n\r\n  .f-20{\r\n    font-size: 20px;\r\n  }\r\n\r\n  .border{\r\n    border-style: solid;\r\n    border-bottom-style: solid;\r\n    border-color: black;\r\n    border: 1px;\r\n  }\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQ29tcG9uZW50cy9ob21lL2hvbWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiRUFBRSw0QkFBNEI7O0VBRTVCO0lBQ0Usa0JBQWtCO0VBQ3BCOztFQUVBO0lBQ0Usc0JBQXNCO0VBQ3hCOztFQUlBO0lBQ0UsZUFBZTtFQUNqQjs7RUFFQTtJQUNFLG1CQUFtQjtJQUNuQiwwQkFBMEI7SUFDMUIsbUJBQW1CO0lBQ25CLFdBQVc7RUFDYiIsImZpbGUiOiJzcmMvYXBwL0NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgIC8qIGNsYXNzaSBjc3MgcGVyIHRleHQtYmFyICovXHJcblxyXG4gIGJvZHkge1xyXG4gICAgZm9udC1mYW1pbHk6IEFyaWFsO1xyXG4gIH1cclxuXHJcbiAgKiB7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIH1cclxuXHJcblxyXG5cclxuICAuZi0yMHtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICB9XHJcblxyXG4gIC5ib3JkZXJ7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgYm9yZGVyLWJvdHRvbS1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItY29sb3I6IGJsYWNrO1xyXG4gICAgYm9yZGVyOiAxcHg7XHJcbiAgfVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/Components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/Components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div>\r\n  <br />\r\n\r\n    <mat-tab-group>\r\n      <mat-tab class=\"w-50\" label=\"Esternalizzazioni\">\r\n\r\n        <app-search-bar></app-search-bar>\r\n\r\n        <div class=\"div-title\">\r\n            <button\r\n              type=\"button\"\r\n              class=\"btn btn-success\"\r\n              style=\"margin-left:20px;margin-top: 10px;\"\r\n              (click) = \"nuovaEsternalizzazione()\"\r\n              routerLink=\"/esternalizzazione-new\"\r\n              routerLinkActive=\"router-link-active\"\r\n            >\r\n              Nuova Esternalizzazione\r\n            </button>\r\n        </div>\r\n\r\n        <app-esternalizzazioni></app-esternalizzazioni>\r\n\r\n      </mat-tab>\r\n      <mat-tab label=\"Lista Task\">\r\n\r\n        <app-task></app-task>\r\n\r\n      </mat-tab>\r\n    </mat-tab-group>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/Components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/Components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");



var HomeComponent = /** @class */ (function () {
    function HomeComponent(varG) {
        this.varG = varG;
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent.prototype.nuovaEsternalizzazione = function () {
        this.varG.setPosizione('nuova');
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/Components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/Components/home/home.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_2__["VariabiliGlobali"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Components_fase_valutazione_fase_valutazione_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Components/fase-valutazione/fase-valutazione.component */ "./src/app/Components/fase-valutazione/fase-valutazione.component.ts");
/* harmony import */ var _Components_fase_predisposizione_fase_predisposizione_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Components/fase-predisposizione/fase-predisposizione.component */ "./src/app/Components/fase-predisposizione/fase-predisposizione.component.ts");
/* harmony import */ var _Components_fase_scad_outsourcing_fase_scad_outsourcing_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Components/fase-scad-outsourcing/fase-scad-outsourcing.component */ "./src/app/Components/fase-scad-outsourcing/fase-scad-outsourcing.component.ts");
/* harmony import */ var _Components_estrazione_estrazione_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Components/estrazione/estrazione.component */ "./src/app/Components/estrazione/estrazione.component.ts");
/* harmony import */ var _Components_home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Components/home/home.component */ "./src/app/Components/home/home.component.ts");
/* harmony import */ var _Components_fase_val_outsourcing_fase_val_outsourcing_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./Components/fase-val-outsourcing/fase-val-outsourcing.component */ "./src/app/Components/fase-val-outsourcing/fase-val-outsourcing.component.ts");
/* harmony import */ var _components_lista_task_applicativi_lista_task_applicativi_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/lista-task-applicativi/lista-task-applicativi.component */ "./src/app/components/lista-task-applicativi/lista-task-applicativi.component.ts");
/* harmony import */ var _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component.ts");
/* harmony import */ var _components_task_dettaglio_task_dettaglio_task_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/task/dettaglio-task/dettaglio-task.component */ "./src/app/components/task/dettaglio-task/dettaglio-task.component.ts");












var routes = [
    { path: '', component: _Components_home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"] },
    { path: 'home', component: _Components_home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"] },
    { path: 'fase-valutazione', component: _Components_fase_valutazione_fase_valutazione_component__WEBPACK_IMPORTED_MODULE_3__["FaseValutazioneComponent"] },
    { path: 'fase-predisposizione', component: _Components_fase_predisposizione_fase_predisposizione_component__WEBPACK_IMPORTED_MODULE_4__["FasePredisposizioneComponent"] },
    { path: 'fase-val-outsourcing', component: _Components_fase_val_outsourcing_fase_val_outsourcing_component__WEBPACK_IMPORTED_MODULE_8__["FaseValOutsourcingComponent"] },
    { path: 'fase-scad-outsourcing', component: _Components_fase_scad_outsourcing_fase_scad_outsourcing_component__WEBPACK_IMPORTED_MODULE_5__["FaseScadOutsourcingComponent"] },
    { path: 'estrazione', component: _Components_estrazione_estrazione_component__WEBPACK_IMPORTED_MODULE_6__["EstrazioneComponent"] },
    { path: 'lista-task-applicativi', component: _components_lista_task_applicativi_lista_task_applicativi_component__WEBPACK_IMPORTED_MODULE_9__["ListaTaskApplicativiComponent"] },
    { path: 'esternalizzazione-details', component: _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_component__WEBPACK_IMPORTED_MODULE_10__["DettaglioEsternalizzazioneComponent"] },
    { path: 'esternalizzazione-new', component: _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_component__WEBPACK_IMPORTED_MODULE_10__["DettaglioEsternalizzazioneComponent"] },
    { path: 'task-details', component: _components_task_dettaglio_task_dettaglio_task_component__WEBPACK_IMPORTED_MODULE_11__["DettaglioTaskComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  /* classi css per tabella sinistra */\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtFQUNFLG9DQUFvQyIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbiAgLyogY2xhc3NpIGNzcyBwZXIgdGFiZWxsYSBzaW5pc3RyYSAqL1xyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <app-login (logged)=\"logIn($event)\" *ngIf=\"!log\"></app-login> -->\r\n\r\n<div style=\"height: 90%;\" *ngIf=\"true\" >\r\n  <div class=\"flex\" style=\"height: 100%;\">\r\n    <app-body class=\"w-100 padding\"></app-body>\r\n  </div>\r\n</div>\r\n\r\n<!-- <div style=\"height: 90%;\" *ngIf=\"log\" >\r\n    <div class=\"flex\" style=\"height: 100%;\">\r\n      <app-body class=\"w-100 padding\"></app-body>\r\n    </div>\r\n  </div>\r\n   -->"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_esternalizzazione_fornitori_fornitori_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/esternalizzazione/fornitori/fornitori.service */ "./src/app/services/esternalizzazione/fornitori/fornitori.service.ts");
/* harmony import */ var _variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");





var AppComponent = /** @class */ (function () {
    function AppComponent(fornitoriService, varG, cookieService) {
        this.fornitoriService = fornitoriService;
        this.varG = varG;
        this.cookieService = cookieService;
        this.title = 'Esternalizzazione';
        this.cookieValue = '';
        this.log = false;
        this.getListaFornitori('a');
    }
    // tslint:disable-next-line:use-life-cycle-interface
    AppComponent.prototype.ngOnInit = function () {
        this.cookieValue = this.cookieService.get('Cookie');
        console.log('@@@@@@ COOKIE VALUE: ' + this.cookieValue);
    };
    AppComponent.prototype.logIn = function (login) {
        this.log = login;
    };
    AppComponent.prototype.getListaFornitori = function (ambito) {
        var _this = this;
        this.fornitoriService.getListaFornitori(ambito).subscribe(function (res) {
            _this.varG.setFornitori(res);
        });
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_esternalizzazione_fornitori_fornitori_service__WEBPACK_IMPORTED_MODULE_2__["FornitoriService"], _variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/esm5/chips.es5.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _components_task_task_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/task/task.component */ "./src/app/components/task/task.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _w11k_angular_sticky_things__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @w11k/angular-sticky-things */ "./node_modules/@w11k/angular-sticky-things/fesm5/w11k-angular-sticky-things.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var angular_material_fileupload__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! angular-material-fileupload */ "./node_modules/angular-material-fileupload/matFileUpload.esm.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm5/autocomplete.es5.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _Components_fase_predisposizione_fase_predisposizione_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./Components/fase-predisposizione/fase-predisposizione.component */ "./src/app/Components/fase-predisposizione/fase-predisposizione.component.ts");
/* harmony import */ var _Components_fase_val_outsourcing_fase_val_outsourcing_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./Components/fase-val-outsourcing/fase-val-outsourcing.component */ "./src/app/Components/fase-val-outsourcing/fase-val-outsourcing.component.ts");
/* harmony import */ var _Components_fase_scad_outsourcing_fase_scad_outsourcing_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./Components/fase-scad-outsourcing/fase-scad-outsourcing.component */ "./src/app/Components/fase-scad-outsourcing/fase-scad-outsourcing.component.ts");
/* harmony import */ var _Components_estrazione_estrazione_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./Components/estrazione/estrazione.component */ "./src/app/Components/estrazione/estrazione.component.ts");
/* harmony import */ var _Components_fase_valutazione_fase_valutazione_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./Components/fase-valutazione/fase-valutazione.component */ "./src/app/Components/fase-valutazione/fase-valutazione.component.ts");
/* harmony import */ var _Components_home_home_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./Components/home/home.component */ "./src/app/Components/home/home.component.ts");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/components/header/header.component.ts");
/* harmony import */ var _components_body_body_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./components/body/body.component */ "./src/app/components/body/body.component.ts");
/* harmony import */ var _components_search_bar_search_bar_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./components/search-bar/search-bar.component */ "./src/app/components/search-bar/search-bar.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_lista_task_applicativi_lista_task_applicativi_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./components/lista-task-applicativi/lista-task-applicativi.component */ "./src/app/components/lista-task-applicativi/lista-task-applicativi.component.ts");
/* harmony import */ var _components_valutazione_valutazione_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./components/valutazione/valutazione.component */ "./src/app/components/valutazione/valutazione.component.ts");
/* harmony import */ var _components_predisposizione_predisposizione_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./components/predisposizione/predisposizione.component */ "./src/app/components/predisposizione/predisposizione.component.ts");
/* harmony import */ var _components_outsourcing_outsourcing_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./components/outsourcing/outsourcing.component */ "./src/app/components/outsourcing/outsourcing.component.ts");
/* harmony import */ var _components_scad_outsourcing_scad_outsourcing_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./components/scad-outsourcing/scad-outsourcing.component */ "./src/app/components/scad-outsourcing/scad-outsourcing.component.ts");
/* harmony import */ var _components_task_app_task_app_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./components/task-app/task-app.component */ "./src/app/components/task-app/task-app.component.ts");
/* harmony import */ var _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component.ts");
/* harmony import */ var _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_components_fase_inserimento_fase_inserimento_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-inserimento/fase-inserimento.component */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-inserimento/fase-inserimento.component.ts");
/* harmony import */ var _components_dettaglio_esternalizzazione_actions_bar_actions_bar_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./components/dettaglio-esternalizzazione/actions-bar/actions-bar.component */ "./src/app/components/dettaglio-esternalizzazione/actions-bar/actions-bar.component.ts");
/* harmony import */ var _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_components_fase_valutazione_fase_valutazione_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-valutazione/fase-valutazione.component */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-valutazione/fase-valutazione.component.ts");
/* harmony import */ var _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_components_fase_predisposizione_fase_predisposizione_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-predisposizione/fase-predisposizione.component */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-predisposizione/fase-predisposizione.component.ts");
/* harmony import */ var _components_esternalizzazioni_esternalizzazioni_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./components/esternalizzazioni/esternalizzazioni.component */ "./src/app/components/esternalizzazioni/esternalizzazioni.component.ts");
/* harmony import */ var _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_components_sezione_controlli_sezione_controlli_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/sezione-controlli/sezione-controlli.component */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/sezione-controlli/sezione-controlli.component.ts");
/* harmony import */ var _components_task_dettaglio_task_dettaglio_task_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./components/task/dettaglio-task/dettaglio-task.component */ "./src/app/components/task/dettaglio-task/dettaglio-task.component.ts");
/* harmony import */ var _components_task_actions_bar_task_actions_bar_task_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./components/task/actions-bar-task/actions-bar-task.component */ "./src/app/components/task/actions-bar-task/actions-bar-task.component.ts");
/* harmony import */ var _components_user_user_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./components/user/user.component */ "./src/app/components/user/user.component.ts");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");
/* harmony import */ var _variabili_globali_edit_combinato__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./variabili-globali/edit-combinato */ "./src/app/variabili-globali/edit-combinato.ts");
// tslint:disable: max-line-length

// <-- IMPORTAZIONE MODULI -->






















// <--- IMPORTAZIONE COMPONETS -->



























// <-- FILE TS CONTENENTI VARIBILI USATE DA PIÙ -->


var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_22__["AppComponent"],
                _Components_fase_valutazione_fase_valutazione_component__WEBPACK_IMPORTED_MODULE_27__["FaseValutazioneComponent"],
                _Components_fase_predisposizione_fase_predisposizione_component__WEBPACK_IMPORTED_MODULE_23__["FasePredisposizioneComponent"],
                _Components_fase_val_outsourcing_fase_val_outsourcing_component__WEBPACK_IMPORTED_MODULE_24__["FaseValOutsourcingComponent"],
                _Components_fase_scad_outsourcing_fase_scad_outsourcing_component__WEBPACK_IMPORTED_MODULE_25__["FaseScadOutsourcingComponent"],
                _Components_estrazione_estrazione_component__WEBPACK_IMPORTED_MODULE_26__["EstrazioneComponent"],
                _Components_home_home_component__WEBPACK_IMPORTED_MODULE_28__["HomeComponent"],
                _components_header_header_component__WEBPACK_IMPORTED_MODULE_29__["HeaderComponent"],
                _components_body_body_component__WEBPACK_IMPORTED_MODULE_30__["BodyComponent"],
                _components_search_bar_search_bar_component__WEBPACK_IMPORTED_MODULE_31__["SearchBarComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_32__["LoginComponent"],
                _components_lista_task_applicativi_lista_task_applicativi_component__WEBPACK_IMPORTED_MODULE_33__["ListaTaskApplicativiComponent"],
                _components_esternalizzazioni_esternalizzazioni_component__WEBPACK_IMPORTED_MODULE_44__["EsternalizzazioniComponent"],
                _components_task_task_component__WEBPACK_IMPORTED_MODULE_14__["TaskComponent"],
                _components_valutazione_valutazione_component__WEBPACK_IMPORTED_MODULE_34__["ValutazioneComponent"],
                _components_predisposizione_predisposizione_component__WEBPACK_IMPORTED_MODULE_35__["PredisposizioneComponent"],
                _components_outsourcing_outsourcing_component__WEBPACK_IMPORTED_MODULE_36__["OutsourcingComponent"],
                _components_scad_outsourcing_scad_outsourcing_component__WEBPACK_IMPORTED_MODULE_37__["ScadOutsourcingComponent"],
                _components_task_app_task_app_component__WEBPACK_IMPORTED_MODULE_38__["TaskAppComponent"],
                _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_component__WEBPACK_IMPORTED_MODULE_39__["DettaglioEsternalizzazioneComponent"],
                _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_components_fase_inserimento_fase_inserimento_component__WEBPACK_IMPORTED_MODULE_40__["FaseInserimentoComponent"],
                _components_dettaglio_esternalizzazione_actions_bar_actions_bar_component__WEBPACK_IMPORTED_MODULE_41__["ActionsBarComponent"],
                _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_components_fase_valutazione_fase_valutazione_component__WEBPACK_IMPORTED_MODULE_42__["FaseValutazioneDettComponent"],
                _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_components_fase_predisposizione_fase_predisposizione_component__WEBPACK_IMPORTED_MODULE_43__["FasePredisposizioneDettComponent"],
                _components_dettaglio_esternalizzazione_dettaglio_esternalizzazione_components_sezione_controlli_sezione_controlli_component__WEBPACK_IMPORTED_MODULE_45__["SezioneControlliComponent"],
                _components_task_dettaglio_task_dettaglio_task_component__WEBPACK_IMPORTED_MODULE_46__["DettaglioTaskComponent"],
                _components_task_actions_bar_task_actions_bar_task_component__WEBPACK_IMPORTED_MODULE_47__["ActionsBarTaskComponent"],
                _components_user_user_component__WEBPACK_IMPORTED_MODULE_48__["UserComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__["BrowserAnimationsModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_9__["MatCheckboxModule"],
                _angular_material_chips__WEBPACK_IMPORTED_MODULE_10__["MatChipsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_11__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatPaginatorModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_16__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatNativeDateModule"],
                _w11k_angular_sticky_things__WEBPACK_IMPORTED_MODULE_17__["AngularStickyThingsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
                angular_material_fileupload__WEBPACK_IMPORTED_MODULE_19__["MatFileUploadModule"],
                _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_20__["MatAutocompleteModule"]
            ],
            providers: [
                src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_49__["VariabiliGlobali"],
                _variabili_globali_edit_combinato__WEBPACK_IMPORTED_MODULE_50__["EditCombinato"],
                ngx_cookie_service__WEBPACK_IMPORTED_MODULE_21__["CookieService"]
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_15__["MatPaginatorModule"]
            ],
            bootstrap: [
                _app_component__WEBPACK_IMPORTED_MODULE_22__["AppComponent"],
                _components_header_header_component__WEBPACK_IMPORTED_MODULE_29__["HeaderComponent"]
            ]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/body/body.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/body/body.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".div-body {\r\n  color: #09357a;\r\n  padding-top: 10px;\r\n  background:white;\r\n  align-items: center;\r\n  height: 100%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ib2R5L2JvZHkuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixZQUFZO0FBQ2QiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2JvZHkvYm9keS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRpdi1ib2R5IHtcclxuICBjb2xvcjogIzA5MzU3YTtcclxuICBwYWRkaW5nLXRvcDogMTBweDtcclxuICBiYWNrZ3JvdW5kOndoaXRlO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/body/body.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/body/body.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"div-body\">\n  <router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "./src/app/components/body/body.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/body/body.component.ts ***!
  \***************************************************/
/*! exports provided: BodyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyComponent", function() { return BodyComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var BodyComponent = /** @class */ (function () {
    function BodyComponent() {
    }
    BodyComponent.prototype.ngOnInit = function () {
    };
    BodyComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-body',
            template: __webpack_require__(/*! ./body.component.html */ "./src/app/components/body/body.component.html"),
            styles: [__webpack_require__(/*! ./body.component.css */ "./src/app/components/body/body.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], BodyComponent);
    return BodyComponent;
}());



/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/actions-bar/actions-bar.component.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/actions-bar/actions-bar.component.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".previous-page-btn {\r\n    background-color: #09357a;\r\n    color: white;\r\n  }\r\n  .previous-page-btn:hover {\r\n    border: solid 0.5px;\r\n    background-color: white;\r\n    color: #09357a;\r\n    cursor: pointer;\r\n  }\r\n  .edit-btn {\r\n    background-color:#15BD17;\r\n    color: white;\r\n  }\r\n  .edit-btn:hover {\r\n    background-color: white;\r\n    color: #15BD17;\r\n    border: solid 0.5px;\r\n    cursor: pointer;\r\n  }\r\n  .resp-btn {\r\n    background-color:#E02A2A;\r\n    color: white;\r\n  }\r\n  .resp-btn:hover {\r\n    background-color: white;\r\n    color: #E02A2A;\r\n    border: solid 0.5px;\r\n    cursor: pointer;\r\n  }\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9kZXR0YWdsaW8tZXN0ZXJuYWxpenphemlvbmUvYWN0aW9ucy1iYXIvYWN0aW9ucy1iYXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHlCQUF5QjtJQUN6QixZQUFZO0VBQ2Q7RUFDQTtJQUNFLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsY0FBYztJQUNkLGVBQWU7RUFDakI7RUFDQTtJQUNFLHdCQUF3QjtJQUN4QixZQUFZO0VBQ2Q7RUFFQTtJQUNFLHVCQUF1QjtJQUN2QixjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLGVBQWU7RUFDakI7RUFFQTtJQUNFLHdCQUF3QjtJQUN4QixZQUFZO0VBQ2Q7RUFFQTtJQUNFLHVCQUF1QjtJQUN2QixjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLGVBQWU7RUFDakIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2RldHRhZ2xpby1lc3Rlcm5hbGl6emF6aW9uZS9hY3Rpb25zLWJhci9hY3Rpb25zLWJhci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnByZXZpb3VzLXBhZ2UtYnRuIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMwOTM1N2E7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG4gIC5wcmV2aW91cy1wYWdlLWJ0bjpob3ZlciB7XHJcbiAgICBib3JkZXI6IHNvbGlkIDAuNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBjb2xvcjogIzA5MzU3YTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcbiAgLmVkaXQtYnRuIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IzE1QkQxNztcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbiAgXHJcbiAgLmVkaXQtYnRuOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgY29sb3I6ICMxNUJEMTc7XHJcbiAgICBib3JkZXI6IHNvbGlkIDAuNXB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxuICBcclxuICAucmVzcC1idG4ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojRTAyQTJBO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuICBcclxuICAucmVzcC1idG46aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBjb2xvcjogI0UwMkEyQTtcclxuICAgIGJvcmRlcjogc29saWQgMC41cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG4gICJdfQ== */"

/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/actions-bar/actions-bar.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/actions-bar/actions-bar.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #spacer></div>\r\n\r\n<div stickyThing [spacer]=\"spacer\" style=\"z-index:5;background-color: white;\" >\r\n  <div style=\"margin-bottom: 10px;\" class=\"d-flex center m-t10\">\r\n    <button class=\"btn previous-page-btn\" [routerLink]=\"[routerLink]\" routerLinkActive=\"router-link-active\"  >Torna alla lista</button>\r\n    <button class=\"btn edit-btn m-l10\" (click)=\"activeEditMode()\" *ngIf=\"edit\"> Edit</button>\r\n    <button class=\"btn edit-btn m-l10\" *ngIf=\"!edit\" (click)=\"saveChanges()\">Salva Modifiche</button>\r\n    <button class=\"btn resp-btn m-l10\" >Assegna Responsabile</button>\r\n    <button class=\"btn edit-btn m-l10\">Approva</button>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/actions-bar/actions-bar.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/actions-bar/actions-bar.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: ActionsBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionsBarComponent", function() { return ActionsBarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");



var ActionsBarComponent = /** @class */ (function () {
    function ActionsBarComponent(varG) {
        this.varG = varG;
        this.editOutput = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.edit = true;
        this.routerLink = this.varG.getRouterLink().valueOf();
    }
    ActionsBarComponent.prototype.ngOnInit = function () {
    };
    ActionsBarComponent.prototype.activeEditMode = function () {
        this.edit = false;
        this.editOutput.emit(false);
    };
    ActionsBarComponent.prototype.saveChanges = function () {
        this.edit = true;
        this.editOutput.emit(true);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ActionsBarComponent.prototype, "editOutput", void 0);
    ActionsBarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-actions-bar',
            template: __webpack_require__(/*! ./actions-bar.component.html */ "./src/app/components/dettaglio-esternalizzazione/actions-bar/actions-bar.component.html"),
            styles: [__webpack_require__(/*! ./actions-bar.component.css */ "./src/app/components/dettaglio-esternalizzazione/actions-bar/actions-bar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_2__["VariabiliGlobali"]])
    ], ActionsBarComponent);
    return ActionsBarComponent;
}());



/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-inserimento/fase-inserimento.component.css":
/*!***********************************************************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-inserimento/fase-inserimento.component.css ***!
  \***********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGV0dGFnbGlvLWVzdGVybmFsaXp6YXppb25lL2RldHRhZ2xpby1lc3Rlcm5hbGl6emF6aW9uZS1jb21wb25lbnRzL2Zhc2UtaW5zZXJpbWVudG8vZmFzZS1pbnNlcmltZW50by5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-inserimento/fase-inserimento.component.html":
/*!************************************************************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-inserimento/fase-inserimento.component.html ***!
  \************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!saved\">\r\n    <div class=\"flex\">\r\n        <div class=\"font w-80\">\r\n          <div class=\"row m-t10\" style=\"align-items: center;\">\r\n            <div class=\"col-sm-2\">\r\n              Data Creazione:\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n              <mat-form-field>\r\n                <input matInput \r\n                  [matDatepicker]=\"pickerDataCreazione\" \r\n                  placeholder=\"{{esternalizzazione.dataCreazioneEsternalizzazione}}\"\r\n                  [(ngModel)]=\"esternalizzazione.dataCreazioneEsternalizzazione\"\r\n                  [disabled]=\"editCombinato\"\r\n                  >\r\n                <mat-datepicker-toggle matSuffix [for]=\"pickerDataCreazione\"></mat-datepicker-toggle>\r\n                <mat-datepicker #pickerDataCreazione></mat-datepicker>\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          \r\n        \r\n          <div class=\"row m-t10\">\r\n            <div class=\"col-sm-2\">\r\n              Titolo* :\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n                 <input type=\"text\"\r\n                  [(ngModel)]=\"esternalizzazione.titoloEsternalizzazione\"\r\n                  [disabled]=\"editCombinato\"\r\n                 >\r\n                 \r\n                 <h5  *ngIf=\"errTitolo\" style=\"color:red;\">\r\n                   Campo Obbligatorio\r\n                 </h5>\r\n            </div>\r\n          </div>\r\n        \r\n          <div class=\"row m-t10\" style=\"align-items: center;\" >\r\n            <div class=\"col-sm-2\">\r\n              Tipologia Esternalizzazione* :\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n              <mat-form-field class=\"select w-70\">\r\n        \r\n                <mat-select\r\n                placeholder=\"{{esternalizzazione.descrizioneTipoEsternalizzazione}}\"\r\n                [(ngModel)]=\"esternalizzazione.tipoEsternalizzazione\"\r\n                [disabled]=\"editCombinato\"\r\n                >\r\n        \r\n                <mat-option>--</mat-option>\r\n                <mat-option *ngFor=\"let tipologia of listaTipologiaEsternalizzazioni\" [value]=\"tipologia.codice\">\r\n                 {{tipologia.descrizione}}\r\n                </mat-option>\r\n              </mat-select>\r\n              </mat-form-field>\r\n              \r\n              <h5  *ngIf=\"errTipo\" style=\"color:red;\">\r\n                  Campo Obbligatorio\r\n              </h5>\r\n           </div>\r\n          </div>\r\n        \r\n         <div *ngIf=\"esternalizzazione.stato === '2'\">\r\n          <div class=\"row m-t10\">\r\n            <div class=\"col-sm-2\">\r\n              Compilata Da:\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n                <input type=\"text\"\r\n                [(ngModel)]=\"esternalizzazione.compilataDa\"\r\n                [disabled]=\"editCombinato\">\r\n            </div>\r\n          </div>\r\n         </div>\r\n      \r\n         <div *ngIf=\"esternalizzazione.stato === '1'\" >\r\n          <div class=\"row m-t10\">\r\n            <div class=\"col-sm-2\">\r\n              Compilata Da:\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n                <input type=\"text\"\r\n                  [(ngModel)]=\"username\"          \r\n                  [disabled]=\"true\">\r\n            </div>\r\n          </div>\r\n         </div>\r\n        \r\n          <div class=\"row m-t10\" *ngIf=\"esternalizzazione.stato === '2'\">\r\n            <div class=\"col-sm-2\">\r\n              Data Modifica Tipologia:\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n                <input type=\"text\"\r\n                [(ngModel)]=\"esternalizzazione.dataModificaTipologiaEsternalizzazione\"\r\n                [disabled]=\"editCombinato\">\r\n            </div>\r\n          </div>\r\n      \r\n          <div class=\"row m-t10\" *ngIf=\"esternalizzazione.stato === '1'\" >\r\n              <div class=\"col-sm-2\">\r\n                Data Modifica Tipologia:\r\n              </div>\r\n              <div class=\"col-sm-10\">\r\n                  <input type=\"text\"\r\n                  [(ngModel)]=\"esternalizzazione.dataModificaTipologiaEsternalizzazione\"\r\n                  [disabled]=\"true\">\r\n              </div>\r\n            </div>\r\n        \r\n          <div class=\"row m-t10\" style=\"align-items: center\">\r\n            <div class=\"col-sm-2\">\r\n              Utente Modifica:\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n                <input type=\"text\"\r\n                [(ngModel)]=\"esternalizzazione.utenteModificaEsternalizzazione\"\r\n                [disabled]=\"editCombinato\"\r\n                >\r\n            </div>\r\n          </div>\r\n        \r\n          <div class=\"row m-t10\">\r\n           <div class=\"col-sm-2\">\r\n            Motivazione* :\r\n           </div>\r\n            <div class=\"col-sm-10\">\r\n                <input type=\"text\"\r\n                [(ngModel)]=\"esternalizzazione.motivazioneEsternalizzazione\"\r\n                [disabled]=\"editCombinato\">\r\n      \r\n                <h5  *ngIf=\"errMotivazione\" style=\"color:red;\">\r\n                    Campo Obbligatorio\r\n                </h5>\r\n            </div>\r\n          </div>\r\n        \r\n          <div class=\"row m-t10\" style=\"align-items:center\">\r\n           <div class=\"col-sm-2\">\r\n            Responsabile Controllo:\r\n           </div>\r\n            <div class=\"col-sm-10\">\r\n        \r\n              <mat-form-field class=\"select w-70\">\r\n        \r\n              <mat-select\r\n              placeholder=\"{{esternalizzazione.responsabileControlloEsternalizzazione}}\"\r\n              [(ngModel)]=\"esternalizzazione.responsabileControlloEsternalizzazione\"\r\n              [disabled]=\"editCombinato\"\r\n              >\r\n        \r\n              <mat-option>--</mat-option>\r\n              <mat-option *ngFor=\"let RE of listaConoRE\" [value]=\"RE.username\">\r\n                {{ RE.nome }} - {{ RE.cognome }}\r\n              </mat-option>\r\n            </mat-select>\r\n            </mat-form-field>\r\n            </div>\r\n          </div>\r\n        \r\n        \r\n          <div class=\"row m-t10\" style=\"align-items:center\">\r\n            <div class=\"col-sm-2\">\r\n             Responsabile Singolo Controllo:\r\n            </div>\r\n             <div class=\"col-sm-10\">\r\n        \r\n               <mat-form-field class=\"select w-70\">\r\n        \r\n               <mat-select\r\n               [(ngModel)]=\"esternalizzazione.responsabileSingoloControllo\"\r\n               placeholder=\"{{esternalizzazione.responsabileSingoloControllo}}\"\r\n               [disabled]=\"editCombinato\"\r\n               >\r\n        \r\n               <mat-option>--</mat-option>\r\n               <mat-option *ngFor=\"let RE of listaConoRE\" [value]=\"RE.username\">\r\n                 {{ RE.nome }} - {{ RE.cognome }}\r\n               </mat-option>\r\n             </mat-select>\r\n             </mat-form-field>\r\n             </div>\r\n           </div>\r\n        \r\n        </div>\r\n      \r\n        <div class=\"allegati padding w-20 m-t10\">\r\n      \r\n          <h4>Allegati Nomina Responsabile Controllo</h4>\r\n        \r\n                <div *ngIf=\"fileNomina.length!==0\">\r\n                    <div class=\"animation\" *ngFor=\"let file of fileNomina\">\r\n                        {{file.nomeFileOrigine}}\r\n                    </div>\r\n                  </div>\r\n              \r\n                  <div *ngIf=\"fileNomina.length === 0\">\r\n                      Nessun File Presente\r\n                  </div>\r\n        </div>\r\n      </div>\r\n      \r\n      <br>\r\n</div>\r\n\r\n<div *ngIf=\"gif\" class=\"flex gif\">\r\n    <img src=\"https://it.dplay.com/00406/static/resources/images/spinner.gif\" />\r\n  </div>\r\n\r\n<div *ngIf=\"!gif\">\r\n    <h4>\r\n      ESTERNALIZZAZIONE SALVATA CORRETTAMENTE\r\n    </h4>\r\n    <button routerLink=\"\" routerLinkActive=\"router-link-active\" class=\"btn edit-btn\" >\r\n      OK\r\n    </button>\r\n</div>"

/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-inserimento/fase-inserimento.component.ts":
/*!**********************************************************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-inserimento/fase-inserimento.component.ts ***!
  \**********************************************************************************************************************************************/
/*! exports provided: FaseInserimentoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaseInserimentoComponent", function() { return FaseInserimentoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_lista_conoRE_lista_cono_re_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/lista-conoRE/lista-cono-re.service */ "./src/app/services/lista-conoRE/lista-cono-re.service.ts");
/* harmony import */ var src_app_services_esternalizzazione_tipologiche_select_tipologiche_select_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service */ "./src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service.ts");
/* harmony import */ var src_app_services_esternalizzazione_salva_esternalizzazione_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/esternalizzazione/salva-esternalizzazione.service */ "./src/app/services/esternalizzazione/salva-esternalizzazione.service.ts");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");
/* harmony import */ var src_app_variabili_globali_edit_combinato__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/variabili-globali/edit-combinato */ "./src/app/variabili-globali/edit-combinato.ts");







var FaseInserimentoComponent = /** @class */ (function () {
    function FaseInserimentoComponent(listaConoREService, tipologicheService, salvaEsternalizzazioneService, varG, editService) {
        this.listaConoREService = listaConoREService;
        this.tipologicheService = tipologicheService;
        this.salvaEsternalizzazioneService = salvaEsternalizzazioneService;
        this.varG = varG;
        this.editService = editService;
        this.saved = false;
        this.errMotivazione = false;
        this.errTipo = false;
        this.errTitolo = false;
        this.gif = false;
        this.editCombinato = true;
        this.getListaConoRE();
        this.getListaTipologiaEsternalizzazione();
    }
    FaseInserimentoComponent.prototype.ngOnChanges = function (changes) {
        if (this.varG.getPosizione().valueOf() === 'nuova') {
            this.username = this.varG.getUtente().username;
            this.editCombinato = false;
            if (this.edit === true) {
                this.editCombinato = true;
                console.log('save');
                this.salvaEsternalizzazioneNew();
            }
        }
        else {
            if (this.edit === false) {
                this.editService.setEditCombinato(this.esternalizzazione.stato.valueOf());
                this.editCombinato = this.editService.returnEditInserimento();
            }
            else {
                this.editCombinato = true;
            }
        }
    };
    FaseInserimentoComponent.prototype.ngOnInit = function () {
    };
    FaseInserimentoComponent.prototype.salvaEsternalizzazioneNew = function () {
        var _this = this;
        this.esternalizzazione.codiceDirezione = this.varG.getUtente().zzenteacq;
        this.esternalizzazione.compilataDa = this.username;
        if (!this.validaCampi()) {
            return;
        }
        this.saved = true;
        this.gif = true;
        this.salvaEsternalizzazioneService.salvaNew(this.esternalizzazione).subscribe(function (res) {
            _this.gif = false;
            console.log('saved');
        });
    };
    FaseInserimentoComponent.prototype.getListaConoRE = function () {
        var _this = this;
        this.listaConoREService.getListaConoRE().subscribe(function (res) {
            _this.listaConoRE = res;
        });
    };
    FaseInserimentoComponent.prototype.getListaTipologiaEsternalizzazione = function () {
        var _this = this;
        this.tipologicheService.getListaTipologiaEsternalizzazione().subscribe(function (res) {
            _this.listaTipologiaEsternalizzazioni = res;
        });
    };
    FaseInserimentoComponent.prototype.validaCampi = function () {
        this.errMotivazione = false;
        this.errTipo = false;
        this.errTitolo = false;
        if (this.esternalizzazione.motivazioneEsternalizzazione === null ||
            this.esternalizzazione.motivazioneEsternalizzazione === '') {
            this.errMotivazione = true;
        }
        if (this.esternalizzazione.titoloEsternalizzazione === null ||
            this.esternalizzazione.titoloEsternalizzazione === '') {
            this.errTitolo = true;
        }
        if (this.esternalizzazione.tipoEsternalizzazione === null ||
            this.esternalizzazione.tipoEsternalizzazione === '') {
            this.errTipo = true;
        }
        if (this.errMotivazione && this.errTipo && this.errTitolo) {
            return false;
        }
        else {
            return true;
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FaseInserimentoComponent.prototype, "esternalizzazione", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], FaseInserimentoComponent.prototype, "edit", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], FaseInserimentoComponent.prototype, "fileNomina", void 0);
    FaseInserimentoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fase-inserimento',
            template: __webpack_require__(/*! ./fase-inserimento.component.html */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-inserimento/fase-inserimento.component.html"),
            styles: [__webpack_require__(/*! ./fase-inserimento.component.css */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-inserimento/fase-inserimento.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_lista_conoRE_lista_cono_re_service__WEBPACK_IMPORTED_MODULE_2__["ListaConoREService"], src_app_services_esternalizzazione_tipologiche_select_tipologiche_select_service__WEBPACK_IMPORTED_MODULE_3__["TipologicheSelectService"],
            src_app_services_esternalizzazione_salva_esternalizzazione_service__WEBPACK_IMPORTED_MODULE_4__["SalvaEsternalizzazioneService"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_5__["VariabiliGlobali"],
            src_app_variabili_globali_edit_combinato__WEBPACK_IMPORTED_MODULE_6__["EditCombinato"]])
    ], FaseInserimentoComponent);
    return FaseInserimentoComponent;
}());



/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-predisposizione/fase-predisposizione.component.css":
/*!*******************************************************************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-predisposizione/fase-predisposizione.component.css ***!
  \*******************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGV0dGFnbGlvLWVzdGVybmFsaXp6YXppb25lL2RldHRhZ2xpby1lc3Rlcm5hbGl6emF6aW9uZS1jb21wb25lbnRzL2Zhc2UtcHJlZGlzcG9zaXppb25lL2Zhc2UtcHJlZGlzcG9zaXppb25lLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-predisposizione/fase-predisposizione.component.html":
/*!********************************************************************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-predisposizione/fase-predisposizione.component.html ***!
  \********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"flex\">\n  <div class=\"w-80 font\">\n\n    <div class=\"row\" style=\"align-items: center\">\n      <div class=\"col-sm-2\">\n        Valutazione Rischio BC:\n      </div>\n      <div class=\"col-sm-10\">\n        <input type=\"text\" \n        [(ngModel)]=\"esternalizzazione.valutazioneRischiBC\"\n        [disabled]=\"editCombinato || !(utente.zzenteacq=='052')\">\n      </div>\n    </div>\n\n    <div class=\"row m-t10\" style=\"align-items: center\">\n      <div class=\"col-sm-2\">\n        Numero Comunicazione IVASS:\n      </div>\n      <div class=\"col-sm-10\">\n        <input type=\"text\" \n        [(ngModel)]=\"esternalizzazione.numeroComunicazioneIVASS\"\n        [disabled]=\"editCombinato || !(utente.zzenteacq=='059')\">\n      </div>\n    </div>\n    \n    <div class=\"row m-t10\" style=\"align-items: center\">\n      <div class=\"col-sm-2\">\n        Numero Protocollo Contrattuale:\n      </div>\n      <div class=\"col-sm-10\">\n        <input type=\"text\" \n        [(ngModel)]=\"esternalizzazione.numeroProtocolloContrattuale\"\n        [disabled]=\"editCombinato ||  !(utente.zzenteacq=='052')\">\n      </div>\n    </div>\n  \n    <div class=\"row m-t10\" style=\"align-items: center\">\n      <div class=\"col-sm-2\">\n        Periodo Preavviso Contrattuale:\n      </div>\n      <div class=\"col-sm-10\">\n        <input type=\"text\" \n        [(ngModel)]=\"esternalizzazione.periodoPreavvisoContrattuale\"\n        [disabled]=\"editCombinato ||  !(utente.zzenteacq=='052')\">\n      </div>\n    </div>\n  \n    <div class=\"row\" style=\"align-items: center\">\n      <div class=\"col-sm-2\">\n        Tipologia Rinnovo:\n      </div>\n      <div class=\"col-sm-10\">\n        <input type=\"text\" \n        [(ngModel)]=\"esternalizzazione.tipologiaRinnovo\"\n        [disabled]=\"editCombinato ||  !(utente.zzenteacq=='052')\">\n      </div>\n    </div>\n  \n    <div class=\"row\" style=\"align-items: center\">\n      <div class=\"col-sm-2\">\n        Periodicità:\n      </div>\n      <div class=\"col-sm-10\">\n        <mat-form-field class=\"select w-70\">\n\n          <mat-select\n          [(ngModel)]=\"esternalizzazione.periodicita\"\n          placeholder=\"{{esternalizzazione.periodicita}}\"\n          [disabled]=\"editCombinato || !(utente.zzenteacq=='052')\"\n          >\n\n          <mat-option>--</mat-option>\n          <mat-option *ngFor=\"let periodicita of listaPeriodicita\" [value]=\"periodicita.codice\">\n            {{periodicita.descrizione}}\n          </mat-option>\n        </mat-select>\n        </mat-form-field>\n      </div>\n    </div>\n  \n    <div class=\"row\" style=\"align-items: center\">\n      <div class=\"col-sm-2\">\n        Numero RDA:\n      </div>\n      <div class=\"col-sm-10\">\n        <input type=\"text\" \n        [(ngModel)]=\"esternalizzazione.numeroRDA\"\n        [disabled]=\"editCombinato ||  !(utente.zzenteacq=='052')\">\n      </div>\n    </div>\n\n    <div class=\"row m-t10\" style=\"align-items: center\">\n      <div class=\"col-sm-2\">\n        Numero ODA:\n      </div>\n      <div class=\"col-sm-10\">\n        <input type=\"text\" \n        [(ngModel)]=\"esternalizzazione.numeroODA\"\n        [disabled]=\"editCombinato  ||  !(utente.zzenteacq=='052')\">\n      </div>\n    </div>\n\n  </div>\n  \n  <div class=\"allegati padding w-20 m-t10\" *ngIf=\"(utente.zzenteacq ==='052' || utente.zzenteacq ==='059')\">\n\n      <h4>Allegato Contratto</h4>\n    \n            <div *ngIf=\"fileContratto.length !==0\">\n                <div class=\"animation\" *ngFor=\"let file of fileContratto\">\n                    {{file.nomeFileOrigine}}\n                </div>\n              </div>\n          \n              <div *ngIf=\"fileContratto.length === 0\">\n                  Nessun File Presente\n              </div>\n    </div>\n\n</div>\n\n<div class=\"m-t10\">\n    <app-sezione-controlli *ngIf=\"esternalizzazione.slaContrattualmenteDefiniti==='Si'\" \n      [esternalizzazione]=\"esternalizzazione\" [fileControlli]=\"fileControllo\"  [edit]=\"editCombinato\"></app-sezione-controlli>\n</div>"

/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-predisposizione/fase-predisposizione.component.ts":
/*!******************************************************************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-predisposizione/fase-predisposizione.component.ts ***!
  \******************************************************************************************************************************************************/
/*! exports provided: FasePredisposizioneDettComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FasePredisposizioneDettComponent", function() { return FasePredisposizioneDettComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_esternalizzazione_tipologiche_select_tipologiche_select_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service */ "./src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service.ts");
/* harmony import */ var src_app_variabili_globali_edit_combinato__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/edit-combinato */ "./src/app/variabili-globali/edit-combinato.ts");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");





var FasePredisposizioneDettComponent = /** @class */ (function () {
    function FasePredisposizioneDettComponent(tipologicheService, editService, varG) {
        this.tipologicheService = tipologicheService;
        this.editService = editService;
        this.varG = varG;
        this.utente = this.varG.getUtente();
        this.editCombinato = true;
        this.getListaPeriodicita();
    }
    // tslint:disable-next-line:use-life-cycle-interface
    FasePredisposizioneDettComponent.prototype.ngOnChanges = function (changes) {
        if (this.edit === false) {
            this.editService.setEditCombinato(this.esternalizzazione.stato.valueOf());
            if (this.editService.returnEditPredisposizione() === true) {
                this.editCombinato = true;
            }
            else {
                this.editCombinato = (this.editService.returnEditPredisposizione()) &&
                    !(this.utente.zzenteacq === '052' || this.utente.zzenteacq === '059');
            }
        }
        else {
            this.editCombinato = true;
        }
    };
    FasePredisposizioneDettComponent.prototype.ngOnInit = function () {
    };
    FasePredisposizioneDettComponent.prototype.getListaPeriodicita = function () {
        var _this = this;
        this.tipologicheService.getListaPeriodicita().subscribe(function (res) {
            _this.listaPeriodicita = res;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FasePredisposizioneDettComponent.prototype, "esternalizzazione", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], FasePredisposizioneDettComponent.prototype, "edit", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], FasePredisposizioneDettComponent.prototype, "fileControllo", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], FasePredisposizioneDettComponent.prototype, "fileContratto", void 0);
    FasePredisposizioneDettComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fase-predisposizione-dett',
            template: __webpack_require__(/*! ./fase-predisposizione.component.html */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-predisposizione/fase-predisposizione.component.html"),
            styles: [__webpack_require__(/*! ./fase-predisposizione.component.css */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-predisposizione/fase-predisposizione.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_esternalizzazione_tipologiche_select_tipologiche_select_service__WEBPACK_IMPORTED_MODULE_2__["TipologicheSelectService"], src_app_variabili_globali_edit_combinato__WEBPACK_IMPORTED_MODULE_3__["EditCombinato"],
            src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__["VariabiliGlobali"]])
    ], FasePredisposizioneDettComponent);
    return FasePredisposizioneDettComponent;
}());



/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-valutazione/fase-valutazione.component.css":
/*!***********************************************************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-valutazione/fase-valutazione.component.css ***!
  \***********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-form {\r\n    min-width: 150px;\r\n    max-width: 500px;\r\n    width: 100%;\r\n  }\r\n  \r\n  .example-full-width {\r\n    width: 100%;\r\n  }\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9kZXR0YWdsaW8tZXN0ZXJuYWxpenphemlvbmUvZGV0dGFnbGlvLWVzdGVybmFsaXp6YXppb25lLWNvbXBvbmVudHMvZmFzZS12YWx1dGF6aW9uZS9mYXNlLXZhbHV0YXppb25lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLFdBQVc7RUFDYjs7RUFFQTtJQUNFLFdBQVc7RUFDYiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGV0dGFnbGlvLWVzdGVybmFsaXp6YXppb25lL2RldHRhZ2xpby1lc3Rlcm5hbGl6emF6aW9uZS1jb21wb25lbnRzL2Zhc2UtdmFsdXRhemlvbmUvZmFzZS12YWx1dGF6aW9uZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmV4YW1wbGUtZm9ybSB7XHJcbiAgICBtaW4td2lkdGg6IDE1MHB4O1xyXG4gICAgbWF4LXdpZHRoOiA1MDBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICBcclxuICAuZXhhbXBsZS1mdWxsLXdpZHRoIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICAiXX0= */"

/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-valutazione/fase-valutazione.component.html":
/*!************************************************************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-valutazione/fase-valutazione.component.html ***!
  \************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!saved\">\r\n    <div class=\"flex\">\r\n        <div class=\"font w-80\">\r\n          <div class=\"row\" style=\"align-items:center\" *ngIf=\"!editCombinato\">\r\n            <div class=\"col-sm-2\">\r\n              Data Aprrovazione CDA* :\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n              <mat-form-field>\r\n                <input\r\n                  matInput\r\n                  [matDatepicker]=\"pickerDataApprovazioneCDA\"\r\n                  placeholder=\"{{ esternalizzazione.approvazioneCDA }}\"\r\n                  [(ngModel)]=\"esternalizzazione.approvazioneCDA\"\r\n                />\r\n                <mat-datepicker-toggle\r\n                  matSuffix\r\n                  [for]=\"pickerDataApprovazioneCDA\"\r\n                ></mat-datepicker-toggle>\r\n                <mat-datepicker #pickerDataApprovazioneCDA></mat-datepicker>\r\n              </mat-form-field>\r\n               \r\n              <h5  *ngIf=\"errApprovazioneCDA\" style=\"color:red;\">\r\n                  Campo Obbligatorio\r\n              </h5>\r\n            </div>\r\n          </div>\r\n      \r\n          <div class=\"row\" style=\"align-items:center\">\r\n            <div class=\"col-sm-2\">\r\n              Data Probabile Inizio Esternalizzazione* :\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n              <mat-form-field>\r\n                <input\r\n                  matInput\r\n                  [matDatepicker]=\"pickerDataProbabileInizioEsternalizzazione\"\r\n                  placeholder=\"{{\r\n                    esternalizzazione.dataProbabileInizioEsternalizzazione\r\n                  }}\"\r\n                  [(ngModel)]=\"esternalizzazione.dataProbabileInizioEsternalizzazione\"\r\n                  [disabled]=\"editCombinato\"\r\n                />\r\n                <mat-datepicker-toggle\r\n                  matSuffix\r\n                  [for]=\"pickerDataProbabileInizioEsternalizzazione\"\r\n                ></mat-datepicker-toggle>\r\n                <mat-datepicker\r\n                  #pickerDataProbabileInizioEsternalizzazione\r\n                ></mat-datepicker>\r\n              </mat-form-field>\r\n               \r\n              <h5  *ngIf=\"errDataProbalibileInizio\" style=\"color:red;\">\r\n                  Campo Obbligatorio\r\n              </h5>\r\n            </div>\r\n          </div>\r\n      \r\n          <div class=\"row\" style=\"align-items:center\">\r\n            <div class=\"col-sm-2\">\r\n              Beneficio Conseguibile:\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n              <mat-form-field class=\"select w-70\">\r\n                <mat-select\r\n                  [(ngModel)]=\"esternalizzazione.beneficioConseguibile\"\r\n                  placeholder=\"{{\r\n                    esternalizzazione.descrizioneBeneficioConseguibile\r\n                  }}\"\r\n                  [disabled]=\"editCombinato\"\r\n                >\r\n                  <mat-option>--</mat-option>\r\n                  <mat-option\r\n                    *ngFor=\"let benefici of listaBefici\"\r\n                    [value]=\"benefici.codice\"\r\n                  >\r\n                    {{ benefici.descrizione }}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n      \r\n          <div class=\"row\" style=\"align-items:center\">\r\n            <div class=\"col-sm-2\">\r\n              Nome Fornitore* :\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n                <mat-form-field class=\"w-70\">\r\n                  <input\r\n                    type=\"text\"\r\n                    aria-label=\"Assignee\"\r\n                    matInput\r\n                    [formControl]=\"myControl\"\r\n                    [matAutocomplete]=\"auto\"\r\n                    [(ngModel)]=\"esternalizzazione.nomeFornitore\"\r\n                    [disabled]=\"editCombinato\"\r\n                  />\r\n                  <mat-autocomplete #auto=\"matAutocomplete\" [displayWith]=\"displayFn\">\r\n                    <mat-option\r\n                      *ngFor=\"let option of (filteredOptions | async)\"\r\n                      [value]=\"option.ragSoc1\"\r\n                      [disabled]=\"editCombinato\"\r\n                    >\r\n                      {{ option.ragSoc1 }}\r\n                    </mat-option>\r\n                  </mat-autocomplete>\r\n                </mat-form-field>\r\n                 \r\n              <h5  *ngIf=\"errNomeFornitore\" style=\"color:red;\">\r\n                  Campo Obbligatorio\r\n              </h5>\r\n            </div>\r\n          </div>\r\n      \r\n          <br />\r\n      \r\n          <div class=\"row\" style=\"align-items:center\">\r\n            <div class=\"col-sm-2\">\r\n              Affidabilità e Competenze Fornitore:\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n              <input\r\n                type=\"text\"\r\n                [(ngModel)]=\"esternalizzazione.affidabilitaCompetenzaFornitore\"\r\n                [disabled]=\"editCombinato\"\r\n              />\r\n            </div>\r\n          </div>\r\n      \r\n          <br />\r\n      \r\n          <div class=\"row\" style=\"align-items:center\">\r\n            <div class=\"col-sm-2\">\r\n              Spazio Economico Europeo* :\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n              <mat-form-field class=\"select w-70\">\r\n                <mat-select\r\n                  [(ngModel)]=\"esternalizzazione.spazioEconomicoEuropeo\"\r\n                  placeholder=\"{{ esternalizzazione.spazioEconomicoEuropeo }}\"\r\n                  [disabled]=\"editCombinato\"\r\n                >\r\n                  <mat-option>--</mat-option>\r\n                  <mat-option value=\"Si\">\r\n                    Si\r\n                  </mat-option>\r\n                  <mat-option value=\"No\">\r\n                    No\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n               \r\n              <h5  *ngIf=\"errSpazioEconomico\" style=\"color:red;\">\r\n                  Campo Obbligatorio\r\n              </h5>\r\n            </div>\r\n          </div>\r\n      \r\n          <div class=\"row\" style=\"align-items:center\">\r\n            <div class=\"col-sm-2\">\r\n              Rischiosità Esternalizzazione* :\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n              <mat-form-field class=\"select w-70\">\r\n                <mat-select\r\n                  [(ngModel)]=\"esternalizzazione.rischiositaEsternalizzazione\"\r\n                  placeholder=\"{{\r\n                    esternalizzazione.descrizioneRischiositaEsternalizzazione\r\n                  }}\"\r\n                  [disabled]=\"editCombinato\"\r\n                >\r\n                  <mat-option>--</mat-option>\r\n                  <mat-option\r\n                    *ngFor=\"let rischiosita of listaRischiositaEsternalizzazione\"\r\n                    [value]=\"rischiosita.codice\"\r\n                  >\r\n                    {{ rischiosita.descrizione }}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n               \r\n              <h5  *ngIf=\"errRischiosita\" style=\"color:red;\">\r\n                  Campo Obbligatorio\r\n              </h5>\r\n            </div>\r\n          </div>\r\n      \r\n          <div class=\"row\" style=\"align-items: center\">\r\n            <div class=\"col-sm-2\">\r\n              Valutazione Rischio Esternalizzazione :\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n              <input\r\n                type=\"text\"\r\n                [(ngModel)]=\"esternalizzazione.valutazioneRischioEsternalizzazione\"\r\n                [disabled]=\"editCombinato\"\r\n              />\r\n            </div>\r\n          </div>\r\n      \r\n          <div class=\"row\" style=\"align-items: center\">\r\n            <div class=\"col-sm-2\">\r\n              Verifica Conflitti Interessi* :\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n              <mat-form-field class=\"select w-70\">\r\n                <mat-select\r\n                  [(ngModel)]=\"esternalizzazione.verificaConflittiInteressi\"\r\n                  placeholder=\"{{\r\n                    esternalizzazione.descrizioneVerificaConflittiInteressi\r\n                  }}\"\r\n                  [disabled]=\"editCombinato\"\r\n                >\r\n                  <mat-option>--</mat-option>\r\n                  <mat-option\r\n                    *ngFor=\"let conflitti of listaConflittiInteressi\"\r\n                    [value]=\"conflitti.codice\"\r\n                  >\r\n                    {{ conflitti.descrizione }}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n               \r\n              <h5  *ngIf=\"errVerificaConflitti\" style=\"color:red;\">\r\n                  Campo Obbligatorio\r\n              </h5>\r\n            </div>\r\n          </div>\r\n      \r\n          <div class=\"row\" style=\"align-items: center\">\r\n            <div class=\"col-sm-2\">\r\n              Rischi BC* :\r\n            </div>\r\n      \r\n            <div class=\"col-sm-10\">\r\n              <mat-form-field class=\"select w-70\">\r\n                <mat-select\r\n                  [(ngModel)]=\"esternalizzazione.rischiBC\"\r\n                  placeholder=\"{{ esternalizzazione.descrizioneRischiBC }}\"\r\n                  [disabled]=\"editCombinato\"\r\n                >\r\n                  <mat-option>--</mat-option>\r\n                  <mat-option *ngFor=\"let bc of listaRischiBC\" [value]=\"bc.codice\">\r\n                    {{ bc.descrizione }}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n               \r\n              <h5  *ngIf=\"errRischiBC\" style=\"color:red;\">\r\n                  Campo Obbligatorio\r\n              </h5>\r\n            </div>\r\n          </div>\r\n      \r\n          <div class=\"row\" style=\"align-items: center\">\r\n            <div class=\"col-sm-2\">\r\n              Piano Reinternalizzazione* :\r\n            </div>\r\n      \r\n            <div class=\"col-sm-10\">\r\n              <mat-form-field class=\"select w-70\">\r\n                <mat-select\r\n                  [(ngModel)]=\"esternalizzazione.pianoReinternalizzazione\"\r\n                  placeholder=\"{{\r\n                    esternalizzazione.descrizionePianoReinternalizzazione\r\n                  }}\"\r\n                  [disabled]=\"editCombinato\"\r\n                >\r\n                  <mat-option>--</mat-option>\r\n                  <mat-option\r\n                    *ngFor=\"\r\n                      let pianoReinternalizzazione of listaPianoReinternalizzazione\r\n                    \"\r\n                    [value]=\"pianoReinternalizzazione.codice\"\r\n                  >\r\n                    {{ pianoReinternalizzazione.descrizione }}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n\r\n              <h5  *ngIf=\"errPiano\" style=\"color:red;\">\r\n                  Campo Obbligatorio\r\n              </h5>\r\n            </div>\r\n          </div>\r\n      \r\n          <div class=\"row\" style=\"align-items: center\">\r\n            <div class=\"col-sm-2\">\r\n              Rispetto Remunerazione:\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n              <mat-form-field class=\"select w-70\">\r\n                <mat-select\r\n                  [(ngModel)]=\"esternalizzazione.rispettoRemunerazione\"\r\n                  placeholder=\"{{ esternalizzazione.rispettoRemunerazione }}\"\r\n                  [disabled]=\"editCombinato\"\r\n                >\r\n                  <mat-option>--</mat-option>\r\n                  <mat-option value=\"Si\">\r\n                    Si\r\n                  </mat-option>\r\n                  <mat-option value=\"No\">\r\n                    No\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n               \r\n              <h5  *ngIf=\"errRemunerazione\" style=\"color:red;\">\r\n                  Campo Obbligatorio\r\n              </h5>\r\n            </div>\r\n          </div>\r\n      \r\n          <div class=\"row\" style=\"align-items: center\">\r\n            <div class=\"col-sm-2\">\r\n              SLA Contrattualmente Definiti *:\r\n            </div>\r\n            <div class=\"col-sm-10\">\r\n              <mat-form-field class=\"select w-70\">\r\n                <mat-select\r\n                  [(ngModel)]=\"esternalizzazione.slaContrattualmenteDefiniti\"\r\n                  placeholder=\"{{ esternalizzazione.slaContrattualmenteDefiniti }}\"\r\n                  [disabled]=\"editCombinato\"\r\n                >\r\n                  <mat-option>--</mat-option>\r\n                  <mat-option value=\"Si\">\r\n                    Si\r\n                  </mat-option>\r\n                  <mat-option value=\"No\">\r\n                    No\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n               \r\n              <h5  *ngIf=\"errSLA\" style=\"color:red;\">\r\n                  Campo Obbligatorio\r\n              </h5>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      \r\n        <div class=\"allegati padding m-t10 w-20\">\r\n          <h4>\r\n            Allegati Piano Reinternalizzazione\r\n          </h4>\r\n          <div *ngIf=\"filePiano.length !== 0\">\r\n            <div class=\"animation\" *ngFor=\"let file of filePiano\">\r\n              {{ file.nomeFileOrigine }}\r\n            </div>\r\n          </div>\r\n      \r\n          <div *ngIf=\"filePiano.length === 0\">\r\n            Nessun File Presente\r\n          </div>\r\n        </div>\r\n      </div>\r\n      \r\n      <div class=\"m-t10\">\r\n        <app-sezione-controlli\r\n          *ngIf=\"esternalizzazione.slaContrattualmenteDefiniti === 'Si'\"\r\n          [esternalizzazione]=\"esternalizzazione\"\r\n          [fileControlli]=\"fileControlli\"\r\n          [edit]=\"editCombinato\"\r\n        ></app-sezione-controlli>\r\n      </div>\r\n</div>\r\n\r\n\r\n<div *ngIf=\"gif\" class=\"flex gif\">\r\n    <img src=\"https://it.dplay.com/00406/static/resources/images/spinner.gif\" />\r\n  </div>\r\n\r\n<div *ngIf=\"!gif\">\r\n    <h4>\r\n      ESTERNALIZZAZIONE SALVATA CORRETTAMENTE\r\n    </h4>\r\n    <button routerLink=\"\" routerLinkActive=\"router-link-active\" class=\"btn edit-btn\" >\r\n      OK\r\n    </button>\r\n</div>"

/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-valutazione/fase-valutazione.component.ts":
/*!**********************************************************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-valutazione/fase-valutazione.component.ts ***!
  \**********************************************************************************************************************************************/
/*! exports provided: FaseValutazioneDettComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaseValutazioneDettComponent", function() { return FaseValutazioneDettComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_esternalizzazione_tipologiche_select_tipologiche_select_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service */ "./src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service.ts");
/* harmony import */ var src_app_variabili_globali_edit_combinato__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/edit-combinato */ "./src/app/variabili-globali/edit-combinato.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");
/* harmony import */ var src_app_services_esternalizzazione_salva_esternalizzazione_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/esternalizzazione/salva-esternalizzazione.service */ "./src/app/services/esternalizzazione/salva-esternalizzazione.service.ts");








var FaseValutazioneDettComponent = /** @class */ (function () {
    function FaseValutazioneDettComponent(tipologicheService, editService, varG, salvaEsternalizzazioneService) {
        this.tipologicheService = tipologicheService;
        this.editService = editService;
        this.varG = varG;
        this.salvaEsternalizzazioneService = salvaEsternalizzazioneService;
        this.myControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.listaFornitori = this.varG.getFornitori();
        this.inizializzaErrori();
        this.getListaBenefici();
        this.getListaRischiositaEsternalizzazione();
        this.getListaVerificaConflittiInteressi();
        this.getListaRischiBC();
        this.getListaPianoReinternalizzazione();
    }
    // tslint:disable-next-line:use-life-cycle-interface
    FaseValutazioneDettComponent.prototype.ngOnChanges = function () {
        if (this.edit === true) {
            this.editCombinato = true;
            this.salvaEsternalizzazione();
        }
        else {
            this.editCombinato = false;
        }
    };
    FaseValutazioneDettComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.filteredOptions = this.myControl.valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (value) { return typeof value === 'string' ? value : value.ragSoc1; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (name) { return name ? _this._filter(name) : _this.listaFornitori.slice(); }));
    };
    FaseValutazioneDettComponent.prototype.displayFn = function (fornitore) {
        return fornitore ? fornitore : undefined;
    };
    FaseValutazioneDettComponent.prototype._filter = function (name) {
        var filterValue = name.toLowerCase();
        return this.listaFornitori.filter(function (option) { return option.ragSoc1.toLowerCase().indexOf(filterValue) === 0; });
    };
    FaseValutazioneDettComponent.prototype.getListaBenefici = function () {
        var _this = this;
        this.tipologicheService.getListaBenefici().subscribe(function (res) {
            _this.listaBefici = res;
        });
    };
    FaseValutazioneDettComponent.prototype.getListaRischiositaEsternalizzazione = function () {
        var _this = this;
        this.tipologicheService.getListaRischiositaEsternalizzazione().subscribe(function (res) {
            _this.listaRischiositaEsternalizzazione = res;
        });
    };
    FaseValutazioneDettComponent.prototype.getListaVerificaConflittiInteressi = function () {
        var _this = this;
        this.tipologicheService.getListaConflittiInteressi().subscribe(function (res) {
            _this.listaConflittiInteressi = res;
        });
    };
    FaseValutazioneDettComponent.prototype.getListaRischiBC = function () {
        var _this = this;
        this.tipologicheService.getListaRischiBC().subscribe(function (res) {
            _this.listaRischiBC = res;
        });
    };
    FaseValutazioneDettComponent.prototype.getListaPianoReinternalizzazione = function () {
        var _this = this;
        this.tipologicheService.getListaPianiReinternalizzazione().subscribe(function (res) {
            _this.listaPianoReinternalizzazione = res;
        });
    };
    FaseValutazioneDettComponent.prototype.salvaEsternalizzazione = function () {
        if (this.esternalizzazione.stato !== '2') {
            return;
        }
        this.inizializzaErrori();
        if (!this.validaCampi()) {
            return;
        }
        // this.saved = true;
        // this.gif = true;
        console.log('salvo');
        // this.salvaEsternalizzazioneService.salvaNew(this.esternalizzazione).subscribe( (res) => {
        //   this.gif = false;
        //   console.log('saved');
        // });
    };
    FaseValutazioneDettComponent.prototype.validaCampi = function () {
        if (this.esternalizzazione.dataProbabileInizioEsternalizzazione === null ||
            this.esternalizzazione.dataProbabileInizioEsternalizzazione === '') {
            this.errDataProbalibileInizio = true;
        }
        if (this.esternalizzazione.approvazioneCDA === null ||
            this.esternalizzazione.approvazioneCDA === '') {
            this.errApprovazioneCDA = true;
        }
        if (this.esternalizzazione.nomeFornitore === null ||
            this.esternalizzazione.nomeFornitore === '') {
            this.errNomeFornitore = true;
        }
        if (this.esternalizzazione.rischiositaEsternalizzazione === null ||
            this.esternalizzazione.rischiositaEsternalizzazione === '') {
            this.errRischiosita = true;
        }
        if (this.esternalizzazione.verificaConflittiInteressi === null ||
            this.esternalizzazione.verificaConflittiInteressi === '') {
            this.errVerificaConflitti = true;
        }
        if (this.esternalizzazione.rischiBC === null ||
            this.esternalizzazione.rischiBC === '') {
            this.errRischiBC = true;
        }
        if (this.esternalizzazione.spazioEconomicoEuropeo === null ||
            this.esternalizzazione.spazioEconomicoEuropeo === '') {
            this.errSpazioEconomico = true;
        }
        if (this.esternalizzazione.slaContrattualmenteDefiniti === null ||
            this.esternalizzazione.slaContrattualmenteDefiniti === '') {
            this.errSLA = true;
        }
        if (this.esternalizzazione.pianoReinternalizzazione === null ||
            this.esternalizzazione.pianoReinternalizzazione === '') {
            this.errPiano = true;
        }
        if (this.esternalizzazione.rispettoRemunerazione === null ||
            this.esternalizzazione.rispettoRemunerazione === '') {
            this.errRemunerazione = true;
        }
        if (this.errApprovazioneCDA && this.errDataProbalibileInizio && this.errNomeFornitore &&
            this.errPiano && this.errRemunerazione && this.errRischiBC && this.errRischiosita &&
            this.errSLA && this.errSpazioEconomico && this.errVerificaConflitti) {
            return false;
        }
        else {
            return true;
        }
    };
    FaseValutazioneDettComponent.prototype.inizializzaErrori = function () {
        this.errDataProbalibileInizio = false;
        this.errApprovazioneCDA = false;
        this.errNomeFornitore = false;
        this.errRischiosita = false;
        this.errVerificaConflitti = false;
        this.errRischiBC = false;
        this.errSpazioEconomico = false;
        this.errSLA = false;
        this.errPiano = false;
        this.errRemunerazione = false;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], FaseValutazioneDettComponent.prototype, "esternalizzazione", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], FaseValutazioneDettComponent.prototype, "edit", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], FaseValutazioneDettComponent.prototype, "filePiano", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], FaseValutazioneDettComponent.prototype, "fileControlli", void 0);
    FaseValutazioneDettComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fase-valutazione-dett',
            template: __webpack_require__(/*! ./fase-valutazione.component.html */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-valutazione/fase-valutazione.component.html"),
            styles: [__webpack_require__(/*! ./fase-valutazione.component.css */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/fase-valutazione/fase-valutazione.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_esternalizzazione_tipologiche_select_tipologiche_select_service__WEBPACK_IMPORTED_MODULE_2__["TipologicheSelectService"], src_app_variabili_globali_edit_combinato__WEBPACK_IMPORTED_MODULE_3__["EditCombinato"],
            src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_6__["VariabiliGlobali"], src_app_services_esternalizzazione_salva_esternalizzazione_service__WEBPACK_IMPORTED_MODULE_7__["SalvaEsternalizzazioneService"]])
    ], FaseValutazioneDettComponent);
    return FaseValutazioneDettComponent;
}());

/*
                !$scope.validaResponsabileSingoloControllo() |
        !$scope.validaRispettoRemunerazione() */


/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/sezione-controlli/sezione-controlli.component.css":
/*!*************************************************************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/sezione-controlli/sezione-controlli.component.css ***!
  \*************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGV0dGFnbGlvLWVzdGVybmFsaXp6YXppb25lL2RldHRhZ2xpby1lc3Rlcm5hbGl6emF6aW9uZS1jb21wb25lbnRzL3NlemlvbmUtY29udHJvbGxpL3NlemlvbmUtY29udHJvbGxpLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/sezione-controlli/sezione-controlli.component.html":
/*!**************************************************************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/sezione-controlli/sezione-controlli.component.html ***!
  \**************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"flex\" style=\"border: solid 1px #09357a;\">\n  <div class=\"font padding w-80\" >\n\n    <span style=\"align-content: center\">\n      <h4>\n        Sezione Controlli\n      </h4>\n    </span>\n    \n    <div class=\"d-flex\">\n        <div class=\"w-70\">\n            <div class=\"row\" style=\"align-items:center\">\n                <div class=\"col-sm-3\">\n                  Data Inizio Controllo:\n                </div>\n                <div class=\"col-sm-9\">\n                  <mat-form-field>\n                    <input matInput \n                      [matDatepicker]=\"pickerDataInizioControllo\" \n                      placeholder=\"{{esternalizzazione.dataInizioControllo}}\"\n                      [(ngModel)]=\"esternalizzazione.dataInizioControllo\"\n                      [disabled]=\"edit\">\n                    <mat-datepicker-toggle matSuffix [for]=\"pickerDataInizioControllo\"></mat-datepicker-toggle>\n                    <mat-datepicker #pickerDataInizioControllo></mat-datepicker>\n                  </mat-form-field>\n                </div>\n              </div>\n        </div>\n  \n        <div class=\"w-70\">\n            <div class=\"row\" style=\"align-items:center\">\n                <div class=\"col-sm-3\">\n                  Data Fine Controllo:\n                </div>\n                <div class=\"col-sm-9\">\n                  <mat-form-field>\n                    <input matInput \n                      [matDatepicker]=\"pickerDataFineControllo\" \n                      placeholder=\"{{esternalizzazione.dataFineControllo}}\"\n                      [(ngModel)]=\"esternalizzazione.dataFineControllo\"\n                      [disabled]=\"edit\"\n                      >\n                    <mat-datepicker-toggle matSuffix [for]=\"pickerDataFineControllo\"></mat-datepicker-toggle>\n                    <mat-datepicker #pickerDataFineControllo></mat-datepicker>\n                  </mat-form-field>\n                </div>\n              </div>\n        </div>\n    </div>\n  \n    <div class=\"row\" style=\"align-items:center\">\n      <div class=\"col-sm-2\">\n        Tipo Controllo:\n      </div>\n      <div class=\"col-sm-10\">\n          <mat-form-field class=\"select w-70\">\n  \n              <mat-select\n              placeholder=\"{{esternalizzazione.descrizioneTipoControllo}}\"\n              [(ngModel)]=\"esternalizzazione.tipoControllo\"\n              [disabled]=\"edit\"\n              >\n  \n              <mat-option>--</mat-option>\n              <mat-option *ngFor=\"let tipoControllo of listaTipiControllo\" \n              [value]=\"tipoControllo.codice\">\n              {{tipoControllo.descrizione}}\n            </mat-option>\n            </mat-select>\n            </mat-form-field>\n      </div>\n    </div>\n  \n    <div class=\"row\" style=\"align-items: center\">\n      <div class=\"col-sm-2\">\n        Descrizione Controllo:\n      </div>\n  \n      <div class=\"col-sm-10\">\n          <input type=\"text\"\n          [(ngModel)]=\"esternalizzazione.descrizioneControllo\"\n          [disabled]=\"edit\">\n      </div> \n    </div>\n  \n    <div class=\"row\" style=\"align-items: center\">\n      <div class=\"col-sm-2\">\n        Vincolo Pagamento Fattura:\n      </div>\n      <div class=\"col-sm-10\">\n          <mat-form-field class=\"select w-70\">\n  \n              <mat-select\n              [(ngModel)]=\"esternalizzazione.vincoloPagamentoFattura\"\n              [disabled]=\"edit\"\n              >\n  \n              <mat-option>--</mat-option>\n              <mat-option  value=\"Si\">\n                Si\n              </mat-option>\n              <mat-option  value=\"No\">\n                No\n              </mat-option>\n            </mat-select>\n            </mat-form-field>\n      </div>\n    </div>\n  \n    <div class=\"row\" style=\"align-items:center\">\n        <div class=\"col-sm-2\">\n          Esiti Del Controllo:\n        </div>\n        <div class=\"col-sm-10\">\n            <mat-form-field class=\"select w-70\">\n    \n                <mat-select\n                [(ngModel)]=\"esternalizzazione.esitoControllo\"\n                placeholder=\"{{esternalizzazione.descrEsitoControllo}}\"\n                [disabled]=\"edit\"\n                >\n    \n                <mat-option>--</mat-option>\n                <mat-option *ngFor=\"let esitoControllo of listaEsitiControllo\" \n                [value]=\"esitoControllo.codice\">\n                {{esitoControllo.descrizione}}\n              </mat-option>\n              </mat-select>\n              </mat-form-field>\n        </div>\n    </div>\n\n    <div class=\"row\" style=\"align-items: center\">\n      <div class=\"col-sm-2\">\n        Descrizione Esito Controllo:\n      </div>\n      <div class=\"col-sm-10\">\n        <input type=\"text\"\n        [(ngModel)]=\"esternalizzazione.affidabilitaCompetenzaFornitore\"\n        [disabled]=\"edit\">\n      </div>\n    </div>\n\n    <div class=\"row\" style=\"align-items: center\">\n      <div class=\"col-sm-2\">\n        Nota Azione Correttiva:\n      </div>\n      <div class=\"col-sm-10\">\n        <input type=\"text\"\n        [(ngModel)]=\"esternalizzazione.notaAzioneCorrettiva\"\n        [disabled]=\"edit\">\n      </div>\n    </div>\n  </div>\n  \n  <div class=\"allegati padding w-20 m-t10 m-r10 m-b10\">\n    <h4>\n      Allegati Controllo Operativo\n    </h4>\n      <div *ngIf=\"fileControlli.length!==0\">\n        <div class=\"animation\" *ngFor=\"let file of fileControlli\">\n            {{file.nomeFileOrigine}}\n        </div>\n      </div>\n  \n      <div *ngIf=\"fileControlli.length === 0\">\n          Nessun File Presente\n      </div>\n  </div>\n  \n</div>\n\n<div>\n  <br>\n</div>\n\n"

/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/sezione-controlli/sezione-controlli.component.ts":
/*!************************************************************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/sezione-controlli/sezione-controlli.component.ts ***!
  \************************************************************************************************************************************************/
/*! exports provided: SezioneControlliComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SezioneControlliComponent", function() { return SezioneControlliComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_esternalizzazione_tipologiche_select_tipologiche_select_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service */ "./src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service.ts");



var SezioneControlliComponent = /** @class */ (function () {
    function SezioneControlliComponent(tipologicheService) {
        this.tipologicheService = tipologicheService;
        this.getListaTipiControllo();
        this.getListaEsitiControllo();
    }
    SezioneControlliComponent.prototype.ngOnInit = function () {
    };
    SezioneControlliComponent.prototype.getListaTipiControllo = function () {
        var _this = this;
        this.tipologicheService.getListaTipiControllo().subscribe(function (res) {
            _this.listaTipiControllo = res;
        });
    };
    SezioneControlliComponent.prototype.getListaEsitiControllo = function () {
        var _this = this;
        this.tipologicheService.getListaEsitiControllo().subscribe(function (res) {
            _this.listaEsitiControllo = res;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], SezioneControlliComponent.prototype, "esternalizzazione", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], SezioneControlliComponent.prototype, "edit", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], SezioneControlliComponent.prototype, "fileControlli", void 0);
    SezioneControlliComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sezione-controlli',
            template: __webpack_require__(/*! ./sezione-controlli.component.html */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/sezione-controlli/sezione-controlli.component.html"),
            styles: [__webpack_require__(/*! ./sezione-controlli.component.css */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione-components/sezione-controlli/sezione-controlli.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_esternalizzazione_tipologiche_select_tipologiche_select_service__WEBPACK_IMPORTED_MODULE_2__["TipologicheSelectService"]])
    ], SezioneControlliComponent);
    return SezioneControlliComponent;
}());



/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component.css":
/*!**************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component.css ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".previous-page-btn {\r\n    background-color: #09357a;\r\n    color: white;\r\n  }\r\n  .previous-page-btn:hover {\r\n    border: solid 0.5px;\r\n    background-color: white;\r\n    color: #09357a;\r\n    cursor: pointer;\r\n  }\r\n  .edit-btn {\r\n    background-color:#15BD17;\r\n    color: white;\r\n  }\r\n  .edit-btn:hover {\r\n    background-color: white;\r\n    color: #15BD17;\r\n    border: solid 0.5px;\r\n    cursor: pointer;\r\n  }\r\n  .resp-btn {\r\n    background-color:#E02A2A;\r\n    color: white;\r\n  }\r\n  .resp-btn:hover {\r\n    background-color: white;\r\n    color: #E02A2A;\r\n    border: solid 0.5px;\r\n    cursor: pointer;\r\n  }\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9kZXR0YWdsaW8tZXN0ZXJuYWxpenphemlvbmUvZGV0dGFnbGlvLWVzdGVybmFsaXp6YXppb25lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSx5QkFBeUI7SUFDekIsWUFBWTtFQUNkO0VBQ0E7SUFDRSxtQkFBbUI7SUFDbkIsdUJBQXVCO0lBQ3ZCLGNBQWM7SUFDZCxlQUFlO0VBQ2pCO0VBQ0E7SUFDRSx3QkFBd0I7SUFDeEIsWUFBWTtFQUNkO0VBRUE7SUFDRSx1QkFBdUI7SUFDdkIsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixlQUFlO0VBQ2pCO0VBRUE7SUFDRSx3QkFBd0I7SUFDeEIsWUFBWTtFQUNkO0VBRUE7SUFDRSx1QkFBdUI7SUFDdkIsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixlQUFlO0VBQ2pCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9kZXR0YWdsaW8tZXN0ZXJuYWxpenphemlvbmUvZGV0dGFnbGlvLWVzdGVybmFsaXp6YXppb25lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucHJldmlvdXMtcGFnZS1idG4ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzA5MzU3YTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbiAgLnByZXZpb3VzLXBhZ2UtYnRuOmhvdmVyIHtcclxuICAgIGJvcmRlcjogc29saWQgMC41cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGNvbG9yOiAjMDkzNTdhO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxuICAuZWRpdC1idG4ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojMTVCRDE3O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuICBcclxuICAuZWRpdC1idG46aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBjb2xvcjogIzE1QkQxNztcclxuICAgIGJvcmRlcjogc29saWQgMC41cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG4gIFxyXG4gIC5yZXNwLWJ0biB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNFMDJBMkE7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5yZXNwLWJ0bjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGNvbG9yOiAjRTAyQTJBO1xyXG4gICAgYm9yZGVyOiBzb2xpZCAwLjVweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcbiAgIl19 */"

/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"view\">\r\n  <h3>\r\n    Dettaglio Esternalizzazione N. {{ esternalizzazione.idEsternalizzazione }}\r\n  </h3>\r\n\r\n  <div #spacer></div>\r\n\r\n  <div stickyThing [spacer]=\"spacer\" style=\"z-index:5;background-color: white;\" >\r\n    <div style=\"margin-bottom: 10px;\" class=\"d-flex center m-t10\">\r\n      <button class=\"btn previous-page-btn\" [routerLink]=\"[routerLink]\" routerLinkActive=\"router-link-active\"  >Torna alla lista</button>\r\n      <button class=\"btn edit-btn m-l10\" (click)=\"activeEditMode()\" *ngIf=\"edit\"> Edit</button>\r\n      <button class=\"btn edit-btn m-l10\" *ngIf=\"!edit\" (click)=\"saveChanges()\">Salva Modifiche</button>\r\n      <button class=\"btn resp-btn m-l10\" *ngIf=\"esternalizzazione.stato === '2' || esternalizzazione.stato === '3'\">Assegna Responsabile</button>\r\n      <button class=\"btn edit-btn m-l10\" *ngIf=\"esternalizzazione.stato === '3'\">Approva</button>\r\n    </div>\r\n  </div>\r\n  \r\n  <mat-tab-group  [selectedIndex]=\"selectedIndex\">\r\n    <!-- fase di inserimento -->\r\n    <mat-tab label=\"Inserimento\">\r\n      <app-fase-inserimento [fileNomina]=\"fileNomina\" [esternalizzazione]=\"esternalizzazione\" [edit]=\"edit\" >\r\n      </app-fase-inserimento>\r\n    </mat-tab>\r\n\r\n    <!-- fase di valutazione -->\r\n    <mat-tab label=\"Fase Valutazione\">\r\n      <app-fase-valutazione-dett [filePiano]=\"filePiano\" [fileControlli]=\"fileControllo\" [esternalizzazione]=\"esternalizzazione\" \r\n      [edit]=\"edit\">\r\n      </app-fase-valutazione-dett>\r\n    </mat-tab>\r\n\r\n    <!-- fase di predisposizione -->\r\n    <mat-tab label=\"Fase Predisposizione\">\r\n      <app-fase-predisposizione-dett [fileControllo]=\"fileControllo\" [esternalizzazione]=\"esternalizzazione\"\r\n      [fileContratto]=\"fileContratto\" [edit]=\"edit\">\r\n      </app-fase-predisposizione-dett>\r\n    </mat-tab>\r\n  </mat-tab-group>\r\n</div>\r\n\r\n<div *ngIf=\"!view\" class=\"flex gif\">\r\n  <img src=\"https://it.dplay.com/00406/static/resources/images/spinner.gif\" />\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: DettaglioEsternalizzazioneComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DettaglioEsternalizzazioneComponent", function() { return DettaglioEsternalizzazioneComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_findById_esternalizzazione_find_by_id_e_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/findById/esternalizzazione/find-by-id-e.service */ "./src/app/services/findById/esternalizzazione/find-by-id-e.service.ts");
/* harmony import */ var src_app_services_File_Service_file_nomina_responsabile_controllo_file_esternalizzazione_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/File-Service/file-nomina-responsabile-controllo/file-esternalizzazione.service */ "./src/app/services/File-Service/file-nomina-responsabile-controllo/file-esternalizzazione.service.ts");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");
/* harmony import */ var src_app_services_http_service_http_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/http-service/http.service */ "./src/app/services/http-service/http.service.ts");



// tslint:disable-next-line:max-line-length



var DettaglioEsternalizzazioneComponent = /** @class */ (function () {
    function DettaglioEsternalizzazioneComponent(service, fileService, varG, http) {
        this.service = service;
        this.fileService = fileService;
        this.varG = varG;
        this.http = http;
        this.esternalizzazioneOutput = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.fileNominaOutput = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.filePianoOutput = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.fileControlloOutput = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.fileContrattoOutput = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.editOutput = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.fileNomina = [];
        this.filePiano = [];
        this.fileControllo = [];
        this.fileContratto = [];
        this.view = false;
        this.edit = true;
        this.saveExisting = false;
        this.posizione = this.varG.getPosizione();
        if (this.posizione.valueOf() === 'nuova') {
            this.selectedIndex = 0;
            this.routerLink = '/home';
            this.edit = false;
            this.getNewEsternalizzazione();
        }
        else {
            if (this.posizione.valueOf() === 'home') {
                this.selectedIndex = 0;
                this.routerLink = '/home';
            }
            else if (this.posizione.valueOf() === 'valutazione') {
                this.selectedIndex = 1;
                this.routerLink = '/fase-valutazione';
            }
            else if (this.posizione.valueOf() === 'predisposizione') {
                this.selectedIndex = 2;
                this.routerLink = '/fase-predisposizione';
            }
            this.getEsternalizzazione();
        }
    }
    DettaglioEsternalizzazioneComponent.prototype.ngOnInit = function () {
    };
    DettaglioEsternalizzazioneComponent.prototype.activeEditMode = function () {
        this.edit = false;
        this.editOutput.emit(false);
    };
    DettaglioEsternalizzazioneComponent.prototype.saveChanges = function () {
        this.edit = true;
        this.editOutput.emit(true);
    };
    DettaglioEsternalizzazioneComponent.prototype.getNewEsternalizzazione = function () {
        var _this = this;
        this.http.doGet('rest/esternalizzazione/new').subscribe(function (res) {
            _this.esternalizzazione = res;
            _this.view = true;
            _this.editOutput.emit(false);
            _this.esternalizzazioneOutput.emit(_this.esternalizzazione);
        });
    };
    DettaglioEsternalizzazioneComponent.prototype.getEsternalizzazione = function () {
        var _this = this;
        setTimeout(function () {
            _this.service.findEsternalizzazione().subscribe(function (res) {
                _this.esternalizzazione = res;
                _this.view = true;
                _this.esternalizzazioneOutput.emit(_this.esternalizzazione);
            });
            _this.fileService.getListaFile().subscribe(function (res) {
                _this.returnedFileList = res;
                _this.returnedFileList.forEach(function (file) {
                    switch (file.ambito) {
                        case 'NOMINA': {
                            _this.fileNomina.push(file);
                            break;
                        }
                        case 'PIANO': {
                            _this.filePiano.push(file);
                            break;
                        }
                        case 'CONTROLLO': {
                            _this.fileControllo.push(file);
                            break;
                        }
                        case 'CONTRATTO': {
                            _this.fileContratto.push(file);
                        }
                    }
                });
                _this.fileNominaOutput.emit(_this.fileNomina);
                _this.fileControlloOutput.emit(_this.fileControllo);
                _this.filePianoOutput.emit(_this.filePiano);
                _this.fileContrattoOutput.emit(_this.fileContratto);
            });
        }, 300);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DettaglioEsternalizzazioneComponent.prototype, "esternalizzazioneOutput", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DettaglioEsternalizzazioneComponent.prototype, "fileNominaOutput", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DettaglioEsternalizzazioneComponent.prototype, "filePianoOutput", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DettaglioEsternalizzazioneComponent.prototype, "fileControlloOutput", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DettaglioEsternalizzazioneComponent.prototype, "fileContrattoOutput", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DettaglioEsternalizzazioneComponent.prototype, "editOutput", void 0);
    DettaglioEsternalizzazioneComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dettaglio-esternalizzazione',
            template: __webpack_require__(/*! ./dettaglio-esternalizzazione.component.html */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component.html"),
            styles: [__webpack_require__(/*! ./dettaglio-esternalizzazione.component.css */ "./src/app/components/dettaglio-esternalizzazione/dettaglio-esternalizzazione.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_findById_esternalizzazione_find_by_id_e_service__WEBPACK_IMPORTED_MODULE_2__["FindByIdEService"], src_app_services_File_Service_file_nomina_responsabile_controllo_file_esternalizzazione_service__WEBPACK_IMPORTED_MODULE_3__["FileEsternalizzazione"],
            src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__["VariabiliGlobali"], src_app_services_http_service_http_service__WEBPACK_IMPORTED_MODULE_5__["HttpService"]])
    ], DettaglioEsternalizzazioneComponent);
    return DettaglioEsternalizzazioneComponent;
}());



/***/ }),

/***/ "./src/app/components/esternalizzazioni/esternalizzazioni.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/components/esternalizzazioni/esternalizzazioni.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\r\n  width: 100%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9lc3Rlcm5hbGl6emF6aW9uaS9lc3Rlcm5hbGl6emF6aW9uaS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztBQUNiIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9lc3Rlcm5hbGl6emF6aW9uaS9lc3Rlcm5hbGl6emF6aW9uaS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/esternalizzazioni/esternalizzazioni.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/components/esternalizzazioni/esternalizzazioni.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div *ngIf=\"resp\" style=\"margin-top:10px;\">\n\n    <table mat-table  [dataSource]=\"dataSource\">\n\n      <!-- id Column -->\n      <ng-container matColumnDef=\"ID\">\n        <th mat-header-cell *matHeaderCellDef> ID </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.idEsternalizzazione}} </td>\n      </ng-container>\n\n      <!-- data creazione Column -->\n      <ng-container matColumnDef=\"DATA CREAZIONE\">\n        <th mat-header-cell *matHeaderCellDef> DATA CREAZIONE </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.dataCreazioneEsternalizzazione}} </td>\n      </ng-container>\n\n      <!-- titolo Column -->\n      <ng-container matColumnDef=\"TITOLO\">\n        <th mat-header-cell *matHeaderCellDef> TITOLO </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.titoloEsternalizzazione}} </td>\n      </ng-container>\n     \n      <!-- responsabile Column -->\n      <ng-container matColumnDef=\"RESPONSABILE\">\n        <th mat-header-cell *matHeaderCellDef> RESPONSABILE </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.compilataDa}} </td>\n      </ng-container>\n\n      <!-- responsabile Column -->\n      <ng-container matColumnDef=\"RESPONSABILE CONTROLLO\">\n        <th mat-header-cell *matHeaderCellDef> RESPONSABILE CONTROLLO </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.responsabileControlloEsternalizzazione}} </td>\n      </ng-container>\n\n      <ng-container matColumnDef = \"nuovaRda\">\n        <th mat-header-cell *matHeaderCellDef> </th>\n        <td mat-cell *matCellDef=\"let element\">\n          <a href=\"https://www.mondo.sara.it/RichiesteDAcquisto/index.html\" target=\"_blank\"  \n          style=\"z-index:1000;\" class=\"btn btn-success\">\n            <span>Nuova Rda</span></a>\n        </td>\n      </ng-container>\n\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n      <tr mat-row routerLink=\"/esternalizzazione-details\" routerLinkActive=\"router-link-active\"  (click)=\"viewDetails(row)\" \n      *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n    </table>\n\n    <mat-paginator  [length]=\"length\"\n      [pageIndex]=\"pageIndex\"\n      [pageSize]=\"pageSize\"\n      [pageSizeOptions]=\"[5, 10, 20]\"\n      (page)=\"pageEvent = getEsternalizzazioni($event)\" showFirstLastButtons>\n    </mat-paginator>\n</div>\n\n<div *ngIf=\"!view\"  class=\"flex gif\">\n    <img src=\"https://it.dplay.com/00406/static/resources/images/spinner.gif\">\n</div>\n\n<div *ngIf=\"notResp\">\n  <h4>\n    Nessuna Esternalizzazione presente\n  </h4>\n</div>\n"

/***/ }),

/***/ "./src/app/components/esternalizzazioni/esternalizzazioni.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/components/esternalizzazioni/esternalizzazioni.component.ts ***!
  \*****************************************************************************/
/*! exports provided: EsternalizzazioniComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EsternalizzazioniComponent", function() { return EsternalizzazioniComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_esternalizzazione_lista_esternalizzazioni_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/esternalizzazione/lista-esternalizzazioni.service */ "./src/app/services/esternalizzazione/lista-esternalizzazioni.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");
/* harmony import */ var _services_user_service_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/user-service/user.service */ "./src/app/services/user-service/user.service.ts");






var EsternalizzazioniComponent = /** @class */ (function () {
    function EsternalizzazioniComponent(service, varG, userService) {
        this.service = service;
        this.varG = varG;
        this.userService = userService;
        this.displayedColumns = ['ID', 'DATA CREAZIONE', 'TITOLO', 'RESPONSABILE', 'RESPONSABILE CONTROLLO', 'nuovaRda'];
        this.dataSource = null;
        this.view = false;
        this.resp = false;
        this.notResp = false;
    }
    EsternalizzazioniComponent.prototype.ngOnInit = function () {
        this.userService.getUser();
        this.getEsternalizzazioni(null);
    };
    EsternalizzazioniComponent.prototype.getEsternalizzazioni = function (event) {
        if (event === null) {
            this.pageIndex = 0;
            this.pageSize = 5;
            this.firstTimeGet();
        }
        else {
            this.pageIndex = event.pageIndex;
            this.pageSize = event.pageSize;
            this.get();
        }
    };
    EsternalizzazioniComponent.prototype.viewDetails = function (details) {
        this.varG.setIdEsternalizzazione(details.idEsternalizzazione);
        this.varG.setPosizione('home');
    };
    EsternalizzazioniComponent.prototype.get = function () {
        var _this = this;
        this.service.getEsternalizzazioni(this.pageIndex, this.pageSize).subscribe(function (res) {
            _this.listaEsternalizzazioni = res;
            _this.view = true;
            if (_this.listaEsternalizzazioni.totaleRecord === 0) {
                _this.notResp = true;
            }
            else {
                _this.resp = true;
            }
            if (_this.dataSource === null) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.listaEsternalizzazioni.listaEsternalizzazioni);
                _this.dataSource.paginator = _this.paginator;
                _this.pageIndex = _this.listaEsternalizzazioni.paginaCorrente;
                _this.length = _this.listaEsternalizzazioni.totaleRecord;
            }
            else {
                _this.pageIndex = _this.listaEsternalizzazioni.paginaCorrente;
                _this.length = _this.listaEsternalizzazioni.totaleRecord;
                _this.dataSource.data = _this.listaEsternalizzazioni.listaEsternalizzazioni;
                _this._updatePaginator(_this.paginator.length, _this.pageIndex);
            }
        });
    };
    EsternalizzazioniComponent.prototype.firstTimeGet = function () {
        var _this = this;
        setTimeout(function () {
            _this.get();
        }, 1000);
    };
    EsternalizzazioniComponent.prototype._updatePaginator = function (filteredDataLength, pageIndex) {
        var _this = this;
        Promise.resolve().then(function () {
            _this.paginator.pageIndex = pageIndex;
            if (!_this.paginator) {
                return;
            }
            _this.paginator.length = filteredDataLength;
            if (_this.paginator.pageIndex > 0) {
                var lastPageIndex = Math.ceil(_this.paginator.length / _this.paginator.pageSize) - 1 || 0;
                _this.paginator.pageIndex = Math.min(_this.paginator.pageIndex, lastPageIndex);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], EsternalizzazioniComponent.prototype, "page", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], EsternalizzazioniComponent.prototype, "paginator", void 0);
    EsternalizzazioniComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-esternalizzazioni',
            template: __webpack_require__(/*! ./esternalizzazioni.component.html */ "./src/app/components/esternalizzazioni/esternalizzazioni.component.html"),
            styles: [__webpack_require__(/*! ./esternalizzazioni.component.css */ "./src/app/components/esternalizzazioni/esternalizzazioni.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_esternalizzazione_lista_esternalizzazioni_service__WEBPACK_IMPORTED_MODULE_2__["ListaEsternalizzazioniService"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__["VariabiliGlobali"], _services_user_service_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]])
    ], EsternalizzazioniComponent);
    return EsternalizzazioniComponent;
}());



/***/ }),

/***/ "./src/app/components/header/header.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/header/header.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " img {\r\n  font-size: 75px;\r\n  margin-right: 2%;\r\n  width: 150px;\r\n}\r\n\r\n.dropdown-content a:hover {background-color: #D8D8D8;}\r\n\r\n.dropdown:hover .dropdown-content {\r\n  display: block;\r\n  z-index: 2;\r\n}\r\n\r\nnav {\r\n  background-color: #F2F2F2;\r\n}\r\n\r\nli {\r\n  font-size: 17px;\r\n  color: #09357a;\r\n}\r\n\r\nli:hover {\r\n  background-color: #D8D8D8;\r\n}\r\n\r\n.dropdown-content a {\r\n  color: #09357a;\r\n  padding: 12px 16px;\r\n  text-decoration: none;\r\n  display: block;\r\n}\r\n\r\n.dropdown {\r\n  position: relative;\r\n  display: inline-block;\r\n}\r\n\r\na {\r\n  color: #09357a;\r\n}\r\n\r\n.dropdown-content {\r\n  display: none;\r\n  position: absolute;\r\n  background-color: #F2F2F2;\r\n}\r\n\r\n.scritte {\r\n  color: #09357a;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkNBQUM7RUFDQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLFlBQVk7QUFDZDs7QUFFQSwyQkFBMkIseUJBQXlCLENBQUM7O0FBRXJEO0VBQ0UsY0FBYztFQUNkLFVBQVU7QUFDWjs7QUFFQTtFQUNFLHlCQUF5QjtBQUMzQjs7QUFFQTtFQUNFLGVBQWU7RUFDZixjQUFjO0FBQ2hCOztBQUVBO0VBQ0UseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsY0FBYztBQUNoQjs7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixxQkFBcUI7QUFDdkI7O0FBRUE7RUFDRSxjQUFjO0FBQ2hCOztBQUVBO0VBQ0UsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQix5QkFBeUI7QUFDM0I7O0FBRUE7RUFDRSxjQUFjO0FBQ2hCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgaW1nIHtcclxuICBmb250LXNpemU6IDc1cHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiAyJTtcclxuICB3aWR0aDogMTUwcHg7XHJcbn1cclxuXHJcbi5kcm9wZG93bi1jb250ZW50IGE6aG92ZXIge2JhY2tncm91bmQtY29sb3I6ICNEOEQ4RDg7fVxyXG5cclxuLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB6LWluZGV4OiAyO1xyXG59XHJcblxyXG5uYXYge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNGMkYyRjI7XHJcbn1cclxuXHJcbmxpIHtcclxuICBmb250LXNpemU6IDE3cHg7XHJcbiAgY29sb3I6ICMwOTM1N2E7XHJcbn1cclxuXHJcbmxpOmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRDhEOEQ4O1xyXG59XHJcblxyXG4uZHJvcGRvd24tY29udGVudCBhIHtcclxuICBjb2xvcjogIzA5MzU3YTtcclxuICBwYWRkaW5nOiAxMnB4IDE2cHg7XHJcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uZHJvcGRvd24ge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbmEge1xyXG4gIGNvbG9yOiAjMDkzNTdhO1xyXG59XHJcblxyXG4uZHJvcGRvd24tY29udGVudCB7XHJcbiAgZGlzcGxheTogbm9uZTtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI0YyRjJGMjtcclxufVxyXG5cclxuLnNjcml0dGUge1xyXG4gIGNvbG9yOiAjMDkzNTdhO1xyXG59XHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/header/header.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<nav class=\"navbar navbar-expand-sm\">\r\n    <ul class=\"navbar-nav\" style=\"width:100%\">\r\n      <li class=\"nav-item\">\r\n        <a routerLink=\"home\" class=\"nav-link\">HOME</a>\r\n      </li>\r\n\r\n      <li class=\"nav-item dropdown\">\r\n        <a class=\"nav-link dropbtn\">FASI\r\n          <i class=\"fa fa-caret-down\"></i>\r\n        </a>\r\n        <div class=\"dropdown-content\">\r\n          <a routerLink=\"fase-valutazione\">VALUTAZIONE</a>\r\n          <a routerLink=\"fase-predisposizione\">PREDISPOSIZIONE</a>\r\n          <a routerLink=\"fase-val-outsourcing\">VALUTAZIONE OUTSOURCING</a>\r\n          <a routerLink=\"fase-scad-outsourcing\">SCADENZA OUTSOURCING</a>\r\n        </div>\r\n      </li>\r\n\r\n      <li class=\"nav-item\">\r\n        <a routerLink=\"estrazione\" class=\"nav-link\">ESTRAZIONE</a>\r\n      </li>\r\n      <li class=\"nav-item\" >\r\n        <a routerLink=\"lista-task-applicativi\" class=\"nav-link\">LISTA TASK APPLICATIVI</a>\r\n      </li>\r\n\r\n    </ul>\r\n\r\n    <div class=\"w-30\">\r\n      <app-user></app-user>\r\n    </div>\r\n\r\n    <img src=\"https://upload.wikimedia.org/wikipedia/it/0/0f/Logo_Sara_Assicurazioni.png\">\r\n  </nav>\r\n"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/components/header/header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/lista-task-applicativi/lista-task-applicativi.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/components/lista-task-applicativi/lista-task-applicativi.component.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbGlzdGEtdGFzay1hcHBsaWNhdGl2aS9saXN0YS10YXNrLWFwcGxpY2F0aXZpLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/lista-task-applicativi/lista-task-applicativi.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/components/lista-task-applicativi/lista-task-applicativi.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <app-search-bar></app-search-bar>\r\n\r\n  <h3>Lista Task Appilicativi</h3>\r\n\r\n\r\n  <app-task-app></app-task-app>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/lista-task-applicativi/lista-task-applicativi.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/components/lista-task-applicativi/lista-task-applicativi.component.ts ***!
  \***************************************************************************************/
/*! exports provided: ListaTaskApplicativiComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaTaskApplicativiComponent", function() { return ListaTaskApplicativiComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ListaTaskApplicativiComponent = /** @class */ (function () {
    function ListaTaskApplicativiComponent() {
    }
    ListaTaskApplicativiComponent.prototype.ngOnInit = function () {
    };
    ListaTaskApplicativiComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-lista-task-applicativi',
            template: __webpack_require__(/*! ./lista-task-applicativi.component.html */ "./src/app/components/lista-task-applicativi/lista-task-applicativi.component.html"),
            styles: [__webpack_require__(/*! ./lista-task-applicativi.component.css */ "./src/app/components/lista-task-applicativi/lista-task-applicativi.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ListaTaskApplicativiComponent);
    return ListaTaskApplicativiComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/login/login.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-button:hover {\r\n  border: solid 0.5px;\r\n  background-color: white;\r\n  color: #09357a;\r\n  cursor: pointer;\r\n}\r\n\r\n  h3 {\r\n    color: #09357a;\r\n  }\r\n\r\n  .login-button {\r\n  background-color: #09357a;\r\n  color: white;\r\n  margin-top: 5px;\r\n\r\n}\r\n\r\n  .div-login {\r\n  justify-content: center;\r\n  margin-top: 5%;\r\n}\r\n\r\n  form {\r\n  border: solid 1px #09357a;\r\n  padding: 5%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2QsZUFBZTtBQUNqQjs7RUFFRTtJQUNFLGNBQWM7RUFDaEI7O0VBRUY7RUFDRSx5QkFBeUI7RUFDekIsWUFBWTtFQUNaLGVBQWU7O0FBRWpCOztFQUVBO0VBQ0UsdUJBQXVCO0VBQ3ZCLGNBQWM7QUFDaEI7O0VBRUE7RUFDRSx5QkFBeUI7RUFDekIsV0FBVztBQUNiIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ2luLWJ1dHRvbjpob3ZlciB7XHJcbiAgYm9yZGVyOiBzb2xpZCAwLjVweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICBjb2xvcjogIzA5MzU3YTtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbiAgaDMge1xyXG4gICAgY29sb3I6ICMwOTM1N2E7XHJcbiAgfVxyXG5cclxuLmxvZ2luLWJ1dHRvbiB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA5MzU3YTtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgbWFyZ2luLXRvcDogNXB4O1xyXG5cclxufVxyXG5cclxuLmRpdi1sb2dpbiB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgbWFyZ2luLXRvcDogNSU7XHJcbn1cclxuXHJcbmZvcm0ge1xyXG4gIGJvcmRlcjogc29saWQgMXB4ICMwOTM1N2E7XHJcbiAgcGFkZGluZzogNSU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" flex div-login\">\n  <form [formGroup]=\"login\">\n\n    <H3>WEST LOGIN</H3>\n\n    <br/>\n\n    <mat-form-field>\n      <input matInput placeholder=\"username\" formControlName=\"username\">\n    </mat-form-field>\n\n    <br/>\n    <mat-form-field>\n     <input matInput placeholder=\"password\" formControlName=\"password\">\n    </mat-form-field>\n\n    <br/>\n\n    <div class=\"flex center\">\n        <input type=\"submit\" class=\"btn login-button\" (click)=\"checkLog()\" value=\"Login\">\n    </div>\n\n  </form>\n\n</div>\n"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");



var LoginComponent = /** @class */ (function () {
    function LoginComponent(fb) {
        this.fb = fb;
        this.logged = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.login = this.fb.group({
            username: '',
            password: ''
        });
    };
    LoginComponent.prototype.checkLog = function () {
        this.logged.emit(true);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], LoginComponent.prototype, "logged", void 0);
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/components/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/outsourcing/outsourcing.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/outsourcing/outsourcing.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\r\n  width: 100%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9vdXRzb3VyY2luZy9vdXRzb3VyY2luZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztBQUNiIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9vdXRzb3VyY2luZy9vdXRzb3VyY2luZy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/outsourcing/outsourcing.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/outsourcing/outsourcing.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"resp\" style=\"margin-top:10px;\">\n  <table mat-table  [dataSource]=\"dataSource\">\n\n    <!-- id Column -->\n    <ng-container matColumnDef=\"ID\">\n      <th mat-header-cell *matHeaderCellDef> ID </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.idEsternalizzazione}} </td>\n    </ng-container>\n\n    <!-- data creazione Column -->\n    <ng-container matColumnDef=\"DATA CREAZIONE\">\n      <th mat-header-cell *matHeaderCellDef> DATA CREAZIONE </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.dataCreazioneEsternalizzazione}} </td>\n    </ng-container>\n\n    <!-- titolo Column -->\n    <ng-container matColumnDef=\"TITOLO\">\n      <th mat-header-cell *matHeaderCellDef> TITOLO </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.titoloEsternalizzazione}} </td>\n    </ng-container>\n    \n    <!-- responsabile Column -->\n    <ng-container matColumnDef=\"RESPONSABILE\">\n      <th mat-header-cell *matHeaderCellDef> RESPONSABILE </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.compilataDa}} </td>\n    </ng-container>\n\n    <!-- responsabile Column -->\n    <ng-container matColumnDef=\"RESPONSABILE CONTROLLO\">\n      <th mat-header-cell *matHeaderCellDef> RESPONSABILE CONTROLLO </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.responsabileControlloEsternalizzazione}} </td>\n    </ng-container>\n\n    <ng-container matColumnDef = \"nuovaRda\">\n        <th mat-header-cell *matHeaderCellDef> </th>\n        <td mat-cell *matCellDef=\"let element\">\n          <button class=\"btn btn-success\">Nuova Rda</button>\n        </td>\n    </ng-container>\n\n\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n  </table>\n\n  <mat-paginator  [length]=\"length\"\n  [pageIndex]=\"pageIndex\"\n  [pageSize]=\"pageSize\"\n  [pageSizeOptions]=\"[5, 10, 20]\"\n  (page)=\"pageEvent = getOutsourcing($event)\" showFirstLastButtons></mat-paginator>\n</div>\n\n<div *ngIf=\"!view\"  class=\"flex gif\">\n  <img src=\"https://it.dplay.com/00406/static/resources/images/spinner.gif\">\n</div>\n\n\n  <div *ngIf=\"notResp\">\n    <h4>\n      Nessuna Esternalizzazione presente\n    </h4>\n  </div>\n"

/***/ }),

/***/ "./src/app/components/outsourcing/outsourcing.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/outsourcing/outsourcing.component.ts ***!
  \*****************************************************************/
/*! exports provided: OutsourcingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OutsourcingComponent", function() { return OutsourcingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_outsourcing_outsourcing_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/outsourcing/outsourcing.service */ "./src/app/services/outsourcing/outsourcing.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");




var OutsourcingComponent = /** @class */ (function () {
    function OutsourcingComponent(service) {
        this.service = service;
        this.displayedColumns = ['ID', 'DATA CREAZIONE', 'TITOLO', 'RESPONSABILE', 'RESPONSABILE CONTROLLO', 'nuovaRda'];
        this.dataSource = null;
        this.view = false;
        this.resp = false;
        this.notResp = false;
    }
    OutsourcingComponent.prototype.ngOnInit = function () {
        this.getOutsourcing(null);
    };
    OutsourcingComponent.prototype.getOutsourcing = function (event) {
        if (event === null) {
            this.pageIndex = 0;
            this.pageSize = 5;
            this.firstTimeGet();
        }
        else {
            this.pageIndex = event.pageIndex;
            this.pageSize = event.pageSize;
            this.get();
        }
    };
    OutsourcingComponent.prototype.firstTimeGet = function () {
        var _this = this;
        setTimeout(function () {
            _this.get();
        }, 1000);
    };
    OutsourcingComponent.prototype.get = function () {
        var _this = this;
        this.service.getOutsourcing(this.pageIndex, this.pageSize).subscribe(function (res) {
            _this.listaEsternalizzazioni = res;
            _this.view = true;
            if (_this.listaEsternalizzazioni.totaleRecord === 0) {
                _this.notResp = true;
            }
            else {
                _this.resp = true;
            }
            if (_this.dataSource === null) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.listaEsternalizzazioni.listaEsternalizzazioni);
                _this.dataSource.paginator = _this.paginator;
                _this.pageIndex = _this.listaEsternalizzazioni.paginaCorrente;
                _this.length = _this.listaEsternalizzazioni.totaleRecord;
            }
            else {
                _this.pageIndex = _this.listaEsternalizzazioni.paginaCorrente;
                _this.length = _this.listaEsternalizzazioni.totaleRecord;
                _this.dataSource.data = _this.listaEsternalizzazioni.listaEsternalizzazioni;
                _this._updatePaginator(_this.paginator.length, _this.pageIndex);
            }
        });
    };
    OutsourcingComponent.prototype._updatePaginator = function (filteredDataLength, pageIndex) {
        var _this = this;
        Promise.resolve().then(function () {
            _this.paginator.pageIndex = pageIndex;
            if (!_this.paginator) {
                return;
            }
            _this.paginator.length = filteredDataLength;
            // If the page index is set beyond the page, reduce it to the last page.
            if (_this.paginator.pageIndex > 0) {
                var lastPageIndex = Math.ceil(_this.paginator.length / _this.paginator.pageSize) - 1 || 0;
                _this.paginator.pageIndex = Math.min(_this.paginator.pageIndex, lastPageIndex);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], OutsourcingComponent.prototype, "page", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], OutsourcingComponent.prototype, "paginator", void 0);
    OutsourcingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-outsourcing',
            template: __webpack_require__(/*! ./outsourcing.component.html */ "./src/app/components/outsourcing/outsourcing.component.html"),
            styles: [__webpack_require__(/*! ./outsourcing.component.css */ "./src/app/components/outsourcing/outsourcing.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_outsourcing_outsourcing_service__WEBPACK_IMPORTED_MODULE_2__["OutsourcingService"]])
    ], OutsourcingComponent);
    return OutsourcingComponent;
}());



/***/ }),

/***/ "./src/app/components/predisposizione/predisposizione.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/components/predisposizione/predisposizione.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\r\n  width: 100%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wcmVkaXNwb3NpemlvbmUvcHJlZGlzcG9zaXppb25lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFXO0FBQ2IiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3ByZWRpc3Bvc2l6aW9uZS9wcmVkaXNwb3NpemlvbmUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/predisposizione/predisposizione.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/predisposizione/predisposizione.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"resp\" style=\"margin-top:10px;\">\n    <table mat-table  [dataSource]=\"dataSource\">\n\n      <!-- id Column -->\n      <ng-container matColumnDef=\"ID\">\n        <th mat-header-cell *matHeaderCellDef> ID </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.idEsternalizzazione}} </td>\n      </ng-container>\n\n      <!-- data creazione Column -->\n      <ng-container matColumnDef=\"DATA CREAZIONE\">\n        <th mat-header-cell *matHeaderCellDef> DATA CREAZIONE </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.dataCreazioneEsternalizzazione}} </td>\n      </ng-container>\n\n      <!-- titolo Column -->\n      <ng-container matColumnDef=\"TITOLO\">\n        <th mat-header-cell *matHeaderCellDef> TITOLO </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.titoloEsternalizzazione}} </td>\n      </ng-container>\n\n        <!-- responsabile Column -->\n        <ng-container matColumnDef=\"RESPONSABILE\">\n        <th mat-header-cell *matHeaderCellDef> RESPONSABILE </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.compilataDa}} </td>\n      </ng-container>\n\n      <!-- responsabile Column -->\n      <ng-container matColumnDef=\"RESPONSABILE CONTROLLO\">\n        <th mat-header-cell *matHeaderCellDef> RESPONSABILE CONTROLLO </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.responsabileControlloEsternalizzazione}} </td>\n      </ng-container>\n\n      <ng-container matColumnDef = \"nuovaRda\">\n        <th mat-header-cell *matHeaderCellDef> </th>\n        <td mat-cell *matCellDef=\"let element\">\n          <button class=\"btn btn-success\">Nuova Rda</button>\n        </td>\n      </ng-container>\n\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"\n      routerLink=\"/esternalizzazione-details\" routerLinkActive=\"router-link-active\"  (click)=\"viewDetails(row)\">\n    </tr>\n    </table>\n\n    <mat-paginator  [length]=\"length\"\n    [pageIndex]=\"pageIndex\"\n    [pageSize]=\"pageSize\"\n    [pageSizeOptions]=\"[5, 10, 20]\"\n    (page)=\"pageEvent = getPredisposizione($event)\" showFirstLastButtons></mat-paginator>\n</div>\n\n<div *ngIf=\"!view\"  class=\"flex gif\">\n  <img src=\"https://it.dplay.com/00406/static/resources/images/spinner.gif\">\n</div>\n\n\n<div *ngIf=\"notResp\">\n  <h4>\n    Nessuna Esternalizzazione presente\n  </h4>\n</div>\n"

/***/ }),

/***/ "./src/app/components/predisposizione/predisposizione.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/predisposizione/predisposizione.component.ts ***!
  \*************************************************************************/
/*! exports provided: PredisposizioneComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PredisposizioneComponent", function() { return PredisposizioneComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_services_fase_predisposizione_fase_predisposizione_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/fase-predisposizione/fase-predisposizione.service */ "./src/app/services/fase-predisposizione/fase-predisposizione.service.ts");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");





var PredisposizioneComponent = /** @class */ (function () {
    function PredisposizioneComponent(service, varG) {
        this.service = service;
        this.varG = varG;
        this.displayedColumns = ['ID', 'DATA CREAZIONE', 'TITOLO', 'RESPONSABILE', 'RESPONSABILE CONTROLLO', 'nuovaRda'];
        this.dataSource = null;
        this.view = false;
        this.resp = false;
        this.notResp = false;
    }
    PredisposizioneComponent.prototype.ngOnInit = function () {
        this.getPredisposizione(null);
    };
    PredisposizioneComponent.prototype.getPredisposizione = function (event) {
        if (event === null) {
            this.pageIndex = 0;
            this.pageSize = 5;
            this.firstTimeGet();
        }
        else {
            this.pageIndex = event.pageIndex;
            this.pageSize = event.pageSize;
            this.get();
        }
    };
    PredisposizioneComponent.prototype.firstTimeGet = function () {
        var _this = this;
        setTimeout(function () {
            _this.get();
        }, 1000);
    };
    PredisposizioneComponent.prototype.viewDetails = function (details) {
        this.varG.setIdEsternalizzazione(details.idEsternalizzazione);
        this.varG.setPosizione('predisposizione');
    };
    PredisposizioneComponent.prototype.get = function () {
        var _this = this;
        this.service.getFaseValutazione(this.pageIndex, this.pageSize).subscribe(function (res) {
            _this.listaEsternalizzazioni = res;
            _this.view = true;
            if (_this.listaEsternalizzazioni.totaleRecord === 0) {
                _this.notResp = true;
            }
            else {
                _this.resp = true;
            }
            if (_this.dataSource === null) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](_this.listaEsternalizzazioni.listaEsternalizzazioni);
                _this.dataSource.paginator = _this.paginator;
                _this.pageIndex = _this.listaEsternalizzazioni.paginaCorrente;
                _this.length = _this.listaEsternalizzazioni.totaleRecord;
            }
            else {
                _this.pageIndex = _this.listaEsternalizzazioni.paginaCorrente;
                _this.length = _this.listaEsternalizzazioni.totaleRecord;
                _this.dataSource.data = _this.listaEsternalizzazioni.listaEsternalizzazioni;
                _this._updatePaginator(_this.paginator.length, _this.pageIndex);
            }
        });
    };
    PredisposizioneComponent.prototype._updatePaginator = function (filteredDataLength, pageIndex) {
        var _this = this;
        Promise.resolve().then(function () {
            _this.paginator.pageIndex = pageIndex;
            if (!_this.paginator) {
                return;
            }
            _this.paginator.length = filteredDataLength;
            // If the page index is set beyond the page, reduce it to the last page.
            if (_this.paginator.pageIndex > 0) {
                var lastPageIndex = Math.ceil(_this.paginator.length / _this.paginator.pageSize) - 1 || 0;
                _this.paginator.pageIndex = Math.min(_this.paginator.pageIndex, lastPageIndex);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], PredisposizioneComponent.prototype, "page", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], PredisposizioneComponent.prototype, "paginator", void 0);
    PredisposizioneComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-predisposizione',
            template: __webpack_require__(/*! ./predisposizione.component.html */ "./src/app/components/predisposizione/predisposizione.component.html"),
            styles: [__webpack_require__(/*! ./predisposizione.component.css */ "./src/app/components/predisposizione/predisposizione.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_fase_predisposizione_fase_predisposizione_service__WEBPACK_IMPORTED_MODULE_3__["FasePredisposizioneService"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__["VariabiliGlobali"]])
    ], PredisposizioneComponent);
    return PredisposizioneComponent;
}());



/***/ }),

/***/ "./src/app/components/scad-outsourcing/scad-outsourcing.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/components/scad-outsourcing/scad-outsourcing.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\r\n  width: 100%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zY2FkLW91dHNvdXJjaW5nL3NjYWQtb3V0c291cmNpbmcuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQVc7QUFDYiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2NhZC1vdXRzb3VyY2luZy9zY2FkLW91dHNvdXJjaW5nLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/scad-outsourcing/scad-outsourcing.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/components/scad-outsourcing/scad-outsourcing.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"resp\" style=\"margin-top:10px;\">\n  <table mat-table  [dataSource]=\"dataSource\">\n\n    <!-- id Column -->\n    <ng-container matColumnDef=\"ID\">\n      <th mat-header-cell *matHeaderCellDef> ID </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.idEsternalizzazione}} </td>\n    </ng-container>\n\n    <!-- data creazione Column -->\n    <ng-container matColumnDef=\"DATA CREAZIONE\">\n      <th mat-header-cell *matHeaderCellDef> DATA CREAZIONE </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.dataCreazioneEsternalizzazione}} </td>\n    </ng-container>\n\n    <!-- titolo Column -->\n    <ng-container matColumnDef=\"TITOLO\">\n      <th mat-header-cell *matHeaderCellDef> TITOLO </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.titoloEsternalizzazione}} </td>\n    </ng-container>\n\n    <!-- responsabile Column -->\n    <ng-container matColumnDef=\"RESPONSABILE\">\n      <th mat-header-cell *matHeaderCellDef> RESPONSABILE </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.compilataDa}} </td>\n    </ng-container>\n\n    <!-- responsabile Column -->\n    <ng-container matColumnDef=\"RESPONSABILE CONTROLLO\">\n      <th mat-header-cell *matHeaderCellDef> RESPONSABILE CONTROLLO </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.responsabileControlloEsternalizzazione}} </td>\n    </ng-container>\n\n    <ng-container matColumnDef = \"nuovaRda\">\n        <th mat-header-cell *matHeaderCellDef> </th>\n        <td mat-cell *matCellDef=\"let element\">\n          <button class=\"btn btn-success\">Nuova Rda</button>\n        </td>\n    </ng-container>\n\n\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n  </table>\n\n  <mat-paginator  [length]=\"length\"\n  [pageIndex]=\"pageIndex\"\n  [pageSize]=\"pageSize\"\n  [pageSizeOptions]=\"[5, 10, 20]\"\n  (page)=\"pageEvent = getScadOutsourcing($event)\" showFirstLastButtons></mat-paginator>\n</div>\n\n<div *ngIf=\"!view\"  class=\"flex gif\">\n  <img src=\"https://it.dplay.com/00406/static/resources/images/spinner.gif\">\n</div>\n\n\n  <div *ngIf=\"notResp\">\n    <h4>\n      Nessuna Esternalizzazione presente\n    </h4>\n  </div>\n"

/***/ }),

/***/ "./src/app/components/scad-outsourcing/scad-outsourcing.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/scad-outsourcing/scad-outsourcing.component.ts ***!
  \***************************************************************************/
/*! exports provided: ScadOutsourcingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScadOutsourcingComponent", function() { return ScadOutsourcingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_scadOutsourcing_scad_outsourcing_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/scadOutsourcing/scad-outsourcing.service */ "./src/app/services/scadOutsourcing/scad-outsourcing.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");




var ScadOutsourcingComponent = /** @class */ (function () {
    function ScadOutsourcingComponent(service) {
        this.service = service;
        this.displayedColumns = ['ID', 'DATA CREAZIONE', 'TITOLO', 'RESPONSABILE', 'RESPONSABILE CONTROLLO', 'nuovaRda'];
        this.dataSource = null;
        this.view = false;
        this.resp = false;
        this.notResp = false;
    }
    ScadOutsourcingComponent.prototype.ngOnInit = function () {
        this.getScadOutsourcing(null);
    };
    ScadOutsourcingComponent.prototype.getScadOutsourcing = function (event) {
        if (event === null) {
            this.pageIndex = 0;
            this.pageSize = 5;
            this.firstTimeGet();
        }
        else {
            this.pageIndex = event.pageIndex;
            this.pageSize = event.pageSize;
            this.get();
        }
    };
    ScadOutsourcingComponent.prototype.firstTimeGet = function () {
        var _this = this;
        setTimeout(function () {
            _this.get();
        }, 1000);
    };
    ScadOutsourcingComponent.prototype.get = function () {
        var _this = this;
        this.service.getScadOutsourcing(this.pageIndex, this.pageSize).subscribe(function (res) {
            _this.listaEsternalizzazioni = res;
            _this.view = true;
            if (_this.listaEsternalizzazioni.totaleRecord === 0) {
                _this.notResp = true;
            }
            else {
                _this.resp = true;
            }
            if (_this.dataSource === null) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.listaEsternalizzazioni.listaEsternalizzazioni);
                _this.dataSource.paginator = _this.paginator;
                _this.pageIndex = _this.listaEsternalizzazioni.paginaCorrente;
                _this.length = _this.listaEsternalizzazioni.totaleRecord;
            }
            else {
                _this.pageIndex = _this.listaEsternalizzazioni.paginaCorrente;
                _this.length = _this.listaEsternalizzazioni.totaleRecord;
                _this.dataSource.data = _this.listaEsternalizzazioni.listaEsternalizzazioni;
                _this._updatePaginator(_this.paginator.length, _this.pageIndex);
            }
        });
    };
    ScadOutsourcingComponent.prototype._updatePaginator = function (filteredDataLength, pageIndex) {
        var _this = this;
        Promise.resolve().then(function () {
            _this.paginator.pageIndex = pageIndex;
            if (!_this.paginator) {
                return;
            }
            _this.paginator.length = filteredDataLength;
            // If the page index is set beyond the page, reduce it to the last page.
            if (_this.paginator.pageIndex > 0) {
                var lastPageIndex = Math.ceil(_this.paginator.length / _this.paginator.pageSize) - 1 || 0;
                _this.paginator.pageIndex = Math.min(_this.paginator.pageIndex, lastPageIndex);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ScadOutsourcingComponent.prototype, "page", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], ScadOutsourcingComponent.prototype, "paginator", void 0);
    ScadOutsourcingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-scad-outsourcing',
            template: __webpack_require__(/*! ./scad-outsourcing.component.html */ "./src/app/components/scad-outsourcing/scad-outsourcing.component.html"),
            styles: [__webpack_require__(/*! ./scad-outsourcing.component.css */ "./src/app/components/scad-outsourcing/scad-outsourcing.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_scadOutsourcing_scad_outsourcing_service__WEBPACK_IMPORTED_MODULE_2__["ScadOutsourcingService"]])
    ], ScadOutsourcingComponent);
    return ScadOutsourcingComponent;
}());



/***/ }),

/***/ "./src/app/components/search-bar/search-bar.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/search-bar/search-bar.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #spacer></div>\r\n<div stickyThing [spacer]=\"spacer\" style=\"z-index:5; margin-top: 5px;\" >\r\n  <div class=\"flex right\">\r\n    <div class=\"flex m-r\">\r\n      <input\r\n        class=\"form-control\"\r\n        type=\"search\"\r\n        placeholder=\"Cerca..\"\r\n        aria-label=\"Cerca\"\r\n      />\r\n      <button class=\"btn search-button\" style=\"margin-left: 2px;\" type=\"submit\">Cerca</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/search-bar/search-bar.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/components/search-bar/search-bar.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".search-button:hover {\n  border: solid 0.5px;\n  background-color: white;\n  color: #09357a;\n  cursor: pointer; }\n\n.search-button {\n  background-color: #09357a;\n  color: white;\n  margin-left: 1px; }\n\n.m-r {\n  margin-right: 2.5%;\n  position: relative;\n  width: 30%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zZWFyY2gtYmFyL0M6XFxVc2Vyc1xcZS5jb2xhc2FudGlcXGdpdFxcZXN0ZXJuYWxpenphemlvbmUtMi4wXFxlc3Rlcm5hbGl6emF6aW9uZS1zdXBlcnBvbVxcRXN0ZXJuYWxpenphemlvbmUtRnJvbnRlbmRcXHNyY1xcbWFpblxcd2ViL3NyY1xcYXBwXFxjb21wb25lbnRzXFxzZWFyY2gtYmFyXFxzZWFyY2gtYmFyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2QsZUFBZSxFQUFBOztBQUdqQjtFQUNFLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0Usa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixVQUFVLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3NlYXJjaC1iYXIvc2VhcmNoLWJhci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zZWFyY2gtYnV0dG9uOmhvdmVyIHtcclxuICBib3JkZXI6IHNvbGlkIDAuNXB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gIGNvbG9yOiAjMDkzNTdhO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLnNlYXJjaC1idXR0b24ge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwOTM1N2E7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIG1hcmdpbi1sZWZ0OiAxcHg7XHJcbn1cclxuXHJcbi5tLXIge1xyXG4gIG1hcmdpbi1yaWdodDogMi41JTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgd2lkdGg6IDMwJTtcclxuXHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/search-bar/search-bar.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/search-bar/search-bar.component.ts ***!
  \***************************************************************/
/*! exports provided: SearchBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchBarComponent", function() { return SearchBarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SearchBarComponent = /** @class */ (function () {
    function SearchBarComponent() {
    }
    SearchBarComponent.prototype.ngOnInit = function () {
    };
    SearchBarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-search-bar',
            template: __webpack_require__(/*! ./search-bar.component.html */ "./src/app/components/search-bar/search-bar.component.html"),
            styles: [__webpack_require__(/*! ./search-bar.component.scss */ "./src/app/components/search-bar/search-bar.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SearchBarComponent);
    return SearchBarComponent;
}());



/***/ }),

/***/ "./src/app/components/task-app/task-app.component.css":
/*!************************************************************!*\
  !*** ./src/app/components/task-app/task-app.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\r\n  width: 100%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90YXNrLWFwcC90YXNrLWFwcC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztBQUNiIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy90YXNrLWFwcC90YXNrLWFwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/task-app/task-app.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/task-app/task-app.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"resp\" style=\"margin-top:10px;\">\n  <table mat-table  [dataSource]=\"dataSource\">\n\n    <!-- id Column -->\n    <ng-container matColumnDef=\"ID TASK\">\n      <th mat-header-cell *matHeaderCellDef> ID TASK </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.idTask}} </td>\n    </ng-container>\n\n    <!-- data creazione Column -->\n    <ng-container matColumnDef=\"DATA CREAZIONE\">\n      <th mat-header-cell *matHeaderCellDef> DATA CREAZIONE </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.dataCreazione}} </td>\n    </ng-container>\n\n    <ng-container matColumnDef=\"RE\">\n      <th mat-header-cell *matHeaderCellDef> RE </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.utenteResponsabile}} </td>\n    </ng-container>\n\n\n    <!-- titolo Column -->\n    <ng-container matColumnDef=\"UTENTE DELEGATO\">\n      <th mat-header-cell *matHeaderCellDef> UTENTE DELEGATO </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.utenteDelegato}} </td>\n    </ng-container>\n\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns;\"></tr>\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"\n        routerLink=\"/task-details\"\n        routerLinkActive=\"router-link-active\"\n        (click)=\"viewDetails(row)\"></tr>\n  </table>\n\n  <mat-paginator  [length]=\"length\"\n  [pageIndex]=\"pageIndex\"\n  [pageSize]=\"pageSize\"\n  [pageSizeOptions]=\"[5, 10, 20]\"\n  (page)=\"pageEvent = getListaTaskApp($event)\" showFirstLastButtons></mat-paginator>\n</div>\n\n<div *ngIf=\"!view\"  class=\"flex gif\">\n  <img src=\"https://it.dplay.com/00406/static/resources/images/spinner.gif\">\n</div>\n\n  <div *ngIf=\"notResp\">\n    <h4>\n      Nessun Task presente\n    </h4>\n  </div>\n"

/***/ }),

/***/ "./src/app/components/task-app/task-app.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/task-app/task-app.component.ts ***!
  \***********************************************************/
/*! exports provided: TaskAppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskAppComponent", function() { return TaskAppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_lista_task_app_lista_task_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/lista-task-app/lista-task-app.service */ "./src/app/services/lista-task-app/lista-task-app.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");





var TaskAppComponent = /** @class */ (function () {
    function TaskAppComponent(service, varG) {
        this.service = service;
        this.varG = varG;
        this.displayedColumns = ['ID TASK', 'DATA CREAZIONE', 'UTENTE DELEGATO', 'RE'];
        this.dataSource = null;
        this.view = false;
        this.resp = false;
        this.notResp = false;
    }
    TaskAppComponent.prototype.ngOnInit = function () {
        this.getListaTaskApp(null);
    };
    TaskAppComponent.prototype.getListaTaskApp = function (event) {
        if (event === null) {
            this.pageIndex = 0;
            this.pageSize = 5;
            this.firstTimeGet();
        }
        else {
            this.pageIndex = event.pageIndex;
            this.pageSize = event.pageSize;
            this.get();
        }
    };
    TaskAppComponent.prototype.viewDetails = function (details) {
        this.varG.setIdTask(details.idTask.valueOf());
        this.varG.setPosizione('lista-task-app');
    };
    TaskAppComponent.prototype.firstTimeGet = function () {
        var _this = this;
        setTimeout(function () {
            _this.get();
        }, 1000);
    };
    TaskAppComponent.prototype.get = function () {
        var _this = this;
        this.service.getListaTaskApp(this.pageIndex, this.pageSize).subscribe(function (res) {
            _this.listaAttivita = res;
            _this.view = true;
            if (_this.listaAttivita.totaleRecord === 0) {
                _this.notResp = true;
            }
            else {
                _this.resp = true;
            }
            if (_this.dataSource === null) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.listaAttivita.listaAttivita);
                _this.dataSource.paginator = _this.paginator;
                _this.pageIndex = _this.listaAttivita.paginaCorrente;
                _this.length = _this.listaAttivita.totaleRecord;
            }
            else {
                _this.pageIndex = _this.listaAttivita.paginaCorrente;
                _this.length = _this.listaAttivita.totaleRecord;
                _this.dataSource.data = _this.listaAttivita.listaAttivita;
                _this._updatePaginator(_this.paginator.length, _this.pageIndex);
            }
        });
    };
    TaskAppComponent.prototype._updatePaginator = function (filteredDataLength, pageIndex) {
        var _this = this;
        Promise.resolve().then(function () {
            _this.paginator.pageIndex = pageIndex;
            if (!_this.paginator) {
                return;
            }
            _this.paginator.length = filteredDataLength;
            // If the page index is set beyond the page, reduce it to the last page.
            if (_this.paginator.pageIndex > 0) {
                var lastPageIndex = Math.ceil(_this.paginator.length / _this.paginator.pageSize) - 1 || 0;
                _this.paginator.pageIndex = Math.min(_this.paginator.pageIndex, lastPageIndex);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], TaskAppComponent.prototype, "page", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], TaskAppComponent.prototype, "paginator", void 0);
    TaskAppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task-app',
            template: __webpack_require__(/*! ./task-app.component.html */ "./src/app/components/task-app/task-app.component.html"),
            styles: [__webpack_require__(/*! ./task-app.component.css */ "./src/app/components/task-app/task-app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_lista_task_app_lista_task_app_service__WEBPACK_IMPORTED_MODULE_2__["ListaTaskAppService"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__["VariabiliGlobali"]])
    ], TaskAppComponent);
    return TaskAppComponent;
}());



/***/ }),

/***/ "./src/app/components/task/actions-bar-task/actions-bar-task.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/components/task/actions-bar-task/actions-bar-task.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".previous-page-btn {\r\n    background-color: #09357a;\r\n    color: white;\r\n  }\r\n  .previous-page-btn:hover {\r\n    border: solid 0.5px;\r\n    background-color: white;\r\n    color: #09357a;\r\n    cursor: pointer;\r\n  }\r\n  .edit-btn {\r\n    background-color:#15BD17;\r\n    color: white;\r\n  }\r\n  .edit-btn:hover {\r\n    background-color: white;\r\n    color: #15BD17;\r\n    border: solid 0.5px;\r\n    cursor: pointer;\r\n  }\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90YXNrL2FjdGlvbnMtYmFyLXRhc2svYWN0aW9ucy1iYXItdGFzay5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0kseUJBQXlCO0lBQ3pCLFlBQVk7RUFDZDtFQUNBO0lBQ0UsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixjQUFjO0lBQ2QsZUFBZTtFQUNqQjtFQUNBO0lBQ0Usd0JBQXdCO0lBQ3hCLFlBQVk7RUFDZDtFQUVBO0lBQ0UsdUJBQXVCO0lBQ3ZCLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsZUFBZTtFQUNqQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFzay9hY3Rpb25zLWJhci10YXNrL2FjdGlvbnMtYmFyLXRhc2suY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wcmV2aW91cy1wYWdlLWJ0biB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDkzNTdhO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuICAucHJldmlvdXMtcGFnZS1idG46aG92ZXIge1xyXG4gICAgYm9yZGVyOiBzb2xpZCAwLjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgY29sb3I6ICMwOTM1N2E7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG4gIC5lZGl0LWJ0biB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiMxNUJEMTc7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5lZGl0LWJ0bjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGNvbG9yOiAjMTVCRDE3O1xyXG4gICAgYm9yZGVyOiBzb2xpZCAwLjVweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcbiAgIl19 */"

/***/ }),

/***/ "./src/app/components/task/actions-bar-task/actions-bar-task.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/components/task/actions-bar-task/actions-bar-task.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div #spacer></div>\n\n<div stickyThing [spacer]=\"spacer\" style=\"z-index:5;background-color: white;\" >\n  <div style=\"margin-bottom: 10px;\" class=\"d-flex center m-t10\">\n    <button class=\"btn previous-page-btn\" [routerLink]=\"[routerLink]\" routerLinkActive=\"router-link-active\"  >Torna alla lista</button>\n    <button class=\"btn edit-btn m-l10\" (click)=\"activeEditMode()\" *ngIf=\"edit\"> Edit</button>\n    <button class=\"btn edit-btn m-l10\" *ngIf=\"!edit\" (click)=\"saveChanges()\">Salva Modifiche</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/task/actions-bar-task/actions-bar-task.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/components/task/actions-bar-task/actions-bar-task.component.ts ***!
  \********************************************************************************/
/*! exports provided: ActionsBarTaskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionsBarTaskComponent", function() { return ActionsBarTaskComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");



var ActionsBarTaskComponent = /** @class */ (function () {
    function ActionsBarTaskComponent(varG) {
        this.varG = varG;
        this.editOutput = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.edit = true;
        this.routerLink = this.varG.getRouterLink().valueOf();
    }
    ActionsBarTaskComponent.prototype.ngOnInit = function () {
    };
    ActionsBarTaskComponent.prototype.activeEditMode = function () {
        this.edit = false;
        this.editOutput.emit(false);
    };
    ActionsBarTaskComponent.prototype.saveChanges = function () {
        this.edit = true;
        this.editOutput.emit(true);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ActionsBarTaskComponent.prototype, "editOutput", void 0);
    ActionsBarTaskComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-actions-bar-task',
            template: __webpack_require__(/*! ./actions-bar-task.component.html */ "./src/app/components/task/actions-bar-task/actions-bar-task.component.html"),
            styles: [__webpack_require__(/*! ./actions-bar-task.component.css */ "./src/app/components/task/actions-bar-task/actions-bar-task.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_2__["VariabiliGlobali"]])
    ], ActionsBarTaskComponent);
    return ActionsBarTaskComponent;
}());



/***/ }),

/***/ "./src/app/components/task/dettaglio-task/dettaglio-task.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/components/task/dettaglio-task/dettaglio-task.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFzay9kZXR0YWdsaW8tdGFzay9kZXR0YWdsaW8tdGFzay5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/task/dettaglio-task/dettaglio-task.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/components/task/dettaglio-task/dettaglio-task.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"view\">\n    <div class=\"m-l10\">\n        <h4>\n            Dettaglio Task : {{task.idTask}}\n        </h4>\n    </div>\n    \n    <app-actions-bar-task (editOutput)=\"edit = $event\"></app-actions-bar-task>\n\n    <div class=\"flex bordered padding m-l10\">\n        <div class=\"w-80 font \">\n    \n            <div class=\"row\" style=\"align-items:center;\">\n                <div class=\"col-sm-2\">\n                    ID Esternalizzazione :\n                </div>\n                <div class=\"col-sm-10\">\n                    <input \n                        type=\"text\"\n                        [(ngModel)]=\"task.idEstrenalizzazione\"\n                        [disabled]=\"edit\" >\n                </div>\n            </div>\n            \n            <div class=\"row\" style=\"align-items:center;\">\n                <div class=\"col-sm-2\">\n                    Data Creazione :\n                </div>\n                <div class=\"col-sm-10\">\n                    <mat-form-field>\n                        <input matInput \n                          [matDatepicker]=\"pickerDataCreazioneTask\" \n                          placeholder=\"{{task.dataCreazione}}\"\n                          [(ngModel)]=\"task.dataCreazione\"\n                          [disabled]=\"edit\"\n                          >\n                        <mat-datepicker-toggle matSuffix [for]=\"pickerDataCreazioneTask\"></mat-datepicker-toggle>\n                        <mat-datepicker #pickerDataCreazioneTask></mat-datepicker>\n                      </mat-form-field>\n                </div>\n            </div>\n    \n            <div class=\"row\" style=\"align-items:center;\">\n                <div class=\"col-sm-2\">\n                    Data Assegnazione :\n                </div>\n                <div class=\"col-sm-10\">\n                    <mat-form-field>\n                        <input matInput \n                          [matDatepicker]=\"pickerDataAssegnazioneTask\" \n                          placeholder=\"{{task.dataAssegnazione}}\"\n                          [(ngModel)]=\"task.dataAssegnazione\"\n                          [disabled]=\"edit\"\n                          >\n                        <mat-datepicker-toggle matSuffix [for]=\"pickerDataAssegnazioneTask\"></mat-datepicker-toggle>\n                        <mat-datepicker #pickerDataAssegnazioneTask></mat-datepicker>\n                      </mat-form-field>\n                </div>\n            </div>\n    \n            <div class=\"row\" style=\"align-items:center;\">\n                <div class=\"col-sm-2\">\n                    Data Fine :\n                </div>\n                <div class=\"col-sm-10\">\n                    <mat-form-field>\n                        <input matInput \n                          [matDatepicker]=\"pickerDataFineTask\" \n                          placeholder=\"{{task.dataFine}}\"\n                          [(ngModel)]=\"task.dataFine\"\n                          [disabled]=\"edit\"\n                          >\n                        <mat-datepicker-toggle matSuffix [for]=\"pickerDataFineTask\"></mat-datepicker-toggle>\n                        <mat-datepicker #pickerDataFineTask></mat-datepicker>\n                      </mat-form-field>\n                </div>\n            </div>\n    \n            <div class=\"row\" style=\"align-items:center;\">\n                <div class=\"col-sm-2\">\n                    Utente Owner :\n                </div>\n                <div class=\"col-sm-10\">\n                    <input \n                        type=\"text\"\n                        [(ngModel)]=\"task.utenteOwner\"\n                        [disabled]=\"edit\" >\n                </div>\n            </div>\n\n            <div class=\"row m-t10\" style=\"align-items:center;\">\n                <div class=\"col-sm-2\">\n                    Responsabile :\n                </div>\n                <div class=\"col-sm-10\">\n                    <input \n                        type=\"text\"\n                        [(ngModel)]=\"task.utenteResponsabile\"\n                        [disabled]=\"edit\" >\n                </div>\n            </div>\n\n            <div class=\"row\" style=\"align-items: center;\">\n                <div class=\"col-sm-2\">\n                    Tipologia Task:\n                </div>\n                <div class=\"col-sm-10\">\n                    <mat-form-field class=\"select w-70\">\n\n                        <mat-select\n                        [(ngModel)]=\"task.idTipoTask\"\n                        placeholder=\"{{task.idTipoTask}}\"\n                        [disabled]=\"edit\"\n                        >\n            \n                        <mat-option>--</mat-option>\n                        <mat-option *ngFor=\"let tipo of listaTipologiaTask\" [value]=\"tipo.codice\">\n                          {{tipo.descrizione}}\n                        </mat-option>\n                      </mat-select>\n                      </mat-form-field>\n                </div>\n            </div>\n\n            <div class=\"row\" style=\"align-items: center;\">\n                <div class=\"col-sm-2\">\n                    Stato:\n                </div>\n                <div class=\"col-sm-10\">\n                    <mat-form-field class=\"select w-70\">\n\n                        <mat-select\n                        [(ngModel)]=\"task.stato\"\n                        placeholder=\"{{task.stato}}\"\n                        [disabled]=\"edit\"\n                        >\n            \n                        <mat-option>--</mat-option>\n                        <mat-option value=\"A\">Aperto</mat-option>\n                        <mat-option value=\"C\">Chiuso</mat-option>\n                        <mat-option value=\"V\">Validazione</mat-option>\n                      </mat-select>\n                      </mat-form-field>\n                </div>\n            </div>\n\n            <div class=\"row\" style=\"align-items: center;\">\n                <div class=\"col-sm-2\">\n                    Competenza:\n                </div>\n                <div class=\"col-sm-10\">\n                    <mat-form-field class=\"select w-70\">\n\n                        <mat-select\n                        [(ngModel)]=\"task.idTipoUtente\"\n                        placeholder=\"{{task.idTipoUtente}}\"\n                        [disabled]=\"edit\"\n                        >\n            \n                        <mat-option>--</mat-option>\n                        <mat-option *ngFor=\"let tipo of listaTipiUtente\" [value]=\"tipo.codice\">\n                          {{tipo.descrizione}}\n                        </mat-option>\n                      </mat-select>\n                      </mat-form-field>\n                </div>\n            </div>\n\n            <div>\n                <div>\n                    <h4>\n                        Esternalizzazione\n                    </h4>\n                </div>\n                <div class=\"row\" style=\"align-items:center;\">\n                    <div class=\"col-sm-2\">\n                        Campo :\n                    </div>\n                    <div class=\"col-sm-10\">\n                        <input \n                            type=\"text\"\n                            [(ngModel)]=\"task.tipoCampoAttivatoEst\"\n                            [disabled]=\"true\" >\n                    </div>\n                </div>\n    \n                <div class=\"row m-t10\" style=\"align-items:center;\">\n                    <div class=\"col-sm-2\">\n                        Fase :\n                    </div>\n                    <div class=\"col-sm-10\">\n                        <input \n                            type=\"text\"\n                            [(ngModel)]=\"task.faseEsternalizzazione\"\n                            [disabled]=\"true\" >\n                    </div>\n                </div>\n            </div>\n\n        </div>\n\n        <div class=\"allegati padding w-20 m-t10\">\n\n                <h4>Allegati Task</h4>\n              \n                      <div *ngIf=\"fileTask.length!==0\">\n                          <div class=\"animation\" *ngFor=\"let file of fileTask\">\n                              {{file.nomeFileOrigine}}\n                          </div>\n                        </div>\n                    \n                        <div *ngIf=\"fileTask.length === 0\">\n                            Nessun File Presente\n                        </div>\n              </div>\n    </div>\n\n    <br>\n    \n</div>\n\n<div *ngIf=\"!view\"  class=\"flex gif\">\n    <img src=\"https://it.dplay.com/00406/static/resources/images/spinner.gif\">\n</div>\n"

/***/ }),

/***/ "./src/app/components/task/dettaglio-task/dettaglio-task.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/components/task/dettaglio-task/dettaglio-task.component.ts ***!
  \****************************************************************************/
/*! exports provided: DettaglioTaskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DettaglioTaskComponent", function() { return DettaglioTaskComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_lista_task_get_task_get_task_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/lista-task/get-task/get-task.service */ "./src/app/services/lista-task/get-task/get-task.service.ts");
/* harmony import */ var src_app_services_lista_task_tipologiche_task_tipologiche_task_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/lista-task/tipologiche-task/tipologiche-task.service */ "./src/app/services/lista-task/tipologiche-task/tipologiche-task.service.ts");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");
/* harmony import */ var src_app_services_File_Service_file_task_file_task_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/File-Service/file-task/file-task.service */ "./src/app/services/File-Service/file-task/file-task.service.ts");






var DettaglioTaskComponent = /** @class */ (function () {
    function DettaglioTaskComponent(taskService, tipologicheService, varG, fileService) {
        this.taskService = taskService;
        this.tipologicheService = tipologicheService;
        this.varG = varG;
        this.fileService = fileService;
        this.fileTask = [];
        this.edit = true;
        if (this.varG.getPosizione().valueOf() === 'home') {
            this.varG.setRouterLink('/home');
        }
        else if (this.varG.getPosizione().valueOf() === 'lista-task-app') {
            this.varG.setRouterLink('/lista-task-applicativi');
        }
        this.view = false;
        this.getListaTipiUtente();
        this.getListaTipologiaTask();
        this.getFileTask();
        this.getTask();
    }
    DettaglioTaskComponent.prototype.ngOnInit = function () {
    };
    DettaglioTaskComponent.prototype.getFileTask = function () {
        var _this = this;
        this.fileService.getListaFile().subscribe(function (res) {
            _this.fileTask = res;
        });
    };
    DettaglioTaskComponent.prototype.getTask = function () {
        var _this = this;
        this.taskService.getTask().subscribe(function (res) {
            _this.task = res;
            _this.view = true;
        });
    };
    DettaglioTaskComponent.prototype.getListaTipologiaTask = function () {
        var _this = this;
        this.tipologicheService.getListaTipologiaTask().subscribe(function (res) {
            _this.listaTipologiaTask = res;
        });
    };
    DettaglioTaskComponent.prototype.getListaTipiUtente = function () {
        var _this = this;
        this.tipologicheService.getListaTipiUtente().subscribe(function (res) {
            _this.listaTipiUtente = res;
        });
    };
    DettaglioTaskComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dettaglio-task',
            template: __webpack_require__(/*! ./dettaglio-task.component.html */ "./src/app/components/task/dettaglio-task/dettaglio-task.component.html"),
            styles: [__webpack_require__(/*! ./dettaglio-task.component.css */ "./src/app/components/task/dettaglio-task/dettaglio-task.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_lista_task_get_task_get_task_service__WEBPACK_IMPORTED_MODULE_2__["GetTaskService"], src_app_services_lista_task_tipologiche_task_tipologiche_task_service__WEBPACK_IMPORTED_MODULE_3__["TipologicheTaskSelectService"],
            src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__["VariabiliGlobali"], src_app_services_File_Service_file_task_file_task_service__WEBPACK_IMPORTED_MODULE_5__["FileTaskService"]])
    ], DettaglioTaskComponent);
    return DettaglioTaskComponent;
}());



/***/ }),

/***/ "./src/app/components/task/task.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/task/task.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\r\n  width: 100%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90YXNrL3Rhc2suY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQVc7QUFDYiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGFzay90YXNrLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/task/task.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/task/task.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"resp\" style=\"margin-top:10px;\">\n\n\n  <app-search-bar></app-search-bar>\n\n\n  <table mat-table  [dataSource]=\"dataSource\">\n\n    <!-- id Column -->\n    <ng-container matColumnDef=\"ID TASK\">\n      <th mat-header-cell *matHeaderCellDef> ID TASK </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.idTask}} </td>\n    </ng-container>\n\n    <!-- data creazione Column -->\n    <ng-container matColumnDef=\"DATA CREAZIONE\">\n      <th mat-header-cell *matHeaderCellDef> DATA CREAZIONE </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.dataCreazione}} </td>\n    </ng-container>\n\n    <!-- utente delegato Column -->\n    <ng-container matColumnDef=\"UTENTE DELEGATO\">\n      <th mat-header-cell *matHeaderCellDef> UTENTE DELEGATO </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.utenteDelegato}} </td>\n    </ng-container>\n\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns;\"></tr>\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"\n        routerLink=\"/task-details\"\n        routerLinkActive=\"router-link-active\"\n        (click)=\"viewDetails(row)\"></tr>\n  </table>\n\n  <mat-paginator  [length]=\"length\"\n  [pageIndex]=\"pageIndex\"\n  [pageSize]=\"pageSize\"\n  [pageSizeOptions]=\"[5, 10, 20]\"\n  (page)=\"pageEvent = getListaTask($event)\" showFirstLastButtons></mat-paginator>\n</div>\n\n<div *ngIf=\"!view\"  class=\"flex gif\">\n  <img src=\"https://it.dplay.com/00406/static/resources/images/spinner.gif\">\n</div>\n\n  <div *ngIf=\"notResp\">\n    <h4>\n      Nessun Task presente\n    </h4>\n  </div>\n"

/***/ }),

/***/ "./src/app/components/task/task.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/task/task.component.ts ***!
  \***************************************************/
/*! exports provided: TaskComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskComponent", function() { return TaskComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_lista_task_lista_task_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/lista-task/lista-task.service */ "./src/app/services/lista-task/lista-task.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");





var TaskComponent = /** @class */ (function () {
    function TaskComponent(service, varG) {
        this.service = service;
        this.varG = varG;
        this.displayedColumns = ['ID TASK', 'DATA CREAZIONE', 'UTENTE DELEGATO'];
        this.dataSource = null;
        this.view = false;
        this.resp = false;
        this.notResp = false;
    }
    TaskComponent.prototype.ngOnInit = function () {
        this.getListaTask(null);
    };
    TaskComponent.prototype.getListaTask = function (event) {
        if (event === null) {
            this.pageIndex = 0;
            this.pageSize = 5;
            this.firstTimeGet();
        }
        else {
            this.pageIndex = event.pageIndex;
            this.pageSize = event.pageSize;
            this.get();
        }
    };
    TaskComponent.prototype.viewDetails = function (details) {
        this.varG.setIdTask(details.idTask.valueOf());
        this.varG.setPosizione('home');
    };
    TaskComponent.prototype.firstTimeGet = function () {
        var _this = this;
        setTimeout(function () {
            _this.get();
        }, 1000);
    };
    TaskComponent.prototype.get = function () {
        var _this = this;
        this.service.getListaTask(this.pageIndex, this.pageSize).subscribe(function (res) {
            _this.listaAttivita = res;
            _this.view = true;
            if (_this.listaAttivita.totaleRecord === 0) {
                _this.notResp = true;
            }
            else {
                _this.resp = true;
            }
            if (_this.dataSource === null) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](_this.listaAttivita.listaAttivita);
                _this.dataSource.paginator = _this.paginator;
                _this.pageIndex = _this.listaAttivita.paginaCorrente;
                _this.length = _this.listaAttivita.totaleRecord;
            }
            else {
                _this.pageIndex = _this.listaAttivita.paginaCorrente;
                _this.length = _this.listaAttivita.totaleRecord;
                _this.dataSource.data = _this.listaAttivita.listaAttivita;
                _this._updatePaginator(_this.paginator.length, _this.pageIndex);
            }
        });
    };
    TaskComponent.prototype._updatePaginator = function (filteredDataLength, pageIndex) {
        var _this = this;
        Promise.resolve().then(function () {
            _this.paginator.pageIndex = pageIndex;
            if (!_this.paginator) {
                return;
            }
            _this.paginator.length = filteredDataLength;
            // If the page index is set beyond the page, reduce it to the last page.
            if (_this.paginator.pageIndex > 0) {
                var lastPageIndex = Math.ceil(_this.paginator.length / _this.paginator.pageSize) - 1 || 0;
                _this.paginator.pageIndex = Math.min(_this.paginator.pageIndex, lastPageIndex);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], TaskComponent.prototype, "page", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], TaskComponent.prototype, "paginator", void 0);
    TaskComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-task',
            template: __webpack_require__(/*! ./task.component.html */ "./src/app/components/task/task.component.html"),
            styles: [__webpack_require__(/*! ./task.component.css */ "./src/app/components/task/task.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_lista_task_lista_task_service__WEBPACK_IMPORTED_MODULE_2__["ListaTaskService"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__["VariabiliGlobali"]])
    ], TaskComponent);
    return TaskComponent;
}());



/***/ }),

/***/ "./src/app/components/user/user.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/user/user.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlci91c2VyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/user/user.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/user/user.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"resp\" class=\"west\">\n <span style=\"font-weight: bold\">\n    Utente \n </span>\n : {{user.nome}}  {{user.cognome}}\n</div>"

/***/ }),

/***/ "./src/app/components/user/user.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/user/user.component.ts ***!
  \***************************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_user_service_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/user-service/user.service */ "./src/app/services/user-service/user.service.ts");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");





var UserComponent = /** @class */ (function () {
    function UserComponent(userService, cookie, varG) {
        this.userService = userService;
        this.cookie = cookie;
        this.varG = varG;
        console.log('UserService');
        this.resp = false;
        this.getUser();
    }
    UserComponent.prototype.ngOnInit = function () {
    };
    UserComponent.prototype.getUser = function () {
        var _this = this;
        this.userService.getUser().subscribe(function (res) {
            if (_this.user === undefined) {
                console.log('Utente nuovo connesso in sessione ');
                _this.user = res;
                _this.cookie.set('Cookie', '');
                _this.cookie.set('Cookie', _this.user.cookie);
                _this.resp = true;
                _this.varG.setUtente(_this.user);
            }
            else {
                if (_this.user.username === res.username && _this.user.sessionID !== res.sessionID) {
                    _this.user.sessionID = res.sessionID;
                    _this.cookie.set('Cookie', '');
                    _this.cookie.set('Cookie', res.cookie);
                }
                else {
                    console.log('Utente connesso: ' + res.username);
                }
            }
        });
    };
    UserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! ./user.component.html */ "./src/app/components/user/user.component.html"),
            styles: [__webpack_require__(/*! ./user.component.css */ "./src/app/components/user/user.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_user_service_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./src/app/components/valutazione/valutazione.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/valutazione/valutazione.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\r\n  width: 100%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy92YWx1dGF6aW9uZS92YWx1dGF6aW9uZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztBQUNiIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy92YWx1dGF6aW9uZS92YWx1dGF6aW9uZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/valutazione/valutazione.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/valutazione/valutazione.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"resp\" style=\"margin-top:10px;\">\n  <table mat-table  [dataSource]=\"dataSource\">\n\n    <!-- id Column -->\n    <ng-container matColumnDef=\"ID\">\n      <th mat-header-cell *matHeaderCellDef> ID </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.idEsternalizzazione}} </td>\n    </ng-container>\n\n    <!-- data creazione Column -->\n    <ng-container matColumnDef=\"DATA CREAZIONE\">\n      <th mat-header-cell *matHeaderCellDef> DATA CREAZIONE </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.dataCreazioneEsternalizzazione}} </td>\n    </ng-container>\n\n    <!-- titolo Column -->\n    <ng-container matColumnDef=\"TITOLO\">\n      <th mat-header-cell *matHeaderCellDef> TITOLO </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.titoloEsternalizzazione}} </td>\n    </ng-container>\n\n    <!-- responsabile Column -->\n    <ng-container matColumnDef=\"RESPONSABILE\">\n      <th mat-header-cell *matHeaderCellDef> RESPONSABILE </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.compilataDa}} </td>\n    </ng-container>\n\n    <!-- responsabile Column -->\n    <ng-container matColumnDef=\"RESPONSABILE CONTROLLO\">\n      <th mat-header-cell *matHeaderCellDef> RESPONSABILE CONTROLLO </th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.responsabileControlloEsternalizzazione}} </td>\n    </ng-container>\n\n    <ng-container matColumnDef = \"nuovaRda\">\n        <th mat-header-cell *matHeaderCellDef> </th>\n        <td mat-cell *matCellDef=\"let element\">\n          <button class=\"btn btn-success\">Nuova Rda</button>\n        </td>\n    </ng-container>\n\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"\n         routerLink=\"/esternalizzazione-details\" routerLinkActive=\"router-link-active\"  (click)=\"viewDetails(row)\">\n    </tr>\n  </table>\n\n  <mat-paginator  [length]=\"length\"\n  [pageIndex]=\"pageIndex\"\n  [pageSize]=\"pageSize\"\n  [pageSizeOptions]=\"[5, 10, 20]\"\n  (page)=\"pageEvent = getValutazione($event)\" showFirstLastButtons></mat-paginator>\n</div>\n\n<div *ngIf=\"!view\"  class=\"flex gif\">\n  <img src=\"https://it.dplay.com/00406/static/resources/images/spinner.gif\">\n</div>\n\n  <div *ngIf=\"notResp\">\n    <h4>\n      Nessuna Esternalizzazione presente\n    </h4>\n  </div>\n\n"

/***/ }),

/***/ "./src/app/components/valutazione/valutazione.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/valutazione/valutazione.component.ts ***!
  \*****************************************************************/
/*! exports provided: ValutazioneComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValutazioneComponent", function() { return ValutazioneComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_services_fase_valutazione_fase_valutazione_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/fase-valutazione/fase-valutazione.service */ "./src/app/services/fase-valutazione/fase-valutazione.service.ts");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");





var ValutazioneComponent = /** @class */ (function () {
    function ValutazioneComponent(service, varG) {
        this.service = service;
        this.varG = varG;
        this.displayedColumns = ['ID', 'DATA CREAZIONE', 'TITOLO', 'RESPONSABILE', 'RESPONSABILE CONTROLLO', 'nuovaRda'];
        this.dataSource = null;
        this.view = false;
        this.resp = false;
        this.notResp = false;
    }
    ValutazioneComponent.prototype.ngOnInit = function () {
        this.getValutazione(null);
    };
    ValutazioneComponent.prototype.getValutazione = function (event) {
        if (event === null) {
            this.pageIndex = 0;
            this.pageSize = 5;
            this.firstTimeGet();
        }
        else {
            this.pageIndex = event.pageIndex;
            this.pageSize = event.pageSize;
            this.get();
        }
    };
    ValutazioneComponent.prototype.viewDetails = function (details) {
        this.varG.setIdEsternalizzazione(details.idEsternalizzazione);
        this.varG.setPosizione('valutazione');
    };
    ValutazioneComponent.prototype.firstTimeGet = function () {
        var _this = this;
        setTimeout(function () {
            _this.get();
        }, 1000);
    };
    ValutazioneComponent.prototype.get = function () {
        var _this = this;
        this.service.getFaseValutazione(this.pageIndex, this.pageSize).subscribe(function (res) {
            _this.listaEsternalizzazioni = res;
            _this.view = true;
            if (_this.listaEsternalizzazioni.totaleRecord === 0) {
                _this.notResp = true;
            }
            else {
                _this.resp = true;
            }
            if (_this.dataSource === null) {
                _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](_this.listaEsternalizzazioni.listaEsternalizzazioni);
                _this.dataSource.paginator = _this.paginator;
                _this.pageIndex = _this.listaEsternalizzazioni.paginaCorrente;
                _this.length = _this.listaEsternalizzazioni.totaleRecord;
            }
            else {
                _this.pageIndex = _this.listaEsternalizzazioni.paginaCorrente;
                _this.length = _this.listaEsternalizzazioni.totaleRecord;
                _this.dataSource.data = _this.listaEsternalizzazioni.listaEsternalizzazioni;
                _this._updatePaginator(_this.paginator.length, _this.pageIndex);
            }
        });
    };
    ValutazioneComponent.prototype._updatePaginator = function (filteredDataLength, pageIndex) {
        var _this = this;
        Promise.resolve().then(function () {
            _this.paginator.pageIndex = pageIndex;
            if (!_this.paginator) {
                return;
            }
            _this.paginator.length = filteredDataLength;
            // If the page index is set beyond the page, reduce it to the last page.
            if (_this.paginator.pageIndex > 0) {
                var lastPageIndex = Math.ceil(_this.paginator.length / _this.paginator.pageSize) - 1 || 0;
                _this.paginator.pageIndex = Math.min(_this.paginator.pageIndex, lastPageIndex);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ValutazioneComponent.prototype, "page", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], ValutazioneComponent.prototype, "paginator", void 0);
    ValutazioneComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-valutazione',
            template: __webpack_require__(/*! ./valutazione.component.html */ "./src/app/components/valutazione/valutazione.component.html"),
            styles: [__webpack_require__(/*! ./valutazione.component.css */ "./src/app/components/valutazione/valutazione.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_fase_valutazione_fase_valutazione_service__WEBPACK_IMPORTED_MODULE_3__["FaseValutazioneService"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__["VariabiliGlobali"]])
    ], ValutazioneComponent);
    return ValutazioneComponent;
}());



/***/ }),

/***/ "./src/app/services/File-Service/file-nomina-responsabile-controllo/file-esternalizzazione.service.ts":
/*!************************************************************************************************************!*\
  !*** ./src/app/services/File-Service/file-nomina-responsabile-controllo/file-esternalizzazione.service.ts ***!
  \************************************************************************************************************/
/*! exports provided: FileEsternalizzazione */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileEsternalizzazione", function() { return FileEsternalizzazione; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var FileEsternalizzazione = /** @class */ (function () {
    function FileEsternalizzazione(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    FileEsternalizzazione.prototype.getListaFile = function () {
        this.id = this.varG.getIdEsternalizzazione();
        return (this.http.get(this.url + "rest/file/lista/" + this.id + "/ESTERNALIZZAZIONE"));
    };
    FileEsternalizzazione.prototype.downloadFile = function (idFile) {
        this.id = this.varG.getIdEsternalizzazione();
        return (this.http.get(this.url + "rest/file/download/" + idFile));
    };
    FileEsternalizzazione = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], FileEsternalizzazione);
    return FileEsternalizzazione;
}());



/***/ }),

/***/ "./src/app/services/File-Service/file-task/file-task.service.ts":
/*!**********************************************************************!*\
  !*** ./src/app/services/File-Service/file-task/file-task.service.ts ***!
  \**********************************************************************/
/*! exports provided: FileTaskService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileTaskService", function() { return FileTaskService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var FileTaskService = /** @class */ (function () {
    function FileTaskService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    FileTaskService.prototype.getListaFile = function () {
        this.id = this.varG.getIdTask();
        return (this.http.get(this.url + "rest/file/lista/" + this.id + "/TASK"));
    };
    FileTaskService.prototype.downloadFile = function (idFile) {
        this.id = this.varG.getIdTask();
        return (this.http.get(this.url + "rest/file/download/" + idFile));
    };
    FileTaskService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], FileTaskService);
    return FileTaskService;
}());



/***/ }),

/***/ "./src/app/services/esternalizzazione/fornitori/fornitori.service.ts":
/*!***************************************************************************!*\
  !*** ./src/app/services/esternalizzazione/fornitori/fornitori.service.ts ***!
  \***************************************************************************/
/*! exports provided: FornitoriService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FornitoriService", function() { return FornitoriService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var FornitoriService = /** @class */ (function () {
    function FornitoriService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    FornitoriService.prototype.getListaFornitori = function (ambitoSociale) {
        return this.http.get(this.url + "rest/esternalizzazione/listaFornitori/" + ambitoSociale + "/");
    };
    FornitoriService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], FornitoriService);
    return FornitoriService;
}());



/***/ }),

/***/ "./src/app/services/esternalizzazione/lista-esternalizzazioni.service.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/services/esternalizzazione/lista-esternalizzazioni.service.ts ***!
  \*******************************************************************************/
/*! exports provided: ListaEsternalizzazioniService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaEsternalizzazioniService", function() { return ListaEsternalizzazioniService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var ListaEsternalizzazioniService = /** @class */ (function () {
    function ListaEsternalizzazioniService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    ListaEsternalizzazioniService.prototype.getEsternalizzazioni = function (pageIndex, pageSize) {
        return (this.http.get(this.url + "rest/esternalizzazione/lista/-1/" + pageIndex + "/" + pageSize));
    };
    ListaEsternalizzazioniService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], ListaEsternalizzazioniService);
    return ListaEsternalizzazioniService;
}());



/***/ }),

/***/ "./src/app/services/esternalizzazione/salva-esternalizzazione.service.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/services/esternalizzazione/salva-esternalizzazione.service.ts ***!
  \*******************************************************************************/
/*! exports provided: SalvaEsternalizzazioneService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalvaEsternalizzazioneService", function() { return SalvaEsternalizzazioneService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _http_service_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../http-service/http.service */ "./src/app/services/http-service/http.service.ts");



var SalvaEsternalizzazioneService = /** @class */ (function () {
    function SalvaEsternalizzazioneService(http) {
        this.http = http;
    }
    SalvaEsternalizzazioneService.prototype.salvaNew = function (esternalizzazione) {
        return this.http.doPost("rest/esternalizzazione/salva", esternalizzazione, 'application/json; charset=utf-8');
    };
    SalvaEsternalizzazioneService.prototype.salva = function (esternalizzazione) {
        if (esternalizzazione.idEsternalizzazione == null) {
            return null;
        }
        else {
            return this.http.doPost("rest/esternalizzazione/salva", esternalizzazione, 'application/json; charset=utf-8');
        }
    };
    SalvaEsternalizzazioneService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_http_service_http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"]])
    ], SalvaEsternalizzazioneService);
    return SalvaEsternalizzazioneService;
}());



/***/ }),

/***/ "./src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/services/esternalizzazione/tipologiche-select/tipologiche-select.service.ts ***!
  \*********************************************************************************************/
/*! exports provided: TipologicheSelectService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TipologicheSelectService", function() { return TipologicheSelectService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var TipologicheSelectService = /** @class */ (function () {
    function TipologicheSelectService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    TipologicheSelectService.prototype.getListaRischiBC = function () {
        return (this.http.get(this.url + "rest/esternalizzazione/listaRischiBC"));
    };
    TipologicheSelectService.prototype.getListaTipologiaEsternalizzazione = function () {
        return (this.http.get(this.url + "rest/esternalizzazione/listaTipiEsternalizzazione"));
    };
    TipologicheSelectService.prototype.getListaBenefici = function () {
        return (this.http.get(this.url + "rest/esternalizzazione/listaBenefici"));
    };
    TipologicheSelectService.prototype.getListaRischiositaEsternalizzazione = function () {
        return (this.http.get(this.url + "rest/esternalizzazione/listaRischiositaEsternalizzazione"));
    };
    TipologicheSelectService.prototype.getListaConflittiInteressi = function () {
        return (this.http.get(this.url + "rest/esternalizzazione/listaConflittiInteressi"));
    };
    TipologicheSelectService.prototype.getListaPianiReinternalizzazione = function () {
        return (this.http.get(this.url + "rest/esternalizzazione/listaPianiReinternalizzazione"));
    };
    TipologicheSelectService.prototype.getListaTipiControllo = function () {
        return (this.http.get(this.url + "rest/esternalizzazione/listaTipiControllo"));
    };
    TipologicheSelectService.prototype.getListaPeriodicita = function () {
        return (this.http.get(this.url + "rest/esternalizzazione/listaPeriodicita"));
    };
    TipologicheSelectService.prototype.getListaAzioniCorrettive = function () {
        return (this.http.get(this.url + "rest/esternalizzazione/listaAzioniCorrettive"));
    };
    TipologicheSelectService.prototype.getListaEsitiControllo = function () {
        return (this.http.get(this.url + "rest/esternalizzazione/listaEsitiControllo"));
    };
    TipologicheSelectService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], TipologicheSelectService);
    return TipologicheSelectService;
}());



/***/ }),

/***/ "./src/app/services/fase-predisposizione/fase-predisposizione.service.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/services/fase-predisposizione/fase-predisposizione.service.ts ***!
  \*******************************************************************************/
/*! exports provided: FasePredisposizioneService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FasePredisposizioneService", function() { return FasePredisposizioneService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var FasePredisposizioneService = /** @class */ (function () {
    function FasePredisposizioneService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    FasePredisposizioneService.prototype.getFaseValutazione = function (pageIndex, pageSize) {
        return (this.http.get(this.url + "rest/esternalizzazione/lista/3/" + pageIndex + "/" + pageSize));
    };
    FasePredisposizioneService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], FasePredisposizioneService);
    return FasePredisposizioneService;
}());



/***/ }),

/***/ "./src/app/services/fase-valutazione/fase-valutazione.service.ts":
/*!***********************************************************************!*\
  !*** ./src/app/services/fase-valutazione/fase-valutazione.service.ts ***!
  \***********************************************************************/
/*! exports provided: FaseValutazioneService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaseValutazioneService", function() { return FaseValutazioneService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var FaseValutazioneService = /** @class */ (function () {
    function FaseValutazioneService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    FaseValutazioneService.prototype.getFaseValutazione = function (pageIndex, pageSize) {
        return (this.http.get(this.url + "rest/esternalizzazione/lista/2/" + pageIndex + "/" + pageSize));
    };
    FaseValutazioneService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], FaseValutazioneService);
    return FaseValutazioneService;
}());



/***/ }),

/***/ "./src/app/services/findById/esternalizzazione/find-by-id-e.service.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/services/findById/esternalizzazione/find-by-id-e.service.ts ***!
  \*****************************************************************************/
/*! exports provided: FindByIdEService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FindByIdEService", function() { return FindByIdEService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var FindByIdEService = /** @class */ (function () {
    function FindByIdEService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    FindByIdEService.prototype.findEsternalizzazione = function () {
        this.id = this.varG.getIdEsternalizzazione();
        return (this.http.get(this.url + "rest/esternalizzazione/get/" + this.id));
    };
    FindByIdEService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], FindByIdEService);
    return FindByIdEService;
}());



/***/ }),

/***/ "./src/app/services/http-service/http.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/http-service/http.service.ts ***!
  \*******************************************************/
/*! exports provided: HttpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpService", function() { return HttpService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/index.js");
/* harmony import */ var _variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");





var HttpService = /** @class */ (function () {
    function HttpService(http, cookie, varG) {
        this.http = http;
        this.cookie = cookie;
        this.varG = varG;
        this.urlContextPath = this.varG.getContest();
    }
    HttpService.prototype.userSessionAuthenticated = function () {
        return true;
    };
    HttpService.prototype.doGet = function (url) {
        return this.http.get(this.urlContextPath + url);
    };
    HttpService.prototype.doPost = function (url, body, contentType) {
        console.log(' HttpService=>doPost cookie: ' + this.cookie.get('Cookie'));
        console.log(' X-XSRF-TOKEN: ' + this.cookie.get('Cookie').split(';')[3].split('=')[1]);
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': contentType,
                'Access-Control-Allow-Headers': 'content-type',
                'Access-Control-Allow-Origin': '*',
                'Allow': ' GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH',
                'Cookie': this.cookie.get('Cookie'),
                'X-XSRF-TOKEN': this.cookie.get('Cookie').split(';')[3].split('=')[1]
            })
        };
        return this.http.post(this.urlContextPath + url, body, httpOptions);
    };
    HttpService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_3__["CookieService"], _variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_4__["VariabiliGlobali"]])
    ], HttpService);
    return HttpService;
}());



/***/ }),

/***/ "./src/app/services/lista-conoRE/lista-cono-re.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/services/lista-conoRE/lista-cono-re.service.ts ***!
  \****************************************************************/
/*! exports provided: ListaConoREService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaConoREService", function() { return ListaConoREService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var ListaConoREService = /** @class */ (function () {
    function ListaConoREService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    ListaConoREService.prototype.getListaConoRE = function () {
        return (this.http.get(this.url + "rest/esternalizzazione/listaConoRE"));
    };
    ListaConoREService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], ListaConoREService);
    return ListaConoREService;
}());



/***/ }),

/***/ "./src/app/services/lista-task-app/lista-task-app.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/services/lista-task-app/lista-task-app.service.ts ***!
  \*******************************************************************/
/*! exports provided: ListaTaskAppService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaTaskAppService", function() { return ListaTaskAppService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var ListaTaskAppService = /** @class */ (function () {
    function ListaTaskAppService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    ListaTaskAppService.prototype.getListaTaskApp = function (pageIndex, pageSize) {
        return (this.http.get(this.url + "rest/task/lista/12/" + pageIndex + "/" + pageSize));
    };
    ListaTaskAppService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], ListaTaskAppService);
    return ListaTaskAppService;
}());



/***/ }),

/***/ "./src/app/services/lista-task/get-task/get-task.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/services/lista-task/get-task/get-task.service.ts ***!
  \******************************************************************/
/*! exports provided: GetTaskService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetTaskService", function() { return GetTaskService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var GetTaskService = /** @class */ (function () {
    function GetTaskService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.idTask = '';
        this.url = this.varG.getContest();
    }
    GetTaskService.prototype.getTask = function () {
        this.idTask = this.varG.getIdTask().valueOf();
        return this.http.get(this.url + "rest/task/get/" + this.idTask);
    };
    GetTaskService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], GetTaskService);
    return GetTaskService;
}());



/***/ }),

/***/ "./src/app/services/lista-task/lista-task.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/lista-task/lista-task.service.ts ***!
  \***********************************************************/
/*! exports provided: ListaTaskService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaTaskService", function() { return ListaTaskService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var ListaTaskService = /** @class */ (function () {
    function ListaTaskService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    ListaTaskService.prototype.getListaTask = function (pageIndex, pageSize) {
        return (this.http.get(this.url + "rest/task/lista/-1/" + pageIndex + "/" + pageSize));
    };
    ListaTaskService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], ListaTaskService);
    return ListaTaskService;
}());



/***/ }),

/***/ "./src/app/services/lista-task/tipologiche-task/tipologiche-task.service.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/services/lista-task/tipologiche-task/tipologiche-task.service.ts ***!
  \**********************************************************************************/
/*! exports provided: TipologicheTaskSelectService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TipologicheTaskSelectService", function() { return TipologicheTaskSelectService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var TipologicheTaskSelectService = /** @class */ (function () {
    function TipologicheTaskSelectService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    TipologicheTaskSelectService.prototype.getListaTipologiaTask = function () {
        return this.http.get(this.url + "rest/task/listaTipiTask");
    };
    TipologicheTaskSelectService.prototype.getListaTipiUtente = function () {
        return this.http.get(this.url + "rest/task/listaTipiUtente");
    };
    TipologicheTaskSelectService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], TipologicheTaskSelectService);
    return TipologicheTaskSelectService;
}());



/***/ }),

/***/ "./src/app/services/outsourcing/outsourcing.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/services/outsourcing/outsourcing.service.ts ***!
  \*************************************************************/
/*! exports provided: OutsourcingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OutsourcingService", function() { return OutsourcingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var OutsourcingService = /** @class */ (function () {
    function OutsourcingService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    OutsourcingService.prototype.getOutsourcing = function (pageIndex, pageSize) {
        return (this.http.get(this.url + "rest/esternalizzazione/lista/4/" + pageIndex + "/" + pageSize));
    };
    OutsourcingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], src_app_variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], OutsourcingService);
    return OutsourcingService;
}());



/***/ }),

/***/ "./src/app/services/scadOutsourcing/scad-outsourcing.service.ts":
/*!**********************************************************************!*\
  !*** ./src/app/services/scadOutsourcing/scad-outsourcing.service.ts ***!
  \**********************************************************************/
/*! exports provided: ScadOutsourcingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScadOutsourcingService", function() { return ScadOutsourcingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../variabili-globali/variabili-globali */ "./src/app/variabili-globali/variabili-globali.ts");




var ScadOutsourcingService = /** @class */ (function () {
    function ScadOutsourcingService(http, varG) {
        this.http = http;
        this.varG = varG;
        this.url = this.varG.getContest();
    }
    ScadOutsourcingService.prototype.getScadOutsourcing = function (pageIndex, pageSize) {
        return (this.http.get(this.url + "rest/esternalizzazione/lista/5/" + pageIndex + "/" + pageSize));
    };
    ScadOutsourcingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _variabili_globali_variabili_globali__WEBPACK_IMPORTED_MODULE_3__["VariabiliGlobali"]])
    ], ScadOutsourcingService);
    return ScadOutsourcingService;
}());



/***/ }),

/***/ "./src/app/services/user-service/user.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/user-service/user.service.ts ***!
  \*******************************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _http_service_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../http-service/http.service */ "./src/app/services/http-service/http.service.ts");



var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.getUser = function () {
        return this.http.doGet("rest/utente/username");
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_http_service_http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/variabili-globali/edit-combinato.ts":
/*!*****************************************************!*\
  !*** ./src/app/variabili-globali/edit-combinato.ts ***!
  \*****************************************************/
/*! exports provided: EditCombinato */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditCombinato", function() { return EditCombinato; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var EditCombinato = /** @class */ (function () {
    function EditCombinato() {
        this.editInserimento = true;
        this.editValutazione = true;
        this.editPredisposizione = true;
    }
    EditCombinato.prototype.setEditCombinato = function (stato) {
        switch (stato) {
            case '1': {
                this.editInserimento = false;
                this.editPredisposizione = true;
                this.editValutazione = true;
                break;
            }
            case '2': {
                this.editInserimento = true;
                this.editPredisposizione = true;
                this.editValutazione = false;
                break;
            }
            case '3': {
                this.editInserimento = true;
                this.editPredisposizione = false;
                this.editValutazione = true;
                break;
            }
        }
    };
    EditCombinato.prototype.returnEditInserimento = function () {
        return this.editInserimento;
    };
    EditCombinato.prototype.returnEditPredisposizione = function () {
        return this.editPredisposizione;
    };
    EditCombinato.prototype.returnEditValutazione = function () {
        return this.editValutazione;
    };
    EditCombinato = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], EditCombinato);
    return EditCombinato;
}());



/***/ }),

/***/ "./src/app/variabili-globali/variabili-globali.ts":
/*!********************************************************!*\
  !*** ./src/app/variabili-globali/variabili-globali.ts ***!
  \********************************************************/
/*! exports provided: VariabiliGlobali */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VariabiliGlobali", function() { return VariabiliGlobali; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var VariabiliGlobali = /** @class */ (function () {
    function VariabiliGlobali() {
        this.contest = 'http://localhost:8080/West/';
        this.idEsternalizzazione = '';
        this.idTask = '';
        this.posizione = '';
        this.routerLink = '';
        this.fornitori = [];
    }
    VariabiliGlobali.prototype.setUtente = function (u) {
        this.user = u;
    };
    VariabiliGlobali.prototype.getUtente = function () {
        return this.user;
    };
    VariabiliGlobali.prototype.setFornitori = function (f) {
        this.fornitori = f;
    };
    VariabiliGlobali.prototype.getFornitori = function () {
        return this.fornitori;
    };
    VariabiliGlobali.prototype.setIdTask = function (id) {
        this.idTask = id.valueOf();
    };
    VariabiliGlobali.prototype.getIdTask = function () {
        return this.idTask;
    };
    VariabiliGlobali.prototype.getIdEsternalizzazione = function () {
        return this.idEsternalizzazione;
    };
    VariabiliGlobali.prototype.setIdEsternalizzazione = function (id) {
        this.idEsternalizzazione = id.valueOf();
    };
    VariabiliGlobali.prototype.setRouterLink = function (link) {
        this.routerLink = link.valueOf();
    };
    VariabiliGlobali.prototype.getRouterLink = function () {
        return this.routerLink;
    };
    VariabiliGlobali.prototype.setPosizione = function (pos) {
        this.posizione = pos.valueOf();
    };
    VariabiliGlobali.prototype.getPosizione = function () {
        return this.posizione;
    };
    VariabiliGlobali.prototype.getContest = function () {
        return this.contest;
    };
    VariabiliGlobali = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], VariabiliGlobali);
    return VariabiliGlobali;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\e.colasanti\git\esternalizzazione-2.0\esternalizzazione-superpom\Esternalizzazione-Frontend\src\main\web\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map